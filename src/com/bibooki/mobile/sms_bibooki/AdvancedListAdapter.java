package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bibooki.mobile.sms_bibooki.rows.*;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;

public class AdvancedListAdapter extends ArrayAdapter<ListRowItem> {

    private String setting_ignore_ownerphone,
                    setting_ignore_ownerphone_info,
                    setting_ignore_phoneCallType;

    private LayoutInflater vi;
    private Activity mActivity = null;
    private UtilsFactory _utilsFactory;
    private AdvancedSettingPreference _advancedSettingPreference;
    private NotificationUtils _notificationUtils;

    static AlertDialog.Builder alertbox;

    public AdvancedListAdapter(Activity activity) {
        super(activity, 0);

        mActivity = activity;

        alertbox = new AlertDialog.Builder(mActivity);

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _advancedSettingPreference = _utilsFactory.getAdvancedSettingPreference();
        _notificationUtils = _utilsFactory.getNotificationUtils();

        setupSectionTitle();

        vi = (LayoutInflater)mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    private void setupSectionTitle(){
        setting_ignore_ownerphone = mActivity.getResources().getString(R.string.ignore_owner_phonenumber);
        setting_ignore_ownerphone_info = mActivity.getResources().getString(R.string.ignore_ownerphoneInfo);
        setting_ignore_phoneCallType = mActivity.getResources().getString(R.string.ignore_phone_callType);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ListRowItem listRowItem = getItem(position);
        ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
        ListRowSeparator separator;
        switch (rowItemType){
            case SEPARATOR:
                // inflate Calendar Row Separator Item
                v = vi.inflate(R.layout.row_separator, null);
                // Set data for Separator item
                separator = (ListRowSeparator) listRowItem;
                TextView textView_separator = (TextView) v.findViewById(R.id.textView_row_separator);
                textView_separator.setText(separator.text);
                break;
            case CHECKED_TEXT_VIEW_ITEM:
                // inflate Calendar Row Item
                v = vi.inflate(R.layout.row_checked_text_view, null);
                // Set data for Calendar item
                CheckedTextView_RowItem checkedTextView_rowItem = (CheckedTextView_RowItem) listRowItem;
                CheckedTextView checkedTextView = (CheckedTextView) v.findViewById(R.id.checkedTextView_rowItem);
                checkedTextView.setText(checkedTextView_rowItem.title);
                checkedTextView.setSelected(checkedTextView_rowItem.checked);
                if(checkedTextView_rowItem.checked)
                    checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                // Setup onclick Listener for ignoring owner phone number
                if (checkedTextView_rowItem.title.equals(setting_ignore_ownerphone)){
                    checkedTextView.setOnClickListener(ignoreOwnerPhoneNumber_onClicLister);
                    ImageView icon = (ImageView) v.findViewById(R.id.icon_checkedTextView_rowItem);
                    if (checkedTextView_rowItem.photoUri!=null)
                        icon.setImageURI(checkedTextView_rowItem.photoUri);
                    icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showInfoBox(setting_ignore_ownerphone,mActivity.getString(R.string.ingore_ownerphonenumber_info));
                        }
                    });
                }
                // Setup onclick Listener for ignoring custom call log type
                if (checkedTextView_rowItem.title.equals(setting_ignore_phoneCallType)){
                    checkedTextView.setOnClickListener(ignorePhoneCallType_onClicLister);
                    ImageView icon = (ImageView) v.findViewById(R.id.icon_checkedTextView_rowItem);
                    if (checkedTextView_rowItem.photoUri!=null)
                        icon.setImageURI(checkedTextView_rowItem.photoUri);
                    icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showInfoBox(setting_ignore_phoneCallType, mActivity.getString(R.string.ignore_phone_callType_info));
                        }
                    });
                }
                // Setup onclick Listener for ignoring custom call log type
                if (checkedTextView_rowItem.title.equals(setting_ignore_ownerphone_info)){
                    checkedTextView.setOnClickListener(ignorePhoneOldInfo_onClicLister);
                    ImageView icon = (ImageView) v.findViewById(R.id.icon_checkedTextView_rowItem);
                    if (checkedTextView_rowItem.photoUri!=null)
                        icon.setImageURI(checkedTextView_rowItem.photoUri);
                    icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showInfoBox(setting_ignore_ownerphone_info, mActivity.getString(R.string.ignore_ownerphoneInfo_info));
                        }
                    });
                }
                break;
            default:
                break;
        }
        return v;
    }
    private View.OnClickListener ignoreOwnerPhoneNumber_onClicLister = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                _advancedSettingPreference.store_ignoreOwner_setting(false);
            } else {
                showInfoBox(setting_ignore_ownerphone,mActivity.getString(R.string.ingore_ownerphonenumber_info));
                checkedTextView.setSelected(true);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                _advancedSettingPreference.store_ignoreOwner_setting(true);
            }
        }
    };
    private View.OnClickListener ignorePhoneCallType_onClicLister = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                _advancedSettingPreference.store_ignorePhoneCallType_setting(false);
            } else {
                showInfoBox(setting_ignore_phoneCallType,mActivity.getString(R.string.ignore_phone_callType_info));
                checkedTextView.setSelected(true);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                _advancedSettingPreference.store_ignorePhoneCallType_setting(true);
            }
        }
    };
    private View.OnClickListener ignorePhoneOldInfo_onClicLister = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                _advancedSettingPreference.store_ignoreOwnerInfo_setting(false);
            } else {
                showInfoBox(setting_ignore_ownerphone_info,mActivity.getString(R.string.ignore_ownerphoneInfo_info));
                checkedTextView.setSelected(true);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                _advancedSettingPreference.store_ignoreOwnerInfo_setting(true);
            }
        }
    };
    public void showInfoBox(final String title, final String content){
        // set the message to display
        // show it
        alertbox.setTitle(title);
        alertbox.setMessage(content);
        alertbox.setPositiveButton("Got it!",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertbox.show();
    }
}
