package com.bibooki.mobile.sms_bibooki;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.*;
import android.widget.*;
import android.widget.AbsListView.*;

import com.bibooki.mobile.sms_bibooki.rows.ListRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;

import java.util.*;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Fragment_Schedule extends BaseFragment {

    public static ScheduleListAdapter scheduleListAdapter;
    private SelectionAdapter mAdapter;
    private ArrayList<String> mItems = new ArrayList<String>();

    int selected_scheduled_SMS_calendar_id;
    static ActionMode mActionMode;

    static ContactsHelper _contactsHelper;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _contactsHelper = ContactsHelper.getInstance(getActivity());

        scheduleListAdapter = new ScheduleListAdapter(getActivity());
        setListAdapter(scheduleListAdapter);
        // BEGIN_INCLUDE (setup_refreshlistener)
        /**
         * Implement {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener}. When users do the "swipe to
         * refresh" gesture, SwipeRefreshLayout invokes
         * {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}. In
         * {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}, call a method that
         * refreshes the content. Call the same method in response to the Refresh action from the
         * action bar.
         */
        setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // END_INCLUDE (setup_refreshlistener)

        TrackCalendarDbItem dbItem = _dbTrackCalendarOpenHelper.selectScheduleRecord();
        selected_scheduled_SMS_calendar_id = -1;
        if (dbItem!=null)
            selected_scheduled_SMS_calendar_id = dbItem.getSmsCalendarId();

        // get list scheduled sms from the current time from the selected calendar
        if (selected_scheduled_SMS_calendar_id>0){
            List<SMS> scheduledSMS_List = _scheduled_sms_utils.getScheduledSMS(selected_scheduled_SMS_calendar_id, getActivity(), 50, scheduleListAdapter.getCount());
            addMoreSmsItem(getActivity(), scheduledSMS_List);
        }
        //new AddScheduleItem().execute(50, 0);
        getListView().setOnScrollListener(new OnScrollListener() {

            private int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                if (scrollState == SCROLL_STATE_IDLE) {
                }
                // get more data for scrolling
                if ((this.currentFirstVisibleItem + this.currentVisibleItemCount > (scheduleListAdapter.getCount() - 10))) {
                    // get list scheduled sms from the current time from the selected calendar
                    List<SMS> scheduledSMS_List = _scheduled_sms_utils.getScheduledSMS(selected_scheduled_SMS_calendar_id, getActivity(), 100, scheduleListAdapter.getCount());
                    addMoreSmsItem(getActivity(), scheduledSMS_List);
                }
            }
        });

        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setMultiChoiceModeListener(multiChoiceModeListener);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    private MultiChoiceModeListener multiChoiceModeListener = new MultiChoiceModeListener() {
        private int selectedItemNumber = 0;
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.cabselection_menu, menu);
            return true;
        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            StringBuilder sb = new StringBuilder();
            Set<Integer> positions = scheduleListAdapter.getCurrentCheckedPosition();
            for (Integer pos : positions) {
                sb.append(" " + pos + ",");
            }
            switch (item.getItemId()) {
                case R.id.delete_entry:
                    //_notificationUtils.showShortToast(getResources().getString(R.string.notification_cab_deleted) + sb.toString());
                    Dialog deleteOptionDialog = _sms_utils.getDeleteScheduleDialog(mActivity, scheduleListAdapter.getSelectedEvent());
                    deleteOptionDialog.show();
                    deleteOptionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            refresh();
                        }
                    });
                    break;
                default:
                    selectedItemNumber = 0;
                    scheduleListAdapter.clearSelection();
            }
            mode.finish();
            return false;
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            selectedItemNumber = 0;
            scheduleListAdapter.clearSelection();
        }
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            if (checked) {
                selectedItemNumber++;
                scheduleListAdapter.setNewSelection(position, checked);
            } else {
                selectedItemNumber--;
                scheduleListAdapter.removeSelection(position);
            }
            if (selectedItemNumber ==1)
                mode.setTitle(selectedItemNumber + " item selected");
            else if (selectedItemNumber > 1)
                mode.setTitle(selectedItemNumber + " items selected");
        }
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ScheduleListAdapter extends ArrayAdapter<ListRowItem> {
        private LayoutInflater vi;

        public ScheduleListAdapter(Context context) {
            super(context, 0);
            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
        public List<ListRowItemEvent> getSelectedEvent(){
            List<ListRowItemEvent> selectedItem = new ArrayList<ListRowItemEvent>();
            for (int pos: mSelection.keySet()){
                selectedItem.add((ListRowItemEvent) getItem(pos));
            }
            return selectedItem;
        }

        @Override
        public void clear() {
            super.clear();
            clearSelection();
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final ListRowItem listRowItem = getItem(position);
            ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
            ListRowSeparator separator;
            final ListRowItemEvent eventItem;
            switch (rowItemType) {
                case SEPARATOR:
                    v = vi.inflate(R.layout.row_separator, null);
                    // Set data for Separator item
                    separator = (ListRowSeparator) listRowItem;
                    TextView dateTextView = (TextView) v.findViewById(R.id.textView_row_separator);
                    dateTextView.setText(separator.text);
                    dateTextView.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    break;
                case ITEM_EVENT:
                    // inflate SMS Row Item
                    v = vi.inflate(R.layout.row_item_event, null);
                    // Set data for SMS item
                    eventItem = (ListRowItemEvent) listRowItem;
                    ImageView icon = (ImageView) v.findViewById(R.id.sms_icon);
                    if (eventItem.contactId > 0)
                        icon.setImageBitmap(_contactsHelper.getContactDisplayImageByID(eventItem.contactId));
                    icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    // Title of event
                    TextView title = (TextView) v.findViewById(R.id.item_event_title);
                    title.setText(eventItem.title);

                    // Content of event
                    TextView content = (TextView) v.findViewById(R.id.item_event_content);
                    content.setText(eventItem.content);

                    // Time of event
                    TextView event_time = (TextView) v.findViewById(R.id.item_event_time);
                    event_time.setText(_timeHelper.getFormatedTime(eventItem.fromTime));
                    // set selected items to be green
                    if (mSelection.get(position) != null) {
                        v.setBackgroundColor(getResources().getColor(R.color.selected_item));// this is a selected position so make it red
                    }else{
                        v.setBackgroundColor(Color.TRANSPARENT);// this is a selected position so make it red
                    }

                    title.setGravity(Gravity.RIGHT);
                    content.setGravity(Gravity.RIGHT);
                    // Tracked Icon
                    final ImageView ic_Tracked = (ImageView) v.findViewById(R.id.item_event_sttImg1);
                    if (!eventItem.title.contains(ContactsHelper._UNKNOWN_NUMBER))
                        ic_Tracked.setImageResource(R.drawable.calendar_ok);
                    else
                        ic_Tracked.setImageResource(R.drawable.calendar_warning);
                    break;
                default:
                    break;
            }
            return v;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //l.setItemChecked(position, !mAdapter.isPositionChecked(position));
    }

    private class SelectionAdapter extends ArrayAdapter<String> {

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

        public SelectionAdapter(Context context, int resource,
                                int textViewResourceId, List<String> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);//let the scheduleListAdapter handle setting up the row views
            v.setBackgroundColor(Color.parseColor("#99cc00")); //default color
            if (mSelection.get(position) != null) {
                v.setBackgroundColor(Color.RED);// this is a selected position so make it red
            }
            return v;
        }
    }

    private static void addMoreSmsItem(Context context, List<SMS> smsList) {
        String title, subTitle, smsNumber, timeTitle;
        long currentSms_datetime, previousSms_datetime;
        int countSmsItem = scheduleListAdapter.getCount();
        setRefreshing(true);

        for (int i = 0; i < smsList.size(); i++) {
            countSmsItem++;
            smsNumber = smsList.get(i).getNumbers();
            currentSms_datetime = smsList.get(i).getSmsTime();
            subTitle = smsList.get(i).getSmsBody();
            if (i == 0) {
                timeTitle = _timeHelper.getDateSttForThePast(currentSms_datetime);
                scheduleListAdapter.add(new ListRowSeparator(timeTitle));
            }
            if (i > 0) {
                previousSms_datetime = smsList.get(i - 1).getSmsTime();
                timeTitle = _timeHelper.processIfDifferentDays(currentSms_datetime, previousSms_datetime);
                if (!timeTitle.equals(""))
                    scheduleListAdapter.add(new ListRowSeparator(timeTitle));
            }
            if (smsNumber != null && !smsNumber.equals("")) {
                title = countSmsItem + ". " + smsList.get(i).getContactNames() + " (" + smsList.get(i).getNumbers() + ")";
            } else {
                title = countSmsItem + ". " + ContactsHelper._UNKNOWN_CONTACT + " (" + ContactsHelper._UNKNOWN_NUMBER + ")";
            }
            scheduleListAdapter.add(new ListRowItemEvent(
                    _contactsHelper.getContactIdByNumber(smsList.get(i).getNumbers()),
                    smsList.get(i).getContactNames(),
                    smsList.get(i).getSmsId(), smsList.get(i).getCal_event_id(), title, subTitle,
                    true, true,
                    smsList.get(i).getSmsTime(), smsList.get(i).getSmsTime(), false));
        }
        scheduleListAdapter.notifyDataSetChanged();
        onRefreshComplete();
    }

    @Override
    public void refresh() {
        scheduleListAdapter.clear();
        super.refresh();
    }
}
