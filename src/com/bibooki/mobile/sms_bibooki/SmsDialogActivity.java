package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bibooki.mobile.TimeHelper;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class SmsDialogActivity extends Activity {
    private static final String TAG = "SmsDialogActivity";
    private static Activity mActivity;

    private UtilsFactory _utilsFactory;
    private TimeHelper _timeHelper;
    private ContactsHelper _contactsHelper;
    private SMS_Utils _smsUtils;
    private NotificationUtils _notificationUtils;

    TextView contactTitle, contactNumber, smsTime, smsContent;
    private EditText smsBodyEditText;
    ImageView imageView_Contact;
    ImageView imageView_sendBtn;
    private TextView countWordTextView, countSmsTextView;

    String smsBody;
    private int[] smsLength;

    String from, msg, smsUri;
    long time;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_new_sms);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setFinishOnTouchOutside(true);

        mActivity = this;

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _timeHelper = TimeHelper.getInstance();
        _contactsHelper = ContactsHelper.getInstance(mActivity);
        _smsUtils = _utilsFactory.getSmsUtils();
        _notificationUtils = _utilsFactory.getNotificationUtils();

        Intent incomingIntent = getIntent();
        smsUri = incomingIntent.getStringExtra("smsUri");
        from = incomingIntent.getStringExtra("From");
        msg = incomingIntent.getStringExtra("Msg");
        time = incomingIntent.getLongExtra("Time", 0);

        setupUI();

        // set this sms as already read
        _smsUtils.setSmsRead(smsUri);
    }
    private void setupUI(){
        smsBodyEditText = (EditText) findViewById(R.id.editText_smsBody);
        contactTitle = (TextView) findViewById(R.id.textView_contact);
        contactNumber = (TextView) findViewById(R.id.textView_number);
        smsTime = (TextView) findViewById(R.id.textView_time);
        smsContent = (TextView) findViewById(R.id.textView_smsContent);
        imageView_Contact = (ImageView) findViewById(R.id.imageView_contact);
        imageView_sendBtn = (ImageView) findViewById(R.id.imageView_sendSMS);
        countSmsTextView = (TextView) findViewById(R.id.textView_smsCount);
        countWordTextView = (TextView) findViewById(R.id.textView_wordCount);

        contactTitle.setText(_contactsHelper.getContactDisplayNameByNumber(from));
        contactNumber.setText(from);
        smsTime.setText(_timeHelper.getFormatedTime(time));
        smsContent.setText(msg);
        imageView_Contact.setImageBitmap(_contactsHelper.getContactBitmapImageByNumber(from));
        imageView_sendBtn.setOnClickListener(sendSmsListener);
        smsBodyEditText.addTextChangedListener(smsBodyTextWatcher);
        countSmsTextView.setText("0 sms");
        countWordTextView.setText("0");
    }
    private View.OnClickListener sendSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            smsBody = smsBodyEditText.getText().toString();
            SMS sms = new SMS();
            if (sms!=null){
                sms.setNumbers(from);
                sms.setSmsBody(smsBody);
                _smsUtils.sendingSMS(mActivity, sms);
                mActivity.finish();
            }else
                _notificationUtils.showShortToast("Phone number or sms content has problem, please try again.");
            //resetSmsContent();
            imageView_sendBtn.setClickable(false);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageView_sendBtn.setClickable(true);
                }
            }, 5000);//Message will be delivered in 5 second.
        }
    };
    private TextWatcher smsBodyTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            smsBody = s.toString();
            smsLength = SmsMessage.calculateLength(smsBody, false);
            //Log.i(TAG, smsLength.toString());
            countSmsTextView.setText(smsLength[0] + " sms");
            countWordTextView.setText(smsLength[1] +"/" + smsLength[2]);
        }
    };
}
