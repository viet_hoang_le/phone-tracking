package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.ListFragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bibooki.mobile.sms_bibooki.rows.CheckedTextView_RowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;

public class Fragment_Advanced_Setting extends ListFragment {
    private String tracking_setting_title;
    private String setting_ignore_ownerphone,
                    setting_ignore_ownerphoneInfo,
                    setting_ignore_phoneCallType;

    boolean isChecked = false;
    Uri photoUri = null;

    private Activity mActivity;
    private UtilsFactory _utilsFactory;
    private AdvancedSettingPreference _advancedSettingPreference;

    private AdvancedListAdapter advancedListAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _advancedSettingPreference = _utilsFactory.getAdvancedSettingPreference();

        advancedListAdapter = new AdvancedListAdapter(mActivity);
        setListAdapter(advancedListAdapter);

        setupSectionTitle();

        add_AdvancedSettingItems();
    }
    private void setupSectionTitle(){
        tracking_setting_title = getResources().getString(R.string.tracking_section_title);
        setting_ignore_ownerphone = getResources().getString(R.string.ignore_owner_phonenumber);
        setting_ignore_ownerphoneInfo = getResources().getString(R.string.ignore_ownerphoneInfo);
        setting_ignore_phoneCallType = getResources().getString(R.string.ignore_phone_callType);
    }
    private void add_AdvancedSettingItems(){

        advancedListAdapter.add(new ListRowSeparator(tracking_setting_title));

        // Add Setting for Ignore Owner Phone Number in Fixed Hash (for tracking status or restore)
        isChecked = _advancedSettingPreference.read_ignoreOwner_setting();
        photoUri = null;
        advancedListAdapter.add(new CheckedTextView_RowItem(setting_ignore_ownerphone, isChecked, photoUri));

        // Add Setting for Ignore Phone Type Setting (for tracking & restore)
        isChecked = _advancedSettingPreference.read_ignorePhoneCallType_setting();
        photoUri = Uri.parse("android.resource://com.bibooki.mobile.sms_bibooki/"+R.drawable.ic_menu_call);
        advancedListAdapter.add(new CheckedTextView_RowItem(setting_ignore_phoneCallType, isChecked, photoUri));

        // Add Setting for Ignore Phone Type Setting (for tracking & restore)
        isChecked = _advancedSettingPreference.read_ignoreOwnerInfo_setting();
        photoUri = Uri.parse("android.resource://com.bibooki.mobile.sms_bibooki/"+R.drawable.ic_restore_75_128);
        advancedListAdapter.add(new CheckedTextView_RowItem(setting_ignore_ownerphoneInfo, isChecked, photoUri));
    }
}
