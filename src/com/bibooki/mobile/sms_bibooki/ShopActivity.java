package com.bibooki.mobile.sms_bibooki;

import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.MyBillingApp;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

public class ShopActivity extends BaseMenuActivity {

    private static UtilsFactory _utilsFactory;
    private static PreferencesHelper _preferenceHelper;
    private MyBillingApp _myBillingApp;

    private static AudioManager audioManager;
    private static MediaPlayer mMediaPlayer;

    Handler handler = new Handler();
    Runnable pauseMusicRunable = new Runnable() {
        public void run() {
            mMediaPlayer.pause();
        }
    };

    private static int OLD_VOLUME = 0;
    private static int MAX_VOLUME = 0;
    private static int VOLUME = 0;

    private static Context _context;

    @Override
    public void onBackPressed(){
        finish();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            _context = this;
            _utilsFactory = UtilsFactory.newInstance(_context);
            _preferenceHelper = _utilsFactory.getPreferencesHelper();
            _myBillingApp = new MyBillingApp(this, this, getString(R.string.app_public_key));

            audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
            OLD_VOLUME = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            MAX_VOLUME = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mMediaPlayer = MediaPlayer.create(this, R.raw.buy);

            setContentView(R.layout.shop);
            setupBoughItem();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setupBoughItem(){
        ImageView adsPackBtn = (ImageView) findViewById(R.id.buy_ads_btn);
        TextView removeAdsStt = (TextView) findViewById(R.id.remove_ads_desc_1);

        boolean mIsFreeAds = _preferenceHelper.isFreeAds(_context);
        if (mIsFreeAds){
            adsPackBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            removeAdsStt.setText("Ads Free. Thank you for your support.");
            removeAdsStt.setTextColor(Color.BLUE);
            adsPackBtn.setImageResource(R.drawable.handshake_1_128);
        }else{
            adsPackBtn.setOnClickListener(buyFreeAdsListener);
        }
    }

    View.OnClickListener buyFreeAdsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                _myBillingApp.buyItem(getString(R.string.sku_free_ads));
                //_myBillingApp.buyItem("android.test.purchased");
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    };

    @Override
    protected void onDestroy(){
        resetVolume();
        super.onDestroy();
    }
    @Override
    protected void onPause(){
        resetVolume();
        super.onPause();
    }
    @Override
    protected void onStop(){
        resetVolume();
        super.onStop();
    }
    private void resetVolume(){
        AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, OLD_VOLUME, AudioManager.FLAG_PLAY_SOUND);
    }

    public void playDefaultAlarm(){
        /** Creating an Alert Dialog Window */
        mMediaPlayer.setLooping(false); // Set looping
        VOLUME = MAX_VOLUME*2/3;
        AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, VOLUME, AudioManager.FLAG_PLAY_SOUND);
        mMediaPlayer.start();
    }

    // updates UI to reflect model
    public void updateUi() {
        setupBoughItem();
        playDefaultAlarm();
    }
}
