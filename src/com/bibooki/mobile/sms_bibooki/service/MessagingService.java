/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bibooki.mobile.sms_bibooki.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.SmsMessage;
import android.util.Log;

import com.bibooki.mobile.sms_bibooki.broadcast_receiver.MessagingReceiver;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.SmsChatHead_Service;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

/**
 * This service is triggered internally only and is used to process incoming SMS and MMS messages
 * that the {@link com.bibooki.mobile.sms_bibooki.broadcast_receiver.MessagingReceiver} passes over. It's
 * preferable to handle these in a service in case there is significant work to do which may exceed
 * the time allowed in a receiver.
 */
public class MessagingService extends IntentService {
    private static final String TAG = "MessagingService";

    // These actions are for this app only and are used by MessagingReceiver to start this service
    public static final String ACTION_MY_RECEIVE_SMS = "com.bibooki.mobile.sms_bibooki.RECEIVE_SMS";
    public static final String ACTION_MY_RECEIVE_MMS = "com.bibooki.mobile.sms_bibooki.RECEIVE_MMS";

    private UtilsFactory _utilsFactory;
    private SMS_Utils _smsUtils;

    public MessagingService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Context context = this;
            _utilsFactory = UtilsFactory.newInstance(context);
            _smsUtils = _utilsFactory.getSmsUtils();

            String intentAction = intent.getAction();
            if (ACTION_MY_RECEIVE_SMS.equals(intentAction)) {
                // TODO: Handle incoming SMS
                SMS sms = new SMS();
                Uri uriResult;
                SmsMessage[] msgs = null;
                String sender = "";
                long firstReceiveTime = -1;
                StringBuilder smsContent = new StringBuilder();
                Bundle bundle = intent.getExtras();        			// SMS message passed

                if (bundle != null) {

                    Object[] pdus = (Object[]) bundle.get("pdus");	// SMS message received
                    msgs = new SmsMessage[pdus.length];

                    for (int i=0; i<msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        if (sender.equals(""))
                            sender = msgs[i].getOriginatingAddress();
                        if (firstReceiveTime==-1)
                            firstReceiveTime = msgs[i].getTimestampMillis();
                        smsContent.append(msgs[i].getDisplayMessageBody());
                    }
                    // Toast.makeText(context, str, Toast.LENGTH_SHORT).show();    //display SMS
                    // write the sms into database
                    sms.setNumbers(sender);
                    sms.setType(SMS.RECEIVE_TYPE);
                    sms.setSmsBody(smsContent.toString());
                    sms.setSmsTime(firstReceiveTime);
                    uriResult = _smsUtils.writingSMS_Database(context, sms);

                    // play alert notification sound
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(context, notification);
                    r.play();
                    Vibrator vibrator;
                    vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(500);
                    vibrator.vibrate(500);

                    // Start service for showing sms chathead
                    Intent chatHead = new Intent(context, SmsChatHead_Service.class);
                    chatHead.putExtra("smsUri", uriResult.toString());
                    chatHead.putExtra("Msg", sms.getSmsBody());
                    chatHead.putExtra("From", sms.getNumbers());
                    chatHead.putExtra("Time", sms.getSmsTime());
                    context.startService(chatHead);
                    //MainActivity.resetFragment();
                }
                // Ensure wakelock is released that was created by the WakefulBroadcastReceiver
                MessagingReceiver.completeWakefulIntent(intent);
            } else if (ACTION_MY_RECEIVE_MMS.equals(intentAction)) {
                // TODO: Handle incoming MMS
                String action = intent.getAction();
                String type = intent.getType();
                Bundle bundle = intent.getExtras();

                Log.d(TAG, "bundle " + bundle);
                SmsMessage[] msgs = null;
                String str = "";
                int contactId = -1;
                String address;

                if (bundle != null) {

                    byte[] buffer = bundle.getByteArray("data");
                    Log.d(TAG, "buffer " + buffer);
                    String incomingNumber = new String(buffer);
                    int indx = incomingNumber.indexOf("/TYPE");
                    if(indx>0 && (indx-15)>0){
                        int newIndx = indx - 15;
                        incomingNumber = incomingNumber.substring(newIndx, indx);
                        indx = incomingNumber.indexOf("+");
                        if(indx>0){
                            incomingNumber = incomingNumber.substring(indx);
                            Log.d(TAG, "Mobile Number: " + incomingNumber);
                        }
                    }

                    int transactionId = bundle.getInt("transactionId");
                    Log.d(TAG, "transactionId " + transactionId);

                    int pduType = bundle.getInt("pduType");
                    Log.d(TAG, "pduType " + pduType);

                    byte[] buffer2 = bundle.getByteArray("header");
                    String header = new String(buffer2);
                    Log.d(TAG, "header " + header);

                    if(contactId != -1){
                        //showNotification(contactId, str);
                    }

                    // ---send a broadcast intent to update the MMS received in the
                    // activity---
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("MMS_RECEIVED_ACTION");
                    broadcastIntent.putExtra("mms", str);
                    context.sendBroadcast(broadcastIntent);

                }
                // Ensure wakelock is released that was created by the WakefulBroadcastReceiver
                MessagingReceiver.completeWakefulIntent(intent);
            }
        }
    }
}
