package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bibooki.mobile.sms_bibooki.rows.ListRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class ConversationSms_Fragment extends BaseFragment {

    static final String ME_STT = "me";

    private static Context mContext;
    private static Activity mActivity;

    private static Handler mHandler = new Handler();

    private static UtilsFactory _utilsFactory;
    private static SMS_Utils _smsUtils;
    private static ContactsHelper _contactsHelper;

    private static ListAdapter listAdapter;
    static List<ListRowItem> listRowItems = new ArrayList<ListRowItem>();
    private static int countItem = 0;
    private static int offset = 0;

    private static ListView smsListView;

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();

        smsListView = getListView();

        // BEGIN_INCLUDE (setup_refreshlistener)
        setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setupAdapter();
            }
        });
        // END_INCLUDE (setup_refreshlistener)

        // setup utils
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _smsUtils = _utilsFactory.getSmsUtils();
        _contactsHelper = ContactsHelper.getInstance(mContext);

        listAdapter = new ListAdapter(mContext);
        setListAdapter(listAdapter);

        // setup adapter --- load data from sms inbox database
        setupAdapter();
    }

    private void setupAdapter() {
        new AddSMSItem().execute(item_loading_number);
    }
    private static class AddSMSItem extends AsyncTask<Integer, Void, Integer> {
        protected void onPreExecute(){
            mHandler.post(new Runnable() {
                public void run() {
                    setRefreshing(true);
                }
            });
            listRowItems.clear();
            countItem = 0;
            listAdapter.clear();
        }
        protected Integer doInBackground(Integer... params) {
            List<SMS> conversationList = new ArrayList<SMS>();
            try {
                SMS sms = _smsUtils.getSMS_byId(mContext, ConversationSms_Activity.itemId);
                conversationList = _smsUtils.getSmsByPhoneNumber(mActivity, params[0], offset, sms.getNumbers());

                if (conversationList==null || conversationList.size()==0){
                    return 0;
                }else{
                    String title, subTitle, timeTitle;
                    long currentSms_datetime, previousSms_datetime;
                    // if list is not empty, continue fire items
                    for (int i=conversationList.size()-1; i>=0; i--){
                        title = conversationList.get(i).getContactNames();
                        subTitle = conversationList.get(i).getSmsBody();

                        if (conversationList.get(i).getType()==2){
                            title = ME_STT;
                        }
                        currentSms_datetime = conversationList.get(i).getSmsTime();
                        if (i==0){
                            timeTitle = _timeHelper.getDateSttForThePast(currentSms_datetime);
                            if (timeTitle!=null && !timeTitle.equals(""))
                                listRowItems.add(new ListRowSeparator(timeTitle));
                        }
                        if (i>0){
                            // the sms items were sorted by DateTime ASC, therefore:
                            previousSms_datetime = conversationList.get(i-1).getSmsTime();
                            timeTitle = _timeHelper.processIfDifferentDays(currentSms_datetime, previousSms_datetime);
                            if (timeTitle!=null && !timeTitle.equals(""))
                                listRowItems.add(new ListRowSeparator(timeTitle));
                        }
                        listRowItems.add(new ListRowItemEvent(
                                _contactsHelper.getContactIdByNumber(conversationList.get(i).getNumbers()),
                                conversationList.get(i).getContactNames(),
                                conversationList.get(i).getSmsId(), conversationList.get(i).getCal_event_id(), title, subTitle,
                                conversationList.get(i).isCalendarTracked, conversationList.get(i).isCalendarUpdated,
                                conversationList.get(i).getSmsTime(), conversationList.get(i).getSmsTime(), conversationList.get(i).isRead()));
                    }
                    // update offset
                    countItem += conversationList.size();
                    offset += params[0];
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return conversationList.size();
        }
        protected void onPostExecute(final Integer result) {
            if (result > 0){
                if (result==item_loading_number){
                    listAdapter.add(new ListRowSeparator(mContext.getResources().getString(R.string.load_more_items)));
                }
                listAdapter.addAll(listRowItems);
            }else {
                listAdapter.add(new ListRowSeparator("We cannot find any message for this conversation, please try other phone numbers."));
            }
            listAdapter.notifyDataSetChanged();
            smsListView.setSelection(listAdapter.getCount()-1);
            mHandler.post(new Runnable() {
                public void run() {
                    onRefreshComplete();
                }
            });
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static class ViewHolder {
        View view;

        ImageView contact;
        TextView title;
        TextView content;
        ImageView sttImage1;
        TextView time;
        ListRowItemEvent listRowItemEvent;

        TextView textViewSeparator;
    }
    ViewHolder viewHolder = null;
    static Map<Integer, ViewHolder> viewHolderMap = new HashMap<Integer, ViewHolder>();

    private class ListAdapter extends ArrayAdapter<ListRowItem> {
        private LayoutInflater vi;
        public ListAdapter(Context context) {
            super(context, 0);
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewHolderMap.clear();
        }

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
        public List<ListRowItemEvent> getSelectedEvent(){
            List<ListRowItemEvent> selectedItem = new ArrayList<ListRowItemEvent>();
            for (int pos: mSelection.keySet()){
                selectedItem.add((ListRowItemEvent) getItem(pos));
            }
            return selectedItem;
        }
        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void clear(){
            clearStatic();
            clearSelection();
            super.clear();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final ListRowItem listRowItem = getItem(position);
            ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
            viewHolder = viewHolderMap.get(position);
            int selected_sms_calendar_id;
            ListRowSeparator separator;
            final ListRowItemEvent eventItem;
            switch (rowItemType){
                case SEPARATOR:
                    separator = (ListRowSeparator) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        // store the holder with the view.
                        viewHolder.view = vi.inflate(R.layout.row_separator, null);
                        viewHolder.textViewSeparator = (TextView) viewHolder.view.findViewById(R.id.textView_row_separator);
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    viewHolder.textViewSeparator.setText(separator.text);
                    // loading more items
                    if (separator.text.equals(mContext.getResources().getString(R.string.load_more_items))){
                        viewHolder.textViewSeparator.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                listAdapter.remove(listRowItem);
                                // add sms items into adapter
                                new AddSMSItem().execute(100);
                            }
                        });
                    }
                    viewHolder.textViewSeparator.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    break;
                case ITEM_EVENT:
                    eventItem = (ListRowItemEvent) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        viewHolder.view = vi.inflate(R.layout.row_sms, null);

                        viewHolder.contact = (ImageView) viewHolder.view.findViewById(R.id.sms_icon);
                        viewHolder.title = (TextView) viewHolder.view.findViewById(R.id.item_event_title);
                        viewHolder.content = (TextView) viewHolder.view.findViewById(R.id.item_event_content);
                        viewHolder.time = (TextView) viewHolder.view.findViewById(R.id.item_event_time);
                        viewHolder.sttImage1 = (ImageView) viewHolder.view.findViewById(R.id.item_event_sttImg1);

                        viewHolder.listRowItemEvent = eventItem;

                        // store the holder with the view.
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    // Set data for item
                    viewHolder.contact.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    viewHolder.title.setText(eventItem.title);
                    viewHolder.content.setText(eventItem.content);
                    viewHolder.time.setText(_timeHelper.getFormatedTime(eventItem.fromTime));

                    Bitmap bm=null;
                    if (eventItem.title.equals(ME_STT)){
                        bm = _contactsHelper.getMyAvatar();
                    }else {
                        if (eventItem.contactId > 0) {
                            bm = _contactsHelper.getContactDisplayImageByID(eventItem.contactId);
                        }
                    }
                    if (bm != null)
                        viewHolder.contact.setImageBitmap(bm);
                    // set selected items to be green
                    if (mSelection.get(position) != null) {
                        v.setBackgroundColor(getResources().getColor(R.color.selected_item));// this is a selected position so make it red
                    }else{
                        if (eventItem.title.equals(ME_STT)){
                            v.setBackgroundColor(getResources().getColor(R.color.light_blue));
                        }else
                            v.setBackgroundColor(Color.TRANSPARENT);// this is a selected position so make it red
                    }
                    if (!eventItem.isRead) {
                        viewHolder.title.setTypeface(null, Typeface.BOLD);
                        viewHolder.content.setTypeface(null, Typeface.BOLD);
                    }else{
                        viewHolder.title.setTypeface(null, Typeface.NORMAL);
                        viewHolder.content.setTypeface(null, Typeface.NORMAL);
                    }

                    // set status image button of calendar
                    if (eventItem.isCalendarTracked && eventItem.isCalendarUpdated)
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_ok);
                    else if (eventItem.isCalendarTracked && !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_sync);
                    }
                    // setup on click event of calendar
                    if (_defaultItem==null || _defaultItem.getSmsCalendarId() < 1){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog calendarDialog = _sms_utils.pickingCalendarDialog(mContext, mActivity, null);
                                calendarDialog.show();
                                calendarDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        refresh();
                                    }
                                });
                            }
                        });
                    }
                    else if (!eventItem.isCalendarTracked || !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(mContext, "Selected Tracked Icon", Toast.LENGTH_SHORT).show();
                                if (_trackingUtils.trackingSMS(mContext, eventItem.fromTime)) {
                                    ImageView checkIcon = (ImageView) v;
                                    checkIcon.setImageResource(R.drawable.calendar_ok);
                                    checkIcon.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                                    viewHolder.sttImage1 = checkIcon;
                                    showNotification(getResources().getString(R.string.notification_sync_stt),
                                            getResources().getInteger(R.integer.alert_duration_time));
                                }
                            }
                        });
                    }
                    break;
                default:
                    break;
            }
            return v;
        }
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        viewHolder = viewHolderMap.get(position);
        if (viewHolder != null){
            if(viewHolder.listRowItemEvent!=null) {
                showNotification(viewHolder.listRowItemEvent.content, 0);
                if (!viewHolder.listRowItemEvent.isRead) {
                    _sms_utils.setSmsRead(mContext, viewHolder.listRowItemEvent.itemId, true);
                    viewHolder.listRowItemEvent.isRead = true;
                    listAdapter.notifyDataSetChanged();
                }
            }
        }
    }
    public void clearStatic(){
        listRowItems.clear();
        countItem = 0;
        offset = 0;
        viewHolderMap.clear();
    }
}
