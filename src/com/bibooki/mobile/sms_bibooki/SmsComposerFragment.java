package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

import java.util.Iterator;
import java.util.Map;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;


public class SmsComposerFragment extends DialogFragment {

    Activity mActivity;
    final String TAG = "SmsComposerFragment";

    private UtilsFactory _utilsFactory;
    private ContactsHelper _contactsHelper;
    private SMS_Utils _smsUtils;
    private NotificationUtils _notificationUtils;

    private static String toNumber="", smsBody="";
    private static long contactId = -1;
    private AutoCompleteTextView autoComplete_toNumber;
    private ImageView contactImage, sendImageBtn, scheduleImageBtn, deleteImageBtn;
    private EditText smsBodyEditText;
    private TextView countWordTextView, countSmsTextView;

    private int[] smsLength;
    private SMS _sms;

    static SmsComposerFragment newInstance() {
        SmsComposerFragment f = new SmsComposerFragment();
        return f;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        _contactsHelper = ContactsHelper.getInstance(mActivity);
        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _smsUtils = _utilsFactory.getSmsUtils();
        _notificationUtils = _utilsFactory.getNotificationUtils();

        Dialog dialog = new Dialog(mActivity);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.TOP);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.setContentView(R.layout.dialog_sms_composer);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // setup elements
        autoComplete_toNumber = (AutoCompleteTextView) dialog.findViewById(R.id.autoCompleteTextView_sendTo);
        contactImage = (ImageView) dialog.findViewById(R.id.imageView_contact);
        sendImageBtn = (ImageView) dialog.findViewById(R.id.imageView_sendSMS);
        scheduleImageBtn = (ImageView) dialog.findViewById(R.id.imageView_scheduleSMS);
        deleteImageBtn = (ImageView) dialog.findViewById(R.id.imageView_deleteSms);
        smsBodyEditText = (EditText) dialog.findViewById(R.id.editText_smsBody);
        countWordTextView = (TextView) dialog.findViewById(R.id.textView_wordCount);
        countSmsTextView = (TextView) dialog.findViewById(R.id.textView_smsCount);

        setupStatic();

        // setup click action listener for buttons
        contactImage.setOnClickListener(searchContactListener);
        sendImageBtn.setOnClickListener(sendSmsListener);
        scheduleImageBtn.setOnClickListener(scheduleSmsListener);
        deleteImageBtn.setOnClickListener(deleteSmsListener);
        smsBodyEditText.addTextChangedListener(smsBodyTextWatcher);
        autoComplete_toNumber.addTextChangedListener(toNumberTextWatcher);

        // setup autocomplete number
        autoComplete_toNumber.setThreshold(1);
        // get Contacts
        Map<String, String> numbers_names = _contactsHelper.getNumbers_Names();
        final String[] CONTACTS = new String[numbers_names.size()];
        Iterator it = numbers_names.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            it.remove(); // avoids a ConcurrentModificationException
            CONTACTS[i] = pairs.getValue() + " (" + pairs.getKey()+")";
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line, CONTACTS);
        autoComplete_toNumber.setAdapter(adapter);
        // try this
        autoComplete_toNumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                String str = (String) adapterView.getItemAtPosition(position);
                str = str.substring(str.lastIndexOf('(')+1,str.lastIndexOf(')')).trim().replaceAll(" ", "");
                autoComplete_toNumber.setText(str);
                Uri photoUri = _contactsHelper.getContactDisplayImageByNumber(str);
                if (photoUri!=null && !photoUri.equals(""))
                    contactImage.setImageURI(photoUri);
            }
        });

        /** Creating the alert dialog window */
        return dialog;
    }
    private View.OnClickListener searchContactListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mActivity instanceof MainActivity){
                ((MainActivity)mActivity).pickContact(autoComplete_toNumber, contactImage);
            }
        }
    };
    private View.OnClickListener sendSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _sms = new SMS();
            if (_sms!=null){
                _sms.setNumbers(toNumber);
                _sms.setSmsBody(smsBody);
                _smsUtils.sendingSMS(mActivity, _sms);
                resetSmsContent();
                getDialog().dismiss();
            }else
                _notificationUtils.showShortToast("Phone number or sms content has problem, please try again.");
            //resetSmsContent();
            sendImageBtn.setClickable(false);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendImageBtn.setClickable(true);
                }
            }, 5000);//Message will be delivered in 5 second.
        }
    };
    private View.OnClickListener scheduleSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
    private View.OnClickListener deleteSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            resetSmsContent();
            dismiss();
        }
    };
    private TextWatcher smsBodyTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            smsBody = s.toString();
            smsLength = SmsMessage.calculateLength(smsBody, false);
            //Log.i(TAG, smsLength.toString());
            countSmsTextView.setText(smsLength[0] + " sms");
            countWordTextView.setText(smsLength[1] +"/" + smsLength[2]);
        }
    };
    private TextWatcher toNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            toNumber = s.toString();
            contactId = _contactsHelper.getContactIdByNumber(toNumber);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /** The application should be exit, if the user presses the back button */
    @Override
    public void onDestroy() {
        super.onDestroy();
        MainActivity.setFragment();
    }

    private void setupStatic(){
        autoComplete_toNumber.setText(toNumber);
        smsBodyEditText.setText(smsBody);
        smsLength = SmsMessage.calculateLength(smsBody, false);
        //Log.i(TAG, smsLength.toString());
        countSmsTextView.setText(smsLength[0] + " sms");
        countWordTextView.setText(smsLength[1] +"/" + smsLength[2]);
        Bitmap bitmap = _contactsHelper.getContactDisplayImageByID(contactId);
        if (bitmap!=null)
            contactImage.setImageBitmap(bitmap);
    }

    private void resetSmsContent(){
        toNumber="";
        contactId=-1;
        smsBody="";
        countSmsTextView.setText("0 sms");
        countWordTextView.setText("0");
    }
}
