package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bibooki.mobile.TimeHelper;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.Tracking_Utils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.call_log.Call_Log_Utils;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.Scheduled_SMS_Utils;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class BaseFragment extends ListFragment {

    protected static String TAG = "Main_BaseFragment";

    protected static Handler mHandler = new Handler();

    protected static Context mContext;
    protected static Activity mActivity;
    protected static ContactsHelper _contactsHelper;
    protected UtilsFactory _utilsFactory;
    protected static Tracking_Utils _trackingUtils;
    protected static SMS_Utils _sms_utils;
    protected static Call_Log_Utils _callLogUtils;
    protected static PreferencesHelper _preferencesHelper;
    protected NotificationUtils _notificationUtils;
    protected Scheduled_SMS_Utils _scheduled_sms_utils;
    protected static TimeHelper _timeHelper;

    protected DBTrackCalendarOpenHelper _dbTrackCalendarOpenHelper;
    protected TrackCalendarDbItem _defaultItem;

    private static SwipeRefreshLayout mSwipeRefreshLayout;

    protected static int item_loading_number;

    private Runnable contentItemRunnable;

    TextView contentTextView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Create the list fragment's content view by calling the super method
        final View listFragmentView = super.onCreateView(inflater, container, savedInstanceState);

        // Now create a SwipeRefreshLayout to wrap the fragment's content view
        mSwipeRefreshLayout = new ListFragmentSwipeRefreshLayout(container.getContext());

        // Add the list fragment's content view to the SwipeRefreshLayout, making sure that it fills
        // the SwipeRefreshLayout
        mSwipeRefreshLayout.addView(listFragmentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        // Make sure that the SwipeRefreshLayout will fill the fragment
        mSwipeRefreshLayout.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        getSwipeRefreshLayout().setProgressViewOffset(false, -200, height / 9);
        // Now return the SwipeRefreshLayout as this fragment's content view
        return mSwipeRefreshLayout;
    }

    // refresh list plugin
    //protected PullToRefreshLayout mPullToRefreshLayout;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();

        _dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(mContext);
        _defaultItem = _dbTrackCalendarOpenHelper.selectDefaultRecord();

        _contactsHelper = ContactsHelper.getInstance(mContext);
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _trackingUtils = _utilsFactory.getTracking_Utils();
        _sms_utils = _utilsFactory.getSmsUtils();
        _callLogUtils = _utilsFactory.getCallLogUtils();
        _scheduled_sms_utils = _utilsFactory.getScheduled_SMS_Utils();
        _preferencesHelper = _utilsFactory.getPreferencesHelper();
        _notificationUtils = _utilsFactory.getNotificationUtils();
        _timeHelper = TimeHelper.getInstance();

        _preferencesHelper.store_SMS_listMode(mContext, 0);
        _preferencesHelper.store_CALL_listMode(mContext, 0);

        item_loading_number = getResources().getInteger(R.integer.item_loading_number);

        contentTextView = (TextView) mActivity.findViewById(R.id.item_content_textView);
    }
    /**
     * Set the {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener} to listen for
     * initiated refreshes.
     *
     * @see android.support.v4.widget.SwipeRefreshLayout#setOnRefreshListener(android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener)
     */
    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        mSwipeRefreshLayout.setOnRefreshListener(listener);
    }

    /**
     * Returns whether the {@link android.support.v4.widget.SwipeRefreshLayout} is currently
     * refreshing or not.
     *
     * @see android.support.v4.widget.SwipeRefreshLayout#isRefreshing()
     */
    public static boolean isRefreshing() {
        return mSwipeRefreshLayout.isRefreshing();
    }

    /**
     * Set whether the {@link android.support.v4.widget.SwipeRefreshLayout} should be displaying
     * that it is refreshing or not.
     *
     * @see android.support.v4.widget.SwipeRefreshLayout#setRefreshing(boolean)
     */
    public static void setRefreshing(boolean refreshing) {
        mSwipeRefreshLayout.setRefreshing(refreshing);
    }

    // BEGIN_INCLUDE (refresh_complete)
    /**
     * When the AsyncTask finishes, it calls onRefreshComplete(), which updates the data in the
     * ListAdapter and turns off the progress bar.
     */
    public static void onRefreshComplete() {
        setRefreshing(false);
    }
    // END_INCLUDE (refresh_complete)

    /**
     * Set the color scheme for the {@link android.support.v4.widget.SwipeRefreshLayout}.
     *
     * @see android.support.v4.widget.SwipeRefreshLayout
     */
    public void setColorScheme(int colorRes1, int colorRes2, int colorRes3, int colorRes4) {
        mSwipeRefreshLayout.setColorScheme(colorRes1, colorRes2, colorRes3, colorRes4);
    }

    /**
     * @return the fragment's {@link android.support.v4.widget.SwipeRefreshLayout} widget.
     */
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }
    // BEGIN_INCLUDE (check_list_can_scroll)
    /**
     * Utility method to check whether a {@link ListView} can scroll up from it's current position.
     * Handles platform version differences, providing backwards compatible functionality where
     * needed.
     */
    private static boolean canListViewScrollUp(ListView listView) {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            // For ICS and above we can call canScrollVertically() to determine this
            return ViewCompat.canScrollVertically(listView, -1);
        } else {
            // Pre-ICS we need to manually check the first visible item and the child view's top
            // value
            return listView.getChildCount() > 0 &&
                    (listView.getFirstVisiblePosition() > 0
                            || listView.getChildAt(0).getTop() < listView.getPaddingTop());
        }
    }
    // END_INCLUDE (check_list_can_scroll)
    /**
     * Sub-class of {@link android.support.v4.widget.SwipeRefreshLayout} for use in this
     * {@link android.support.v4.app.ListFragment}. The reason that this is needed is because
     * {@link android.support.v4.widget.SwipeRefreshLayout} only supports a single child, which it
     * expects to be the one which triggers refreshes. In our case the layout's child is the content
     * view returned from
     * {@link android.support.v4.app.ListFragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)}
     * which is a {@link android.view.ViewGroup}.
     *
     * <p>To enable 'swipe-to-refresh' support via the {@link android.widget.ListView} we need to
     * override the default behavior and properly signal when a gesture is possible. This is done by
     * overriding {@link #canChildScrollUp()}.
     */
    private class ListFragmentSwipeRefreshLayout extends SwipeRefreshLayout {

        public ListFragmentSwipeRefreshLayout(Context context) {
            super(context);
        }

        /**
         * As mentioned above, we need to override this method to properly signal when a
         * 'swipe-to-refresh' is possible.
         *
         * @return true if the {@link android.widget.ListView} is visible and can scroll up.
         */
        @Override
        public boolean canChildScrollUp() {
            final ListView listView = getListView();
            if (listView.getVisibility() == View.VISIBLE) {
                return canListViewScrollUp(listView);
            } else {
                return false;
            }
        }

    }

    public void refresh(){
        MainActivity.resetFragment();
    }

    public void showNotification(String content, int duration){
        int durationTime = getResources().getInteger(R.integer.notification_duration_time);
        if (duration > 0)
            durationTime = duration;
        contentTextView.setText(content);
        contentTextView.setVisibility(View.VISIBLE);

        if (contentItemRunnable!=null)
            mHandler.removeCallbacks(contentItemRunnable);

        contentItemRunnable = new Runnable(){
            public void run() {
                contentTextView.setText("");
                contentTextView.setVisibility(View.GONE);
                // do something
            }};
        mHandler.postDelayed(contentItemRunnable, durationTime);
    }
    public void cancelNotification(){
        if (contentItemRunnable!=null)
            mHandler.removeCallbacks(contentItemRunnable);
        contentTextView.clearComposingText();
        contentTextView.setVisibility(View.GONE);
    }
}
