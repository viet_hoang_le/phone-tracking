package com.bibooki.mobile.sms_bibooki;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android.widget.AbsListView.MultiChoiceModeListener;

import com.bibooki.mobile.sms_bibooki.rows.ListRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;

import java.util.*;

public class Fragment_CallLog extends BaseFragment {

    static List<ListRowItem> listRowItems = new ArrayList<ListRowItem>();
    public static CallListAdapter callListAdapter;
    private static int countItem = 0;
    private static int offset = 0;
    static Map<Integer, ViewHolder> viewHolderMap = new HashMap<Integer, ViewHolder>();

    ActionMode mActionMode;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        callListAdapter = new CallListAdapter(mContext);
        setListAdapter(callListAdapter);
        // BEGIN_INCLUDE (setup_refreshlistener)
        /**
         * Implement {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener}. When users do the "swipe to
         * refresh" gesture, SwipeRefreshLayout invokes
         * {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}. In
         * {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}, call a method that
         * refreshes the content. Call the same method in response to the Refresh action from the
         * action bar.
         */
        setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // END_INCLUDE (setup_refreshlistener)

        // update offset
        countItem = 0;
        updateStaticOffset(0);
        // continue process
        // add call log items into adapter
        new AddCallLogItem().execute(item_loading_number);

        // setup actionbar for contextual menu delete
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setMultiChoiceModeListener(new MultiChoiceModeListener() {
            private int selectedItemNumber = 0;
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mActionMode = mode;
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.cabselection_menu, menu);
                return true;
            }
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
            @Override
            public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
                StringBuilder sb = new StringBuilder();
                Set<Integer> positions = callListAdapter.getCurrentCheckedPosition();
                for (Integer pos : positions) {
                    sb.append(" " + pos + ",");
                }
                switch (item.getItemId()) {
                    case R.id.delete_entry:
                        //_notificationUtils.showShortToast(getResources().getString(R.string.notification_cab_deleted) + sb.toString());
                        Dialog deleteOptionDialog = _callLogUtils.getDeleteOptionDialog(mActivity, callListAdapter.getSelectedEvent());
                        deleteOptionDialog.show();
                        deleteOptionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                refresh();
                            }
                        });
                        break;
                    default:
                        selectedItemNumber = 0;
                        callListAdapter.clearSelection();
                        mode.finish();
                }
                return false;
            }
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selectedItemNumber = 0;
                callListAdapter.clearSelection();
            }
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if (checked) {
                    selectedItemNumber++;
                    callListAdapter.setNewSelection(position, checked);
                } else {
                    selectedItemNumber--;
                    callListAdapter.removeSelection(position);
                }
                if (selectedItemNumber ==1)
                    mode.setTitle(selectedItemNumber + " item selected");
                else if (selectedItemNumber > 1)
                    mode.setTitle(selectedItemNumber + " items selected");
            }
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static class ViewHolder {
        View view;

        ImageView contact;
        TextView title;
        TextView content;
        ImageView sttImage1;
        TextView time;
        ListRowItemEvent listRowItemEvent;

        TextView textViewSeparator;
    }
    ViewHolder viewHolder = null;

    public class CallListAdapter extends ArrayAdapter<ListRowItem> {
        private LayoutInflater vi;
        public CallListAdapter(Context context) {
            super(context, 0);
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewHolderMap.clear();
        }
        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
        public List<ListRowItemEvent> getSelectedEvent(){
            List<ListRowItemEvent> selectedItem = new ArrayList<ListRowItemEvent>();
            for (int pos: mSelection.keySet()){
                selectedItem.add((ListRowItemEvent) getItem(pos));
            }
            return selectedItem;
        }
        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void clear(){
            clearStatic();
            clearSelection();
            super.clear();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final ListRowItem listRowItem = getItem(position);
            ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
            viewHolder = viewHolderMap.get(position);
            int selected_call_calendar_id;
            ListRowSeparator separator;
            final ListRowItemEvent eventItem;
            switch (rowItemType){
                case SEPARATOR:
                    separator = (ListRowSeparator) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        // store the holder with the view.
                        viewHolder.view = vi.inflate(R.layout.row_separator, null);
                        viewHolder.textViewSeparator = (TextView) viewHolder.view.findViewById(R.id.textView_row_separator);
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    viewHolder.textViewSeparator.setText(separator.text);
                    // loading more items
                    if (separator.text.equals(mContext.getResources().getString(R.string.load_more_items))){
                        viewHolder.textViewSeparator.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callListAdapter.remove(listRowItem);
                                // add items into adapter
                                new AddCallLogItem().execute(100);
                            }
                        });
                    }
                    viewHolder.textViewSeparator.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    break;
                case ITEM_EVENT:
                    eventItem = (ListRowItemEvent) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        viewHolder.view = vi.inflate(R.layout.row_item_event, null);

                        viewHolder.contact = (ImageView) viewHolder.view.findViewById(R.id.sms_icon);
                        viewHolder.title = (TextView) viewHolder.view.findViewById(R.id.item_event_title);
                        viewHolder.content = (TextView) viewHolder.view.findViewById(R.id.item_event_content);
                        viewHolder.time = (TextView) viewHolder.view.findViewById(R.id.item_event_time);
                        viewHolder.sttImage1 = (ImageView) viewHolder.view.findViewById(R.id.item_event_sttImg1);

                        viewHolder.listRowItemEvent = eventItem;

                        // store the holder with the view.
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    // Set data for item
                    if (eventItem.contactId>0){
                        Bitmap bm = _contactsHelper.getContactDisplayImageByID(eventItem.contactId);
                        if (bm!=null)
                            viewHolder.contact.setImageBitmap(bm);
                    }
                    viewHolder.contact.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancelNotification();
                            Intent smsContentIntent = new Intent(mContext, ConversationCall_Activity.class);
                            smsContentIntent.putExtra("itemId", eventItem.itemId);
                            smsContentIntent.putExtra("contact", eventItem.contact);
                            startActivity(smsContentIntent);
                        }
                    });
                    viewHolder.title.setText(eventItem.title);
                    viewHolder.content.setText(eventItem.content);
                    viewHolder.time.setText(_timeHelper.getFormatedTime(eventItem.fromTime));

                    // set selected items to be green
                    if (mSelection.get(position) != null) {
                        v.setBackgroundColor(getResources().getColor(R.color.selected_item));// this is a selected position so make it red
                    }else{
                        if (eventItem.title.contains("(ME) ")){
                            v.setBackgroundColor(getResources().getColor(R.color.light_green));
                        }else
                            v.setBackgroundColor(Color.TRANSPARENT);// this is a selected position so make it red
                    }

                    if (eventItem.isCalendarTracked && eventItem.isCalendarUpdated)
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_ok);
                    else if (eventItem.isCalendarTracked && !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_sync);
                    }

                    if (_defaultItem==null || _defaultItem.getCallCalendarId() < 1){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog calendarDialog = _utilsFactory.getCallLogUtils().pickingCalendarDialog(mContext, mActivity, null);
                                calendarDialog.show();
                                calendarDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        refresh();
                                    }
                                });
                            }
                        });
                    }
                    else if (!eventItem.isCalendarTracked || !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(mContext, "Selected Tracked Icon", Toast.LENGTH_SHORT).show();
                                if (_trackingUtils.trackingCallLog(getContext(), eventItem.fromTime)) {
                                    ImageView checkIcon = (ImageView) v;
                                    checkIcon.setImageResource(R.drawable.calendar_ok);
                                    checkIcon.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                                    viewHolder.sttImage1 = checkIcon;
                                    showNotification(getResources().getString(R.string.notification_sync_stt),
                                            getResources().getInteger(R.integer.alert_duration_time));
                                }
                            }
                        });
                    }
                    break;
                default:
                    break;
            }
            return v;
        }
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //l.setItemChecked(position, !mAdapter.isPositionChecked(position));
        viewHolder = viewHolderMap.get(position);
        if (viewHolder != null){
            if(viewHolder.listRowItemEvent!=null)
                showNotification(viewHolder.listRowItemEvent.content, 0);
        }
    }

    private class SelectionAdapter extends ArrayAdapter<String> {

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

        public SelectionAdapter(Context context, int resource,
                                int textViewResourceId, List<String> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);//let the callListAdapter handle setting up the row views
            v.setBackgroundColor(Color.parseColor("#99cc00")); //default color
            if (mSelection.get(position) != null) {
                v.setBackgroundColor(Color.RED);// this is a selected position so make it red
            }
            return v;
        }
    }
    private class AddCallLogItem extends AsyncTask<Integer, Void, Integer> {
        protected void onPreExecute(){
            setRefreshing(true);
        }
        protected Integer doInBackground(Integer... params) {
            List<Call_Log> callLogs = new ArrayList<Call_Log>();
            try {
                callLogs = _callLogUtils.getLimitCall(mContext, params[0], offset);
                long callFrom = _preferencesHelper.read_CALL_Time_From(mContext);
                long callTo = _preferencesHelper.read_CALL_Time_To(mContext);
                int mode = _preferencesHelper.read_CALL_listMode(mContext);
                if (mode > 0){
                    callLogs = _callLogUtils.getUnprocessedCall(mContext, callLogs, callFrom, callTo, params[0], offset);
                }
                if (callLogs==null || callLogs.size()==0){
                    return 0;
                }
                listRowItems.clear();
                int countCallItem = countItem;
                String title, subTitle, timeTitle;
                long currentItem_datetime, previousItem_datetime;
                // if list is not empty, continue fire items
                for (int i=0; i<callLogs.size(); i++){
                    title = callLogs.get(i).getGeneratedTitle();
                    subTitle = callLogs.get(i).getGeneratedDescription();

                    if (callLogs.get(i).getType()==2){
                        title = "(ME) " + title;
                        //photoUri = _utilsFactory.getContactUtils().getContactDisplayImageByNumber(mContext, callLogs.get(i).getOwnerNumber());
                    }
                    title = countCallItem+1 + ". " + title;
                    currentItem_datetime = callLogs.get(i).getFromTime();
                    if (i==0){
                        timeTitle = _timeHelper.getDateSttForThePast(currentItem_datetime);
                        listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    if (i>0){
                        previousItem_datetime = callLogs.get(i-1).getFromTime();
                        timeTitle = _timeHelper.processIfDifferentDays(currentItem_datetime, previousItem_datetime);
                        if (!timeTitle.equals(""))
                            listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    listRowItems.add(new ListRowItemEvent(
                            _contactsHelper.getContactIdByNumber(callLogs.get(i).getNumbers()),
                            callLogs.get(i).getContactNames(),
                            callLogs.get(i).getCallId(), callLogs.get(i).getCal_EventId(), title, subTitle,
                            callLogs.get(i).isCalendarTracked, callLogs.get(i).isCalendarUpdated,
                            callLogs.get(i).getFromTime(), callLogs.get(i).getEndTime(), false));
                    // Update the progress bar
                    countCallItem++;
                }
                // offset need to be updated (it'll be updated from call_log_utils)
                countItem += callLogs.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return callLogs.size();
        }
        protected void onPostExecute(final Integer result) {
            mHandler.post(new Runnable() {
                public void run() {
                    if (result>0){
                        listRowItems.add(new ListRowSeparator(mContext.getResources().getString(R.string.load_more_items)));
                        callListAdapter.addAll(listRowItems);
                    }
                    callListAdapter.notifyDataSetChanged();
                    onRefreshComplete();
                }
            });

        }
    }
    public static void addMoreCallItem(final Context context, final List<Call_Log> callLogs){
        // return if list is empty
        if (callLogs==null || callLogs.size()==0){
            mHandler.post(new Runnable() {
                public void run() {
                    callListAdapter.add(new ListRowSeparator(context.getString(R.string.all_call_tracked_stt)));
                    callListAdapter.notifyDataSetChanged();
                }
            });
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                listRowItems.clear();
                int countCallItem = countItem;
                String title, subTitle, timeTitle;
                long currentItem_datetime, previousItem_datetime;
                // if list is not empty, continue fire items
                for (int i=0; i<callLogs.size(); i++){
                    title = callLogs.get(i).getGeneratedTitle();
                    subTitle = callLogs.get(i).getGeneratedDescription();

                    if (callLogs.get(i).getType()==2){
                        title = "(ME) " + title;
                        //photoUri = _utilsFactory.getContactUtils().getContactDisplayImageByNumber(mContext, callLogs.get(i).getOwnerNumber());
                    }
                    title = countCallItem+1 + ". " + title;
                    currentItem_datetime = callLogs.get(i).getFromTime();
                    if (i==0){
                        timeTitle = _timeHelper.getDateSttForThePast(currentItem_datetime);
                        listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    if (i>0){
                        previousItem_datetime = callLogs.get(i-1).getFromTime();
                        timeTitle = _timeHelper.processIfDifferentDays(currentItem_datetime, previousItem_datetime);
                        if (!timeTitle.equals(""))
                            listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    listRowItems.add(new ListRowItemEvent(
                            _contactsHelper.getContactIdByNumber(callLogs.get(i).getNumbers()),
                            callLogs.get(i).getContactNames(),
                            callLogs.get(i).getCallId(), callLogs.get(i).getCal_EventId(), title, subTitle,
                            callLogs.get(i).isCalendarTracked, callLogs.get(i).isCalendarUpdated,
                            callLogs.get(i).getFromTime(), callLogs.get(i).getEndTime(), false));
                    // Update the progress bar
                    countCallItem++;
                }
                mHandler.post(new Runnable() {
                    public void run() {
                        listRowItems.add(new ListRowSeparator(context.getResources().getString(R.string.load_more_items)));
                        // update offset
                        countItem += callLogs.size();
                        //progressDialog.dismiss();
                        callListAdapter.addAll(listRowItems);
                        callListAdapter.notifyDataSetChanged();
                        // change the button to track all items
                        setupTrackAllBtn();
                    }
                });
            }
        }).start();
    }
    protected static void setupTrackAllBtn(){
        Log.v(TAG, "setup track all btn");
        final ImageView switchBtn = (ImageView) mActivity.findViewById(R.id.imageView_fragmentSwitch_btn);
        switchBtn.setVisibility(View.GONE);
        final ImageView trackAllBtn = (ImageView) mActivity.findViewById(R.id.imageView_trackingBtn);
        trackAllBtn.setVisibility(View.VISIBLE);
        trackAllBtn.setImageResource(R.drawable.btn_check_on_selected);
        trackAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do something that takes a while
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        mHandler.post(new Runnable() { // This thread runs in the UI
                            @Override
                            public void run() {
                                List<Long> timeList = new ArrayList<Long>();
                                for (int i = 0; i < callListAdapter.getCount(); i++) {
                                    ListRowItem listRowItem = callListAdapter.getItem(i);
                                    if (listRowItem.whatType() == ListRowItem.ROW_ITEM_TYPES.ITEM_EVENT) {
                                        ListRowItemEvent listRowItemEvent = (ListRowItemEvent) listRowItem;
                                        timeList.add(listRowItemEvent.fromTime);
                                        callListAdapter.remove(listRowItemEvent);
                                        i--;
                                    }
                                }
                                // update offset
                                countItem -= timeList.size();
                                if (timeList.size() > 0) {
                                    _trackingUtils.batchTrackingCallLog(mContext, timeList);
                                }
                                // continue process
                                viewHolderMap.clear();
                                callListAdapter.notifyDataSetChanged();
                                // reset the track all button
                                trackAllBtn.setVisibility(View.GONE);
                                switchBtn.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                };
                new Thread(runnable).start();
            }
        });
    }
    @Override
    public void refresh(){
        // super refresh will set fragment from MainActivity again
        clearStatic();
        super.refresh();
    }
    public void clearStatic(){
        listRowItems.clear();
        countItem = 0;
        updateStaticOffset(0);
        viewHolderMap.clear();
        if (mActionMode!=null)
            mActionMode.finish();
    }
    public static void updateStaticOffset(int offset){
        Fragment_CallLog.offset = offset;
    }
}