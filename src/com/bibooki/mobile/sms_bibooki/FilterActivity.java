package com.bibooki.mobile.sms_bibooki;

import android.os.Bundle;

public class FilterActivity extends BaseMenuActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_frame);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new Fragment_Filter())
                .commit();
    }
}
