package com.bibooki.mobile.sms_bibooki;

import android.app.Dialog;
import android.app.ListFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;

import com.bibooki.mobile.sms_bibooki.rows.FilterRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.filter.Filter_Utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Fragment_Filter extends ListFragment {
    private static UtilsFactory _utilsFactory;
    private static Filter_Utils _filterUtils;
    private static ContactsHelper _contactsHelper;

    private ListAdapter listAdapter;

    private static Handler mHandler = new Handler();

    private static Context mContext;
    private static FilterActivity mActivity;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (FilterActivity) getActivity();
        mContext = getActivity();
        // setup utils
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _filterUtils = _utilsFactory.getFilterUtils();
        _contactsHelper = ContactsHelper.getInstance(mContext);

        listAdapter = new ListAdapter(mContext);
        setListAdapter(listAdapter);

        // setup adapter --- load data from sqlite database
        setupAdapter();
    }

    private void setupAdapter() {
        String smsCalendarName = getResources().getString(R.string.sms_calendar),
                callCalendarName = getResources().getString(R.string.call_calendar);
        List<TrackingFilter> trackingFilterList = _filterUtils.selectRecords();
        TrackingFilter trackingFilter;
        if (listAdapter != null)
            listAdapter.clear();
        // add blank filter
        listAdapter.add(new ListRowSeparator(String.valueOf(listAdapter.getCount() / 2)));
        listAdapter.add(new FilterRowItem("", smsCalendarName, callCalendarName, -1));
        // loading saved filter
        ContactsHelper.MyContact myContact;
        for (int i=0; i<trackingFilterList.size(); i++){
            trackingFilter = trackingFilterList.get(i);
            myContact = _contactsHelper.getContactByNumber(trackingFilter.getPhonenumber());
            if (myContact!=null){
                listAdapter.add(new ListRowSeparator(String.valueOf(listAdapter.getCount() / 2) + ". " + myContact.displayName));
                if (trackingFilter.getSmsCalendarId()>0){
                    smsCalendarName = trackingFilter.getSmsCalendarId()+". "+trackingFilter.getSmsCalendarName();
                }
                if (trackingFilter.getCallCalendarId()>0){
                    callCalendarName = trackingFilter.getCallCalendarId()+". "+trackingFilter.getCallCalendarName();
                }
                listAdapter.add(new FilterRowItem(
                        trackingFilter.getPhonenumber(),
                        smsCalendarName,
                        callCalendarName,
                        myContact.id));
            }
        }
    }

    private class ListAdapter extends ArrayAdapter<ListRowItem> {
        private LayoutInflater vi;

        public ListAdapter(Context context) {
            super(context, 0);
            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final ListRowItem listRowItem = getItem(position);
            ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
            ListRowSeparator separator;
            switch (rowItemType){
                case SEPARATOR:
                    // inflate Calendar Row Separator Item
                    v = vi.inflate(R.layout.row_separator_left_align, null);
                    // Set data for Separator item
                    separator = (ListRowSeparator) listRowItem;
                    TextView textView_separator = (TextView) v.findViewById(R.id.textView_row_separator);
                    textView_separator.setText(separator.text);
                    break;
                case FILTER_ITEM:
                    // inflate Calendar Row Item
                    v = vi.inflate(R.layout.row_filter, null);
                    // Set data for Calendar item
                    // Set data for Calendar item
                    final FilterRowItem filterListRowItem = (FilterRowItem) listRowItem;
                    final ImageView icon = (ImageView) v.findViewById(R.id.filter_row_icon);
                    // AutoCompleteTextView for Contact
                    final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView_Contact);
                    autoCompleteTextView.setText(filterListRowItem.phoneNumber);
                    autoCompleteTextView.setThreshold(1);
                    // get Contacts
                    Map<String, String> numbers_names = _contactsHelper.getNumbers_Names();
                    final String[] CONTACTS = new String[numbers_names.size()];
                    Iterator it = numbers_names.entrySet().iterator();
                    int i = 0;
                    while (it.hasNext()) {
                        Map.Entry pairs = (Map.Entry)it.next();
                        it.remove(); // avoids a ConcurrentModificationException
                        CONTACTS[i] = pairs.getValue() + " (" + pairs.getKey()+")";
                        i++;
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, CONTACTS);
                    autoCompleteTextView.setAdapter(adapter);
                    // try this
                    autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                            String str = (String) adapterView.getItemAtPosition(position);
                            str = str.substring(str.lastIndexOf('(')+1,str.lastIndexOf(')')).trim().replaceAll(" ", "");
                            autoCompleteTextView.setText(str);
                            Uri photoUri = _contactsHelper.getContactDisplayImageByNumber(str);
                            icon.setImageURI(photoUri);
                        }
                    });
                    // imageView icon
                    icon.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mActivity instanceof BaseMenuActivity){
                                BaseMenuActivity baseMenuActivity = (BaseMenuActivity) mActivity;
                                baseMenuActivity.pickContact(autoCompleteTextView, icon);
                            }
                        }
                    });
                    if (filterListRowItem.contactId>0)
                        icon.setImageBitmap(_contactsHelper.getContactDisplayImageByID(filterListRowItem.contactId));
                    // SMS CheckedTextView
                    final CheckedTextView checkedSMSCalendar = (CheckedTextView) v.findViewById(R.id.checkedTextView_SMSCalendar_all);
                    checkedSMSCalendar.setText(filterListRowItem.smsCalendar);
                    // Setup onclick Listener
                    checkedSMSCalendar.setOnClickListener(smsCalendarSelector);
                    if (!filterListRowItem.smsCalendar.equals(getResources().getString(R.string.sms_calendar))) {
                        checkedSMSCalendar.setSelected(true);
                        checkedSMSCalendar.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                    }
                    // Call Log CheckedTextView
                    final CheckedTextView checkedCallCalendar = (CheckedTextView) v.findViewById(R.id.checkedTextView_CALLCalendar);
                    checkedCallCalendar.setText(filterListRowItem.callCalendar);
                    // Setup onclick Listener
                    checkedCallCalendar.setOnClickListener(callCalendarSelector);
                    if (!filterListRowItem.callCalendar.equals(getResources().getString(R.string.call_calendar))) {
                        checkedCallCalendar.setSelected(true);
                        checkedCallCalendar.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                    }
                    // Save image (button)
                    ImageView saveImg = (ImageView) v.findViewById(R.id.imageView_restore_all);
                    saveImg.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String phoneNumber = autoCompleteTextView.getText().toString();
                            if (phoneNumber!=null){
                                int smsCal_id = -1, callCal_id = -1;
                                String smsCalId = checkedSMSCalendar.getText().toString();
                                if (!smsCalId.equals(getResources().getString(R.string.sms_calendar))) {
                                    smsCalId = smsCalId.substring(0, smsCalId.indexOf('.'));
                                    smsCal_id = Integer.valueOf(smsCalId);
                                }
                                String callCalId = checkedCallCalendar.getText().toString();
                                if (!callCalId.equals(getResources().getString(R.string.call_calendar))) {
                                    callCalId = callCalId.substring(0, callCalId.indexOf('.'));
                                    callCal_id = Integer.valueOf(callCalId);
                                }
                                //save filter row
                                if (_filterUtils.updateCreateRecords(phoneNumber, smsCal_id, callCal_id)){
                                    setupAdapter();
                                }
                            }
                        }
                    });
                    // Delete image (button)
                    ImageView removeImg = (ImageView) v.findViewById(R.id.removeFilterImg);
                    removeImg.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String phoneNumber = autoCompleteTextView.getText().toString();
                            if (phoneNumber!=null){
                                //remove filter row
                                _filterUtils.deleteFilter(phoneNumber);
                            }
                            listAdapter.remove(listRowItem);
                        }
                    });
                    break;
                default:
                    break;
            }
            return v;
        }

        private OnClickListener smsCalendarSelector = new OnClickListener() {
            @Override
            public void onClick(View v) {
                final CheckedTextView checkedTextView = (CheckedTextView) v;
                if (checkedTextView.isSelected()) {
                    checkedTextView.setSelected(false);
                    checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                    checkedTextView.setText(R.string.sms_calendar);
                    // reset calendar id in shared preferences
                } else {
                    // set dialog message
                    mHandler.post(new Runnable() {
                        public void run() {
                            Dialog calendarDialog = _utilsFactory.getSmsUtils().calendarPicker(mContext, mActivity, checkedTextView);
                            calendarDialog.show();
                        }
                    });
                }
            }
        };
        private OnClickListener callCalendarSelector = new OnClickListener() {
            @Override
            public void onClick(View v) {
                final CheckedTextView checkedTextView = (CheckedTextView) v;
                if (checkedTextView.isSelected()) {
                    checkedTextView.setSelected(false);
                    checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                    checkedTextView.setText(R.string.call_calendar);
                    // reset calendar id in shared preferences
                } else {
                    // set dialog message
                    mHandler.post(new Runnable() {
                        public void run() {
                            Dialog calendarDialog = _utilsFactory.getCallLogUtils().calendarPicker(mContext, mActivity, checkedTextView);
                            calendarDialog.show();
                        }
                    });
                }
            }
        };
    }
}
