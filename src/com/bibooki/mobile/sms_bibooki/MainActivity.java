package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;

import com.bibooki.mobile.sms_bibooki.utils.Dialog_Utils;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.IabHelper;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.IabResult;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.Inventory;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.MyBillingApp;
import com.bibooki.mobile.sms_bibooki.utils.billing_utils.Purchase;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;
import com.codinguser.android.contactpicker.ContactsPickerActivity;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.*;
import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.Sms_Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

import static com.codinguser.android.contactpicker.ContactsPickerActivity.*;

public class MainActivity extends ActionBarActivity {

    private static Context mContext;
    private static Activity mActivity;

    private final String TAG = "MainActivity";

    private static UtilsFactory _utilsFactory;
    private static NotificationUtils _notificationUtils;
    private static PreferencesHelper _preferencesHelper;
    private static SMS_Utils _smsUtils;
    ContactsHelper _contactsHelper;

    // Service for auto tracking/schedule
    private Intent service;

    private MyBillingApp _myBillingApp;
    IabHelper mHelper;

    // Google AdMob
    static boolean mIsFreeAds = false;
    private InterstitialAd mInterstitialAd;
    // Finish Google AdMob
    // The helper in-app billing object

    // Drawer Navigation
    private String[] mDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    public enum FRAGMENTS {SMS_FRAGMENT, CALL_FRAGMENT, SCHEDULES_FRAGMENT, CALENDAR_FRAGMENT,
        AUTO_SYNC_FRAGMENT, FILTER_FRAGMENT, RESTORE_FRAGMENT, SHOP_FRAGMENT, HOWTO_FRAGMENT};
    private static FRAGMENTS currentFragment = FRAGMENTS.SMS_FRAGMENT;
    private static Fragment mFragment = null;

    private static ImageView searchBtn;
    private static ActionBar _actionBar;
    private static FragmentManager _fragmentManager;

    private int makeDefaultSmsId =-1;

    private boolean isComposeSmsAdded = false;


    @Override
    public void onResume(){
        super.onResume();
    }
    @Override
    public void onBackPressed(){
        int smsMode = _preferencesHelper.read_SMS_listMode(mContext);
        int callMode = _preferencesHelper.read_CALL_listMode(mContext);
        if (smsMode>0 || callMode>0) {
            resetFragment();
        }
        else
            super.onBackPressed();
    }
    @Override
    public void onPause(){
        super.onPause();
    }
    @Override
    public void onStart(){
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);

        // Start service for auto tracking SMS & Call Log
        service = new Intent(this, Sms_Service.class);
        startService(service);
    }
    @Override
    public void onStop(){
        EasyTracker.getInstance(this).activityStop(this);
        super.onStop();
    }
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.main);

            mActivity = this;
            mContext = this;

            //_myBillingApp = new MyBillingApp(this, this, getString(R.string.app_public_key));
            //setupProperties(getString(R.string.app_public_key), getString(R.string.sku_free_ads));
            if (mIsFreeAds==false)
                mIsFreeAds = getResources().getBoolean(R.bool.test_free_ads);

            _utilsFactory = UtilsFactory.newInstance(mActivity);
            _notificationUtils = _utilsFactory.getNotificationUtils();
            _preferencesHelper = _utilsFactory.getPreferencesHelper();
            _smsUtils = _utilsFactory.getSmsUtils();
            _contactsHelper = ContactsHelper.getInstance(mActivity);

            _actionBar = getSupportActionBar();
            // Specify that the Home/Up button should not be enabled, since there is no hierarchical
            // parent.
            _actionBar.setHomeButtonEnabled(true);
            _actionBar.setDisplayHomeAsUpEnabled(true);
            _fragmentManager = getSupportFragmentManager();
            // drawer
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
            // setting the nav drawer list adapter
            setupDrawerAdapter();
            adapter = new NavDrawerListAdapter(getApplicationContext(),
                    navDrawerItems);
            mDrawerList.setAdapter(adapter);
            // Set the list's click listener
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
            mDrawerLayout.closeDrawer(mDrawerList);
            // ActionBarDrawerToggle ties together the the proper interactions
            // between the sliding drawer and the action bar app icon
            mDrawerToggle = new ActionBarDrawerToggle(
                    this,                  /* host Activity */
                    mDrawerLayout,         /* DrawerLayout object */
                    R.string.drawer_open,  /* "open drawer" description for accessibility */
                    R.string.drawer_close  /* "close drawer" description for accessibility */
            ) {
                public void onDrawerClosed(View view) {
                    _actionBar.setTitle(R.string.app_name);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }

                public void onDrawerOpened(View drawerView) {
                    _actionBar.setTitle("Setting");
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);

            // search item button
            searchBtn = (ImageView) findViewById(R.id.imageView_search_btn);
            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (currentFragment){
                        case SMS_FRAGMENT:
                            trackingDialog(Dialog_Utils.DIALOG_TYPE.SEARCH_UNPROCESSED_SMS_DIALOG).show();
                            break;
                        case CALL_FRAGMENT:
                            trackingDialog(Dialog_Utils.DIALOG_TYPE.SEARCH_UNPROCESSED_CALL_DIALOG).show();
                            break;
                        case SCHEDULES_FRAGMENT:
                            break;
                        default:
                            break;
                    }
                }
            });

            setFragment();
            // switch fragment button
            setupSwitchBtn();

            // Google AdMob
            loadInterstitualAd();
            // Finish Google AdMob

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static void resetFragment(){
        _preferencesHelper.store_SMS_listMode(mContext, 0);
        _preferencesHelper.store_CALL_listMode(mContext, 0);
        final ImageView switchBtn = (ImageView) mActivity.findViewById(R.id.imageView_fragmentSwitch_btn);
        switchBtn.setVisibility(View.VISIBLE);
        final ImageView trackAllBtn = (ImageView) mActivity.findViewById(R.id.imageView_trackingBtn);
        trackAllBtn.setVisibility(View.GONE);
        setFragment();

    }
    private void setupSwitchBtn(){
        final ImageView switchBtn = (ImageView) findViewById(R.id.imageView_fragmentSwitch_btn);
        if (currentFragment == FRAGMENTS.SMS_FRAGMENT) {
            switchBtn.setImageResource(R.drawable.sms_red_shadow_icon_128);
        } else if (currentFragment == FRAGMENTS.CALL_FRAGMENT) {
            switchBtn.setImageResource(R.drawable.phone_shadow_128);
        } else if (currentFragment == FRAGMENTS.SCHEDULES_FRAGMENT) {
            switchBtn.setImageResource(R.drawable.sms_schedule_shadow_128);
        }
        switchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayInterstitial();
                if (currentFragment == FRAGMENTS.SMS_FRAGMENT) {
                    currentFragment = FRAGMENTS.CALL_FRAGMENT;
                    switchBtn.setImageResource(R.drawable.phone_shadow_128);
                } else if (currentFragment == FRAGMENTS.CALL_FRAGMENT) {
                    currentFragment = FRAGMENTS.SCHEDULES_FRAGMENT;
                    switchBtn.setImageResource(R.drawable.sms_schedule_shadow_128);
                } else if (currentFragment == FRAGMENTS.SCHEDULES_FRAGMENT) {
                    currentFragment = FRAGMENTS.SMS_FRAGMENT;
                    switchBtn.setImageResource(R.drawable.sms_red_shadow_icon_128);
                }
                setFragment();
            }
        });
    }
    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        // boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        menu.findItem(R.id.donate).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        // add menu option make app default
        if (!SMS_Utils.isDefaultSmsApp(mContext) && SMS_Utils.hasKitKat()){
            if (makeDefaultSmsId<0){
                makeDefaultSmsId = View.generateViewId();
            }
            if (menu.findItem(makeDefaultSmsId)==null){
                menu.add(0, makeDefaultSmsId, Menu.NONE, "Make default SMS")
                        .setIcon(R.drawable.ic_launcher)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
        }
        if (isComposeSmsAdded==false
        && currentFragment==FRAGMENTS.SMS_FRAGMENT) {
            menu.add(0, Menu.FIRST, Menu.NONE, "Compose SMS")
                    .setIcon(R.drawable.compose_sms)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            isComposeSmsAdded = true;
        }
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public void invalidateOptionsMenu(){
        isComposeSmsAdded = false;
        makeDefaultSmsId = -1;
        super.invalidateOptionsMenu();
    }
    public static void setFragment(){
        // set Fragment
        switch (currentFragment){
            case SMS_FRAGMENT:
                mFragment = new Fragment_SMS();
                _actionBar.setTitle("Sms Tracking");
                searchBtn.setVisibility(View.VISIBLE);
                break;
            case CALL_FRAGMENT:
                mFragment = new Fragment_CallLog();
                _actionBar.setTitle("Call Log Tracking");
                searchBtn.setVisibility(View.VISIBLE);
                break;
            case SCHEDULES_FRAGMENT:
                mFragment = new Fragment_Schedule();
                _actionBar.setTitle("Sms Schedule");
                searchBtn.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        mActivity.invalidateOptionsMenu();
        _fragmentManager.beginTransaction().replace(R.id.main_frame, mFragment).commit();
    }
    private void setupDrawerAdapter(){
        mDrawerItemTitles = getResources().getStringArray(R.array.sidebar_menu_array);
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array
        // Calendar
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[0], R.drawable.ic_calendar));
        navDrawerItems.add(new NavDrawerItem());
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[1], R.drawable.ic_sync));
        navDrawerItems.add(new NavDrawerItem());
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[2], R.drawable.filter_data_48));
        navDrawerItems.add(new NavDrawerItem());
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[3], R.drawable.ic_restore));
        navDrawerItems.add(new NavDrawerItem());
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[4], R.drawable.setting_128));
        navDrawerItems.add(new NavDrawerItem());
        //navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[5], R.drawable.shop_cart_128));
        //navDrawerItems.add(new NavDrawerItem());
        navDrawerItems.add(new NavDrawerItem(mDrawerItemTitles[6], R.drawable.ic_launcher));
        navDrawerItems.add(new NavDrawerItem());
    }
    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
    private void selectItem(int position) {
        Intent intent = null;
        switch (position){
            case 0:
                intent = new Intent(this, CalendarActivity.class);
                break;
            case 2:
                intent = new Intent(this, SyncActivity.class);
                break;
            case 4:
                intent = new Intent(this, FilterActivity.class);
                break;
            case 6:
                intent = new Intent(this, RestoreActivity.class);
                break;
            case 8:
                intent = new Intent(this, AdvancedSettingActivity.class);
                break;
            //case 10:
            //    intent = new Intent(this, ShopActivity.class);
            //    break;
            case 10:
                intent = new Intent(this, TutorialActivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position/2, true);
        setTitle(mDrawerItemTitles[position/2]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }
    // Invoke display Interstitial when you are ready to display an mInterstitialAd.
    public void displayInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
    private void loadInterstitualAd(){
        // If the mInterstitialAd is finished loading, the user will view the mInterstitialAd before
        // proceeding.
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admod_interstitial));

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Set an AdListener.
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                // Proceed to the next level.
            }
        });

        // Start loading the ad now so that it is ready by the alarm_button the user is ready to go to
        // the next level.
        mInterstitialAd.loadAd(adRequestBuilder.build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        Uri uriUrl;
        Intent launchBrowser;
        if (makeDefaultSmsId>0 && item.getItemId()==makeDefaultSmsId){
            _smsUtils.enableDefaultSmsApp();
        }else {
            switch (item.getItemId()) {
                case android.R.id.home:
                    return true;
                case Menu.FIRST:
                    /** Creating an Alert Dialog Window */
                    SmsComposerFragment smsComposerFragment = SmsComposerFragment.newInstance();
                    /** Opening the Alert Dialog Window. This will be opened when the alarm goes off */
                    smsComposerFragment.show(getFragmentManager(), "SmsComposerFragment");
                    break;
                case R.id.donate:
                    //_myBillingApp.buyItem(getString(R.string.sku_donate));
                    /*uriUrl = Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=939LEJNJGNUF2");
                    launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    this.startActivity(launchBrowser);*/
                    _notificationUtils.showInfoBox(getString(R.string.donate_dialog_title), getString(R.string.donate_msg));
                    return true;
                case R.id.about:
                    _notificationUtils.showInfoBox(getString(R.string.about_dialog_title), getString(R.string.about_msg));
                    break;
                case R.id.licenses:
                    _notificationUtils.showInfoBox(getString(R.string.license_dialog_title), getString(R.string.license_message));
                    break;
                case R.id.contact:
                    final Intent email = new Intent(android.content.Intent.ACTION_SENDTO);
                    String uriText = null;
                    try {
                        uriText = "mailto:mobile@bibooki.com" +
                                "?subject=" + URLEncoder.encode("Sms Bibooki Feedback", "UTF8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    email.setData(Uri.parse(uriText));
                    try {
                        startActivity(email);
                    } catch (Exception e) {
                        _notificationUtils.showShortToast(getResources().getString(R.string.no_email));
                    }
                    break;
                case R.id.rating:
                    uriUrl = Uri.parse("https://play.google.com/store/apps/details?id=com.bibooki.mobile.sms_bibooki");
                    //uriUrl = Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=com.bibooki.mobile.sms_bibooki");
                    launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    this.startActivity(launchBrowser);
                    return true;
                case R.id.stop:
                    getApplicationContext().stopService(service);
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list, menu);
        return true;
    }

    // create the search untracked sms dialog for Tracking
    public Dialog trackingDialog(Dialog_Utils.DIALOG_TYPE dialogType){
        Dialog dialog;
        String title = mActivity.getResources().getString(R.string.untracked_sms_dialog_title);
        String[] items = {
                getString(R.string.time_plan_only),
                getString(R.string.time_plan_from_now),
                getString(R.string.time_plan_from_to),
                getString(R.string.time_plan_all)
        };
        dialog = _utilsFactory.getDialog_Utils().createDialog(mContext, mActivity, dialogType, items, title, null, null, null);
        return  dialog;
    }
    /*
    private void setupProperties(String base64EncodedPublicKey, final String freeAdsSku){
        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(mContext, base64EncodedPublicKey);
        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);
        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup own properties.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up in-app billing: " + result);
                    return;
                }
                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;
                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.e(TAG, "Setup successful. Querying inventory.");

                // Listener that's called when we finish querying the items and subscriptions we own
                mHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                        Log.d(TAG, "Query inventory finished.");

                        // Have we been disposed of in the meantime? If so, quit.
                        if (mHelper == null) return;

                        // Is it a failure?
                        if (result.isFailure()) {
                            Log.e(TAG, "Failed to query inventory: " + result);
                            return;
                        }

                        Log.d(TAG, "Query inventory was successful.");
                        // Do we have the free upgrade?
                        Purchase itemPurchase = inventory.getPurchase(freeAdsSku);
                        mIsFreeAds = (itemPurchase != null && MyBillingApp.verifyDeveloperPayload(itemPurchase));
                        _preferencesHelper.setBillingFreeAds(mContext, mIsFreeAds);
                        Log.d(TAG, "User is " + (mIsFreeAds ? "Paid" : "NOT Paid"));
                        Log.d(TAG, "Initial inventory query finished; enabling main UI.");

                        if (itemPurchase!=null && freeAdsSku.equals("android.test.purchased")) {
                            mHelper.consumeAsync(inventory.getPurchase(freeAdsSku), null);
                            Log.d(TAG, "consumed "+freeAdsSku);
                        }
                    }
                });
            }
        });
    }*/
    public static FRAGMENTS getCurrentFragment(){
        return currentFragment;
    }
    public static void setCurrentFragment(FRAGMENTS fragment){
        currentFragment = fragment;
    }

    // get Contact by picker (library)
    static final int PICK_CONTACT_REQUEST = 0;
    AutoCompleteTextView autoCompleteTextView_Contact;
    ImageView icon;
    public void pickContact(AutoCompleteTextView autoCompleteTextView, ImageView icon){
        this.autoCompleteTextView_Contact = autoCompleteTextView;
        this.icon = icon;
        startActivityForResult(new Intent(this, ContactsPickerActivity.class), BaseMenuActivity.PICK_CONTACT_REQUEST);
    }
    // Listen for results.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            // See which child activity is calling us back.
            switch (requestCode) {
                case PICK_CONTACT_REQUEST:
                    // This is the standard resultCode that is sent back if the
                    // activity crashed or didn't doesn't supply an explicit result.
                    if (resultCode == RESULT_CANCELED) {
                        _notificationUtils.showShortToast(getResources().getString(R.string.notification_noPhoneNumber_found));
                    } else {
                        String phoneNumber = data.getExtras().getString(KEY_PHONE_NUMBER);
                        phoneNumber = phoneNumber.trim().replaceAll(" ", "");
                        //String contactName = (String) data.getExtras().get(ContactsPickerActivity.KEY_CONTACT_NAME);
                        //Toast.makeText(this, "Phone number found: " + phoneNumber , Toast.LENGTH_SHORT).show();
                        this.autoCompleteTextView_Contact.setText(phoneNumber);
                        Uri photoUri = _contactsHelper.getContactDisplayImageByNumber(phoneNumber);
                        icon.setImageURI(photoUri);
                    }
                default:
                    break;
            }
            // Pass on the activity result to the helper for handling
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
            else {
                Log.i(TAG, "onActivityResult handled by IABUtil.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /**
     * This class makes the ad request and loads the ad.
     */
    public static class AdFragment extends Fragment {
        private AdView mAdView;
        private boolean isVisible = true;
        // Does the user have the free-ads upgrade?

        public AdFragment() {
            if (mIsFreeAds==true)
                isVisible=false;
        }

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);

            // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
            // values/strings.xml.
            mAdView = (AdView) getView().findViewById(R.id.adView);

            // Create an ad request. Check logcat output for the hashed device ID to
            // get test ads on a physical device. e.g.
            // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
            if (!isVisible) {
                mAdView.getLayoutParams().height = 0;
                mAdView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_ad, container, false);
        }

        /** Called when leaving the activity */
        @Override
        public void onPause() {
            if (mAdView != null) {
                mAdView.pause();
            }
            super.onPause();
        }

        /** Called when returning to the activity */
        @Override
        public void onResume() {
            super.onResume();
            if (mAdView != null) {
                mAdView.resume();
            }
        }

        /** Called before the activity is destroyed */
        @Override
        public void onDestroy() {
            if (mAdView != null) {
                mAdView.destroy();
            }
            super.onDestroy();
        }
    }
}
