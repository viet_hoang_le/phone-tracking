package com.bibooki.mobile.sms_bibooki.rows;

public class SyncListRowItem implements ListRowItem{
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.SYNC_ITEM;
    }
}