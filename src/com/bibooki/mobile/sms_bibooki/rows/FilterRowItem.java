package com.bibooki.mobile.sms_bibooki.rows;

public class FilterRowItem implements ListRowItem{
    public long contactId;
    public String phoneNumber;
    public String smsCalendar;
    public String callCalendar;
    public FilterRowItem(String phoneNumber, String smsCalendar, String callCalendar, long contactId) {
        this.phoneNumber = phoneNumber;
        this.smsCalendar = smsCalendar;
        this.callCalendar = callCalendar;
        this.contactId = contactId;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.FILTER_ITEM;
    }
}