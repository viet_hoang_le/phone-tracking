package com.bibooki.mobile.sms_bibooki.rows;

public class ListRowAdView implements ListRowItem{
    public ListRowAdView() {
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.ADVIEW;
    }
}