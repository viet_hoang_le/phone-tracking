package com.bibooki.mobile.sms_bibooki.rows;

public class CalendarAdvancedItem implements ListRowItem{
    public String text;
    public CalendarAdvancedItem(String text) {
        this.text = text;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.ACCORDION_SETTING;
    }
}