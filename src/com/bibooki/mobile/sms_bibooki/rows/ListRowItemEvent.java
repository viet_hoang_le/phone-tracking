package com.bibooki.mobile.sms_bibooki.rows;

public class ListRowItemEvent implements ListRowItem{
    public String itemId;
    public String eventCalId;
    public long contactId;
    public String contact;
    public String title;
    public String content;
    public boolean isCalendarTracked;
    public boolean isCalendarUpdated;
    public long fromTime;
    public long toTime;
    public boolean isRead;
    public ListRowItemEvent(long contactId, String contact, String itemId, String eventCalId, String title, String content,
                            boolean isCalendarTracked, boolean isCalendarUpdated,
                            long fromTime, long toTime, boolean isRead) {
        this.itemId = itemId;
        this.eventCalId = eventCalId;
        this.contactId = contactId;
        this.contact = contact;
        this.title = title;
        this.content = content;
        this.isCalendarTracked = isCalendarTracked;
        this.isCalendarUpdated = isCalendarUpdated;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.isRead = isRead;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.ITEM_EVENT;
    }
}