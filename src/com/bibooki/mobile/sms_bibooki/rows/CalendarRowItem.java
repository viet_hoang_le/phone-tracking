package com.bibooki.mobile.sms_bibooki.rows;

import android.net.Uri;

public class CalendarRowItem implements ListRowItem{
    public Uri photoUri;
    public String title;
    public String content;
    public boolean checked;
    public CalendarRowItem(String contact, String content, boolean checked, Uri photoUri) {
        this.title = contact;
        this.content = content;
        this.checked = checked;
        this.photoUri = photoUri;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.CALENDAR;
    }
}