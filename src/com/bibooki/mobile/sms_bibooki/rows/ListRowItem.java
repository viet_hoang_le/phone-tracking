package com.bibooki.mobile.sms_bibooki.rows;

public interface ListRowItem {
    public static enum ROW_ITEM_TYPES {
        ADVIEW,
        SEPARATOR,
        CALENDAR,
        ITEM_EVENT,
        SEARCH_BAR,
        ACCORDION_SETTING,
        RUN_BAR,
        SYNC_ITEM,
        FILTER_ITEM,
        RESTORE_ITEM,
        CHECKED_TEXT_VIEW_ITEM
    }
    public ROW_ITEM_TYPES whatType();
}
