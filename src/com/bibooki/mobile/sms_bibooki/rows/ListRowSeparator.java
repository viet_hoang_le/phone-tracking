package com.bibooki.mobile.sms_bibooki.rows;

public class ListRowSeparator implements ListRowItem{
    public String text;
    public ListRowSeparator(String text) {
        this.text = text;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.SEPARATOR;
    }
}