package com.bibooki.mobile.sms_bibooki.rows;

public class ListRowRunBar implements ListRowItem{
    public String btnText1;
    public String btnText2;
    public ListRowRunBar(String btnText1, String btnText2) {
        this.btnText1 = btnText1;
        this.btnText2 = btnText2;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.RUN_BAR;
    }
}