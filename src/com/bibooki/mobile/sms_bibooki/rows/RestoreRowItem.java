package com.bibooki.mobile.sms_bibooki.rows;

import android.net.Uri;

public class RestoreRowItem implements ListRowItem{
    public Uri photoUri;
    public String phoneNumber;
    public String smsCalendar;
    public String callCalendar;
    public RestoreRowItem(String phoneNumber, String smsCalendar, String callCalendar, Uri photoUri) {
        this.phoneNumber = phoneNumber;
        this.smsCalendar = smsCalendar;
        this.callCalendar = callCalendar;
        this.photoUri = photoUri;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.RESTORE_ITEM;
    }
}