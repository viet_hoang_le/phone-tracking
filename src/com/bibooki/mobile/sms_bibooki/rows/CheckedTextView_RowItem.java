package com.bibooki.mobile.sms_bibooki.rows;

import android.net.Uri;

public class CheckedTextView_RowItem implements ListRowItem{
    public Uri photoUri;
    public String title;
    public boolean checked;
    public CheckedTextView_RowItem(String title, boolean checked, Uri photoUri) {
        this.title = title;
        this.checked = checked;
        this.photoUri = photoUri;
    }
    @Override
    public ROW_ITEM_TYPES whatType(){
        return ROW_ITEM_TYPES.CHECKED_TEXT_VIEW_ITEM;
    }
}