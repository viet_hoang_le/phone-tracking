package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

public class ConversationSms_Activity extends ActionBarActivity {

    Activity mActivity;

    static UtilsFactory _utilsFactory;
    static NotificationUtils _notificationUtils;
    private SMS_Utils _smsUtils;

    private static ActionBar _actionBar;
    private static FragmentManager _fragmentManager;

    public static String itemId;
    public static String contact;

    public static RelativeLayout composeArea;
    private ImageView sendImageBtn, scheduleImageBtn, deleteImageBtn;
    private EditText smsBodyEditText;
    private TextView countWordTextView, countSmsTextView;
    private SMS _sms;

    // Google AdMob
    private RelativeLayout adFragment;
    // Finish Google AdMob

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;

        setContentView(R.layout.sms_conversation);

        adFragment = (RelativeLayout) findViewById(R.id.adFragment_layout);
        composeArea = (RelativeLayout) findViewById(R.id.composing_area_menu);
        hideComposingArea();

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _notificationUtils = _utilsFactory.getNotificationUtils();
        _smsUtils = _utilsFactory.getSmsUtils();

        // setup elements
        sendImageBtn = (ImageView) findViewById(R.id.imageView_sendSmsBtn);
        scheduleImageBtn = (ImageView) findViewById(R.id.imageView_scheduleSmsBtn);
        deleteImageBtn = (ImageView) findViewById(R.id.imageView_clearSmsBtn);
        smsBodyEditText = (EditText) findViewById(R.id.editText_smsContent);
        countWordTextView = (TextView) findViewById(R.id.textView_characterCount);
        countSmsTextView = (TextView) findViewById(R.id.textView_smsCount);
        // setup click action listener for buttons
        sendImageBtn.setOnClickListener(sendSmsListener);
        scheduleImageBtn.setOnClickListener(scheduleSmsListener);
        deleteImageBtn.setOnClickListener(deleteSmsListener);
        smsBodyEditText.addTextChangedListener(smsBodyTextWatcher);

        updateSmsCounter(0, 0, 0);

        _actionBar = getSupportActionBar();
        _fragmentManager = getSupportFragmentManager();

        _actionBar.setHomeButtonEnabled(true);
        _actionBar.setDisplayHomeAsUpEnabled(true);

        itemId = getIntent().getExtras().getString("itemId");
        contact = getIntent().getExtras().getString("contact");
        SMS sms = _smsUtils.getSMS_byId(mActivity, itemId);
        if (sms!=null){
            _sms = new SMS();
            _sms.setNumbers(sms.getNumbers());
        }

        _actionBar.setTitle(contact);

        setSmsFragment();
    }
    private void setSmsFragment(){
        _fragmentManager.beginTransaction()
                .replace(R.id.sms_conversation_frame, new ConversationSms_Fragment())
                .commit();
    }
    private View.OnClickListener sendSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (_sms!=null){
                _sms.setSmsBody(smsBodyEditText.getText().toString());
                _smsUtils.sendingSMS(mActivity, _sms);
                smsBodyEditText.setText("");
                hideComposingArea();
                setSmsFragment();
            }else
                _notificationUtils.showShortToast("Phone number or sms content has problem, please try again.");
            sendImageBtn.setClickable(false);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendImageBtn.setClickable(true);
                }
            }, 5000);//Message will be delivered in 5 second.
        }
    };
    private View.OnClickListener scheduleSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideComposingArea();
        }
    };
    private View.OnClickListener deleteSmsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            smsBodyEditText.setText("");
        }
    };
    private TextWatcher smsBodyTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            int[] smsLength = SmsMessage.calculateLength(s.toString(), false);
            //Log.i(TAG, smsLength.toString());
            updateSmsCounter(smsLength[0],smsLength[1],smsLength[2]);
        }
    };
    private void updateSmsCounter(int numberSms, int usedCount, int remainCount){
        countSmsTextView.setText(numberSms + " sms");
        countWordTextView.setText(usedCount +"/" + remainCount);
    }
    private void showComposingArea(){
        if (composeArea!=null)
            composeArea.setVisibility(View.VISIBLE);
    }
    private void hideComposingArea(){
        if (composeArea!=null)
            composeArea.setVisibility(View.GONE);
    }
    private boolean isComposeMsg(){
        if (composeArea!=null && composeArea.getVisibility()==View.VISIBLE)
            return true;
        return false;
    }
    /* Menu Setup */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sms, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        // boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        menu.findItem(R.id.composing_area_menu).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.composing_area_menu:
                if (isComposeMsg()) {
                    hideComposingArea();
                    if (adFragment!=null)
                        adFragment.setVisibility(View.VISIBLE);
                }
                else {
                    showComposingArea();
                    if (adFragment!=null)
                        adFragment.setVisibility(View.GONE);
                }
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
