package com.bibooki.mobile.sms_bibooki;

import android.os.Bundle;

public class TutorialActivity extends BaseMenuActivity {

    @Override
    public void onBackPressed(){
        finish();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.tutorial);
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
    @Override
    protected void onPause(){
        super.onPause();
    }
    @Override
    protected void onStop(){
        super.onStop();
    }
}
