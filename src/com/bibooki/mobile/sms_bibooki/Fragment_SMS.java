package com.bibooki.mobile.sms_bibooki;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android.widget.AbsListView.*;

import com.bibooki.mobile.sms_bibooki.rows.ListRowItem;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.rows.ListRowSeparator;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;

import java.util.*;

public class Fragment_SMS extends BaseFragment {

    static List<ListRowItem> listRowItems = new ArrayList<ListRowItem>();
    public static SmsListAdapter smsListAdapter;
    private static int countItem = 0;
    private static int offset = 0;
    ActionMode mActionMode;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putSerializable("listRowItems", (Serializable) listRowItems);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //newly created, compute data
        smsListAdapter = new SmsListAdapter(mContext);
        // update offset
        countItem = 0;
        updateStaticOffset(0);
        setListAdapter(smsListAdapter);
        new AddSMSItem().execute(item_loading_number);

        // BEGIN_INCLUDE (setup_refreshlistener)
        setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // END_INCLUDE (setup_refreshlistener)

        // setup actionbar for contextual menu delete
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setMultiChoiceModeListener(multiChoiceModeListener);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    private MultiChoiceModeListener multiChoiceModeListener = new MultiChoiceModeListener() {
        private int selectedItemNumber = 0;
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.cabselection_menu, menu);
            return true;
        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            StringBuilder sb = new StringBuilder();
            Set<Integer> positions = smsListAdapter.getCurrentCheckedPosition();
            for (Integer pos : positions) {
                sb.append(" " + pos + ",");
            }
            switch (item.getItemId()) {
                case R.id.delete_entry:
                    //_notificationUtils.showShortToast(getResources().getString(R.string.notification_cab_deleted) + sb.toString());
                    Dialog deleteOptionDialog = _sms_utils.getDeleteOptionDialog(mActivity, smsListAdapter.getSelectedEvent());
                    deleteOptionDialog.show();
                    deleteOptionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            refresh();
                        }
                    });
                    break;
                default:
                    selectedItemNumber = 0;
                    smsListAdapter.clearSelection();
                    mode.finish();
            }
            return false;
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            selectedItemNumber = 0;
            smsListAdapter.clearSelection();
        }
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            if (checked) {
                selectedItemNumber++;
                smsListAdapter.setNewSelection(position, checked);
            } else {
                selectedItemNumber--;
                smsListAdapter.removeSelection(position);
            }
            if (selectedItemNumber ==1)
                mode.setTitle(selectedItemNumber + " item selected");
            else if (selectedItemNumber > 1)
                mode.setTitle(selectedItemNumber + " items selected");
        }
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static class ViewHolder {
        View view;

        ImageView contact;
        TextView title;
        TextView content;
        ImageView sttImage1;
        TextView time;
        ListRowItemEvent listRowItemEvent;

        TextView textViewSeparator;
    }
    ViewHolder viewHolder = null;
    static Map<Integer, ViewHolder> viewHolderMap = new HashMap<Integer, ViewHolder>();

    public class SmsListAdapter extends ArrayAdapter<ListRowItem> {
        private LayoutInflater vi;
        public SmsListAdapter(Context context) {
            super(context, 0);
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewHolderMap.clear();
        }

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
        public List<ListRowItemEvent> getSelectedEvent(){
            List<ListRowItemEvent> selectedItem = new ArrayList<ListRowItemEvent>();
            for (int pos: mSelection.keySet()){
                selectedItem.add((ListRowItemEvent) getItem(pos));
            }
            return selectedItem;
        }
        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void clear(){
            clearStatic();
            clearSelection();
            super.clear();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final ListRowItem listRowItem = getItem(position);
            ListRowItem.ROW_ITEM_TYPES rowItemType = listRowItem.whatType();
            viewHolder = viewHolderMap.get(position);
            int selected_sms_calendar_id;
            ListRowSeparator separator;
            final ListRowItemEvent eventItem;
            switch (rowItemType){
                case SEPARATOR:
                    separator = (ListRowSeparator) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        // store the holder with the view.
                        viewHolder.view = vi.inflate(R.layout.row_separator, null);
                        viewHolder.textViewSeparator = (TextView) viewHolder.view.findViewById(R.id.textView_row_separator);
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    viewHolder.textViewSeparator.setText(separator.text);
                    // loading more items
                    if (separator.text.equals(mContext.getResources().getString(R.string.load_more_items))){
                        viewHolder.textViewSeparator.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                smsListAdapter.remove(listRowItem);
                                // add sms items into adapter
                                new AddSMSItem().execute(100);
                            }
                        });
                    }
                    viewHolder.textViewSeparator.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    break;
                case ITEM_EVENT:
                    eventItem = (ListRowItemEvent) listRowItem;
                    if (viewHolder==null){
                        viewHolder = new ViewHolder();
                        viewHolder.view = vi.inflate(R.layout.row_item_event, null);

                        viewHolder.contact = (ImageView) viewHolder.view.findViewById(R.id.sms_icon);
                        viewHolder.title = (TextView) viewHolder.view.findViewById(R.id.item_event_title);
                        viewHolder.content = (TextView) viewHolder.view.findViewById(R.id.item_event_content);
                        viewHolder.time = (TextView) viewHolder.view.findViewById(R.id.item_event_time);
                        viewHolder.sttImage1 = (ImageView) viewHolder.view.findViewById(R.id.item_event_sttImg1);

                        viewHolder.listRowItemEvent = eventItem;

                        // store the holder with the view.
                        viewHolderMap.put(position, viewHolder);
                    }
                    v = viewHolder.view;
                    // Set data for item
                    if (eventItem.contactId>0){
                        Bitmap bm = _contactsHelper.getContactDisplayImageByID(eventItem.contactId);
                        if (bm!=null)
                            viewHolder.contact.setImageBitmap(bm);
                    }
                    viewHolder.contact.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancelNotification();
                            Intent smsContentIntent = new Intent(mContext, ConversationSms_Activity.class);
                            smsContentIntent.putExtra("itemId", eventItem.itemId);
                            smsContentIntent.putExtra("contact", eventItem.contact);
                            startActivity(smsContentIntent);
                        }
                    });
                    viewHolder.title.setText(eventItem.title);
                    viewHolder.content.setText(eventItem.content);
                    viewHolder.time.setText(_timeHelper.getFormatedTime(eventItem.fromTime));

                    // set selected items to be green
                    if (mSelection.get(position) != null) {
                        v.setBackgroundColor(getResources().getColor(R.color.selected_item));// this is a selected position so make it red
                    }else{
                        if (eventItem.title.contains("(ME) ")){
                            v.setBackgroundColor(getResources().getColor(R.color.light_green));
                        }else
                            v.setBackgroundColor(Color.TRANSPARENT);// this is a selected position so make it red
                    }
                    if (!eventItem.isRead) {
                        viewHolder.title.setTypeface(null, Typeface.BOLD);
                        viewHolder.content.setTypeface(null, Typeface.BOLD);
                    }else{
                        viewHolder.title.setTypeface(null, Typeface.NORMAL);
                        viewHolder.content.setTypeface(null, Typeface.NORMAL);
                    }

                    // set status image button of calendar
                    if (eventItem.isCalendarTracked && eventItem.isCalendarUpdated)
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_ok);
                    else if (eventItem.isCalendarTracked && !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setImageResource(R.drawable.calendar_sync);
                    }
                    // setup on click event of calendar
                    if (_defaultItem==null || _defaultItem.getSmsCalendarId() < 1){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog calendarDialog = _sms_utils.pickingCalendarDialog(mContext, mActivity, null);
                                calendarDialog.show();
                                calendarDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        refresh();
                                    }
                                });
                            }
                        });
                    }
                    else if (!eventItem.isCalendarTracked || !eventItem.isCalendarUpdated){
                        viewHolder.sttImage1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(mContext, "Selected Tracked Icon", Toast.LENGTH_SHORT).show();
                                if (_trackingUtils.trackingSMS(mContext, eventItem.fromTime)) {
                                    ImageView checkIcon = (ImageView) v;
                                    checkIcon.setImageResource(R.drawable.calendar_ok);
                                    checkIcon.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                                    viewHolder.sttImage1 = checkIcon;
                                    showNotification(getResources().getString(R.string.notification_sync_stt),
                                            getResources().getInteger(R.integer.alert_duration_time));
                                }
                            }
                        });
                    }
                    break;
                default:
                    break;
            }
            return v;
        }
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        viewHolder = viewHolderMap.get(position);
        if (viewHolder != null){
            if(viewHolder.listRowItemEvent!=null) {
                showNotification(viewHolder.listRowItemEvent.content, 0);
                if (!viewHolder.listRowItemEvent.isRead) {
                    _sms_utils.setSmsRead(mContext, viewHolder.listRowItemEvent.itemId, true);
                    viewHolder.listRowItemEvent.isRead = true;
                    smsListAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private static class AddSMSItem extends AsyncTask<Integer, Void, Integer> {
        protected void onPreExecute(){
            setRefreshing(true);
        }
        protected Integer doInBackground(Integer... params) {
            List<SMS> smsList = new ArrayList<SMS>();
            String timeTitle;
            try {
                smsList = _sms_utils.getLimitSMS(mContext, params[0], offset);
                long smsFrom = _preferencesHelper.read_SMS_Time_From(mContext);
                long smsTo = _preferencesHelper.read_SMS_Time_To(mContext);
                int mode = _preferencesHelper.read_SMS_listMode(mContext);
                if (mode > 0){
                    smsList = _sms_utils.getUnprocessedSMS(mContext, smsList, smsFrom, smsTo, params[0], offset);
                }
                listRowItems.clear();
                if (smsList==null || smsList.size()==0){
                    return 0;
                }
                int countSmsItem = countItem;
                String title, subTitle;
                long currentSms_datetime, previousSms_datetime;
                // if list is not empty, continue fire items
                for (int i=0; i<smsList.size(); i++){
                    title = smsList.get(i).getContactNames();
                    subTitle = smsList.get(i).getSmsBody();

                    if (smsList.get(i).getType()==2){
                        title = "(ME) " + title;
                    }
                    title = countSmsItem+1 + ". " + title;
                    currentSms_datetime = smsList.get(i).getSmsTime();
                    if (i==0){
                        timeTitle = _timeHelper.getDateSttForThePast(currentSms_datetime);
                        listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    if (i>0){
                        previousSms_datetime = smsList.get(i-1).getSmsTime();
                        timeTitle = _timeHelper.processIfDifferentDays(currentSms_datetime, previousSms_datetime);
                        if (!timeTitle.equals("")){
                            listRowItems.add(new ListRowSeparator(timeTitle));
                        }
                    }
                    listRowItems.add(new ListRowItemEvent(
                            _contactsHelper.getContactIdByNumber(smsList.get(i).getNumbers()),
                            smsList.get(i).getContactNames(),
                            smsList.get(i).getSmsId(), smsList.get(i).getCal_event_id(), title, subTitle,
                            smsList.get(i).isCalendarTracked, smsList.get(i).isCalendarUpdated,
                            smsList.get(i).getSmsTime(), smsList.get(i).getSmsTime(), smsList.get(i).isRead()));
                    countSmsItem++;
                }
                countItem += smsList.size();
                // offset need to be updated (it'll be updated from Sms_utils)
            } catch (Exception e) {
                e.printStackTrace();
            }
            return smsList.size();
        }
        protected void onPostExecute(final Integer result) {
            mHandler.post(new Runnable() {
                public void run() {
                    if (result > 0){
                        listRowItems.add(new ListRowSeparator(mContext.getResources().getString(R.string.load_more_items)));
                        smsListAdapter.addAll(listRowItems);
                    }
                    smsListAdapter.notifyDataSetChanged();
                    onRefreshComplete();
                }
            });
        }
    }
    // this method is used for remote calling from search untracked item dialog
    public static void addMoreSmsItem(final Context context, final List<SMS> smsList){
        // return if list is empty
        if (smsList==null || smsList.size()==0){
            mHandler.post(new Runnable() {
                public void run() {
                    smsListAdapter.add(new ListRowSeparator(context.getString(R.string.all_sms_tracked_stt)));
                    smsListAdapter.notifyDataSetChanged();
                }
            });
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                listRowItems.clear();
                int countSmsItem = countItem;
                String title, subTitle, timeTitle;
                long currentSms_datetime, previousSms_datetime;
                // if list is not empty, continue fire items
                for (int i=0; i<smsList.size(); i++){
                    title = smsList.get(i).getContactNames();
                    subTitle = smsList.get(i).getSmsBody();

                    if (smsList.get(i).getType()==2){
                        title = " (ME)" + title;
                    }
                    title = countSmsItem + 1 + ". " + title;

                    currentSms_datetime = smsList.get(i).getSmsTime();
                    if (i==0){
                        timeTitle = _timeHelper.getDateSttForThePast(currentSms_datetime);
                        listRowItems.add(new ListRowSeparator(timeTitle));
                    }
                    if (i>0){
                        previousSms_datetime = smsList.get(i-1).getSmsTime();
                        timeTitle = _timeHelper.processIfDifferentDays(currentSms_datetime, previousSms_datetime);
                        if (!timeTitle.equals("")){
                            listRowItems.add(new ListRowSeparator(timeTitle));
                        }
                    }
                    listRowItems.add(new ListRowItemEvent(
                            _contactsHelper.getContactIdByNumber(smsList.get(i).getNumbers()),
                            smsList.get(i).getContactNames(),
                            smsList.get(i).getSmsId(), smsList.get(i).getCal_event_id(), title, subTitle,
                            smsList.get(i).isCalendarTracked, smsList.get(i).isCalendarUpdated,
                            smsList.get(i).getSmsTime(), smsList.get(i).getSmsTime(), smsList.get(i).isRead()));
                    countSmsItem++;
                }
                mHandler.post(new Runnable() {
                    public void run() {
                        listRowItems.add(new ListRowSeparator(context.getResources().getString(R.string.load_more_items)));
                        // update offset
                        countItem += smsList.size();
                        smsListAdapter.addAll(listRowItems);
                        smsListAdapter.notifyDataSetChanged();
                        // change the button to track all items
                        setupTrackAllBtn();
                    }
                });
            }
        }).start();
    }

    protected static void setupTrackAllBtn(){
        Log.v(TAG, "setup track all btn");
        final ImageView switchBtn = (ImageView) mActivity.findViewById(R.id.imageView_fragmentSwitch_btn);
        switchBtn.setVisibility(View.GONE);
        final ImageView trackAllBtn = (ImageView) mActivity.findViewById(R.id.imageView_trackingBtn);
        trackAllBtn.setVisibility(View.VISIBLE);
        trackAllBtn.setImageResource(R.drawable.btn_check_on_selected);
        trackAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do something that takes a while
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        mHandler.post(new Runnable() { // This thread runs in the UI
                            @Override
                            public void run() {
                                List<Long> timeList = new ArrayList<Long>();
                                for (int i=0; i<smsListAdapter.getCount(); i++){
                                    ListRowItem listRowItem = smsListAdapter.getItem(i);
                                    if (listRowItem.whatType()== ListRowItem.ROW_ITEM_TYPES.ITEM_EVENT){
                                        ListRowItemEvent listRowItemEvent = (ListRowItemEvent) listRowItem;
                                        timeList.add(listRowItemEvent.fromTime);
                                        smsListAdapter.remove(listRowItemEvent);
                                        i--;
                                    }
                                }
                                // update offset
                                countItem -= timeList.size();
                                if (timeList.size()>0){
                                    _trackingUtils.batchTrackingSMS(mContext, timeList);
                                }
                                // continue process
                                viewHolderMap.clear();
                                smsListAdapter.notifyDataSetChanged();
                                // reset the track all button
                                trackAllBtn.setVisibility(View.GONE);
                                switchBtn.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                };
                new Thread(runnable).start();
            }
        });
    }
    @Override
    public void refresh(){
        clearStatic();
        // super refresh will set fragment from MainActivity again
        super.refresh();
    }
    public void clearStatic(){
        listRowItems.clear();
        countItem = 0;
        updateStaticOffset(0);
        viewHolderMap.clear();
        if (mActionMode!=null)
            mActionMode.finish();
    }
    public static void updateStaticOffset(int offset){
        Fragment_SMS.offset = offset;
    }
}
