package com.bibooki.mobile.sms_bibooki.utils.state;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ResumeState {
    static ResumeState _resumeState = null;
    //private UtilsFactory _utilsFactory = UtilsFactory.getInstance();
    DateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

    private ResumeState(){
    }
    // return the instance
    public static ResumeState getInstance(){
        if (_resumeState == null)
            _resumeState = new ResumeState();
        return _resumeState;
    }
    // Set status, display text of calendar in main view. There are SMS Calendar CheckedTextView and Call Log Calendar CheckedTextView
                      /*
    public void setResume_CalendarState(Context context, Activity activity){
        final CheckedTextView sms_ck = (CheckedTextView) activity.findViewById(R.id.checkedTextView_smsCalendar);
        final CheckedTextView sms_schedule_ck = (CheckedTextView) activity.findViewById(R.id.checkedTextView_smsScheduledCalendar);
        final CheckedTextView call_ck = (CheckedTextView) activity.findViewById(R.id.checkedTextView_callLogCalendar);
        // first, get calendar id for sms from shared preference storage
        int selected_sms_calendarId = _utilsFactory.getPreferencesHelper().read_SMS_CalendarID(context);
        int selected_scheduled_sms_calendarId = _utilsFactory.getPreferencesHelper().read_scheduled_SMS_CalendarID(context);
        // second, find the name of calendar
        String selected_sms_calendarName = _utilsFactory.getCalendarUtils().getCalendarNameById(activity, selected_sms_calendarId);
        String selected_scheduled_sms_calendarName = _utilsFactory.getCalendarUtils().getCalendarNameById(activity, selected_scheduled_sms_calendarId);
        // set up checked text view of SMS
        if (selected_sms_calendarId != -1){
            // build a new calendar name in format "id. calendarName" for displaying
            selected_sms_calendarName = selected_sms_calendarId + ". " + selected_sms_calendarName;
            sms_ck.setText(context.getResources().getString(R.string.calendar_sms_ck_text_view_on) + " " + selected_sms_calendarName);
            sms_ck.setSelected(true);
            sms_ck.setCheckMarkDrawable(R.drawable.btn_check_on);
        }
        if (selected_scheduled_sms_calendarId != -1){
            selected_scheduled_sms_calendarName = selected_scheduled_sms_calendarId + ". " + selected_scheduled_sms_calendarName;
            sms_schedule_ck.setText(context.getResources().getString(R.string.calendar_scheduled_SMS_ck_text_view_on) + " " + selected_scheduled_sms_calendarName);
            sms_schedule_ck.setSelected(true);
            sms_schedule_ck.setCheckMarkDrawable(R.drawable.btn_check_on);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // first, get calendar id for call log from shared preference storage
        int selected_call_calendarId = _utilsFactory.getPreferencesHelper().read_CallLog_CalendarID(context);
        // second, find the name of calendar
        String selected_call_calendarName = _utilsFactory.getCalendarUtils().getCalendarNameById(activity, selected_call_calendarId);
        // set up checked text view of Call Log
        if (selected_call_calendarId != -1){
            // build a new calendar name in format "id. calendarName" for displaying
            selected_call_calendarName = selected_call_calendarId + ". " + selected_call_calendarName;
            call_ck.setText(context.getResources().getString(R.string.calendar_call_log_ck_text_view_on) + " " + selected_call_calendarName);
            call_ck.setSelected(true);
            call_ck.setCheckMarkDrawable(R.drawable.btn_check_on);
        }
    }
    // Set status, display text of tracking status in main view. There are SMS Tracking Status Textview and Call Log Tracking Status Textview
    public void setResume_TrackingState(Context context, Activity activity){
        String timeString = "NOT YET";
        final TextView sms_tracking_stt = (TextView) activity.findViewById(R.id.textView_smsTrackingStt);
        final TextView call_tracking_stt = (TextView) activity.findViewById(R.id.textView_callTrackingStt);
        // first, get the last time sms was tracked
        long lastTime_Tracking = _utilsFactory.getPreferencesHelper().read_Last_SMS_Auto_Tracking_Time(context);
        // second, if the time is valid, convert it to Date time format
        if (lastTime_Tracking != -1){
            // build a new calendar name in format "id. calendarName" for displaying
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(lastTime_Tracking);
            timeString = formatter.format(calendar.getTime());
        }
        sms_tracking_stt.setText(context.getResources().getString(R.string.sms_text_view_stt) + " " + timeString);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        timeString = "NOT YET";
        // first, get the last time call log was tracked
        lastTime_Tracking = _utilsFactory.getPreferencesHelper().read_Last_CALL_Auto_Tracking_Time(context);
        // second, if the time is valid, convert it to Date time format
        if (lastTime_Tracking != -1){
            // build a new calendar name in format "id. calendarName" for displaying
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(lastTime_Tracking);
            timeString = formatter.format(calendar.getTime());
        }
        call_tracking_stt.setText(context.getResources().getString(R.string.call_text_view_stt) + " " + timeString);
    }
    // Set status, display text of tracking status in main view. There are SMS Tracking Status Textview and Call Log Tracking Status Textview
    public void setResume_ScheduledState(Context context, Activity activity){
        String timeString = "NOT YET";
        final TextView sms_scheduled_stt = (TextView) activity.findViewById(R.id.textView_scheduledSMS_status);
        // first, get the last time sms was tracked
        long lastTime_Tracking = _utilsFactory.getPreferencesHelper().read_Last_SMS_Scheduled_Time(context);
        // second, if the time is valid, convert it to Date time format
        if (lastTime_Tracking != -1){
            // build a new calendar name in format "id. calendarName" for displaying
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(lastTime_Tracking);
            timeString = formatter.format(calendar.getTime());
        }
        sms_scheduled_stt.setText(context.getResources().getString(R.string.sms_scheduled_text_view_stt) + " " + timeString);
    }      */
    /*
    Set status, display text of auto tracking status in main view
     */
      /*
    public void setResume_AutoTrackingState(Context context, Activity activity){
        CheckedTextView auto_tracking_ckTextView = (CheckedTextView) activity.findViewById(R.id.checkedTextView_autoTracking);
        TextView autoTrackingStt_textView = (TextView) activity.findViewById(R.id.textView_autoTracking_stt);
        // first, get the last time sms was tracked
        String[] autoTimeString = _utilsFactory.getPreferencesHelper().read_Auto_Tracking_Time(context);
        // if the configuration had set
        if (autoTimeString != null){
            String type = autoTimeString[0];
            long time_Tracking = Long.parseLong(autoTimeString[1]);
            long hour = TimeUnit.MILLISECONDS.toHours(time_Tracking);
            long minute = TimeUnit.MILLISECONDS.toMinutes(time_Tracking) - TimeUnit.HOURS.toMinutes(hour);
            // second, if the time is valid, convert it to Date time format
            auto_tracking_ckTextView.setSelected(true);
            auto_tracking_ckTextView.setCheckMarkDrawable(R.drawable.btn_check_on);
            String time = hour +" hours " + minute + " mins";
            String stt = context.getResources().getString(R.string.auto_tracking_stt_on);
            if (type==null || type.equals(""))  {
                stt = stt + " " + time;
                // set time to alarm auto tracking
                //_utilsFactory.getSchedule_Utils().startRepeatingTimer_Sms(context);
            }
            else if (type.equals(PreferencesHelper._TRACKING_TIME_TYPE_SMS))
                stt = stt + " SMS Event happened " +time;
            else if (type.equals(PreferencesHelper._TRACKING_TIME_TYPE_CALL))
                stt = stt + " CALL Event happened " +time;
            else if (type.equals(PreferencesHelper._TRACKING_TIME_TYPE_BOTH))
                stt = stt + " SMS/CALL Event happened " +time;
            autoTrackingStt_textView.setText(stt);
        }
        else {
            auto_tracking_ckTextView.setSelected(false);
            auto_tracking_ckTextView.setCheckMarkDrawable(R.drawable.btn_check_off);
            autoTrackingStt_textView.setText(context.getResources().getString(R.string.auto_tracking_stt));
        }
    }        */
    /*
    Resume Alarm Auto Tracking after the Phone Boot Completed
     */
    /*
    // Set status, display text of calendar in main view. There are SMS Calendar CheckedTextView and Call Log Calendar CheckedTextView
    public void setResume_SMS_EventTitleState(Context context, Activity activity){
        final CheckedTextView smsTitle_ck = (CheckedTextView) activity.findViewById(R.id.checkedTextView_eventTitle);
        // first, get calendar id for sms from shared preference storage
        int sms_eventTitle_type = _utilsFactory.getPreferencesHelper().read_SMS_EventTitle(context);
        String ck_text = activity.getResources().getString(R.string.eventTitle_sms_ck_text_view);
        // set up checked text view of SMS
        smsTitle_ck.setSelected(true);
        smsTitle_ck.setCheckMarkDrawable(R.drawable.btn_check_on);
        switch (sms_eventTitle_type){
            case -1:
                // build a new calendar name in format "id. calendarName" for displaying
                smsTitle_ck.setText(ck_text + " (none)");
                break;
            case 0:
                smsTitle_ck.setText(ck_text + " (13 characters)");
                break;
            default:
                smsTitle_ck.setText(ck_text + " (all)");
                break;
        }
    }
    // Set status, display text of calendar in main view. There are SMS Calendar CheckedTextView and Call Log Calendar CheckedTextView
    public void setResume_CALL_EventTitleState(Context context, Activity activity){
        final CheckedTextView callTitle_ck = (CheckedTextView) activity.findViewById(R.id.checkedTextView_callTitle);
        // first, get calendar id for sms from shared preference storage
        int call_eventTitle_type = _utilsFactory.getPreferencesHelper().read_CALL_EventTitle(context);
        // set up checked text view of SMS
        callTitle_ck.setSelected(true);
        switch (call_eventTitle_type){
            case -1:
                callTitle_ck.setCheckMarkDrawable(R.drawable.btn_check_off);
                break;
            default:
                callTitle_ck.setCheckMarkDrawable(R.drawable.btn_check_on);
                break;
        }
    }
    // Set status, display text of Tracking Plan State
    public void setResume_TrackingPlan_State(Context context, Activity activity){
        final CheckedTextView ck_planState = (CheckedTextView) activity.findViewById(R.id.checkedTextView_trackingPlan);
        // first, get state value shared preference storage
        int tracking_planState_value = _utilsFactory.getPreferencesHelper().read_Tracking_Plan(context);
        // set up plan status
        ck_planState.setSelected(true);
        switch (tracking_planState_value){
            case PreferencesHelper._TRACKING_PLAN_INCREMENTAL:
                ck_planState.setCheckMarkDrawable(R.drawable.btn_check_off);
                ck_planState.setText(activity.getResources().getString(R.string.tracking_plan_ck_textView_incremental));
                break;
            default:
                ck_planState.setCheckMarkDrawable(R.drawable.btn_check_on);
                ck_planState.setText(activity.getResources().getString(R.string.tracking_plan_ck_textView_default));
                break;
        }
    }          */
}
