package com.bibooki.mobile.sms_bibooki.utils;

import android.content.Context;
import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.AutoTracking_Utils;
import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.Schedule_Utils;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;
import com.bibooki.mobile.sms_bibooki.utils.filter.Filter_Utils;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;
import com.bibooki.mobile.sms_bibooki.utils.call_log.Call_Log_Utils;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.Scheduled_SMS_Utils;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;
import com.bibooki.mobile.sms_bibooki.utils.state.ResumeState;

public class UtilsFactory {
    ResumeState resumeState = null;

    private static Context mContext;
    static UtilsFactory utilsFactory = null;

    private UtilsFactory(){

    }
    // return new instance
    public static UtilsFactory newInstance(Context context){
        if (utilsFactory == null)
            utilsFactory = new UtilsFactory();
        mContext = context;
        return utilsFactory;
    }

    // create new instance of Calendar_Utils
    public Calendar_Utils getCalendarUtils(){
        return Calendar_Utils.getInstance(mContext);
    }
    // create new instance of smsUtils
    public SMS_Utils getSmsUtils(){
        return SMS_Utils.getInstance(mContext);
    }
    // create new instance of callLogUtils
    public Call_Log_Utils getCallLogUtils(){
        return Call_Log_Utils.newInstance(mContext);
    }
    // create new instance of PreferencesHelper
    public PreferencesHelper getPreferencesHelper(){
        return PreferencesHelper.newInstance();
    }
    // create new instance of Advanced Setting Preference
    public AdvancedSettingPreference getAdvancedSettingPreference(){
        return AdvancedSettingPreference.getInstance(mContext);
    }
    // create new instance of Schedule_Utils
    public Schedule_Utils getSchedule_Utils(){
        return Schedule_Utils.getInstance(mContext);
    }
    // create new instance of Scheduled SMS Utils
    public Scheduled_SMS_Utils getScheduled_SMS_Utils(){
        return Scheduled_SMS_Utils.getInstance(mContext);
    }
    // create new instance of Dialog_Utils
    public Dialog_Utils getDialog_Utils(){
        return Dialog_Utils.getInstance(mContext);
    }
    // create new instance of Tracking_Utils
    public Tracking_Utils getTracking_Utils(){
        return Tracking_Utils.getInstance(mContext);
    }
    // create new instance of AutoTracking_Utils
    public AutoTracking_Utils getAutoTracking_utils(){
        return AutoTracking_Utils.getInstance(mContext);
    }
    // create new instance of Restore_Utils
    public Restore_Utils getRestore_Utils(){
        return Restore_Utils.getInstance(mContext);
    }
    // create new instance of AutoTracking_Utils
    public NotificationUtils getNotificationUtils(){
        return NotificationUtils.getInstance(mContext);
    }
    // create new instance of TrackingFilter utils
    public Filter_Utils getFilterUtils(){
        return Filter_Utils.getInstance(mContext);
    }
    /*
    // create new instance of ResumeState
    public ResumeState getResumeState(){
        return ResumeState.getInstance();
    } */
}
