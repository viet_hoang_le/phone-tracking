package com.bibooki.mobile.sms_bibooki.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Tracking_Utils {

    static int LIMIT_AUTO_TRACKING = 100;

    static Tracking_Utils _tracking_Utils = null;
    private static UtilsFactory _utilsFactory;
    static ContactsHelper _contactsHelper;
    private static NotificationUtils _notificationUtils;
    private static AdvancedSettingPreference _advancedSettingPreference;
    private Format formatter = new SimpleDateFormat("hh:mm:ss a");

    static DBTrackCalendarOpenHelper _dbTrackCalendarOpenHelper = null;


    private ProgressDialog progressDialog;
    private ProgressDialog mprogressDialog;
    public static int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    private List<Call_Log> call_logList = null;
    private int selected_call_log_calendar_id;
    private long lastTime = -1;
    private int checkingPlan;

    private List<SMS> smsList = null;
    private int selected_sms_calendar_id;

    private static TrackCalendarDbItem _defaultItem;

    static Context mContext;
    // return new instance
    public static Tracking_Utils getInstance(Context context) {
        if (_tracking_Utils == null)
            _tracking_Utils = new Tracking_Utils();
        if (_dbTrackCalendarOpenHelper == null)
            _dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(context);
        _defaultItem = _dbTrackCalendarOpenHelper.selectDefaultRecord();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _contactsHelper = ContactsHelper.getInstance(mContext);
        _advancedSettingPreference = _utilsFactory.getAdvancedSettingPreference();
        _notificationUtils = UtilsFactory.newInstance(mContext).getNotificationUtils();
        return _tracking_Utils;
    }

    // tracking or updating SMS
    public boolean trackingSMS(Context context, long smsTime) {
        List<Long> timeList = new ArrayList<Long>();
        timeList.add(smsTime);
        List<SMS> smsList = _utilsFactory.getSmsUtils().get_SMS_At_Time(context, timeList);
        SMS sms;
        if (smsList.size()<1)
            return false;
        sms = smsList.get(0);
        List<Long> startTimeList = new ArrayList<Long>();
        /*
        Checking Calendar
        if valid, process tracking sms into calendar
        * */
        // get Filter
        List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
        TrackingFilter trackingFilter;
        List<String> filterPhoneNumbers = new ArrayList<String>();
        for (int i=0; i<trackingFilterList.size(); i++)
            filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
        // filter
        int selected_sms_calendar_id = -1;
        if (_defaultItem !=null)
            selected_sms_calendar_id = _defaultItem.getSmsCalendarId();
        String purePhoneNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
        if (purePhoneNumber==null || purePhoneNumber.equals(""))
            purePhoneNumber = sms.getNumbers();
        int index = filterPhoneNumbers.indexOf(purePhoneNumber);
        if (index>-1){
            trackingFilter = trackingFilterList.get(index);
            selected_sms_calendar_id = trackingFilter.getSmsCalendarId();
        }
        // finish get Filter
        if (selected_sms_calendar_id != -1) {
            startTimeList.add((sms.getSmsTime() / 1000) * 1000);
            Map<String, SMS> sms_hashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, startTimeList, startTimeList, selected_sms_calendar_id, context);

            List<SMS> createNewSMSList = new ArrayList<SMS>();
            List<SMS> updateSMSList = new ArrayList<SMS>();
            if (sms_hashMap.containsKey(sms.getFixedHashValue())) {
                SMS smsEvent = sms_hashMap.get(sms.getFixedHashValue());
                if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())) {
                    sms.isCalendarTracked = true;
                    sms.setCal_event_id(smsEvent.getCal_event_id());
                    if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                        sms.setOwnerNumber(smsEvent.getOwnerNumber());
                        sms.regenerateDescription();
                    }
                    updateSMSList.add(sms);
                    _utilsFactory.getCalendarUtils().batch_Update_SMS_Event(context, selected_sms_calendar_id, updateSMSList);
                    return true;
                }
            } else {
                createNewSMSList.add(sms);
                _utilsFactory.getCalendarUtils().batch_Create_SMS_Event(context, selected_sms_calendar_id, createNewSMSList);
                return true;
            }
        }
        return false;
    }
    // tracking or updating SMS
    public void batchTrackingSMS(Context context, List<Long> smsTimeList) {
        try{
            List<SMS> smsList = _utilsFactory.getSmsUtils().get_SMS_At_Time(context, smsTimeList);
            List<Long> smsFilterTimeList = new ArrayList<Long>();
            /*
            Checking Calendar
            if valid, process tracking sms into calendar
            * */
            // Toast.makeText(context, "Tracking SMS", Toast.LENGTH_SHORT).show();
            int selected_sms_calendar_id = -1;
            if (_defaultItem!=null)
                selected_sms_calendar_id = _defaultItem.getSmsCalendarId();
            // get Filter
            List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
            TrackingFilter trackingFilter;
            List<String> filterPhoneNumbers = new ArrayList<String>();
            List<Integer> filterSMSIndex = new ArrayList<Integer>();
            for (int i=0; i<trackingFilterList.size(); i++)
                filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
            // finish get Filter
            String smsNumber;
            for (SMS sms : smsList){
                // loading sms filter
                smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
                if (smsNumber==null || smsNumber.equals(""))
                    smsNumber = sms.getNumbers();
                int index = filterPhoneNumbers.indexOf(smsNumber);
                if (index>-1){
                    filterSMSIndex.add(smsList.indexOf(sms));
                    smsFilterTimeList.add((sms.getSmsTime() / 1000) * 1000);
                }
                // finish loading sms filter
            }
            // start tracking sms
            SMS sms, smsEvent;
            List<SMS> createNewSMSList = new ArrayList<SMS>();
            List<SMS> updateSMSList = new ArrayList<SMS>();
            if (selected_sms_calendar_id != -1) {
                Map<String, SMS> sms_hashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, smsTimeList, smsTimeList, selected_sms_calendar_id, context);
                if (smsList != null) {
                    for (int i = 0; i < smsList.size(); i++) {
                        sms = smsList.get(i);
                        // ignore filtered sms
                        if (filterSMSIndex.contains(i)){
                            continue;
                        }
                        // finish ignore filtered sms
                        if (sms_hashMap.containsKey(sms.getFixedHashValue())) {
                            smsEvent = sms_hashMap.get(sms.getFixedHashValue());
                            sms.isCalendarTracked = true;
                            if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())) {
                                sms.isCalendarUpdated = false;
                                sms.isCalendarTracked = true;
                                sms.setCal_event_id(smsEvent.getCal_event_id());
                                if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                    sms.setOwnerNumber(smsEvent.getOwnerNumber());
                                    sms.regenerateDescription();
                                }
                                updateSMSList.add(sms);
                            }
                        } else {
                            sms.isCalendarTracked = false;
                            createNewSMSList.add(sms);
                        }
                    }
                    _utilsFactory.getCalendarUtils().batch_Update_SMS_Event(context, selected_sms_calendar_id, updateSMSList);
                    _utilsFactory.getCalendarUtils().batch_Create_SMS_Event(context, selected_sms_calendar_id, createNewSMSList);
                }
            }
            // start tracking filter sms
            String filterNumber, smsCountry, filterCountry;
            if (filterSMSIndex.size()>0){
                for (int i=0; i<trackingFilterList.size(); i++){
                    createNewSMSList.clear();
                    updateSMSList.clear();
                    trackingFilter = trackingFilterList.get(i);
                    selected_sms_calendar_id = trackingFilter.getSmsCalendarId();
                    if (selected_sms_calendar_id < 1)
                        continue;
                    Map<String, SMS> smsFilterHashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, smsFilterTimeList, smsFilterTimeList, selected_sms_calendar_id, context);
                    int smsIndex;
                    for (int j=0; j< filterSMSIndex.size(); j++){
                        smsIndex = filterSMSIndex.get(j);
                        sms = smsList.get(smsIndex);
                        smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
                        if (smsNumber==null || smsNumber.equals(""))
                            smsNumber = sms.getNumbers();
                        //smsCountry = _contactsHelper.getCountryCode(sms.getNumbers());
                        filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                        //filterCountry = _contactsHelper.getCountryCode(trackingFilter.getPhonenumber());
                        if (!smsNumber.equals(filterNumber)){
                            continue;
                        }
                        if (smsFilterHashMap.containsKey(sms.getFixedHashValue())) {
                            smsEvent = smsFilterHashMap.get(sms.getFixedHashValue());
                            sms.isCalendarTracked = true;
                            if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())) {
                                sms.isCalendarUpdated = false;
                                sms.isCalendarTracked = true;
                                sms.setCal_event_id(smsEvent.getCal_event_id());
                                if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                    sms.setOwnerNumber(smsEvent.getOwnerNumber());
                                    sms.regenerateDescription();
                                }
                                updateSMSList.add(sms);
                            }
                        } else {
                            sms.isCalendarTracked = false;
                            createNewSMSList.add(sms);
                        }
                    }
                    _utilsFactory.getCalendarUtils().batch_Update_SMS_Event(context, selected_sms_calendar_id, updateSMSList);
                    _utilsFactory.getCalendarUtils().batch_Create_SMS_Event(context, selected_sms_calendar_id, createNewSMSList);
                }
            }
            _utilsFactory.getPreferencesHelper().store_Last_SMS_Tracking_Time(context, new Date().getTime());
            _utilsFactory.getPreferencesHelper().store_Last_SMS_Auto_Tracking_Time(context, new Date().getTime());
            // notification
            _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_smsTracking_title), formatter.format(new Date()));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // tracking SMS for ALARM
    public void trackingSMS(Context context, boolean isAlarm) {
        // checking Tracking Plan is ALL or INCREMENTAL
        long lastTime = -1;
        List<Long> smsTimeList = new ArrayList<Long>();
        List<Long> smsFilterTimeList = new ArrayList<Long>();
        // check if Checking Plan is Incremental
        if (isAlarm == true)
            lastTime = _utilsFactory.getPreferencesHelper().read_Last_SMS_Auto_Tracking_Time(context);
        /*
        Checking Calendar
        if valid, process tracking sms into calendar
        * */
        // Toast.makeText(context, "Tracking SMS", Toast.LENGTH_SHORT).show();
        int selected_sms_calendar_id = -1;
        if (_defaultItem!=null)
            selected_sms_calendar_id = _defaultItem.getSmsCalendarId();
        // get Filter
        List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
        TrackingFilter trackingFilter;
        List<String> filterPhoneNumbers = new ArrayList<String>();
        List<Integer> filterSMSIndex = new ArrayList<Integer>();
        for (int i=0; i<trackingFilterList.size(); i++)
            filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
        // finish get Filter
        // loading sms
        List<SMS> smsList = _utilsFactory.getSmsUtils().getSMS_from_to(context, lastTime, null, LIMIT_AUTO_TRACKING, 0);
        String smsNumber;
        for (SMS sms : smsList){
            smsTimeList.add((sms.getSmsTime() / 1000) * 1000);
            // loading sms filter
            smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
            if (smsNumber==null || smsNumber.equals(""))
                smsNumber = sms.getNumbers();
            int index = filterPhoneNumbers.indexOf(smsNumber);
            if (index>-1){
                filterSMSIndex.add(smsList.indexOf(sms));
                smsFilterTimeList.add((sms.getSmsTime() / 1000) * 1000);
            }
            // finish loading sms filter
        }
        // finish loading sms
        // start tracking sms
        SMS sms, smsEvent;
        List<SMS> createNewSMSList = new ArrayList<SMS>();
        List<SMS> updateSMSList = new ArrayList<SMS>();
        if (selected_sms_calendar_id != -1) {
            Map<String, SMS> sms_hashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, smsTimeList, smsTimeList, selected_sms_calendar_id, context);
            if (smsList != null) {
                for (int i = 0; i < smsList.size(); i++) {
                    sms = smsList.get(i);
                    // ignore filtered sms
                    if (filterSMSIndex.contains(i)){
                        continue;
                    }
                    // finish ignore filtered sms
                    if (sms_hashMap.containsKey(sms.getFixedHashValue())) {
                        smsEvent = sms_hashMap.get(sms.getFixedHashValue());
                        sms.isCalendarTracked = true;
                        if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())) {
                            sms.isCalendarUpdated = false;
                            sms.setCal_event_id(smsEvent.getCal_event_id());
                            if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                sms.setOwnerNumber(smsEvent.getOwnerNumber());
                                sms.regenerateDescription();
                            }
                            updateSMSList.add(sms);
                        }
                    } else {
                        sms.isCalendarTracked = false;
                        createNewSMSList.add(sms);
                    }
                }
                _utilsFactory.getCalendarUtils().batch_Update_SMS_Event(context, selected_sms_calendar_id, updateSMSList);
                _utilsFactory.getCalendarUtils().batch_Create_SMS_Event(context, selected_sms_calendar_id, createNewSMSList);
            }
        }
        // start tracking filter sms
        String filterNumber, smsCountry, filterCountry;
        if (filterSMSIndex.size()>0){
            for (int i=0; i<trackingFilterList.size(); i++){
                createNewSMSList.clear();
                updateSMSList.clear();
                trackingFilter = trackingFilterList.get(i);
                selected_sms_calendar_id = trackingFilter.getSmsCalendarId();
                if (selected_sms_calendar_id < 1)
                    continue;
                Map<String, SMS> smsFilterHashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, smsFilterTimeList, smsFilterTimeList, selected_sms_calendar_id, context);
                int smsIndex;
                for (int j=0; j< filterSMSIndex.size(); j++){
                    smsIndex = filterSMSIndex.get(j);
                    sms = smsList.get(smsIndex);
                    smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
                    if (smsNumber==null || smsNumber.equals(""))
                        smsNumber = sms.getNumbers();
                    //smsCountry = _contactsHelper.getCountryCode(sms.getNumbers());
                    filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                    //filterCountry = _contactsHelper.getCountryCode(trackingFilter.getPhonenumber());
                    if (!smsNumber.equals(filterNumber)){
                        continue;
                    }
                    if (smsFilterHashMap.containsKey(sms.getFixedHashValue())) {
                        smsEvent = smsFilterHashMap.get(sms.getFixedHashValue());
                        sms.isCalendarTracked = true;
                        if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())) {
                            sms.isCalendarUpdated = false;
                            sms.setCal_event_id(smsEvent.getCal_event_id());
                            if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                sms.setOwnerNumber(smsEvent.getOwnerNumber());
                                sms.regenerateDescription();
                            }
                            updateSMSList.add(sms);
                        }
                    } else {
                        sms.isCalendarTracked = false;
                        createNewSMSList.add(sms);
                    }
                }
                _utilsFactory.getCalendarUtils().batch_Update_SMS_Event(context, selected_sms_calendar_id, updateSMSList);
                _utilsFactory.getCalendarUtils().batch_Create_SMS_Event(context, selected_sms_calendar_id, createNewSMSList);
            }
        }
        _utilsFactory.getPreferencesHelper().store_Last_SMS_Tracking_Time(context, new Date().getTime());
        _utilsFactory.getPreferencesHelper().store_Last_SMS_Auto_Tracking_Time(context, new Date().getTime());
        // notification
        _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_smsTracking_title), formatter.format(new Date()));
    }

    // tracking or updating Call Log
    public boolean trackingCallLog(Context context, long fromTime) {
        List<Long> timeList = new ArrayList<Long>();
        timeList.add(fromTime);
        List<Call_Log> callLogList = _utilsFactory.getCallLogUtils().get_Call_Log_At_Time(context, timeList);
        if (callLogList.size()<1)
            return false;
        Call_Log callLog = callLogList.get(0);
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        /*
        Checking Calendar
        if valid, process tracking call log into calendar
        * */
        // get Filter
        List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
        TrackingFilter trackingFilter;
        List<String> filterPhoneNumbers = new ArrayList<String>();
        for (int i=0; i<trackingFilterList.size(); i++)
            filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
        // filter
        int selected_call_log_calendar_id = -1;
        if (_defaultItem!=null)
            selected_call_log_calendar_id = _defaultItem.getCallCalendarId();
        String purePhoneNumber = _contactsHelper.removePhoneNumberPrefix(callLog.getNumbers());
        if (purePhoneNumber==null || purePhoneNumber.equals(""))
            purePhoneNumber = callLog.getNumbers();
        int index = filterPhoneNumbers.indexOf(purePhoneNumber);
        if (index>-1){
            trackingFilter = trackingFilterList.get(index);
            selected_call_log_calendar_id = trackingFilter.getCallCalendarId();
        }
        // finish get Filter
        if (selected_call_log_calendar_id != -1) {
            startTimeList.add((callLog.getFromTime() / 1000) * 1000);
            endTimeList.add(((callLog.getFromTime() + callLog.getDurationMiliSecond()) / 1000) * 1000);
            Map<String, Call_Log> callLog_hashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeList, endTimeList, selected_call_log_calendar_id, context);

            // get call log from calendar event
            Call_Log call_logEvent;
            List<Call_Log> createNewCall_LogList = new ArrayList<Call_Log>();
            List<Call_Log> updateCall_LogList = new ArrayList<Call_Log>();
            // update the existing call log event
            if (callLog_hashMap.containsKey(callLog.getFixedHashValue())) {
                call_logEvent = callLog_hashMap.get(callLog.getFixedHashValue());
                if (!call_logEvent.getCalendarTitleHash().equals(callLog.getCalendarTitleHash())) {
                    callLog.setCal_EventId(call_logEvent.getCal_EventId());
                    // in case users don't want to update the onwer phone number, skip it
                    if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                        callLog.setOwnerNumber(call_logEvent.getOwnerNumber());
                        callLog.regenerateDescription();
                    }
                    updateCall_LogList.add(callLog);
                    _utilsFactory.getCalendarUtils().batch_Update_CALL_LOG_Event(context, selected_call_log_calendar_id, updateCall_LogList);
                    return true;
                }
            }
            // or create new call log event
            else {
                createNewCall_LogList.add(callLog);
                _utilsFactory.getCalendarUtils().batch_Create_CALL_LOG_Event(context, selected_call_log_calendar_id, createNewCall_LogList);
                return true;
            }
        }
        return false;
    }
    // tracking or updating Call Log
    public void batchTrackingCallLog(Context context, List<Long> timeList) {
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        // for filter
        List<Long> startTimeFilterList = new ArrayList<Long>();
        List<Long> endTimeFilterList = new ArrayList<Long>();
        /*
        Checking Calendar
        if valid, process tracking call log into calendar
        * */
        //Toast.makeText(context, "Tracking Call Log", Toast.LENGTH_SHORT).show();
        int selected_call_log_calendar_id = -1;
        if (_defaultItem!=null)
            selected_call_log_calendar_id = _defaultItem.getCallCalendarId();
        // get Filter
        List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
        TrackingFilter trackingFilter;
        List<String> filterPhoneNumbers = new ArrayList<String>();
        List<Integer> filterCallIndex = new ArrayList<Integer>();
        for (int i=0; i<trackingFilterList.size(); i++)
            filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
        // finish get Filter

        // loading Call Log
        List<Call_Log> call_logList = _utilsFactory.getCallLogUtils().get_Call_Log_At_Time(context, timeList);
        String callNumber;
        for (Call_Log call_log : call_logList) {
            startTimeList.add((call_log.getFromTime() / 1000) * 1000);
            endTimeList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
            // loading Call Log filter
            callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
            if (callNumber==null || callNumber.equals(""))
                callNumber = call_log.getNumbers();
            int index = filterPhoneNumbers.indexOf(callNumber);
            if (index>-1){
                filterCallIndex.add(call_logList.indexOf(call_log));
                startTimeFilterList.add((call_log.getFromTime() / 1000) * 1000);
                endTimeFilterList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
            }
            // finish loading Call Log filter
        }
        // finish loading Call Log

        // start tracking Call Log
        Call_Log call_log, call_logEvent;
        List<Call_Log> createNewCall_LogList = new ArrayList<Call_Log>();
        List<Call_Log> updateCall_LogList = new ArrayList<Call_Log>();
        if (selected_call_log_calendar_id != -1) {
            Map<String, Call_Log> callLog_hashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeList, endTimeList, selected_call_log_calendar_id, context);
            if (call_logList != null) {
                for (int i = 0; i < call_logList.size(); i++) {
                    call_log = call_logList.get(i);
                    // ignore filtered sms
                    if (filterCallIndex.contains(i)){
                        continue;
                    }
                    // finish ignore filtered sms
                    if (callLog_hashMap.containsKey(call_log.getFixedHashValue())) {
                        call_logEvent = callLog_hashMap.get(call_log.getFixedHashValue());
                        if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                            call_log.setCal_EventId(call_logEvent.getCal_EventId());
                            if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                call_log.setOwnerNumber(call_logEvent.getOwnerNumber());
                                call_log.regenerateDescription();
                            }
                            updateCall_LogList.add(call_log);
                        }
                    } else {
                        createNewCall_LogList.add(call_log);
                    }
                }
                _utilsFactory.getCalendarUtils().batch_Update_CALL_LOG_Event(context, selected_call_log_calendar_id, updateCall_LogList);
                _utilsFactory.getCalendarUtils().batch_Create_CALL_LOG_Event(context, selected_call_log_calendar_id, createNewCall_LogList);
                /*
                _utilsFactory.getPreferencesHelper().store_Total_Tracking_CALL(context, call_logList.size());
                _utilsFactory.getPreferencesHelper().store_Updated_Tracking_CALL(context, updateCall_LogList.size());
                _utilsFactory.getPreferencesHelper().store_Processed_Tracking_CALL(context, createNewCall_LogList.size());   */
            }
        }
        // start tracking filter sms
        if (filterCallIndex.size()>0){
            String filterNumber, callCountry, filterCountry;
            for (int i=0; i<trackingFilterList.size(); i++){
                createNewCall_LogList.clear();
                updateCall_LogList.clear();
                trackingFilter = trackingFilterList.get(i);
                selected_call_log_calendar_id = trackingFilter.getCallCalendarId();
                if (selected_call_log_calendar_id < 1)
                    continue;
                Map<String, Call_Log> callEventFilterHashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeFilterList, endTimeFilterList, selected_call_log_calendar_id, context);

                int callIndex;
                for (int j=0; j< filterCallIndex.size(); j++){
                    callIndex = filterCallIndex.get(j);
                    call_log = call_logList.get(callIndex);
                    callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
                    if (callNumber==null || callNumber.equals(""))
                        callNumber = call_log.getNumbers();
                    //callCountry = _contactsHelper.getCountryCode(call_log.getNumbers());
                    filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                    //filterCountry = _contactsHelper.getCountryCode(trackingFilter.getPhonenumber());
                    if (!callNumber.equals(filterNumber)){
                        continue;
                    }
                    if (callEventFilterHashMap.containsKey(call_log.getFixedHashValue())) {
                        call_logEvent = callEventFilterHashMap.get(call_log.getFixedHashValue());
                        if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                            call_log.setCal_EventId(call_logEvent.getCal_EventId());
                            if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                call_log.setOwnerNumber(call_logEvent.getOwnerNumber());
                                call_log.regenerateDescription();
                            }
                            updateCall_LogList.add(call_log);
                        }
                    } else {
                        createNewCall_LogList.add(call_log);
                    }
                }
                _utilsFactory.getCalendarUtils().batch_Update_CALL_LOG_Event(context, selected_call_log_calendar_id, updateCall_LogList);
                _utilsFactory.getCalendarUtils().batch_Create_CALL_LOG_Event(context, selected_call_log_calendar_id, createNewCall_LogList);

            }
        }
        _utilsFactory.getPreferencesHelper().store_Last_CALL_Tracking_Time(context, new Date().getTime());
        _utilsFactory.getPreferencesHelper().store_Last_CALL_Auto_Tracking_Time(context, new Date().getTime());
        // notification
        _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_callLogTracking_title), formatter.format(new Date()));
    }

    // tracking Call Log for ALARM
    public void trackingCallLog(Context context, boolean isAlarm) {
        // checking Tracking Plan is ALL or INCREMENTAL
        long lastTime = -1;
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        try{
            // check if Checking Plan is Incremental
            if (isAlarm == true)
                lastTime = _utilsFactory.getPreferencesHelper().read_Last_CALL_Auto_Tracking_Time(context);
            // for filter
            List<Long> startTimeFilterList = new ArrayList<Long>();
            List<Long> endTimeFilterList = new ArrayList<Long>();
            /*
            Checking Calendar
            if valid, process tracking call log into calendar
            * */
            //Toast.makeText(context, "Tracking Call Log", Toast.LENGTH_SHORT).show();
            int selected_call_log_calendar_id = -1;
            if (_defaultItem!=null)
                selected_call_log_calendar_id = _defaultItem.getCallCalendarId();
            // get Filter
            List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
            TrackingFilter trackingFilter;
            List<String> filterPhoneNumbers = new ArrayList<String>();
            List<Integer> filterCallIndex = new ArrayList<Integer>();
            for (int i=0; i<trackingFilterList.size(); i++)
                filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
            // finish get Filter

            // loading Call Log
            List<Call_Log> call_logList = _utilsFactory.getCallLogUtils().getCallLog_from_to(context, lastTime, null, LIMIT_AUTO_TRACKING, 0);
            String callNumber;
            for (Call_Log call_log : call_logList) {
                startTimeList.add((call_log.getFromTime() / 1000) * 1000);
                endTimeList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
                // loading Call Log filter
                callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
                if (callNumber==null || callNumber.equals(""))
                    callNumber = call_log.getNumbers();
                int index = filterPhoneNumbers.indexOf(callNumber);
                if (index>-1){
                    filterCallIndex.add(call_logList.indexOf(call_log));
                    startTimeFilterList.add((call_log.getFromTime() / 1000) * 1000);
                    endTimeFilterList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
                }
                // finish loading Call Log filter
            }
            // finish loading Call Log

            // start tracking Call Log
            Call_Log call_log, call_logEvent;
            List<Call_Log> createNewCall_LogList = new ArrayList<Call_Log>();
            List<Call_Log> updateCall_LogList = new ArrayList<Call_Log>();
            if (selected_call_log_calendar_id != -1) {
                Map<String, Call_Log> callLogEvent_hashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeList, endTimeList, selected_call_log_calendar_id, context);
                if (call_logList != null) {
                    for (int i = 0; i < call_logList.size(); i++) {
                        call_log = call_logList.get(i);
                        // ignore filtered sms
                        if (filterCallIndex.contains(i)){
                            continue;
                        }
                        // finish ignore filtered sms
                        if (callLogEvent_hashMap.containsKey(call_log.getFixedHashValue())) {
                            call_logEvent = callLogEvent_hashMap.get(call_log.getFixedHashValue());
                            if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                                call_log.setCal_EventId(call_logEvent.getCal_EventId());
                                if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                    call_log.setOwnerNumber(call_logEvent.getOwnerNumber());
                                    call_log.regenerateDescription();
                                }
                                updateCall_LogList.add(call_log);
                            }
                        } else {
                            createNewCall_LogList.add(call_log);
                        }
                    }
                    _utilsFactory.getCalendarUtils().batch_Update_CALL_LOG_Event(context, selected_call_log_calendar_id, updateCall_LogList);
                    _utilsFactory.getCalendarUtils().batch_Create_CALL_LOG_Event(context, selected_call_log_calendar_id, createNewCall_LogList);
                    /*
                    _utilsFactory.getPreferencesHelper().store_Total_Tracking_CALL(context, call_logList.size());
                    _utilsFactory.getPreferencesHelper().store_Updated_Tracking_CALL(context, updateCall_LogList.size());
                    _utilsFactory.getPreferencesHelper().store_Processed_Tracking_CALL(context, createNewCall_LogList.size());   */
                }
            }
            // start tracking filter sms
            if (filterCallIndex.size()>0){
                String filterNumber, callCountry, filterCountry;
                for (int i=0; i<trackingFilterList.size(); i++){
                    createNewCall_LogList.clear();
                    updateCall_LogList.clear();
                    trackingFilter = trackingFilterList.get(i);
                    selected_call_log_calendar_id = trackingFilter.getCallCalendarId();
                    if (selected_call_log_calendar_id < 1)
                        continue;
                    Map<String, Call_Log> callEvent_FilterHashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeFilterList, endTimeFilterList, selected_call_log_calendar_id, context);

                    int callIndex;
                    for (int j=0; j< filterCallIndex.size(); j++){
                        callIndex = filterCallIndex.get(j);
                        call_log = call_logList.get(callIndex);
                        callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
                        if (callNumber==null || callNumber.equals(""))
                            callNumber = call_log.getNumbers();
                        //callCountry = _contactsHelper.getCountryCode(call_log.getNumbers());
                        filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                        //filterCountry = _contactsHelper.getCountryCode(trackingFilter.getPhonenumber());
                        if (!callNumber.equals(filterNumber)){
                            continue;
                        }
                        if (callEvent_FilterHashMap.containsKey(call_log.getFixedHashValue())) {
                            call_logEvent = callEvent_FilterHashMap.get(call_log.getFixedHashValue());
                            if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                                call_log.setCal_EventId(call_logEvent.getCal_EventId());
                                if (_advancedSettingPreference.read_ignoreOwnerInfo_setting()) {
                                    call_log.setOwnerNumber(call_logEvent.getOwnerNumber());
                                    call_log.regenerateDescription();
                                }
                                updateCall_LogList.add(call_log);
                            }
                        } else {
                            createNewCall_LogList.add(call_log);
                        }
                    }
                    _utilsFactory.getCalendarUtils().batch_Update_CALL_LOG_Event(context, selected_call_log_calendar_id, updateCall_LogList);
                    _utilsFactory.getCalendarUtils().batch_Create_CALL_LOG_Event(context, selected_call_log_calendar_id, createNewCall_LogList);

                }
            }
            _utilsFactory.getPreferencesHelper().store_Last_CALL_Tracking_Time(context, new Date().getTime());
            _utilsFactory.getPreferencesHelper().store_Last_CALL_Auto_Tracking_Time(context, new Date().getTime());
            // notification
            _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_callLogTracking_title), formatter.format(new Date()));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
