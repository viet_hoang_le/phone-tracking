package com.bibooki.mobile.sms_bibooki.utils.preferences;

import android.content.Context;

public class AdvancedSettingPreference extends PreferencesHelper {

    static AdvancedSettingPreference _advancedSettingPreferences = null;
    private static Context mContext = null;

    // Ignore Owner Phone Numbers Prefefence Store
    public static final String _IGNORE_OWNER = "com.bibooki.mobile.sms_bibooki.advanced_setting.ignore_owner";
    // Ignore Owner Phone Numbers when update event infos
    public static final String _IGNORE_OWNER_INFO = "com.bibooki.mobile.sms_bibooki.advanced_setting.ignore_owner_info";
    // Ignore Phone Call with type > 3
    public static final String _IGNORE_UNUSUAL_PHONE_CALLTYPE = "com.bibooki.mobile.sms_bibooki.advanced_setting.ignore_phoneType";

    AdvancedSettingPreference(Context context){
        mContext = context;
    }

    // return new instance
    public static AdvancedSettingPreference getInstance(Context context){
        if (_advancedSettingPreferences == null)
            _advancedSettingPreferences = new AdvancedSettingPreference(context);
        return _advancedSettingPreferences;
    }

    // store & read ignore owner phone numbers setting
    public void store_ignoreOwner_setting(boolean isIgnore){
        if (isIgnore)
            storeInt(mContext, _IGNORE_OWNER, 1);
        else
            storeInt(mContext, _IGNORE_OWNER, 0);
    }
    public boolean read_ignoreOwner_setting(){
        int result = readInt(mContext, _IGNORE_OWNER);
        if (result < 0 || result > 0)
            return true;
        return false;
    }

    // store & read ignore owner phone numbers info when updating
    public void store_ignoreOwnerInfo_setting(boolean isIgnore){
        if (isIgnore)
            storeInt(mContext, _IGNORE_OWNER_INFO, 1);
        else
            storeInt(mContext, _IGNORE_OWNER_INFO, 0);
    }
    public boolean read_ignoreOwnerInfo_setting(){
        int result = readInt(mContext, _IGNORE_OWNER_INFO);
        if (result < 0 || result > 0)
            return true;
        return false;
    }

    // store & read ignore manufacturer custom phone type setting
    public void store_ignorePhoneCallType_setting(boolean isIgnore){
        if (isIgnore)
            storeInt(mContext, _IGNORE_UNUSUAL_PHONE_CALLTYPE, 1);
        else
            storeInt(mContext, _IGNORE_UNUSUAL_PHONE_CALLTYPE, 0);
    }
    public boolean read_ignorePhoneCallType_setting(){
        int result = readInt(mContext, _IGNORE_UNUSUAL_PHONE_CALLTYPE);
        if (result < 0 || result > 0)
            return true;
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
