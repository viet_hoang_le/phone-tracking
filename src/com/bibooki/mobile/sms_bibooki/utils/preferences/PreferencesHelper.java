package com.bibooki.mobile.sms_bibooki.utils.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {
    static PreferencesHelper _preferencesHelper = null;

    private final String _PREFS_NAME = "com.bibooki.mobile.sms_bibooki.sms";

    // value of sms content that will be placed at event's title, -1 is none, 0 is only 13 characters, and 1 is all
    public static final String _SMS_CALENDAR_EVENT_TITLE = "com.bibooki.mobile.sms_bibooki.sms.eventTitle";
    public static final String _CALL_CALENDAR_EVENT_TITLE = "com.bibooki.mobile.sms_bibooki.call.eventTitle";
    public static final String _SCHEDULE_CALENDAR_EVENT_DELETE = "com.bibooki.mobile.sms_bibooki.schedule.deleteEvent";

    // last time that program tracking
    public static final String _SMS_LAST_SCHEDULED = "com.bibooki.mobile.sms_bibooki.sms.lastScheduledTime";
    public static final String _SMS_LAST_AUTO_TRACKING = "com.bibooki.mobile.sms_bibooki.sms.lastAutoTrackingTime";
    public static final String _CALL_LOG_LAST_AUTO_TRACKING = "com.bibooki.mobile.sms_bibooki.call.lastAutoTrackingTime";

    // time cycle for auto tracking
    public static final String _AUTO_TRACKING_TIME = "com.bibooki.mobile.sms_bibooki.tracking.time";
    // sending time for auto Sending
    public static final String _AUTO_SENDING_TIME = "com.bibooki.mobile.sms_bibooki.sending.time";

    // setting for tracking plan: all or incremental
    public static final String _TRACKING_PLAN = "com.bibooki.mobile.sms_bibooki.tracking.plan";

    // setting state of Activity: is Resume or Pausing
    public static final String _MAIN_ACTIVITY_STATE = "com.bibooki.mobile.sms_bibooki.activity.state";

    // setting total of SMS for tracking
    public static final String _TOTAL_TRACKING_SMS = "com.bibooki.mobile.sms_bibooki.sms.total_Tracking_SMS";
    // setting number of updated SMS
    public static final String _UPDATED_TRACKING_SMS = "com.bibooki.mobile.sms_bibooki.sms.total_Updated_SMS";
    // setting number of processed SMS
    public static final String _PROCESSED_TRACKING_SMS = "com.bibooki.mobile.sms_bibooki.sms.total_Processed_SMS";
    // setting total of CALL for tracking
    public static final String _TOTAL_TRACKING_CALL = "com.bibooki.mobile.sms_bibooki.call.total_Tracking_CALL";
    // setting number of updated CALL
    public static final String _UPDATED_TRACKING_CALL = "com.bibooki.mobile.sms_bibooki.call.total_Updated_CALL";
    // setting number of processed CALL
    public static final String _PROCESSED_TRACKING_CALL = "com.bibooki.mobile.sms_bibooki.call.total_Processed_CALL";
    // last time that program tracking
    public static final String _SMS_LAST_TRACKING = "com.bibooki.mobile.sms_bibooki.sms.lastTrackingTime";
    public static final String _CALL_LOG_LAST_TRACKING = "com.bibooki.mobile.sms_bibooki.call.lastTrackingTime";


    public static final String _ACCOUNT_NAME = "com.bibooki.mobile.sms_bibooki.sms.accountName";
    public static final String _AUTH_TOKEN = "com.bibooki.mobile.sms_bibooki.sms.authToken";

    // SMS Fragment
    public static final String _SMS_TIME_1 = "com.bibooki.mobile.sms_bibooki.sms.untracked.from";
    public static final String _SMS_TIME_2 = "com.bibooki.mobile.sms_bibooki.sms.untracked.to";
    public static final String _SMS_LIST_MODE = "com.bibooki.mobile.sms_bibooki.sms.list.mode";
    // Call Log Fragment
    public static final String _CALL_TIME_1 = "com.bibooki.mobile.sms_bibooki.call.untracked.from";
    public static final String _CALL_TIME_2 = "com.bibooki.mobile.sms_bibooki.call.untracked.to";
    public static final String _CALL_LIST_MODE = "com.bibooki.mobile.sms_bibooki.call.list.mode";

    // Shopping preferences
    public static final String _BILLING_FREE_ADS = "com.bibooki.mobile.sms_bibooki.shop.free_ads";

    // return new instance
    public static PreferencesHelper newInstance(){
        if (_preferencesHelper == null)
            _preferencesHelper = new PreferencesHelper();
        return _preferencesHelper;
    }
    /*
    Basic Methods
    * */
    public void clearAll(Context context){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.clear();
        editor.commit();
    }
    // store String value to share preference with store key
    public void storeString(Context context, String storeKey, String value){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.putString(storeKey, value);
        editor.commit();
    }
    // read String value from share preference by store key
    public String readString(Context context, String storeKey){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        String result = "";
        try{
            // use a default value using new Date()
            result = prefs.getString(storeKey, null);
        }   catch (Exception ex){
            ex.printStackTrace();
            prefs.edit().remove(storeKey);
        }
        return result;
    }
    // store Integer value to share preference with store key
    public void storeInt(Context context, String storeKey, int value){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.putInt(storeKey, value);
        editor.commit();
    }
    // read Integer value from share preference by store key
    public int readInt(Context context, String storeKey){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        // use a default value using new Date()
        int result = -1;
        try{
            result = prefs.getInt(storeKey, -1);
        } catch (Exception ex){
            ex.printStackTrace();
            prefs.edit().remove(storeKey);
        }
        return result;
    }
    // read Integer value from share preference by store key with default value if it is not existed
    public int readInt(Context context, String storeKey, int defaultValue){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        // use a default value using new Date()
        int result = -1;
        try{
            result = prefs.getInt(storeKey, defaultValue);
        } catch (Exception ex){
            ex.printStackTrace();
            prefs.edit().remove(storeKey);
        }
        return result;
    }
    // store Long value to share preference with store key
    public void storeLong(Context context, String storeKey, long value){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.putLong(storeKey, value);
        editor.commit();
    }
    // read Long value from share preference by store key
    public long readLong(Context context, String storeKey){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        // use a default value using new Date()
        long result = -1;
        try{
            result = prefs.getLong(storeKey, -1);
        } catch (Exception ex){
            ex.printStackTrace();
            prefs.edit().remove(storeKey);
        }
        return result;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process Calendar event's title
    * */
    // store sms event title setting to share preference with store key
    public void store_SMS_EventTitle(Context context, int value){
        storeInt(context, _SMS_CALENDAR_EVENT_TITLE, value);
    }
    // read sms event title setting from share preference by store key
    public Integer read_SMS_EventTitle(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _SMS_CALENDAR_EVENT_TITLE, 1);
        return result;
    }
    // store call event title setting to share preference with store key
    public void store_CALL_EventTitle(Context context, int value){
        storeInt(context, _CALL_CALENDAR_EVENT_TITLE, value);
    }
    // read call event title setting from share preference by store key
    public Integer read_CALL_EventTitle(Context context){
        // use a default value using duration of Call
        int result = readInt(context, _CALL_CALENDAR_EVENT_TITLE, 1);
        return result;
    }
    // store call event title setting to share preference with store key
    public void store_delete_schedule_status(Context context, int value){
        storeInt(context, _SCHEDULE_CALENDAR_EVENT_DELETE, value);
    }
    // read call event title setting from share preference by store key
    public Integer read_delete_schedule_status(Context context){
        // use a default value using duration of Call
        int result = readInt(context, _SCHEDULE_CALENDAR_EVENT_DELETE, 1);
        return result;
    }
    /*
    * Finish Process Calendar event's title
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process last tracking time
    * */
    // store sms calendar ID to share preference with store key
    public void store_Last_SMS_Scheduled_Time(Context context, long value){
        storeLong(context, _SMS_LAST_SCHEDULED, value);
    }
    // read sms calendar ID from share preference by store key
    public Long read_Last_SMS_Scheduled_Time(Context context){
        // use a default value using new Date()
        long result = readLong(context, _SMS_LAST_SCHEDULED);
        return result;
    }
    public void store_Last_SMS_Auto_Tracking_Time(Context context, long value){
        storeLong(context, _SMS_LAST_AUTO_TRACKING, value);
    }
    // read sms calendar ID from share preference by store key
    public Long read_Last_SMS_Auto_Tracking_Time(Context context){
        // use a default value using new Date()
        long result = readLong(context, _SMS_LAST_AUTO_TRACKING);
        return result;
    }
    // store call log calendar ID to share preference with store key
    public void store_Last_CALL_Auto_Tracking_Time(Context context, long value){
        // get editor
        storeLong(context, _CALL_LOG_LAST_AUTO_TRACKING, value);
    }
    // read call log calendar ID from share preference by store key
    public Long read_Last_CALL_Auto_Tracking_Time(Context context){
        // use a default value using new Date()
        long result = readLong(context, _CALL_LOG_LAST_AUTO_TRACKING);
        return result;
    }
    /*
    * Finish Process last tracking time
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process auto tracking time
    * */
    // store auto tracking time to share preference with store key
    public void store_Auto_Tracking_Time(Context context, int minutes){
        storeInt(context, _AUTO_TRACKING_TIME, minutes);
    }
    // read sms calendar ID from share preference by store key
    public int read_Auto_Tracking_Time(Context context){
        int minutes = readInt(context, _AUTO_TRACKING_TIME);
        if (minutes<0)
            minutes=1;
        return minutes;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process auto tracking time
    * */
    // store sms calendar ID to share preference with store key
    public void store_Auto_Sending_Time(Context context, Long value){
        storeLong(context, _AUTO_SENDING_TIME, value);
    }
    // read sms calendar ID from share preference by store key
    public Long read_Auto_Sending_Time(Context context){
        // use a default value using new Date()
        Long time = readLong(context, _AUTO_SENDING_TIME);
        return time;
    }
    /*
    * Finish Process auto tracking time
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process tracking plan
    * */
    // store tracking plan to share preference with store key
    public void store_Tracking_Plan(Context context, int value){
            storeInt(context, _TRACKING_PLAN, value);
    }
    // read tracking plan from share preference by store key
    public int read_Tracking_Plan(Context context){
        // use a default value using new Date()
        int result = readInt(context, _TRACKING_PLAN);
        return result;
    }
    /*
    * Finish Process tracking plan
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process state of Main Activity
    * -1 is Pausing, +1 is Resume (active)
    * */
    // store state of Main Activity to share preference with store key
    public void store_MainActivity_State(Context context, int value){
            storeInt(context, _MAIN_ACTIVITY_STATE, value);
    }
    // read state of Main Activity from share preference by store key
    public int read_MainActivity_State(Context context){
        int result = readInt(context, _MAIN_ACTIVITY_STATE);
        return result;
    }
    /*
    * Finish Process tracking plan
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process Number of Tracking SMS & CALL
    * */
    // store tracking number of total sms setting to share preference with store key
    public void store_Total_Tracking_SMS(Context context, int value){
        storeInt(context, _TOTAL_TRACKING_SMS, value);
    }
    // read tracking number of total SMS setting from share preference by store key
    public Integer read_Total_Tracking_SMS(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _TOTAL_TRACKING_SMS, 0);
        return result;
    }
    // store tracking number of Updated sms setting to share preference with store key
    public void store_Updated_Tracking_SMS(Context context, int value){
        storeInt(context, _UPDATED_TRACKING_SMS, value);
    }
    // read tracking number of Updated SMS setting from share preference by store key
    public Integer read_Updated_Tracking_SMS(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _UPDATED_TRACKING_SMS, 0);
        return result;
    }
    // store tracking number of processed sms setting to share preference with store key
    public void store_Processed_Tracking_SMS(Context context, int value){
        storeInt(context, _PROCESSED_TRACKING_SMS, value);
    }
    // read tracking number of processed SMS setting from share preference by store key
    public Integer read_Processed_Tracking_SMS(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _PROCESSED_TRACKING_SMS, 0);
        return result;
    }
    // store tracking number of total CALL setting to share preference with store key
    public void store_Total_Tracking_CALL(Context context, int value){
        storeInt(context, _TOTAL_TRACKING_CALL, value);
    }
    // read tracking number of total CALL setting from share preference by store key
    public Integer read_Total_Tracking_CALL(Context context){
        // use a default value using duration of Call
        int result = readInt(context, _TOTAL_TRACKING_CALL, 0);
        return result;
    }
    // store tracking number of Updated CALL setting to share preference with store key
    public void store_Updated_Tracking_CALL(Context context, int value){
        storeInt(context, _UPDATED_TRACKING_CALL, value);
    }
    // read tracking number of Updated CALL setting from share preference by store key
    public Integer read_Updated_Tracking_CALL(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _UPDATED_TRACKING_CALL, 0);
        return result;
    }
    // store tracking number of processed CALL setting to share preference with store key
    public void store_Processed_Tracking_CALL(Context context, int value){
        storeInt(context, _PROCESSED_TRACKING_CALL, value);
    }
    // read tracking number of processed CALL setting from share preference by store key
    public Integer read_Processed_Tracking_CALL(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _PROCESSED_TRACKING_CALL, 0);
        return result;
    }
    /*
    * Finish Process Calendar event's title
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    * Process last tracking time
    * */
    // store sms calendar ID to share preference with store key
    public void store_Last_SMS_Tracking_Time(Context context, long value){
        storeLong(context, _SMS_LAST_TRACKING, value);
    }
    // read sms calendar ID from share preference by store key
    public Long read_Last_SMS_Tracking_Time(Context context){
        // use a default value using new Date()
        long result = readLong(context, _SMS_LAST_TRACKING);
        return result;
    }
    // store call log calendar ID to share preference with store key
    public void store_Last_CALL_Tracking_Time(Context context, long value){
        // get editor
        storeLong(context, _CALL_LOG_LAST_TRACKING, value);
    }
    // read call log calendar ID from share preference by store key
    public Long read_Last_CALL_Tracking_Time(Context context){
        // use a default value using new Date()
        long result = readLong(context, _CALL_LOG_LAST_TRACKING);
        return result;
    }
    /*
    * Finish Process last tracking time
    * */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     // store account's Name to share preference with store key
    public void storeAccountName(Context context, String value){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.putString(_ACCOUNT_NAME, value);
        editor.commit();
    }
    // read account's Name from share preference by store key
    public String readAccountName(Context context){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        // use a default value using new Date()
        String result = prefs.getString(_ACCOUNT_NAME, null);
        return result;
    }
    // store authToken to share preference with store key
    public void storeAuthToken(Context context, String value){
        // get editor
        SharedPreferences.Editor editor = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE).edit();
        // set value to shared preferences
        editor.putString(_AUTH_TOKEN, value);
        editor.commit();
    }
    // read authToken from share preference by store key
    public String readAuthToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences(
                _PREFS_NAME, Context.MODE_PRIVATE);
        // use a default value using new Date()
        String result = prefs.getString(_AUTH_TOKEN, null);
        return result;
    }
    // store SMS tracking time from
    public void store_SMS_Time_From(Context context, long time){
        if (time==0){
            storeLong(context, _SMS_TIME_1, 0);
            return;
        }
        storeLong(context, _SMS_TIME_1, time);
    }
    public Long read_SMS_Time_From(Context context){
        return readLong(context, _SMS_TIME_1);
    }
    // store SMS tracking time from
    public void store_SMS_Time_To(Context context, long time){
        if (time==0){
            storeLong(context, _SMS_TIME_2, 0);
            return;
        }
        storeLong(context, _SMS_TIME_2, time);
    }
    public Long read_SMS_Time_To(Context context){
        return readLong(context, _SMS_TIME_2);
    }
    // store SMS List mode
    public void store_SMS_listMode(Context context, int value){
        storeInt(context, _SMS_LIST_MODE, value);
    }
    // read tracking number of processed CALL setting from share preference by store key
    public Integer read_SMS_listMode(Context context){
        // use a default value using all content of SMS
        int result = readInt(context, _SMS_LIST_MODE, 0);
        return result;
    }
    // store CALL tracking time from
    public void store_CALL_Time_From(Context context, long time){
        if (time==0){
            storeLong(context, _CALL_TIME_1, 0);
            return;
        }
        storeLong(context, _CALL_TIME_1, time);
    }
    public Long read_CALL_Time_From(Context context){
        return readLong(context, _CALL_TIME_1);
    }
    // store CALL tracking time from
    public void store_CALL_Time_To(Context context, long time){
        if (time==0){
            storeLong(context, _CALL_TIME_2, 0);
            return;
        }
        storeLong(context, _CALL_TIME_2, time);
    }
    public Long read_CALL_Time_To(Context context){
        return readLong(context, _CALL_TIME_2);
    }
    // store CALL List mode
    public void store_CALL_listMode(Context context, int value){
        storeInt(context, _CALL_LIST_MODE, value);
    }
    // read tracking number of processed CALL setting from share preference by store key
    public Integer read_CALL_listMode(Context context){
        // use a default value using all content of CALL
        int result = readInt(context, _CALL_LIST_MODE, 0);
        return result;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////// Shopping
    // store billing of free ads
    public void setBillingFreeAds(Context context, boolean isFreeAds){
        if (isFreeAds)
            storeInt(context, _BILLING_FREE_ADS, 1);
        else
            storeInt(context, _BILLING_FREE_ADS, 0);
    }
    // read billing of free ads
    public boolean isFreeAds(Context context){
        // default value = -1
        int result = readInt(context, _BILLING_FREE_ADS);
        return (result>0);
    }
}