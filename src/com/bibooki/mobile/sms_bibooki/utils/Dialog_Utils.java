package com.bibooki.mobile.sms_bibooki.utils;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.*;
import com.bibooki.mobile.sms_bibooki.Fragment_CallLog;
import com.bibooki.mobile.sms_bibooki.Fragment_SMS;
import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;
import com.bibooki.mobile.sms_bibooki.utils.call_log.Call_Log_Utils;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Dialog_Utils {
    /**
     * -- Setup loading dialog --**
     */
    private static ProgressDialog progressDialog;
    private static ProgressDialog mprogressDialog;
    /**
     * -- Finish Setup loading dialog --**
     */
    private static Handler mHandler = new Handler();
    List<SMS> smsList;
    List<Call_Log> callLogs;

    static long fromTime = -1,
            toTime = -1;

    private static Context mContext;
    private static Activity _currentParentActivity;

    static Dialog_Utils _dialog = null;
    private static UtilsFactory _utilsFactory;
    private static SMS_Utils _smsUtils;
    private static Call_Log_Utils _callLogUtils;
    private static Calendar_Utils _calendarUtils;
    private static PreferencesHelper _preferencesHelper;
    private static NotificationUtils _notificationUtils;

    public static enum DIALOG_TYPE {
        CALENDAR_CHOOSING_DIALOG,
        EVENT_TITLE_DIALOG,
        TRACKING_TIME_DIALOG,
        RESTORE_TIME_DIALOG,
        TRACKING_DIALOG,
        SEARCH_UNPROCESSED_SMS_DIALOG,
        SEARCH_UNPROCESSED_CALL_DIALOG,
        DELETE_SMS_DIALOG,
        DELETE_SCHEDULE_SMS_DIALOG,
        DELETE_CALL_DIALOG
    };

    private int LIMIT_SEARCH_ITEM = 500;

    // return new instance
    public static Dialog_Utils getInstance(Context context) {
        if (_dialog == null)
            _dialog = new Dialog_Utils();

        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _smsUtils = _utilsFactory.getSmsUtils();
        _callLogUtils = _utilsFactory.getCallLogUtils();
        _calendarUtils = _utilsFactory.getCalendarUtils();
        _preferencesHelper = _utilsFactory.getPreferencesHelper();
        _notificationUtils = _utilsFactory.getNotificationUtils();
        /***-- Setup loading dialog --***/
        // setup progress bar
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
        /***-- Finish Setup loading dialog --***/
        return _dialog;
    }

    // Create a dialog that set checkedTextView into checked and change text into ck_text.
    // Store calendar's id into shared preferences based on store key
    public Dialog createDialog(final Context _context, final Activity activity, final DIALOG_TYPE dialog_type, final String[] items, String title,
                               final CheckedTextView cView, final String ck_text, final String storeKey) {
        // create new AlertDialog
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
        // set title
        alertDialogBuilder.setTitle(title);
        // set dialog message
        alertDialogBuilder
                .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        //Toast.makeText(_context, items[item], Toast.LENGTH_SHORT).show();
                        String selected_Item = items[item];
                        // close the dialog
                        dialog.cancel();

                        switch (dialog_type) {
                            // process calendar
                            case CALENDAR_CHOOSING_DIALOG:
                                processCalendar(_context, cView, storeKey, selected_Item);
                                break;
                            case RESTORE_TIME_DIALOG:
                                processRestore(_context, activity, selected_Item);
                                break;
                            case SEARCH_UNPROCESSED_SMS_DIALOG:
                                processSearchSMS(_context, activity, dialog_type, selected_Item);
                                break;
                            case SEARCH_UNPROCESSED_CALL_DIALOG:
                                processSearchCallLog(_context, activity, dialog_type, selected_Item);
                                break;
                            default:
                                break;
                        }
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getListView().setSelection(-1);
        return alertDialogBuilder.create();
    }

    // process choosing sms and call log calendar
    private void processCalendar(Context context, CheckedTextView cView, String storeKey, String selected_CalName) {
        try{
            int calendarId = Integer.parseInt(selected_CalName.substring(0, selected_CalName.indexOf(".")));
            // store calendar id into database
            DBTrackCalendarOpenHelper dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(context);
            if (storeKey!=null){
                if (storeKey.equals(SMS_Utils.SMS_STORE_DB)){
                    dbTrackCalendarOpenHelper.updateCreateSmsDefault(calendarId);
                }
                else if (storeKey.equals(SMS_Utils.SCHEDULE_STORE_DB)){
                    dbTrackCalendarOpenHelper.updateCreateSchedule(calendarId);
                }
                else if (storeKey.equals(Call_Log_Utils.CALL_STORE_DB)){
                    dbTrackCalendarOpenHelper.updateCreateCallDefault(calendarId);
                }
            }
            // set checkedTextView to selected status after user chose a calendar
            if (cView!=null){
                cView.setSelected(true);
                cView.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
                cView.setText(selected_CalName);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // process Restore after user chose a Restoring Schema
    private void processRestore(final Context context, final Activity activity, String selected_item) {
        try {
            if (selected_item.equals(activity.getResources().getString(R.string.restore_plan_all))) {
                // process restore all SMS and Call Log from Calendar Events
                _utilsFactory.getRestore_Utils().restore_All(context);
            }
            if (selected_item.equals(activity.getResources().getString(R.string.restore_plan_from_now))) {
                // process restore all SMS and Call Log from Calendar Events from Year.Month.Date to Current Time
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean proceeded = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                if (calendar.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else if (proceeded==false){
                                    // process restore SMS, Call Log Event from Calendar
                                    _utilsFactory.getRestore_Utils().restore_From_To_Now(view.getContext(), calendar);
                                    proceeded=true;
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            }
            if (selected_item.equals(activity.getResources().getString(R.string.restore_plan_from_to))) {
                // process restore all SMS and Call Log from Calendar Events from Year1.Month1.Date1 to Year2.Month2.Date2
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                final Calendar cal1 = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean alreadySet = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                cal1.set(year, monthOfYear, dayOfMonth, 0, 0, 0);

                                final Calendar c = Calendar.getInstance();
                                if (cal1.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else {
                                    // process restore SMS, Call Log Event from Calendar
                                    int yearC = c.get(Calendar.YEAR);
                                    int monthC = c.get(Calendar.MONTH);
                                    int dayC = c.get(Calendar.DAY_OF_MONTH);
                                    if (alreadySet==false){
                                        DatePickerDialog datePickerDialog = new DatePickerDialog(view.getContext(),
                                                new DatePickerDialog.OnDateSetListener() {
                                                    boolean proceeded = false;
                                                    public void onDateSet(DatePicker view, int year,
                                                                          int monthOfYear, int dayOfMonth) {
                                                        Calendar calendar = Calendar.getInstance();
                                                        calendar.set(year, monthOfYear, dayOfMonth, 23, 59, 59);
                                                        if (calendar.after(Calendar.getInstance()) || calendar.before(cal1)) {
                                                            // if user choose a time that over current time or before calendar 1, program does not process
                                                        } else if (proceeded==false){
                                                            // process restore SMS, Call Log Event from Calendar
                                                            _utilsFactory.getRestore_Utils().restore_SMS_CALL_LOG(view.getContext(), cal1, calendar);
                                                            proceeded=true;
                                                        }
                                                    }
                                                },
                                                yearC, monthC, dayC);
                                        datePickerDialog.show();
                                        alreadySet=true;
                                    }
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    // process Restore after user chose a Restoring Schema
    private class ProcessSmsData extends AsyncTask<DIALOG_TYPE, Void, Void> {
        protected void onPreExecute(){
            mHandler.post(new Runnable() {
                public void run() {
                    mprogressDialog = new ProgressDialog(_currentParentActivity);
                    mprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mprogressDialog.setCancelable(false);
                    mprogressDialog.setMessage(_currentParentActivity.getResources().getString(R.string.status_loading_sms));
                    mprogressDialog.show();
                    Fragment_SMS.smsListAdapter.clear();
                }
            });
        }
        protected Void doInBackground(DIALOG_TYPE... params) {
            List<SMS> smsList = new ArrayList<SMS>();
            try {
                // store sms time
                _preferencesHelper.store_SMS_Time_From(mContext, fromTime);
                _preferencesHelper.store_SMS_Time_To(mContext, toTime);
                // get list of valid sms
                // process search SMS
                smsList = _utilsFactory.getSmsUtils().getSMS_from_to(mContext, fromTime, toTime, LIMIT_SEARCH_ITEM, 0);
                switch (params[0]) {
                    case SEARCH_UNPROCESSED_SMS_DIALOG:
                        smsList = _utilsFactory.getSmsUtils().getUnprocessedSMS(mContext, smsList, fromTime, toTime, LIMIT_SEARCH_ITEM, 0);
                        _preferencesHelper.store_SMS_listMode(mContext, 1);
                        break;
                }
                // finish process
                Fragment_SMS.addMoreSmsItem(mContext, smsList);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result) {
            mHandler.post(new Runnable() {
                public void run() {
                    mprogressDialog.dismiss();
                }
            });
        }
    }
    private void processSearchSMS(final Context context, final Activity activity,
                                  final DIALOG_TYPE dialogType, String selected_item) {
        try {
            _currentParentActivity = activity;
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            if (selected_item.equals(activity.getResources().getString(R.string.time_plan_only))) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean proceeded = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                if (calendar.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                    _notificationUtils.showShortToast(mContext.getResources().getString(R.string.notification_date_invalid));
                                } else if (proceeded==false){
                                    fromTime = calendar.getTimeInMillis();
                                    toTime = calendar.getTimeInMillis() + TimeUnit.DAYS.toMillis(1);
                                    new ProcessSmsData().execute(dialogType);
                                    proceeded=true;
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_all))) {
                fromTime = toTime = -1;
                new ProcessSmsData().execute(dialogType);
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_from_now))) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean proceeded = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                if (calendar.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else if (proceeded==false){
                                    fromTime = calendar.getTimeInMillis();
                                    toTime = -1;
                                    new ProcessSmsData().execute(dialogType);
                                    proceeded = true;
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_from_to))) {
                // process restore all SMS and Call Log from Calendar Events from Year1.Month1.Date1 to Year2.Month2.Date2
                //Toast.makeText(context, "Search untracked items from...to...", Toast.LENGTH_SHORT).show();
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean alreadySet = false;
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                final Calendar cal1 = Calendar.getInstance();
                                cal1.set(year, monthOfYear, dayOfMonth, 0, 0, 0);

                                final Calendar c = Calendar.getInstance();
                                if (cal1.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else {
                                    // process restore SMS, Call Log Event from Calendar
                                    int yearC = c.get(Calendar.YEAR);
                                    int monthC = c.get(Calendar.MONTH);
                                    int dayC = c.get(Calendar.DAY_OF_MONTH);
                                    if (alreadySet==false){
                                        DatePickerDialog datePickerDialog = new DatePickerDialog(view.getContext(),
                                                new DatePickerDialog.OnDateSetListener() {
                                                    boolean proceeded = false;
                                                    public void onDateSet(DatePicker view, int year,
                                                                          int monthOfYear, int dayOfMonth) {
                                                        Calendar cal2 = Calendar.getInstance();
                                                        cal2.set(year, monthOfYear, dayOfMonth, 23, 59, 59);
                                                        if (cal2.after(Calendar.getInstance()) || cal2.before(cal1)) {
                                                            // if user choose a time that over current time or before calendar 1, program does not process
                                                        } else if (proceeded==false){
                                                            fromTime = cal1.getTimeInMillis();
                                                            toTime = cal2.getTimeInMillis();
                                                            new ProcessSmsData().execute(dialogType);
                                                            proceeded=true;
                                                        }
                                                    }
                                                },
                                                yearC, monthC, dayC);
                                        datePickerDialog.show();
                                        alreadySet=true;
                                    }
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private class ProcessCallData extends AsyncTask<DIALOG_TYPE, Void, Void> {
        protected void onPreExecute(){
            mHandler.post(new Runnable() {
                public void run() {
                    mprogressDialog = new ProgressDialog(_currentParentActivity);
                    mprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mprogressDialog.setCancelable(false);
                    mprogressDialog.setMessage(_currentParentActivity.getResources().getString(R.string.status_loading_call));
                    mprogressDialog.show();
                    Fragment_CallLog.callListAdapter.clear();
                }
            });
        }
        protected Void doInBackground(DIALOG_TYPE... params) {
            List<SMS> smsList = new ArrayList<SMS>();
            try {
                // store Call Log time
                _preferencesHelper.store_CALL_Time_From(mContext, 0);
                _preferencesHelper.store_CALL_Time_To(mContext, 0);
                // get list of valid sms
                // process search Call Log
                callLogs = _utilsFactory.getCallLogUtils().getCallLog_from_to(mContext, fromTime, toTime, LIMIT_SEARCH_ITEM, 0);
                switch (params[0]) {
                    case SEARCH_UNPROCESSED_CALL_DIALOG:
                        callLogs = _utilsFactory.getCallLogUtils().getUnprocessedCall(mContext, callLogs, fromTime, toTime, LIMIT_SEARCH_ITEM, 0);
                        _preferencesHelper.store_CALL_listMode(mContext, 1);
                        break;
                }
                // finish process
                Fragment_CallLog.addMoreCallItem(mContext, callLogs);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void result) {
            mHandler.post(new Runnable() {
                public void run() {
                    mprogressDialog.dismiss();
                }
            });
        }
    }
    private void processSearchCallLog(final Context context, final Activity activity,
                                      final DIALOG_TYPE dialogType, String selected_item) {
        try {
            _currentParentActivity = activity;
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            if (selected_item.equals(activity.getResources().getString(R.string.time_plan_only))) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean proceeded = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                if (calendar.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                    _notificationUtils.showShortToast(mContext.getResources().getString(R.string.notification_date_invalid));
                                } else if (proceeded==false) {
                                    fromTime = calendar.getTimeInMillis();
                                    toTime = calendar.getTimeInMillis() + TimeUnit.DAYS.toMillis(1);
                                    new ProcessCallData().execute(dialogType);
                                    proceeded = true;
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_all))) {
                fromTime = toTime = -1;
                new ProcessCallData().execute(dialogType);
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_from_now))) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean proceeded = false;
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                if (calendar.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else if (proceeded==false){
                                    fromTime = calendar.getTimeInMillis();
                                    toTime = -1;
                                    new ProcessCallData().execute(dialogType);
                                    proceeded = true;
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            } else if (selected_item.equals(activity.getResources().getString(R.string.time_plan_from_to))) {
                // process restore all SMS and Call Log from Calendar Events from Year1.Month1.Date1 to Year2.Month2.Date2
                //Toast.makeText(context, "Search untracked items from...to...", Toast.LENGTH_SHORT).show();
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            boolean alreadySet = false;
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                final Calendar cal1 = Calendar.getInstance();
                                cal1.set(year, monthOfYear, dayOfMonth, 0, 0, 0);

                                final Calendar c = Calendar.getInstance();
                                if (cal1.after(Calendar.getInstance())) {
                                    // if user choose a time that over current time, program does not process
                                } else {
                                    // process restore SMS, Call Log Event from Calendar
                                    int yearC = c.get(Calendar.YEAR);
                                    int monthC = c.get(Calendar.MONTH);
                                    int dayC = c.get(Calendar.DAY_OF_MONTH);
                                    if (alreadySet==false){
                                        DatePickerDialog datePickerDialog = new DatePickerDialog(view.getContext(),
                                                new DatePickerDialog.OnDateSetListener() {
                                                    boolean proceeded = false;
                                                    public void onDateSet(DatePicker view, int year,
                                                                          int monthOfYear, int dayOfMonth) {
                                                        Calendar cal2 = Calendar.getInstance();
                                                        cal2.set(year, monthOfYear, dayOfMonth, 23, 59, 59);
                                                        if (cal2.after(Calendar.getInstance()) || cal2.before(cal1)) {
                                                            // if user choose a time that over current time or before calendar 1, program does not process
                                                        } else if (proceeded==false){
                                                            fromTime = cal1.getTimeInMillis();
                                                            toTime = cal2.getTimeInMillis();
                                                            new ProcessCallData().execute(dialogType);
                                                            proceeded = true;
                                                        }
                                                    }
                                                },
                                                yearC, monthC, dayC);
                                        datePickerDialog.show();
                                        alreadySet = true;
                                    }
                                }
                            }
                        },
                        year, month, day);
                datePickerDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    // Create a dialog that confirm delete action for sms and call_log
    public Dialog createDeleteDialog(final Activity parentActivity, final DIALOG_TYPE dialog_type,
                                     String title, final String[] options, final List<ListRowItemEvent> items) {
        // create new AlertDialog
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(parentActivity);
        // set title
        alertDialogBuilder.setTitle(title);
        // set dialog message
        alertDialogBuilder
                .setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int option) {
                        //Toast.makeText(_context, items[item], Toast.LENGTH_SHORT).show();
                        String selected_option = options[option];
                        // close the dialog
                        dialog.cancel();

                        switch (dialog_type) {
                            case DELETE_SMS_DIALOG:
                                processDeleteSMS(parentActivity, selected_option, items);
                                break;
                            case DELETE_SCHEDULE_SMS_DIALOG:
                                processDeleteScheduleSMS(parentActivity, selected_option, items);
                                break;
                            case DELETE_CALL_DIALOG:
                                processDeleteCall(parentActivity, selected_option, items);
                                break;
                            default:
                                break;
                        }
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getListView().setSelection(-1);
        return alertDialogBuilder.create();
    }
    private void processDeleteSMS(Activity parentActivity, String option, List<ListRowItemEvent> items){
        List<String> smsIdList = new ArrayList<String>();
        List<String> calEventIdList = new ArrayList<String>();
        for (ListRowItemEvent itemEvent : items){
            smsIdList.add(itemEvent.itemId);
            calEventIdList.add(itemEvent.eventCalId);
        }
        String[] options = parentActivity.getResources().getStringArray(R.array.sms_delete_options);
        if (option.equals(options[0])){ // delete sms in inbox only
            _smsUtils.bulkDeleteSMS(parentActivity, smsIdList);
            _notificationUtils.showShortToast("delete sms in inbox = " + smsIdList);
        }else if (option.equals(options[1])){ // delete sms in calendar only
            _calendarUtils.deleteEvents(parentActivity, calEventIdList);
            _notificationUtils.showShortToast("delete sms in calendar = " + calEventIdList);
        }else if (option.equals(options[2])){ // delete sms in inbox & calendar
            _smsUtils.bulkDeleteSMS(parentActivity, smsIdList);
            _calendarUtils.deleteEvents(parentActivity, calEventIdList);
            _notificationUtils.showShortToast("deleted sms in inbox & calendar!");
        }
    }
    private void processDeleteScheduleSMS(Activity parentActivity, String option, List<ListRowItemEvent> items){
        List<String> smsIdList = new ArrayList<String>();
        List<String> calEventIdList = new ArrayList<String>();
        for (ListRowItemEvent itemEvent : items){
            smsIdList.add(itemEvent.itemId);
            calEventIdList.add(itemEvent.eventCalId);
        }
        String[] options = parentActivity.getResources().getStringArray(R.array.sms_deleteSchedule_options);
        if (option.equals(options[0])){ // Yes, let delete it now
            _calendarUtils.deleteEvents(parentActivity, calEventIdList);
            _notificationUtils.showShortToast("delete sms in calendar = " + calEventIdList);
        }
    }
    private void processDeleteCall(Activity parentActivity, String option, List<ListRowItemEvent> items){
        List<String> callIdList = new ArrayList<String>();
        List<String> calEventIdList = new ArrayList<String>();
        for (ListRowItemEvent itemEvent : items){
            callIdList.add(itemEvent.itemId);
            calEventIdList.add(itemEvent.eventCalId);
        }
        String[] options = parentActivity.getResources().getStringArray(R.array.call_delete_options);
        if (option.equals(options[0])){ // delete sms in inbox only
            _callLogUtils.bulkDeleteCall(parentActivity, callIdList);
            _notificationUtils.showShortToast("delete call_log in device = " + callIdList);
        }else if (option.equals(options[1])){ // delete sms in calendar only
            _calendarUtils.deleteEvents(parentActivity, calEventIdList);
            _notificationUtils.showShortToast("delete call_log in calendar = " + calEventIdList);
        }else if (option.equals(options[2])){ // delete sms in inbox & calendar
            _callLogUtils.bulkDeleteCall(parentActivity, callIdList);
            _calendarUtils.deleteEvents(parentActivity, calEventIdList);
            _notificationUtils.showShortToast("deleted call_log in inbox & calendar!");
        }
    }
}
