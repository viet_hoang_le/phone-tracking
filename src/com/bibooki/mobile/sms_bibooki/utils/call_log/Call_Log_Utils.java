package com.bibooki.mobile.sms_bibooki.utils.call_log;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CallLog;
import android.widget.CheckedTextView;

import com.bibooki.mobile.sms_bibooki.Fragment_CallLog;
import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.utils.*;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log_Generator;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;

import java.util.*;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Call_Log_Utils {
    public static final String _TITLE_PREFIX1 = "Call_";
    public static final String _TITLE_PREFIX2 = "Missed_Call_from";
    private static String callLog_dialog_title = "Calendar for Call Log Backup";

    static Call_Log_Utils _callLogUtils = null;
    private UtilsFactory _utilsFactory;
    private static ContactsHelper _contactsHelper;

    private static String delete_dialog_title;

    private static Call_Log_Generator _call_log_generator;

    static Context mContext;

    public static final String CALL_STORE_DB = "call_store_in_database";
    private static DBTrackCalendarOpenHelper dbTrackCalendarOpenHelper = null;
    List<String> prefixes = new ArrayList<String>();

    private Call_Log_Utils() {
        try {
            _call_log_generator = new Call_Log_Generator(mContext);
            _utilsFactory = UtilsFactory.newInstance(mContext);
            _contactsHelper = ContactsHelper.getInstance(mContext);
            prefixes.add(_TITLE_PREFIX1);
            prefixes.add(_TITLE_PREFIX2);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    // return new instance
    public static Call_Log_Utils newInstance(Context context) {
        mContext = context;
        if (_callLogUtils == null) {
            _callLogUtils = new Call_Log_Utils();
        }
        if (dbTrackCalendarOpenHelper == null){
            dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(context);
        }
        callLog_dialog_title = mContext.getString(R.string.calendar_call_selection_title);
        delete_dialog_title = mContext.getString(R.string.delete_call_dialog_title);
        return _callLogUtils;
    }

    // get tracked status of Call Log based on Calendars
    public List<Call_Log> getCallTrackedStatus(Context context, List<Call_Log> call_logList, List<Long> startTimeList, List<Long> endTimeList) {
        try{
            // get Filter
            List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
            TrackingFilter trackingFilter;
            List<String> filterPhoneNumbers = new ArrayList<String>();
            for (int i = 0; i < trackingFilterList.size(); i++)
                filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
            List<Integer> filterCallIndex = new ArrayList<Integer>();
            // finish get Filter
            // get Calendar List
            int selected_call_log_calendar_id = dbTrackCalendarOpenHelper.selectDefaultRecord().getCallCalendarId();
            // loading Call Log
            Map<String, Call_Log> callLog_hashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1,
                    startTimeList, endTimeList, selected_call_log_calendar_id, context);
            if (call_logList != null) {
                Call_Log call_log, call_logEvent;
                String callNumber;
                for (int i = 0; i < call_logList.size(); i++) {
                    call_log = call_logList.get(i);
                    // filter
                    callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
                    if (callNumber==null || callNumber.equals(""))
                        callNumber = call_log.getNumbers();
                    if (filterPhoneNumbers.contains(callNumber)) {
                        filterCallIndex.add(i);
                        continue;
                    }
                    // finish filter
                    if (callLog_hashMap.containsKey(call_log.getFixedHashValue())) {
                        call_log.isCalendarTracked = true;
                        call_logEvent = callLog_hashMap.get(call_log.getFixedHashValue());
                        if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                            call_log.isCalendarUpdated = false;
                        } else
                            call_log.isCalendarUpdated = true;
                        call_log.setCal_EventId(call_logEvent.getCal_EventId());
                    } else {
                        call_log.isCalendarTracked = false;
                        call_log.isCalendarUpdated = false;
                    }
                    call_logList.set(i, call_log);
                }
                // filter again
                String filterNumber, callCountry, filterCountry;
                for (int i = 0; i < trackingFilterList.size(); i++) {
                    trackingFilter = trackingFilterList.get(i);
                    selected_call_log_calendar_id = trackingFilter.getCallCalendarId();
                    if (selected_call_log_calendar_id < 1)
                        continue;
                    callLog_hashMap = _utilsFactory.getCalendarUtils().getCalendarCALL(-1, -1, startTimeList, endTimeList, selected_call_log_calendar_id, context);
                    int callIndex;
                    for (int j = 0; j < filterCallIndex.size(); j++) {
                        callIndex = filterCallIndex.get(j);
                        call_log = call_logList.get(callIndex);
                        callNumber = _contactsHelper.removePhoneNumberPrefix(call_log.getNumbers());
                        if (callNumber==null || callNumber.equals(""))
                            callNumber = call_log.getNumbers();
                        //callCountry = _contactUtils.getCountryCode(sms.getNumbers());
                        filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                        //filterCountry = _contactUtils.getCountryCode(trackingFilter.getPhonenumber());
                        if (!callNumber.equals(filterNumber)) {
                            continue;
                        }
                        if (callLog_hashMap.containsKey(call_log.getFixedHashValue())) {
                            call_log.isCalendarTracked = true;
                            call_logEvent = callLog_hashMap.get(call_log.getFixedHashValue());
                            if (!call_logEvent.getCalendarTitleHash().equals(call_log.getCalendarTitleHash())) {
                                call_log.isCalendarUpdated = false;
                            } else
                                call_log.isCalendarUpdated = true;
                            call_log.setCal_EventId(call_logEvent.getCal_EventId());
                        } else {
                            call_log.isCalendarTracked = false;
                            call_log.isCalendarUpdated = false;
                        }
                        call_logList.set(callIndex, call_log);
                    }
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return call_logList;
    }

    // return List of call log
    public List<Call_Log> getCallLog_from_to(Context context, Long from, Long to, int limit, int offset) {
        Cursor curCallLog = null;
        Uri uri = CallLog.Calls.CONTENT_URI;
        List<Call_Log> call_logs = new ArrayList<Call_Log>();
        Call_Log call_log;
        String selection = null, limitSelection;
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        try {
            if ((from == null || from <= 0) && (to == null || to <= 0))
                selection = null;
            else if ((from == null || from <= 0) && (to != null && to > 0))
                selection = CallLog.Calls.DATE + " < " + to;
            else if (from > 0 && (to == null || to <= 0)) {
                selection = CallLog.Calls.DATE + " > " + from;
            } else if (from > 0 && to > 0) {
                selection = CallLog.Calls.DATE + " > " + from + " AND " + CallLog.Calls.DATE + " < " + to;
            }
            limitSelection = CallLog.Calls.DATE + " DESC ";
            if (limit > 0) {
                limitSelection = limitSelection + " LIMIT " + limit;
            }
            if (offset > 0) {
                limitSelection = limitSelection + " OFFSET " + offset;
            }
            curCallLog = context.getContentResolver().query(uri, null, selection, null, limitSelection);
            while (curCallLog.moveToNext()) {
                call_log = _call_log_generator.getCallLogfromCursor(curCallLog);
                    // adding new call log
                if (call_log==null)
                    continue;
                call_logs.add(call_log);
                // set start and end time list to use in further
                startTimeList.add((call_log.getFromTime() / 1000) * 1000);
                endTimeList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
            }
            call_logs = getCallTrackedStatus(context, call_logs, startTimeList, endTimeList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curCallLog.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return call_logs;
    }

    // loading sms with limited number
    public List<Call_Log> getLimitCall(Context context, int limit, int offset) {
        Cursor curCallLog = null;
        Uri uri = CallLog.Calls.CONTENT_URI;
        List<Call_Log> call_logs = new ArrayList<Call_Log>();
        Call_Log call_log;
        String selection = null;
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        try {
            while (call_logs.size() < limit){
                if (curCallLog!=null && curCallLog.getCount()==0)
                    break;
                selection = CallLog.Calls.DATE + " DESC ";
                if (limit > 0) {
                    selection = selection + " LIMIT " + limit;
                }
                if (offset > 0) {
                    selection = selection + " OFFSET " + offset;
                }
                curCallLog = context.getContentResolver().query(uri, null, null, null, selection);
                while (curCallLog.moveToNext()) {
                    call_log = _call_log_generator.getCallLogfromCursor(curCallLog);
                    // adding new call log
                    if (call_log==null)
                        continue;
                    call_logs.add(call_log);
                    // set start and end time list to use in further
                    startTimeList.add((call_log.getFromTime() / 1000) * 1000);
                    endTimeList.add(((call_log.getFromTime() + call_log.getDurationMiliSecond()) / 1000) * 1000);
                }
                offset = offset + limit;
                Fragment_CallLog.updateStaticOffset(offset);
            }
            call_logs = getCallTrackedStatus(context, call_logs, startTimeList, endTimeList);
            //}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curCallLog.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return call_logs;
    }

    public Call_Log getCallLogFromCalendarEvent(ContentValues contentValues){
        Call_Log call_log = _call_log_generator.convertEventToCallLog(contentValues);
        return call_log;
    }

    // create the calendar dialog for call log tracking
    public Dialog pickingCalendarDialog(Context _context, Activity activity, final CheckedTextView cView) {
        Dialog dialog;
        String title = callLog_dialog_title;
        String[] items = _utilsFactory.getCalendarUtils().getCalendarDisplayName(activity);
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity,
                Dialog_Utils.DIALOG_TYPE.CALENDAR_CHOOSING_DIALOG, items, title, cView, "", CALL_STORE_DB);
        return dialog;
    }

    public Dialog calendarPicker(Context _context, Activity activity, final CheckedTextView cView) {
        Dialog dialog;
        String title = callLog_dialog_title;
        String[] items = _utilsFactory.getCalendarUtils().getCalendarDisplayName(activity);
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity,
                Dialog_Utils.DIALOG_TYPE.CALENDAR_CHOOSING_DIALOG, items, title, cView, "", null);
        return dialog;
    }

    // return map of calendars of Phone, if time > 0, retrieve Call Log at a specific time period
    public List<Call_Log> get_Call_Log_At_Time(Context context, List<Long> timeList) {
        Cursor curCallLog = null;
        Uri uri;
        List<Call_Log> call_logs = new ArrayList<Call_Log>();
        Call_Log call_log;
        String selection = null;
        try {
            if (timeList != null && timeList.size() > 0) {
                selection = CallLog.Calls.DATE + " IN ( ";
                for (int i = 0; i < timeList.size(); i++) {
                    selection += timeList.get(i);
                    if (i + 1 < timeList.size())
                        selection += ",";
                }
                selection += ") ";
            }
            uri = CallLog.Calls.CONTENT_URI;
            curCallLog = context.getContentResolver().query(uri, null, selection, null, null);

            while (curCallLog.moveToNext()) {
                call_log = _call_log_generator.getCallLogfromCursor(curCallLog);
                // adding new call log
                if (call_log==null)
                    continue;
                call_logs.add(call_log);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curCallLog.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return call_logs;
    }

    /*
    Get Call Log Event from Calendar
    * */
    public List<Call_Log> get_All_Call_Log_From_Calendar(Context context, Calendar calendarFrom, Calendar calendarTo, String phoneNumber, int callCal_id) {
        List<Call_Log> call_log_List = new ArrayList<Call_Log>();
        int selected_Call_Log_calendar_id = callCal_id;
        try {
            if (selected_Call_Log_calendar_id != -1) {
                List<ContentValues> contentValuesList = null;
                if (calendarFrom == null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().getAllCalendarEvents(prefixes, phoneNumber, selected_Call_Log_calendar_id, context);
                } else if (calendarFrom != null && calendarTo == null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().get_From_to_Now_CalendarEvents(prefixes, phoneNumber, selected_Call_Log_calendar_id, context, calendarFrom);
                } else if (calendarFrom != null && calendarTo != null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().get_From_To_CalendarEvents(prefixes, phoneNumber, selected_Call_Log_calendar_id, context, calendarFrom, calendarTo);
                }

                if (contentValuesList.size() > 0) {
                    Call_Log call_Log;
                    for (ContentValues contentValues : contentValuesList) {
                        call_Log = _call_log_generator.convertEventToCallLog(contentValues);
                        if (call_Log != null)
                            call_log_List.add(call_Log);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return call_log_List;
    }


    public List<Call_Log> getUntrackedCall(Context context, List<Call_Log> callLogs, Long from, Long to, int limit, int offset) {
        List<Call_Log> resultList = new ArrayList<Call_Log>();
        if (callLogs.size() <= 0)
            callLogs = getCallLog_from_to(context, from, to, limit, offset);
        for (int i = 0; i < callLogs.size(); i++) {
            if (!callLogs.get(i).isCalendarTracked) {
                if (offset > 0) {
                    offset--;
                    continue;
                }
                resultList.add(callLogs.get(i));
            }
        }
        while (limit > 0 && resultList.size() < limit && callLogs.size() > 0) {
            to = callLogs.get(callLogs.size() - 1).getFromTime();
            callLogs = getCallLog_from_to(context, from, to, limit, 0);
            for (int i = 0; i < callLogs.size(); i++) {
                if (!callLogs.get(i).isCalendarTracked) {
                    if (offset > 0) {
                        offset--;
                        continue;
                    }
                    resultList.add(callLogs.get(i));
                }
            }
        }
        return resultList;
    }

    public List<Call_Log> getOutdatedCall(Context context, List<Call_Log> callLogs, Long from, Long to, int limit, int offset) {
        List<Call_Log> resultList = new ArrayList<Call_Log>();
        if (callLogs.size() <= 0)
            callLogs = getCallLog_from_to(context, from, to, limit, offset);
        for (int i = 0; i < callLogs.size(); i++) {
            if (callLogs.get(i).isCalendarTracked && !callLogs.get(i).isCalendarUpdated) {
                if (offset > 0) {
                    offset--;
                    continue;
                }
                resultList.add(callLogs.get(i));
            }
        }
        while (limit > 0 && resultList.size() < limit && callLogs.size() > 0) {
            to = callLogs.get(callLogs.size() - 1).getFromTime();
            callLogs = getCallLog_from_to(context, from, to, limit, 0);
            for (int i = 0; i < callLogs.size(); i++) {
                if (callLogs.get(i).isCalendarTracked && !callLogs.get(i).isCalendarUpdated) {
                    if (offset > 0) {
                        offset--;
                        continue;
                    }
                    resultList.add(callLogs.get(i));
                }
            }
        }
        return resultList;
    }

    public List<Call_Log> getUnprocessedCall(Context context, List<Call_Log> callLogs, Long from, Long to, int limit, int offset) {
        List<Call_Log> resultList = new ArrayList<Call_Log>();
        if (callLogs.size() <= 0)
            callLogs = getCallLog_from_to(context, from, to, limit, offset);
        for (int i = 0; i < callLogs.size(); i++) {
            if (!callLogs.get(i).isCalendarTracked || !callLogs.get(i).isCalendarUpdated) {
                if (offset > 0) {
                    offset--;
                    continue;
                }
                resultList.add(callLogs.get(i));
            }
        }
        while (limit > 0 && resultList.size() < limit && callLogs.size() > 0) {
            to = callLogs.get(callLogs.size() - 1).getFromTime();
            callLogs = getCallLog_from_to(context, from, to, limit, 0);
            for (int i = 0; i < callLogs.size(); i++) {
                if (!callLogs.get(i).isCalendarTracked || !callLogs.get(i).isCalendarUpdated) {
                    if (offset > 0) {
                        offset--;
                        continue;
                    }
                    resultList.add(callLogs.get(i));
                }
            }
        }
        return resultList;
    }

    // create the dialog to confirm delete call items
    public Dialog getDeleteOptionDialog(Activity parentActivity, List<ListRowItemEvent> selectedItems) {
        Dialog dialog;
        String title = delete_dialog_title;
        String[] options = parentActivity.getResources().getStringArray(R.array.call_delete_options);
        dialog = _utilsFactory.getDialog_Utils()
                .createDeleteDialog(parentActivity, Dialog_Utils.DIALOG_TYPE.DELETE_CALL_DIALOG,
                        title, options, selectedItems);
        return dialog;
    }
    /*
    * Delete all Call Log from database based on id
    * */
    public int bulkDeleteCall(Context context, List<String> callIdList) {
        final Uri uri = CallLog.Calls.CONTENT_URI;
        int result = -1;
        try {
            if (callIdList == null) {
                return result;
            }
            String[] args = new String[callIdList.size()];
            String selection = "_id IN (";
            int i=0;
            for (String id : callIdList){
                selection += "?,";
                args[i] = id;
                i++;
            }
            selection = selection.substring(0, selection.length()-1) + ") ";
            result = context.getContentResolver().delete(uri, selection, args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    // Retrieve Call by its _ID
    public Call_Log getCallLog_byId(Context context, String callId) {
        Cursor curCallLog = null;
        Uri uri = CallLog.Calls.CONTENT_URI;
        Call_Log call_log = null;
        String selection = null;
        try {
            if (callId != null && Long.parseLong(callId) > 0) {
                selection = BaseColumns._ID + " = " + callId;
            }
            curCallLog = context.getContentResolver().query(uri, null, selection, null, null);

            while (curCallLog.moveToNext()) {
                // create new SMS for storing to List of SMS
                call_log = _call_log_generator.getCallLogfromCursor(curCallLog);
                if (call_log!=null)
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curCallLog.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return call_log;
    }
    // loading sms with limited number
    public List<Call_Log> getCallLog_byPhoneNumber(Context context, int limit, int offset, String phoneNumber) {
        Cursor curCall = null;
        Uri uri = CallLog.Calls.CONTENT_URI;
        List<Call_Log> callLogList = new ArrayList<Call_Log>();
        Call_Log callLog;
        String selection;
        String sortOrder = CallLog.Calls.DATE + " desc";
        String[] args = {"%"+_contactsHelper.removePhoneNumberPrefix(phoneNumber)};
        List<Long> startTimeList = new ArrayList<Long>();
        List<Long> endTimeList = new ArrayList<Long>();
        try {
            selection = CallLog.Calls.NUMBER + " like ?";
            if (limit > 0) {
                sortOrder = sortOrder + " LIMIT " + limit;
            }
            if (offset >= 0) {
                sortOrder = sortOrder + " OFFSET " + offset;
            }
            curCall = context.getContentResolver().query(uri, null, selection, args, sortOrder);
            // reading callLog

            int callType;

            while (curCall.moveToNext()) {
                // create new Call Log for storing to List of Call
                callLog = _call_log_generator.getCallLogfromCursor(curCall);
                if (callLog==null)
                    continue;
                callLogList.add(callLog);
                // set start and end time list to use in further
                startTimeList.add((callLog.getFromTime() / 1000) * 1000);
                endTimeList.add(((callLog.getFromTime() + callLog.getDurationMiliSecond()) / 1000) * 1000);

            }
            callLogList = getCallTrackedStatus(context, callLogList, startTimeList, endTimeList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curCall.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return callLogList;
    }
}
