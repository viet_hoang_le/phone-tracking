package com.bibooki.mobile.sms_bibooki.utils;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.widget.Toast;

import com.bibooki.mobile.sms_bibooki.R;

public class NotificationUtils {
    static NotificationUtils _notificationUtils = null;
    static Context mContext;
    static AlertDialog.Builder alertbox;
    //protected static Handler mHandler = new Handler();
    private NotificationUtils(){
        alertbox = new AlertDialog.Builder(mContext);
    }
    // return new instance
    public static NotificationUtils getInstance(Context context){
        mContext = context;
        if (_notificationUtils == null)
            _notificationUtils = new NotificationUtils();
        // setup progress spinner
        return _notificationUtils;
    }
    public void notifyMessage(String title, String content){
        // setup notification
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext).setSmallIcon(R.drawable.ic_notification);
        mBuilder.setContentTitle(title)
                .setContentText(content);
        // Hide the notification after its selected
        /*
        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);*/
    }

    public void showShortToast(String content){
        Toast.makeText(mContext, content, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String content){
        Toast.makeText(mContext, content, Toast.LENGTH_SHORT).show();
    }

    public void showInfoBox(final String title, final String content){
        // set the message to display
        // show it
        alertbox.setTitle(Html.fromHtml(title));
        alertbox.setMessage(Html.fromHtml(content));
        alertbox.setPositiveButton("Got it!",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertbox.show();
    }
}
