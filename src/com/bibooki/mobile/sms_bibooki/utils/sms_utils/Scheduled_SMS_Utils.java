package com.bibooki.mobile.sms_bibooki.utils.sms_utils;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.*;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Scheduled_SMS_Utils {

    static Context mContext;

    private static UtilsFactory _utilsFactory;
    private static ContactsHelper _contactsHelper;
    private static Scheduled_SMS_Utils scheduled_sms_utils;
    private static Format simpleDateTimeFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

    private final String titlePrefix = "SMS to";
    List<String> prefixes = new ArrayList<String>();
    private final String contentPrefix = " = ";
    private AlarmSendingSMSReceiver alarmSendingSMSReceiver = new AlarmSendingSMSReceiver();

    public Scheduled_SMS_Utils() {
        prefixes.add(titlePrefix);
    }

    // return new instance
    public static Scheduled_SMS_Utils getInstance(Context context){
        if (scheduled_sms_utils == null)
            scheduled_sms_utils = new Scheduled_SMS_Utils();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _contactsHelper = ContactsHelper.getInstance(mContext);
        return scheduled_sms_utils;
    }

    public List<SMS> getScheduledSMS(int calendar_id, final Context context, int limit, int offset){
        List<SMS> smsList = new ArrayList<SMS>();
        // get list of events from the selected calendar from the current time to future
        List<ContentValues> contentValuesList = _utilsFactory.getCalendarUtils().get_Now_to_Future_ScheduleSMS(prefixes, null, calendar_id, context, limit, offset);
        ContentValues contentValues;
        String eventId, eventTitle, eventDescription;
        long startTime;
        String smsBody;
        Map<String, String> numberNames;
        // setup notification
        for (int i=0; i < contentValuesList.size(); i++){
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_notification);
            contentValues = contentValuesList.get(i);
            // get values from event
            eventId = contentValues.getAsString(Calendar_Utils._EVENT_ID);
            eventTitle = contentValues.getAsString(Calendar_Utils._EVENT_TITLE);
            eventDescription = contentValues.getAsString(Calendar_Utils._EVENT_DESCRIPTION);
            startTime = contentValues.getAsLong(Calendar_Utils._EVENT_START);
            // only get events that have start time before current time
            if (startTime > System.currentTimeMillis()){
                // process to get Contact and SMS body
                if (eventTitle.toLowerCase().indexOf(titlePrefix.toLowerCase())==0)
                    eventTitle = eventTitle.substring(titlePrefix.length());
                if (eventTitle.contains(contentPrefix)){
                    eventDescription = eventTitle.substring(eventTitle.indexOf(contentPrefix)).replaceFirst(contentPrefix, "");
                    eventTitle = eventTitle.substring(0, eventTitle.indexOf(contentPrefix));
                }

                numberNames = _contactsHelper.getNumberNames_by_inputString(eventTitle);
                smsBody = eventDescription;
                SMS sms = new SMS();
                if (numberNames.size()>0){
                    sms.setNumbers((String) (numberNames.keySet().toArray())[0]);
                    sms.setContactNames(numberNames.get(sms.getNumbers()));
                    sms.setSmsBody(smsBody);
                    sms.setCal_event_id(eventId);
                    sms.setStartTime(startTime);
                    sms.setSmsTime(startTime);
                    sms.setType(SMS.SEND_TYPE);

                    // Notify users if detect contact
                    mBuilder.setContentTitle("sms to " + sms.getContactNames())
                            .setContentText("at "+simpleDateTimeFormat.format(startTime)+" -- "+smsBody);
                }
                else{
                    sms.setSmsBody(smsBody);
                    sms.setCal_event_id(eventId);
                    sms.setStartTime(startTime);
                    sms.setSmsTime(startTime);
                    sms.setType(SMS.SEND_TYPE);

                    // Notify users if cannot detect contact
                    mBuilder.setContentTitle("error sms: " + eventTitle)
                            .setContentText("at "+simpleDateTimeFormat.format(startTime)+" -- "+smsBody);
                }
                // Hide the notification after its selected
                /*
                Notification notification = mBuilder.build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                notificationManager.notify(0, notification);*/
                // get return data
                smsList.add(sms);
            }
        }
        return smsList;
    }

    public void onetimeTimer(Context context, Long atTime, List<SMS> smsList){
        if(alarmSendingSMSReceiver != null){
            alarmSendingSMSReceiver.CancelAlarm(context);
            alarmSendingSMSReceiver.setOnetimeTimer(context, atTime, smsList);
        }
    }

}
