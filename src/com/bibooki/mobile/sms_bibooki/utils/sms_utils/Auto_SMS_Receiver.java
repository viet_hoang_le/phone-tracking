package com.bibooki.mobile.sms_bibooki.utils.sms_utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.bibooki.mobile.sms_bibooki.utils.*;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Auto_SMS_Receiver extends BroadcastReceiver
{
    private static final String TAG = "Auto_SMS_Receiver";
    private Context mContext;
    private static UtilsFactory _utilsFactory;
    private DBTrackCalendarOpenHelper _dbTrackCalendarOpenHelper;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(mContext);

        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        //Acquire the lock
        wl.acquire();

        scheduleSMS();

        //Release the lock
        wl.release();
    }
    public void scheduleSMS(){
        TrackCalendarDbItem dbItem = _dbTrackCalendarOpenHelper.selectScheduleRecord();
        if (dbItem!=null){
            int selected_scheduled_SMS_calendar_id = dbItem.getSmsCalendarId();
            long fixedTime = (_utilsFactory.getPreferencesHelper().read_Auto_Sending_Time(mContext)/1000) * 1000;
            long currentTime = ((new Date().getTime())/1000)*1000;
            if (selected_scheduled_SMS_calendar_id != -1 && fixedTime!=currentTime){
                // get list scheduled sms from the current time from the selected calendar
                List<SMS> scheduledSMS_List = _utilsFactory.getScheduled_SMS_Utils().getScheduledSMS(selected_scheduled_SMS_calendar_id, mContext, 100, 0);
                // set alarm Sending SMS service 1 time in the future
                if (scheduledSMS_List.size()>0){
                    // get list of sms that will be sent next time
                    List<SMS> smsList = new ArrayList<SMS>();
                    int i = 0;
                    SMS sms = scheduledSMS_List.get(0);
                    Long nextStartTime = sms.getStartTime();
                    while (sms.getStartTime() == nextStartTime && i<scheduledSMS_List.size()){
                        smsList.add(sms);
                        i++;
                        if (i >= scheduledSMS_List.size())
                            break;
                        sms = scheduledSMS_List.get(i);
                    }
                    _utilsFactory.getScheduled_SMS_Utils().onetimeTimer(mContext, nextStartTime, smsList);
                    _utilsFactory.getPreferencesHelper().store_Auto_Sending_Time(mContext, nextStartTime);
                }
                // Sending SMS & Write SMS into Database
            }
        }
    }
}
