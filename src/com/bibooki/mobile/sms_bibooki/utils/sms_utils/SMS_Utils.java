package com.bibooki.mobile.sms_bibooki.utils.sms_utils;

import android.app.*;
import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.widget.CheckedTextView;

import com.bibooki.mobile.sms_bibooki.Fragment_SMS;
import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.rows.ListRowItemEvent;
import com.bibooki.mobile.sms_bibooki.utils.*;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS_Generator;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;

import java.util.*;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class SMS_Utils {

    public static String _UNKNOWN_PHONE_NUMBER = "Unidentified Phone Number";
    public static final String _TITLE_PREFIX = "SMS_";

    private static String sms_dialog_title;
    private static String sms_schedule_dialog_title;
    private static String delete_dialog_title;
    private static String deleteSchedule_dialog_title;

    static SMS_Utils _smsUtils = null;
    static UtilsFactory _utilsFactory;
    private static NotificationUtils _notificationUtils;
    private static ContactsHelper _contactsHelper;
    static Context mContext;

    public static final String SMS_STORE_DB = "sms_store_in_database";
    public static final String SCHEDULE_STORE_DB = "schedule_store_in_database";
    private static DBTrackCalendarOpenHelper dbTrackCalendarOpenHelper = null;

    private static SMS_Generator _sms_generator;
    List<String> prefixes = new ArrayList<String>();

    private SMS_Utils() {
        try {
            prefixes.add(_TITLE_PREFIX);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    // return new instance
    public static SMS_Utils getInstance(Context context) {
        if (_smsUtils == null)
            _smsUtils = new SMS_Utils();
        if (dbTrackCalendarOpenHelper == null){
            dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(context);
        }
        mContext = context;
        _sms_generator = new SMS_Generator(mContext);
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _contactsHelper = ContactsHelper.getInstance(mContext);
        _notificationUtils = _utilsFactory.getNotificationUtils();

        sms_dialog_title = mContext.getString(R.string.calendar_sms_selection_title);
        sms_schedule_dialog_title = mContext.getString(R.string.calendar_schedule_selection_title);
        delete_dialog_title = mContext.getString(R.string.delete_sms_dialog_title);
        deleteSchedule_dialog_title = mContext.getString(R.string.deleteSchedule_sms_dialog_title);
        return _smsUtils;
    }

    // get tracked status of SMS based on Calendars
    public List<SMS> getSMSTrackedStatus(Context context, List<SMS> smsList, List<Long> startTimeList) {
        // get Filter
        List<TrackingFilter> trackingFilterList = _utilsFactory.getFilterUtils().selectRecords();
        TrackingFilter trackingFilter;
        List<String> filterPhoneNumbers = new ArrayList<String>();
        for (int i=0; i<trackingFilterList.size(); i++)
            filterPhoneNumbers.add(_contactsHelper.removePhoneNumberPrefix(trackingFilterList.get(i).getPhonenumber()));
        List<Integer> filterSMSIndex = new ArrayList<Integer>();
        // finish get Filter
        // get Calendar List
        int selected_sms_calendar_id = dbTrackCalendarOpenHelper.selectDefaultRecord().getSmsCalendarId();
        // checking if SMS is tracked or not in case Calendar Id is set
        Map<String, SMS> sms_calendarHashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, startTimeList, startTimeList, selected_sms_calendar_id, context);
        String smsNumber;
        if (smsList!=null) {
            SMS sms, smsEvent, smsMail;
            for (int i=0; i< smsList.size(); i++){
                sms = smsList.get(i);
                // filter specific Calendar
                smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
                if (smsNumber==null || smsNumber.equals(""))
                    smsNumber = sms.getNumbers();
                if (filterPhoneNumbers.contains(smsNumber)){
                    filterSMSIndex.add(i);
                }else{
                    // finish specific Calendar filter
                    if (sms_calendarHashMap.containsKey(sms.getFixedHashValue())){
                        sms.isCalendarTracked = true;
                        smsEvent = sms_calendarHashMap.get(sms.getFixedHashValue());
                        if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())){
                            sms.isCalendarUpdated = false;
                        }else
                            sms.isCalendarUpdated = true;
                        sms.setCal_event_id(smsEvent.getCal_event_id());
                    } else{
                        sms.isCalendarTracked = false;
                        sms.isCalendarUpdated = false;
                    }
                }
                smsList.set(i, sms);
            }
            // filter specific Calendar again
            String filterNumber, smsCountry, filterCountry;
            for (int i=0; i<trackingFilterList.size(); i++){
                trackingFilter = trackingFilterList.get(i);
                selected_sms_calendar_id = trackingFilter.getSmsCalendarId();
                if (selected_sms_calendar_id < 1)
                    continue;
                Map<String, SMS> smsFilterHashMap = _utilsFactory.getCalendarUtils().getCalendarSMS(-1, -1, startTimeList, startTimeList, selected_sms_calendar_id, context);
                int smsIndex;
                for (int j=0; j< filterSMSIndex.size(); j++){
                    smsIndex = filterSMSIndex.get(j);
                    sms = smsList.get(smsIndex);
                    smsNumber = _contactsHelper.removePhoneNumberPrefix(sms.getNumbers());
                    if (smsNumber==null || smsNumber.equals(""))
                        smsNumber = sms.getNumbers();
                    //smsCountry = _contactsHelper.getCountryCode(sms.getNumbers());
                    filterNumber = _contactsHelper.removePhoneNumberPrefix(trackingFilter.getPhonenumber());
                    //filterCountry = _contactsHelper.getCountryCode(trackingFilter.getPhonenumber());
                    if (!smsNumber.equals(filterNumber)){
                        continue;
                    }
                    if (smsFilterHashMap.containsKey(sms.getFixedHashValue())){
                        sms.isCalendarTracked = true;
                        smsEvent = smsFilterHashMap.get(sms.getFixedHashValue());
                        if (!smsEvent.getCalendarTitleHash().equals(sms.getCalendarTitleHash())){
                            sms.isCalendarUpdated = false;
                        }else
                            sms.isCalendarUpdated = true;
                        sms.setCal_event_id(smsEvent.getCal_event_id());
                    } else{
                        sms.isCalendarTracked = false;
                        sms.isCalendarUpdated = false;
                    }
                    smsList.set(smsIndex, sms);
                }
            }
        }
        return smsList;
    }

    // return map of calendars of Phone, if from > 0, retrieve SMSs from time period to current time
    // loading sms
    public List<SMS> getSMS_from_to(Context context, Long from, Long to, int limit, int offset) {
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        List<SMS> smsList = new ArrayList<SMS>();
        SMS sms;
        String selection = null, limitSelection;
        List<Long> startTimeList = new ArrayList<Long>();
        try {
            if ((from == null || from <=0) && (to == null || to <= 0))
                selection = null;
            else if ((from == null || from <=0) && (to != null && to > 0))
                selection = "date < " + to;
            else if (from > 0 && (to == null || to <= 0)) {
                selection = "date > " + from;
            }else if (from > 0 && to > 0){
                selection = "date > " + from + " AND date < "+to;
            }
            limitSelection = "date DESC ";
            if (limit>0){
                limitSelection = limitSelection + " LIMIT " + limit ;
            }
            if (offset>0){
                limitSelection = limitSelection + " OFFSET " + offset;
            }
            curSms = context.getContentResolver().query(uri, null, selection, null, limitSelection);

            while (curSms.moveToNext()) {
                    // create new SMS for storing to List of SMS
                    sms = _sms_generator.getSMSfromCursor(curSms);
                if (sms==null)
                    continue;
                    // add sms into list
                    smsList.add(sms);
                    // store smsTime
                    startTimeList.add((sms.getSmsTime()/1000)*1000);
                }
            smsList = getSMSTrackedStatus(context, smsList, startTimeList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return smsList;
    }

    // loading sms with limited number
    public List<SMS> getSmsByPhoneNumber(Context context, int limit, int offset, String phoneNumber) {
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        List<SMS> smsList = new ArrayList<SMS>();
        SMS sms;
        String selection;
        String sortOrder = SMS.SMS_COLUMN_DATE + " desc";
        String[] args = {"%"+_contactsHelper.removePhoneNumberPrefix(phoneNumber)};
        List<Long> startTimeList = new ArrayList<Long>();
        try {
            selection = SMS.SMS_COLUMN_ADDRESS + " like ?";
            if (limit > 0) {
                sortOrder = sortOrder + " LIMIT " + limit;
            }
            if (offset >= 0) {
                sortOrder = sortOrder + " OFFSET " + offset;
            }
            curSms = context.getContentResolver().query(uri, null, selection, args, sortOrder);
            // reading sms

            int smsType;

            while (curSms.moveToNext()) {
                    smsType = curSms.getInt(curSms.getColumnIndexOrThrow("type"));
                // if sms is draft, ignore it
                 if (smsType==3)
                    continue;
                    // if the sms is not a draft
                    // create new SMS for storing to List of SMS
                    sms = _sms_generator.getSMSfromCursor(curSms);
                if (sms==null)
                    continue;
                    smsList.add(sms);
                    // store smsTime
                    startTimeList.add((sms.getSmsTime()/1000)*1000);
                }
            smsList = getSMSTrackedStatus(context, smsList, startTimeList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return smsList;
    }

        // Retrieve SMSs from by ThreadID
    public List<SMS> getSMSs_byThreadId(Context context, Long threadID) {// loading sms
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        List<SMS> smsList = new ArrayList<SMS>();
        SMS sms;
        String selection = null;
        try {
            if (threadID != null && threadID > 0) {
                selection = "thread_id = " + threadID;
            }
            curSms = context.getContentResolver().query(uri, null, selection, null, null);

            while (curSms.moveToNext()) {
                // create new SMS for storing to List of SMS
                sms = _sms_generator.getSMSfromCursor(curSms);
                if (sms==null)
                    continue;
                smsList.add(sms);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return smsList;
    }
    // Retrieve SMS by its _ID
    public SMS getSMS_byId(Context context, String smsId) {// loading sms
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        SMS sms = null;
        String selection = null;
        try {
            if (smsId != null && Long.parseLong(smsId) > 0) {
                selection = BaseColumns._ID + " = " + smsId;
            }
            curSms = context.getContentResolver().query(uri, null, selection, null, null);

            while (curSms.moveToNext()) {
                // create new SMS for storing to List of SMS
                sms = _sms_generator.getSMSfromCursor(curSms);
                if (sms!=null)
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return sms;
    }
    // loading sms with limited number
    public List<SMS> getLimitSMS(Context context, int limit, int offset) {
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        List<SMS> smsList = new ArrayList<SMS>();
        SMS sms;
        String selection;
        List<Long> startTimeList = new ArrayList<Long>();
        try {
            int smsType;
            while (smsList.size() < limit){
                if (curSms!=null && curSms.getCount()==0)
                    break;
                selection = "date DESC";
                if (limit > 0) {
                    selection = selection + " LIMIT " + limit;
                }
                if (offset >= 0) {
                    selection = selection + " OFFSET " + offset;
                }
                // reading sms
                curSms = context.getContentResolver().query(uri, null, null, null, selection);
                while (curSms.moveToNext()) {
                    smsType = curSms.getInt(curSms.getColumnIndexOrThrow("type"));
                    // if sms is draft, ignore it
                    if (smsType==3)
                        continue;
                    // if the sms is not a draft
                    // create new SMS for storing to List of SMS
                    sms = _sms_generator.getSMSfromCursor(curSms);
                    if (sms==null)
                        continue;
                    smsList.add(sms);
                    // store smsTime
                    startTimeList.add((sms.getSmsTime()/1000)*1000);
                }
                offset = offset + limit;
                Fragment_SMS.updateStaticOffset(offset);
            }
            smsList = getSMSTrackedStatus(context, smsList, startTimeList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return smsList;
    }

    // return map of calendars of Phone, if time > 0, retrieve SMSs at a specific time period
    public List<SMS> get_SMS_At_Time(Context context, List<Long> timeList) {// loading sms
        Cursor curSms = null;
        Uri uri = Uri.parse("content://sms");
        List<SMS> smsList = new ArrayList<SMS>();
        SMS sms;
        String selection = null;
        String encryptedString, title = null, description = null;
        try {
            if (timeList != null && timeList.size() > 0) {
                selection = "date IN ( ";
                for (int i=0; i<timeList.size(); i++){
                    selection += timeList.get(i);
                    if (i+1 < timeList.size())
                        selection += ",";
                }
                selection += ") ";
            }
            curSms = context.getContentResolver().query(uri, null, selection, null, null);
            // reading sms
            String smsBody, number, contactName;
            long smsTime;
            int smsType;
            // reading owner name and number
            String ownerName = "";
            String ownerNumber = _contactsHelper.getMyPhoneNumber();
            if (ownerNumber == null || ownerNumber.equals("") || ownerNumber.equals("?"))
                ownerNumber = _UNKNOWN_PHONE_NUMBER;
            ownerName = _contactsHelper.getContactDisplayNameByNumber(ownerNumber);

            while (curSms.moveToNext()) {
                    // create new SMS for storing to List of SMS
                    sms = _sms_generator.getSMSfromCursor(curSms);
                if (sms==null)
                    continue;
                    smsList.add(sms);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                curSms.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return smsList;
    }

    public SMS getSmsFromCalendarEvent(ContentValues contentValues){
        SMS sms = _sms_generator.convertEventToSMS(contentValues);
        return sms;
    }

    // create the calendar dialog for sms scheduling
    public Dialog pickingScheduledCalendarDialog(Context _context, Activity activity, final CheckedTextView cView) {
        Dialog dialog;
        String title = sms_schedule_dialog_title;
        String[] items = _utilsFactory.getCalendarUtils().getCalendarDisplayName(activity);
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity,
                Dialog_Utils.DIALOG_TYPE.CALENDAR_CHOOSING_DIALOG, items, title, cView, "", SCHEDULE_STORE_DB);
        return dialog;
    }

    // create the calendar dialog for sms tracking
    public Dialog pickingCalendarDialog(Context _context, Activity activity, final CheckedTextView cView) {
        Dialog dialog;
        String title = sms_dialog_title;
        String[] items = _utilsFactory.getCalendarUtils().getCalendarDisplayName(activity);
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity,
                Dialog_Utils.DIALOG_TYPE.CALENDAR_CHOOSING_DIALOG, items, title, cView, "", SMS_STORE_DB);
        return dialog;
    }
    // create the calendar dialog for sms tracking
    public Dialog calendarPicker(Context _context, Activity activity, final CheckedTextView cView) {
        Dialog dialog;
        String title = sms_dialog_title;
        String[] items = _utilsFactory.getCalendarUtils().getCalendarDisplayName(activity);
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity, Dialog_Utils.DIALOG_TYPE.CALENDAR_CHOOSING_DIALOG,
                items, title, cView, "", null);
        return dialog;
    }

    // get sms content for title
    public String getSmsTitleContent(Context context, SMS sms) {
        int type = _utilsFactory.getPreferencesHelper().read_SMS_EventTitle(context);
        String result = sms.getSmsBody();
        switch (type) {
            case -1:
                result = "";
                break;
            case 0:
                result = result.substring(0, result.length() > 9 ? 9 : result.length());
                break;
            case 1:
                break;
            default:
        }
        return result;
    }
    /*
    Get SMS Event from Calendar
    * */

    public List<SMS> get_All_SMS_From_Calendar(Context context, Calendar calendarFrom, Calendar calendarTo, String phoneNumber, int smsCal_id) {
        List<SMS> smsList = new ArrayList<SMS>();
        int selected_sms_calendar_id = smsCal_id;
        try {
            if (selected_sms_calendar_id >0) {
                List<ContentValues> contentValuesList = null;

                if (calendarFrom == null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().getAllCalendarEvents(prefixes, phoneNumber, selected_sms_calendar_id, context);
                } else if (calendarFrom != null && calendarTo == null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().get_From_to_Now_CalendarEvents(prefixes, phoneNumber, selected_sms_calendar_id, context, calendarFrom);
                } else if (calendarFrom != null && calendarTo != null) {
                    contentValuesList = _utilsFactory.getCalendarUtils().get_From_To_CalendarEvents(prefixes, phoneNumber, selected_sms_calendar_id, context, calendarFrom, calendarTo);
                }

                if (contentValuesList.size() > 0) {
                    SMS sms;
                    for (ContentValues contentValues : contentValuesList) {
                        sms = _sms_generator.convertEventToSMS(contentValues);
                        smsList.add(sms);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return smsList;
    }

    /*
    * Get Thread ID of SMS from database and Create a new ThreadID if there is no result
    * */
    public long getCreateThreadID(Context context, String numberAddr) {
        Uri threadIdUri = Uri.parse("content://mms-sms/threadID");
        Uri.Builder builder = threadIdUri.buildUpon();
        String[] recipients = {numberAddr};
        for (String recipient : recipients) {
            builder.appendQueryParameter("recipient", recipient);
        }
        Uri uri = builder.build();
        long threadId = 0;
        Cursor cursor = context.getContentResolver().query(uri, new String[]{"_id"}, null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    threadId = cursor.getLong(0);
                }
            } finally {
                cursor.close();
            }
        }
        return threadId;
    }

    /*
    * Writing SMS into database
    * */
    public Uri writingSMS_Database(Context context, SMS sms) {
        final Uri uri = Uri.parse("content://sms");
        Uri uriResult = null;
        try {
            ContentValues smsContent = new ContentValues();
            long threadId = getCreateThreadID(context, sms.getNumbers());
            smsContent.put("thread_id", threadId);
            smsContent.put("address", sms.getNumbers());
            smsContent.put("date", sms.getSmsTime());
            smsContent.put("type", sms.getType());
            smsContent.put("body", sms.getSmsBody());
            smsContent.put("read", sms.isRead());
            uriResult = context.getContentResolver().insert(uri, smsContent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return uriResult;
    }
    /*
    * set SMS as Read
    * */
    public int setSmsRead(Context context, String smsId, boolean isRead) {
        final Uri uri = Uri.parse("content://sms");
        int result = -1;
        try {
            ContentValues smsContent = new ContentValues();
            smsContent.put("read",true);
            if (smsId == null) {
                return result;
            }
            String selection = "_id=?";
            String[] args = new String[]{smsId};
            result = context.getContentResolver().update(uri, smsContent, selection, args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    /*
    * set SMS as Read
    * */
    public int setSmsRead(String uriString) {
        int result = -1;
        if (uriString==null || uriString.equals(""))
            return result;
        final Uri uri = Uri.parse(uriString);
        try {
            ContentValues smsContent = new ContentValues();
            smsContent.put("read",true);
            result = mContext.getContentResolver().update(uri, smsContent, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /*
    * Delete all SMS from database based on Recipient
    * */
    public int deleteSMS_id(Context context, String threadId, String smsId) {
        final Uri uri = Uri.parse("content://sms");
        int result = -1;
        try {
            if (smsId == null) {
                return result;
            }
            String selection = "thread_id=? and _id=?";
            String[] args = new String[]{threadId, smsId};
            result = context.getContentResolver().delete(uri, selection, args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    public List<SMS> getUntrackedSMS(Context context, List<SMS> smsList, Long from, Long to, int limit, int offset){
        List<SMS> resultList = new ArrayList<SMS>();
        if (smsList.size()<=0)
            smsList = getSMS_from_to(context, from, to, limit, offset);
        for (int i=0; i<smsList.size(); i++){
            if (!smsList.get(i).isCalendarTracked) {
                if (offset>0){
                    offset--;
                    continue;
                }
                resultList.add(smsList.get(i));
            }
        }
        while (limit>0 && resultList.size()<limit && smsList.size()>0){
            to = smsList.get(smsList.size()-1).getSmsTime();
            smsList = getSMS_from_to(context, from, to, limit, 0);
            for (int i=0; i<smsList.size(); i++){
                if (!smsList.get(i).isCalendarTracked){
                    if (offset>0){
                        offset--;
                        continue;
                    }
                    resultList.add(smsList.get(i));
                }
            }
        }
        return resultList;
    }
    public List<SMS> getOutdatedSMS(Context context, List<SMS> smsList, Long from, Long to, int limit, int offset){
        List<SMS> resultList = new ArrayList<SMS>();
        if (smsList.size()<=0)
            smsList = getSMS_from_to(context, from, to, limit, offset);
        for (int i=0; i<smsList.size(); i++){
            if (smsList.get(i).isCalendarTracked && !smsList.get(i).isCalendarUpdated){
                if (offset>0){
                    offset--;
                    continue;
                }
                resultList.add(smsList.get(i));
            }
        }
        while (limit>0 && resultList.size()<limit && smsList.size()>0){
            to = smsList.get(smsList.size()-1).getSmsTime();
            smsList = getSMS_from_to(context, from, to, limit, 0);
            for (int i=0; i<smsList.size(); i++){
                if (smsList.get(i).isCalendarTracked && !smsList.get(i).isCalendarUpdated) {
                    if (offset>0){
                        offset--;
                        continue;
                    }
                    resultList.add(smsList.get(i));
                }
            }
        }
        return resultList;
    }
    public List<SMS> getUnprocessedSMS(Context context, List<SMS> smsList, Long from, Long to, int limit, int offset){
        List<SMS> resultList = new ArrayList<SMS>();
        if (smsList.size()<=0)
            smsList = getSMS_from_to(context, from, to, limit, offset);
        for (int i=0; i<smsList.size(); i++){
            if (!smsList.get(i).isCalendarTracked || !smsList.get(i).isCalendarUpdated) {
                if (offset>0){
                    offset--;
                    continue;
                }
                resultList.add(smsList.get(i));
            }
        }
        while (limit>0 && resultList.size()<limit && smsList.size()>0){
            to = smsList.get(smsList.size()-1).getSmsTime();
            smsList = getSMS_from_to(context, from, to, limit, 0);
            for (int i=0; i<smsList.size(); i++){
                if (!smsList.get(i).isCalendarTracked || !smsList.get(i).isCalendarUpdated){
                    if (offset>0){
                        offset--;
                        continue;
                    }
                    resultList.add(smsList.get(i));
                }
            }
        }
        return resultList;
    }
    /*
    * Sending SMS
    * */
    public void sendingSMS(final Context context, final SMS sms){
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        Uri uriSMSDB = null;
        try{
            //---when the SMS has been sent---
            context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    // setup notification
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_sent));
                            deleteProcessedSchedule(context, sms);
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_generic_fail));
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_no_service));
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_null_PDU) );
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_radio_off));
                            break;
                    }
                    // Hide the notification after its selected
                }
            }, new IntentFilter(SENT));
            //---when the SMS has been delivered---
            context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    ContentValues contentValues = new ContentValues();
                    Uri uri = Uri.parse("content://sms");
                    String selection = "_id = " +arg1.getStringExtra("smsId");
                    // setup notification
                    NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_delivered));
                            // update status of sms
                            contentValues.put("status",2);
                            arg0.getContentResolver().update(uri, contentValues, selection, null);
                            break;
                        case Activity.RESULT_CANCELED:
                            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_sending_sms_fail));
                            // update status of sms
                            contentValues.put("status",-1);
                            arg0.getContentResolver().update(uri, contentValues, selection, null);
                            break;
                    }
                }
            }, new IntentFilter(DELIVERED));
            // check if sms number is null
            if (sms.getNumbers()==null || sms.getNumbers().equals("")) {
                _notificationUtils.showLongToast("Phone number is wrong, please fix it first.");
                return;
            }
            // if sms number is okay, continue processing
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> parts = smsManager.divideMessage(sms.getSmsBody());
            ArrayList<PendingIntent> sentPIs = new ArrayList<PendingIntent>();
            ArrayList<PendingIntent> deliveredPIs = new ArrayList<PendingIntent>();
            // writing the sms into database
            sms.setType(2);
            sms.setRead(true);
            sms.setSmsTime(System.currentTimeMillis());
            // writing into database
            uriSMSDB = writingSMS_Database(context, sms);
            String id = uriSMSDB.toString();
            id = id.substring(id.lastIndexOf("/")+1);
            for (int i=0; i<parts.size(); i++){
                Intent sentIntent = new Intent(SENT)
                        .putExtra("smsId", id)
                        .putExtra("address", sms.getNumbers())
                        .putExtra("message", parts.get(i));
                PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, 0);
                Intent deliveryIntent = new Intent(DELIVERED)
                        .putExtra("smsId", id)
                        .putExtra("address", sms.getNumbers())
                        .putExtra("message", parts.get(i));
                PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveryIntent, 0);
                sentPIs.add(sentPI);
                deliveredPIs.add(deliveredPI);
            }
            smsManager.sendMultipartTextMessage(sms.getNumbers(), null, parts, sentPIs, deliveredPIs);
            //smsManager.sendTextMessage(sms.getNumbers(), null, sms.getSmsBody(), pi, null);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    private void deleteProcessedSchedule(Context context, SMS sms){
        String smsCalId = sms.getCal_event_id();
        int setting = _utilsFactory.getPreferencesHelper().read_delete_schedule_status(context);
        switch (setting){
            case 1:
                if (smsCalId!=null && !smsCalId.equals("")){
                    long eventID = Long.parseLong(smsCalId);
                    ContentResolver cr = context.getContentResolver();
                    Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
                    cr.delete(deleteUri, null, null);
                }
                break;
            case -1:
                break;
        }
    }

    // create the dialog to confirm delete sms items
    public Dialog getDeleteOptionDialog(Activity parentActivity, List<ListRowItemEvent> selectedItems) {
        Dialog dialog;
        String title = delete_dialog_title;
        String[] options = parentActivity.getResources().getStringArray(R.array.sms_delete_options);
        dialog = _utilsFactory.getDialog_Utils()
                .createDeleteDialog(parentActivity, Dialog_Utils.DIALOG_TYPE.DELETE_SMS_DIALOG,
                        title, options, selectedItems);
        return dialog;
    }
    // create the dialog to confirm delete sms items
    public Dialog getDeleteScheduleDialog(Activity parentActivity, List<ListRowItemEvent> selectedItems) {
        Dialog dialog;
        String title = deleteSchedule_dialog_title;
        String[] options = parentActivity.getResources().getStringArray(R.array.sms_deleteSchedule_options);
        dialog = _utilsFactory.getDialog_Utils()
                .createDeleteDialog(parentActivity, Dialog_Utils.DIALOG_TYPE.DELETE_SCHEDULE_SMS_DIALOG,
                        title, options, selectedItems);
        return dialog;
    }
    /*
    * Delete all SMS from database based on id
    * */
    public int bulkDeleteSMS(Context context, List<String> smsIdList) {
        final Uri uri = Uri.parse("content://sms");
        int result = -1;
        try {
            if (smsIdList == null) {
                return result;
            }
            String[] args = new String[smsIdList.size()];
            String selection = "_id IN (";
            int i=0;
            for (String id : smsIdList){
                selection += "?,";
                args[i] = id;
                i++;
            }
            selection = selection.substring(0, selection.length()-1) + ") ";
            result = context.getContentResolver().delete(uri, selection, args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    /**
     * Check if your app is the default system SMS app.
     * @return True if it is default, False otherwise. Pre-KitKat will always return True.
     */
    public static boolean isDefaultSmsApp(Context context){
        if (hasKitKat()) {
            return context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));
        }

        return true;
    }
    public String getDefaultSmsAppName(){
        String defaultSmsApp = "";
        if (hasKitKat())
            defaultSmsApp = Telephony.Sms.getDefaultSmsPackage(mContext);
        return defaultSmsApp;
    }

    /**
     * Trigger the intent to open the system dialog that asks the user to change the default
     * SMS app.
     */
    public void enableDefaultSmsApp(){
        if (!isDefaultSmsApp(mContext) && hasKitKat()) {
            // App is not default.
            Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, mContext.getPackageName());
            mContext.startActivity(intent);
        }
    }

    /**
     * Check if the device runs Android 4.3 (JB MR2) or higher.
     */
    public static boolean hasJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    /**
     * Check if the device runs Android 4.4 (KitKat) or higher.
     */
    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }


    /*
    * Writing MMS into database
    * */
    public Uri writingMMS_Database(Context context, SMS sms) {
        final Uri uri = Uri.parse("content://sms");
        Uri uriResult = null;
        try {
            ContentValues smsContent = new ContentValues();
            long threadId = getCreateThreadID(context, sms.getNumbers());
            smsContent.put("thread_id", threadId);
            smsContent.put("address", sms.getNumbers());
            smsContent.put("date", sms.getSmsTime());
            smsContent.put("type", sms.getType());
            smsContent.put("body", sms.getSmsBody());
            smsContent.put("read", sms.isRead());
            uriResult = context.getContentResolver().insert(uri, smsContent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return uriResult;
    }
}
