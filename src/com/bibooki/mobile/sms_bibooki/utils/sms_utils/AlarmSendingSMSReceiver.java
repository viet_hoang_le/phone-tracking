package com.bibooki.mobile.sms_bibooki.utils.sms_utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.*;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

public class AlarmSendingSMSReceiver extends BroadcastReceiver {
    static Context mContext;
    static UtilsFactory _utilsFactory;

    private static final String TAG = "AlarmSendingSMSReceiver";
    final public static String ONE_TIME = "onetime";
    private static List<SMS> _smsList;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);

        Format formatter = new SimpleDateFormat("hh:mm:ss a");
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        //Acquire the lock
        wl.acquire();

        //You can do the processing here update the widget/remote views.
        // process Sending SMS
        for (SMS sms:_smsList){
            _utilsFactory.getSmsUtils().sendingSMS(context, sms);
            //Toast.makeText(mContext, sms.getEventTitle(), Toast.LENGTH_SHORT).show();
        }
        Intent Auto_SMS_Receiver_Intent = new Intent(mContext, Auto_SMS_Receiver.class);
        mContext.sendBroadcast(Auto_SMS_Receiver_Intent);

        //Release the lock
        wl.release();
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmSendingSMSReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void setOnetimeTimer(Context context, Long atTime, List<SMS> smsList) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmSendingSMSReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //at future sending sms time
        _smsList = smsList;
        am.set(AlarmManager.RTC_WAKEUP, atTime, pi);
    }
}
