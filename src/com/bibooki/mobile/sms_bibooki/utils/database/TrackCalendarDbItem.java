package com.bibooki.mobile.sms_bibooki.utils.database;

/**
 * Created by viet.hoang.le on 12/5/2014.
 */
public class TrackCalendarDbItem {
    int id, smsCalendarId, callCalendarId;
    String phoneNumber = DBTrackCalendarOpenHelper.DEFAULT_TRACKING_VALUE, contactName = DBTrackCalendarOpenHelper.DEFAULT_TRACKING_VALUE;
    String smsCalendarName, callCalendarName;
    public TrackCalendarDbItem(int id, int smsCalendarId, int callCalendarId, String phoneNumber){
        setId(id);
        setSmsCalendarId(smsCalendarId);
        setCallCalendarId(callCalendarId);
        setPhoneNumber(phoneNumber);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSmsCalendarId() {
        return smsCalendarId;
    }

    public void setSmsCalendarId(int smsCalendarId) {
        this.smsCalendarId = smsCalendarId;
    }

    public int getCallCalendarId() {
        return callCalendarId;
    }

    public void setCallCalendarId(int callCalendarId) {
        this.callCalendarId = callCalendarId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getSmsCalendarName() {
        return smsCalendarName;
    }

    public void setSmsCalendarName(String smsCalendarName) {
        this.smsCalendarName = smsCalendarName;
    }

    public String getCallCalendarName() {
        return callCalendarName;
    }

    public void setCallCalendarName(String callCalendarName) {
        this.callCalendarName = callCalendarName;
    }
}
