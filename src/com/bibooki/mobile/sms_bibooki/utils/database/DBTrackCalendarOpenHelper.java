package com.bibooki.mobile.sms_bibooki.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;

import java.util.ArrayList;
import java.util.List;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class DBTrackCalendarOpenHelper extends SQLiteOpenHelper {
    public final static String FILTER_ID="_id"; // id value for employee
    public final static String FILTER_PHONE="phonenumber";  // name of employee
    public final static String FILTER_SMS_CALENDAR="smsCalendarId";  // name of employee
    public final static String FILTER_CALL_CALENDAR="callCalendarId";  // name of employee
    public static final String DATABASE_NAME = "bibooki_sms_bibooki";
    public static final String TABLE_NAME = "TrackingFilter";

    public final static String DEFAULT_TRACKING_VALUE = "default_tracking";  // name of employee
    public final static String DEFAULT_SCHEDULE_VALUE = "default_schedule";  // name of employee

    private static final int DATABASE_VERSION = 2;
    private static final String TABLE_CREATE_COMMAND =
            "CREATE TABLE " + TABLE_NAME + " ("+FILTER_ID+" integer primary key, "+FILTER_PHONE+" text unique not null, " +
                    FILTER_SMS_CALENDAR+" integer, "+FILTER_CALL_CALENDAR+" integer);";

    Context mContext;
    UtilsFactory _utilsFactory;
    Calendar_Utils _calendarUtils;
    NotificationUtils _notificationUtils;
    ContactsHelper _contactsHelper;

    public DBTrackCalendarOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _calendarUtils = _utilsFactory.getCalendarUtils();
        _notificationUtils = _utilsFactory.getNotificationUtils();
        _contactsHelper = ContactsHelper.getInstance(mContext);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_COMMAND);
    }
    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
        /*Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");      */
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(database);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///// Help Methods ///////////////////////////////////////////////////////////////////////////
    SQLiteDatabase database = getWritableDatabase();
    public boolean updateCreateRecords(String phoneNumber, Integer smsCalendarId, Integer callCalendarId){
        boolean isCreatedNew;
        ContentValues values = new ContentValues();
        values.put(FILTER_PHONE, phoneNumber);
        values.put(FILTER_SMS_CALENDAR, smsCalendarId);
        values.put(FILTER_CALL_CALENDAR, callCalendarId);
        if (selectRecord(phoneNumber)==null){
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_saved) + " " + phoneNumber);
            database.insert(TABLE_NAME, null, values);
            isCreatedNew = true;
        }
        else{
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_updated) + " " + phoneNumber);
            String condition = FILTER_PHONE + "=?";
            database.update(TABLE_NAME, values, condition, new String[]{phoneNumber});
            isCreatedNew = false;
        }
        return isCreatedNew;
    }
    public boolean updateCreateDefault(Integer smsCalendarId, Integer callCalendarId){
        boolean isCreatedNew;
        ContentValues values = new ContentValues();
        values.put(FILTER_PHONE, DEFAULT_TRACKING_VALUE);
        values.put(FILTER_SMS_CALENDAR, smsCalendarId);
        values.put(FILTER_CALL_CALENDAR, callCalendarId);
        if (selectDefaultRecord()==null){
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_saved) +" "+ DEFAULT_TRACKING_VALUE);
            database.insert(TABLE_NAME, null, values);
            isCreatedNew = true;
        }
        else{
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_updated) + " " + DEFAULT_TRACKING_VALUE);
            String condition = FILTER_PHONE + "=?";
            database.update(TABLE_NAME, values, condition, new String[]{DEFAULT_TRACKING_VALUE});
            isCreatedNew = false;
        }
        return isCreatedNew;
    }
    public boolean updateCreateSmsDefault(Integer smsCalendarId){
        boolean isCreatedNew;
        ContentValues values = new ContentValues();
        values.put(FILTER_PHONE, DEFAULT_TRACKING_VALUE);
        values.put(FILTER_SMS_CALENDAR, smsCalendarId);
        TrackCalendarDbItem item = selectDefaultRecord();
        if (item==null){
            values.put(FILTER_CALL_CALENDAR, -1);
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_saved) +" "+ DEFAULT_TRACKING_VALUE);
            database.insert(TABLE_NAME, null, values);
            isCreatedNew = true;
        }
        else{
            values.put(FILTER_CALL_CALENDAR, item.getCallCalendarId());
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_updated) + " " + DEFAULT_TRACKING_VALUE);
            String condition = FILTER_PHONE + "=?";
            database.update(TABLE_NAME, values, condition, new String[]{DEFAULT_TRACKING_VALUE});
            isCreatedNew = false;
        }
        return isCreatedNew;
    }
    public boolean updateCreateCallDefault(Integer callCalendarId){
        boolean isCreatedNew;
        ContentValues values = new ContentValues();
        values.put(FILTER_PHONE, DEFAULT_TRACKING_VALUE);
        values.put(FILTER_CALL_CALENDAR, callCalendarId);
        TrackCalendarDbItem item = selectDefaultRecord();
        if (item==null){
            values.put(FILTER_SMS_CALENDAR, -1);
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_saved) +" "+ DEFAULT_TRACKING_VALUE);
            database.insert(TABLE_NAME, null, values);
            isCreatedNew = true;
        }
        else{
            values.put(FILTER_SMS_CALENDAR, item.getSmsCalendarId());
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_updated) + " " + DEFAULT_TRACKING_VALUE);
            String condition = FILTER_PHONE + "=?";
            database.update(TABLE_NAME, values, condition, new String[]{DEFAULT_TRACKING_VALUE});
            isCreatedNew = false;
        }
        return isCreatedNew;
    }
    public boolean updateCreateSchedule(Integer smsCalendarId    ){
        boolean isCreatedNew;
        ContentValues values = new ContentValues();
        values.put(FILTER_PHONE, DEFAULT_SCHEDULE_VALUE);
        values.put(FILTER_SMS_CALENDAR, smsCalendarId);
        values.put(FILTER_CALL_CALENDAR, -1);
        if (selectScheduleRecord()==null){
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_saved) +" "+ DEFAULT_SCHEDULE_VALUE);
            database.insert(TABLE_NAME, null, values);
            isCreatedNew = true;
        }
        else{
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.status_updated) + " " + DEFAULT_SCHEDULE_VALUE);
            String condition = FILTER_PHONE + "=?";
            database.update(TABLE_NAME, values, condition, new String[]{DEFAULT_SCHEDULE_VALUE});
            isCreatedNew = false;
        }
        return isCreatedNew;
    }
    // Selecting all records in the database table
    public List<TrackCalendarDbItem> selectAllRecords() {
        List<TrackCalendarDbItem> trackingDbItem = new ArrayList<TrackCalendarDbItem>();
        String smsCalName, callCalName;
        TrackCalendarDbItem trackCalendarDbItem;
        String[] cols = new String[] {
                FILTER_ID,
                FILTER_PHONE,
                FILTER_SMS_CALENDAR,
                FILTER_CALL_CALENDAR};
        Cursor mCursor = database.query(true, TABLE_NAME, cols, null, null, null, null, null, null);
        String itemPhoneNumber;
        int id, itemSmsCalendar, itemCallCalendar;
        while (mCursor.moveToNext()) {
            id = mCursor.getInt(0);
            itemPhoneNumber = mCursor.getString(1);
            itemSmsCalendar = mCursor.getInt(2);
            itemCallCalendar = mCursor.getInt(3);
            trackCalendarDbItem = new TrackCalendarDbItem(id, itemSmsCalendar, itemCallCalendar, itemPhoneNumber);
            // get Calendar Name
            smsCalName = _calendarUtils.getCalendarNameById( mContext, trackCalendarDbItem.getSmsCalendarId());
            callCalName = _calendarUtils.getCalendarNameById( mContext, trackCalendarDbItem.getCallCalendarId());
            trackCalendarDbItem.setSmsCalendarName(smsCalName);
            trackCalendarDbItem.setCallCalendarName(callCalName);
            // get Contact Name
            trackCalendarDbItem.setContactName(_contactsHelper.getContactDisplayNameByNumber(itemPhoneNumber));
            // add to result
            trackingDbItem.add(trackCalendarDbItem);
        }
        return trackingDbItem; // iterate to get each value.
    }
    // Selecting record in the database table respect to the phoneNumber
    public TrackCalendarDbItem selectRecord(String phoneNumber) {
        String smsCalName, callCalName;
        TrackCalendarDbItem trackCalendarDbItem = null;
        String[] cols = new String[] {
                FILTER_ID,
                FILTER_PHONE,
                FILTER_SMS_CALENDAR,
                FILTER_CALL_CALENDAR};
        String condition = FILTER_PHONE + "=?";
        Cursor mCursor = database.query(true, TABLE_NAME, cols, condition, new String[]{phoneNumber}, null, null, null, null);
        String itemPhoneNumber;
        int id, itemSmsCalendar, itemCallCalendar;
        while (mCursor.moveToNext()) {
            id = mCursor.getInt(0);
            itemPhoneNumber = mCursor.getString(1);
            itemSmsCalendar = mCursor.getInt(2);
            itemCallCalendar = mCursor.getInt(3);
            trackCalendarDbItem = new TrackCalendarDbItem(id, itemSmsCalendar, itemCallCalendar, itemPhoneNumber);
            // get Calendar Name
            smsCalName = _calendarUtils.getCalendarNameById(mContext, trackCalendarDbItem.getSmsCalendarId());
            callCalName = _calendarUtils.getCalendarNameById(mContext, trackCalendarDbItem.getCallCalendarId());
            trackCalendarDbItem.setSmsCalendarName(smsCalName);
            trackCalendarDbItem.setCallCalendarName(callCalName);
            // get Contact Name
            trackCalendarDbItem.setContactName(_contactsHelper.getContactDisplayNameByNumber(phoneNumber));
        }
        return trackCalendarDbItem;
    }
    public void deleteRecord(String phoneNumber)
    {
        try {
            database.delete(TABLE_NAME,
                    FILTER_PHONE + "=?",
                    new String[]{phoneNumber});
            _notificationUtils.showShortToast(phoneNumber + " filter was deleted !");
        }catch (Exception ex){
            _notificationUtils.showShortToast("Problem! Cannot delete filter of " +phoneNumber);
            ex.printStackTrace();
        }
    }
    // Select Filter Record which has phone number is different from Default Value
    public TrackCalendarDbItem selectDefaultRecord() {
        return selectRecord(DEFAULT_TRACKING_VALUE);
    }
    // Select Filter Record which has phone number is different from Default Value
    public TrackCalendarDbItem selectScheduleRecord() {
        return selectRecord(DEFAULT_SCHEDULE_VALUE);
    }
    // Select Filter Record which has phone number is different from Default Value
    public List<TrackCalendarDbItem> selectRecordsNotDefault() {
        List<TrackCalendarDbItem> trackingDbItem = new ArrayList<TrackCalendarDbItem>();
        String smsCalName, callCalName;
        TrackCalendarDbItem trackCalendarDbItem;
        String[] cols = new String[] {
                FILTER_ID,
                FILTER_PHONE,
                FILTER_SMS_CALENDAR,
                FILTER_CALL_CALENDAR};
        Cursor mCursor = database.query(true, TABLE_NAME, cols, null, null, null, null, null, null);
        String itemPhoneNumber;
        int id, itemSmsCalendar, itemCallCalendar;
        while (mCursor.moveToNext()) {
            id = mCursor.getInt(0);
            itemPhoneNumber = mCursor.getString(1);
            if (itemPhoneNumber.equals(DEFAULT_TRACKING_VALUE) || itemPhoneNumber.equals(DEFAULT_SCHEDULE_VALUE))
                continue;
            itemSmsCalendar = mCursor.getInt(2);
            itemCallCalendar = mCursor.getInt(3);
            trackCalendarDbItem = new TrackCalendarDbItem(id, itemSmsCalendar, itemCallCalendar, itemPhoneNumber);
            // get Calendar Name
            smsCalName = _calendarUtils.getCalendarNameById( mContext, trackCalendarDbItem.getSmsCalendarId());
            callCalName = _calendarUtils.getCalendarNameById( mContext, trackCalendarDbItem.getCallCalendarId());
            trackCalendarDbItem.setSmsCalendarName(smsCalName);
            trackCalendarDbItem.setCallCalendarName(callCalName);
            // get Contact Name
            trackCalendarDbItem.setContactName(_contactsHelper.getContactDisplayNameByNumber(itemPhoneNumber));
            // add to result
            trackingDbItem.add(trackCalendarDbItem);
        }
        return trackingDbItem; // iterate to get each value.
    }
}
