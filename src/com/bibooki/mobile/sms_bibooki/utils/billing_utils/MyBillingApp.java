package com.bibooki.mobile.sms_bibooki.utils.billing_utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.ShopActivity;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

public class MyBillingApp {
    Context _context;
    Activity _activity;
    private static UtilsFactory _utilsFactory;
    private static PreferencesHelper _preferenceHelper;
    private NotificationUtils _notificationUtils;

    String TAG = "MyBillingApp";

    // The helper object
    IabHelper mHelper;
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;

    public MyBillingApp(Activity activity, Context context, String base64EncodedPublicKey){
        _context = context;
        _activity = activity;
        _utilsFactory = UtilsFactory.newInstance(_context);
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        _notificationUtils = _utilsFactory.getNotificationUtils();

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(_context, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.e(TAG, "Setup successful. Querying inventory.");
            }
        });
    }

    public boolean isPaidItem(final String itemSku){
        final Boolean[] mIsPaid = {false};
        // Listener that's called when we finish querying the items and subscriptions we own
        mHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                Log.d(TAG, "Query inventory finished.");

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // Is it a failure?
                if (result.isFailure()) {
                    Log.e(TAG, "Failed to query inventory: " + result);
                    return;
                }

                Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

                // Do we have the free upgrade?
                Purchase itemPurchase = inventory.getPurchase(itemSku);
                mIsPaid[0] = (itemPurchase != null && verifyDeveloperPayload(itemPurchase));
                Log.d(TAG, "User is " + (mIsPaid[0] ? "Paid" : "NOT Paid"));
            /*
            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            Purchase gasPurchase = inventory.getPurchase(SKU_GAS);
            if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
                return;
            }*/
                Log.d(TAG, "Initial inventory query finished; enabling main UI.");
            }
        });
        return mIsPaid[0];
    }

    /** Verifies the developer payload of a purchase. */
    public static boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    public void buyItem(String skuItem){
        Log.d(TAG, "Buy item button clicked.");
        // launch the gas purchase UI flow.
        // We will be notified of completion via mPurchaseFinishedListener
        Log.d(TAG, "Launching purchase flow for gas.");

        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "";
        //_preferenceHelper.storeGolden(_context, 80);
        //((ShopActivity) _activity).updateUi();
        mHelper.launchPurchaseFlow(_activity, skuItem, RC_REQUEST, mPurchaseFinishedListener, payload);
    }
    // Callback for when a purchase is finished
    public IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                //setWaitScreen(false);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                //setWaitScreen(false);
                return;
            }

            Log.d(TAG, "Purchase successful.");
            if (purchase.getSku().equals(_context.getString(R.string.sku_free_ads))) {
                Log.d(TAG, "Ads Free purchased successful.");
                _preferenceHelper.setBillingFreeAds(_context, true);
                ((ShopActivity) _activity).updateUi();
            }else{
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;
            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                if (purchase.getSku().equals(_context.getString(R.string.sku_donate)))
                    _notificationUtils.showShortToast(_context.getString(R.string.thankyou_donation));
                Log.d(TAG, "Consumption successful. Provisioning.");
                //alert("Consumed item !!!");
            }
            else {
                complain("Error while consuming: " + result);
            }
            ((ShopActivity) _activity).updateUi();
            Log.d(TAG, "End consumption flow.");
        }
    };

    private void complain(String s) {
        Log.e(TAG,s);
    }
    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(_context);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }
}
