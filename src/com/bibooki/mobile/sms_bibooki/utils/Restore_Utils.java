package com.bibooki.mobile.sms_bibooki.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog;
import com.bibooki.mobile.sms_bibooki.DisplayMessageActivity;
import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Restore_Utils {
    static Context mContext;
    static Restore_Utils _restore_Utils = null;
    private static UtilsFactory _utilsFactory;
    private ProgressDialog progressDialog;
    private ProgressDialog mprogressDialog;
    private Handler mHandler = new Handler();
    private int mProgressStatus = 0;

    List<SMS> smsList = null;
    List<SMS> smsList_fromDB;
    ContentValues smsContent;
    boolean isExisted = false;

    List<Call_Log> call_log_List = null;
    List<Call_Log> callList_fromDB;
    ContentValues call_log_Content;

    int smsCal_id = -1,
        callCal_id = -1;
    String phoneNumber = null;

    // return new instance
    public static Restore_Utils getInstance(Context context){
        if (_restore_Utils == null)
            _restore_Utils = new Restore_Utils();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        return _restore_Utils;
    }

    // create the restore dialog for Restoring
    public Dialog restoreDialog(Context _context, Activity activity, String phoneNumber, int smsCal_id, int callCal_id){
        this.smsCal_id = smsCal_id;
        this.callCal_id = callCal_id;
        this.phoneNumber = phoneNumber;
        Dialog dialog;
        String title = activity.getResources().getString(R.string.restore_dialog_title);
        String[] items = {
                activity.getResources().getString(R.string.restore_plan_from_now),
                activity.getResources().getString(R.string.restore_plan_from_to),
                activity.getResources().getString(R.string.restore_plan_all)};
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity, Dialog_Utils.DIALOG_TYPE.RESTORE_TIME_DIALOG, items, title, null, null, null);
        return  dialog;
    }
    /*
    Restore All
    * */
     public void restore_All(Context context){
        restore_SMS_CALL_LOG(context, null, null);
    }
    /*
    Restore from a specific Time to Current Time
    * */
     public void restore_From_To_Now(Context context, Calendar calendar){
        restore_SMS_CALL_LOG(context, calendar, null);
    }
    /*
    Restore
    * */
     public void restore_SMS_CALL_LOG(final Context context, final Calendar calendarFrom, final Calendar calendarTo){
        // setup progress bar
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
        // setup progress spinner
        mprogressDialog = new ProgressDialog(context);
        mprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mprogressDialog.setCancelable(false);
        isExisted = false;

        try{
            // restore missing sms if cannot find it in the database
            // Start lengthy operation in a background thread
            final Thread restoringThread = new Thread(new Runnable() {
                public void run() {
                    SMS sms;
                    //MainActivity._PROCESSED_RESTORE_SMS = 0;
                    //MainActivity._TOTAL_RESTORE_SMS = smsList.size();
                    List<Long> timeList = new ArrayList<Long>();
                    for (int i=0; i < smsList.size(); i++){
                        sms = smsList.get(i);
                        // get time of sms, and check if at that time, sms is existing
                        timeList.clear();
                        timeList.add(sms.getSmsTime());
                        smsList_fromDB = _utilsFactory.getSmsUtils().get_SMS_At_Time(context, timeList);
                        if (smsList_fromDB.size() == 0){
                            // if cannot find a sms at the specific time, restore the sms into DB
                            _utilsFactory.getSmsUtils().writingSMS_Database(context, sms);
                            //MainActivity._PROCESSED_RESTORE_SMS++;
                        }
                        else{
                            for (SMS smsDB : smsList_fromDB){
                                if (smsDB.getNumbers().equals(sms.getNumbers()) && smsDB.getSmsBody().equals(sms.getSmsBody())){
                                    isExisted = true;
                                    break;
                                }
                            }
                            if (isExisted == false){
                                _utilsFactory.getSmsUtils().writingSMS_Database(context, sms);
                                //MainActivity._PROCESSED_RESTORE_SMS++;
                            }
                        }
                        // Update the progress bar
                        mProgressStatus = i+1;
                        mHandler.post(new Runnable() {
                            public void run() {
                                progressDialog.setProgress(mProgressStatus);
                            }
                        });
                    }
                    progressDialog.dismiss();
                    mHandler.post(new Runnable() {
                        public void run() {
                            restore_CALL_LOG(context, calendarFrom, calendarTo);
                        }
                    });
                }
            });
            Thread getDataThread = new Thread(new Runnable() {
                public void run() {
                    mHandler.post(new Runnable() {
                        public void run() {
                            mprogressDialog.setMessage(context.getString(R.string.status_loading_sms));
                            mprogressDialog.show();
                        }
                    });
                    // get list of valid sms
                    smsList = _utilsFactory.getSmsUtils().get_All_SMS_From_Calendar(context, calendarFrom, calendarTo, phoneNumber, smsCal_id);
                    mHandler.post(new Runnable() {
                        public void run() {
                            progressDialog.setMax(smsList.size());
                            progressDialog.setMessage(context.getString(R.string.status_processing_sms));
                            mprogressDialog.dismiss();
                            progressDialog.show();
                            restoringThread.start();
                        }
                    });
                }
            });
            getDataThread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /*
    Restore Call Log
    * */

     public void restore_CALL_LOG(final Context context, final Calendar calendarFrom, final Calendar calendarTo){
        // setup progress bar
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
        // setup progress spinner
        mprogressDialog = new ProgressDialog(context);
        mprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mprogressDialog.setCancelable(false);
        // get list of valid call log
        isExisted = false;
        // get list of valid call log
        final Uri uri = CallLog.Calls.CONTENT_URI;
        try{
            // restore missing call log if cannot find it in the database
            // Start lengthy operation in a background thread
            final Thread restoringThread = new Thread(new Runnable() {
                public void run() {
                    Call_Log call_log;
                    //MainActivity._PROCESSED_RESTORE_CALL = 0;
                    //MainActivity._TOTAL_RESTORE_CALL = call_log_List.size();
                    List<Long> timeList = new ArrayList<Long>();
                    for (int i=0; i < call_log_List.size(); i++){
                        call_log = call_log_List.get(i);
                        // get time of call log, and check if at that time, call log is existing
                        timeList.clear();
                        timeList.add(call_log.getFromTime());
                        callList_fromDB = _utilsFactory.getCallLogUtils().get_Call_Log_At_Time(context, timeList);
                        if (callList_fromDB.size() == 0){
                            // if cannot find a call log at the specific time, restore the call log into DB
                            call_log_Content = new ContentValues();
                            call_log_Content.put(CallLog.Calls.DURATION, call_log.getDurationSecond());
                            call_log_Content.put(CallLog.Calls.NUMBER, call_log.getNumbers());
                            call_log_Content.put(CallLog.Calls.DATE, call_log.getFromTime());
                            call_log_Content.put(CallLog.Calls.TYPE, call_log.getType());
                            call_log_Content.put(CallLog.Calls.CACHED_NAME, call_log.getContactNames());

                            context.getContentResolver().insert(uri, call_log_Content);
                            //MainActivity._PROCESSED_RESTORE_CALL++;
                        }
                        else{
                            for (Call_Log callLog : callList_fromDB){
                                if (callLog.getNumbers().equals(call_log.getNumbers())){
                                    isExisted = true;
                                    break;
                                }
                            }
                            if (isExisted == false){
                                call_log_Content = new ContentValues();
                                call_log_Content.put(CallLog.Calls.DURATION, call_log.getDurationSecond());
                                call_log_Content.put(CallLog.Calls.NUMBER, call_log.getNumbers());
                                call_log_Content.put(CallLog.Calls.DATE, call_log.getFromTime());
                                call_log_Content.put(CallLog.Calls.TYPE, call_log.getType());
                                call_log_Content.put(CallLog.Calls.CACHED_NAME, call_log.getContactNames());

                                context.getContentResolver().insert(uri, call_log_Content);
                                //MainActivity._PROCESSED_RESTORE_CALL++;
                            }
                        }
                        // Update the progress bar
                        mProgressStatus = i+1;
                        mHandler.post(new Runnable() {
                            public void run() {
                                progressDialog.setProgress(mProgressStatus);
                            }
                        });
                    }
                    progressDialog.dismiss();
                    // Do something in response to button
                    Intent intent = new Intent(context, DisplayMessageActivity.class);
                    context.startActivity(intent);
                }
            });
            Thread getDataThread = new Thread(new Runnable() {
                public void run() {
                    mHandler.post(new Runnable() {
                        public void run() {
                            mprogressDialog.setMessage(context.getString(R.string.status_loading_call));
                            mprogressDialog.show();
                        }
                    });
                    // get list of valid call log
                    call_log_List = _utilsFactory.getCallLogUtils().get_All_Call_Log_From_Calendar(context, calendarFrom, calendarTo, phoneNumber, callCal_id);
                    mHandler.post(new Runnable() {
                        public void run() {
                            progressDialog.setMax(call_log_List.size());
                            progressDialog.setMessage(context.getString(R.string.status_processing_call));
                            mprogressDialog.dismiss();
                            progressDialog.show();
                            restoringThread.start();
                        }
                    });
                }
            });
            getDataThread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
