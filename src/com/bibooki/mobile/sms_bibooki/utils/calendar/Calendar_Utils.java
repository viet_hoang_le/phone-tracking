package com.bibooki.mobile.sms_bibooki.utils.calendar;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import com.bibooki.mobile.sms_bibooki.utils.*;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.filter.Filter_Utils;

import java.util.*;

public class Calendar_Utils {
    //private static final String TAG = "Calendar_Utils";
    static Calendar_Utils calendarUtils = null;
    static Map<Integer, String> _CALENDARS = new HashMap<Integer, String>();
    static String[] _CALENDARS_DISPLAY = null;

    public static final String _HE_NOT_ANSWER = "(He/She DID NOT ANSWER)";
    public static final String _YOU_NOT_ANSWER = "(You DID NOT ANSWER)";
    public static final String _MISSED_CALL = "(Missed call)";

    public static final String _MOBILE_NUMBER = "Mobile_number: ";
    public static final String _MY_NUMBER = "My_number: ";
    public static final String _SERVICE_CENTER = "Service_center: ";
    public static final String _FROM_ME = "From_(me): ";
    public static final String _TO_ME = "To_(me): ";
    public static final String _AT_TIME = "At_time: ";
    public static final String _CALL_TYPE = "Call_type: ";
    public static final String _LINE = "--------------------------------------\n";

    public static final String _SMS_FROM = "SMS_from: ";
    public static final String _SMS_TO = "SMS_to: ";
    public static final String _SMS_CONTENT = "SMS_Content:\n";

    public static final String _CALL_FROM = "Call_from: ";
    public static final String _CALL_TO = "Call_to: ";
    public static final String _CALL_DURATION = "Call_duration: ";
    public static final String _MISSED_CALL_FROM = "Missed_Call_from: ";
    public static final String _REJECTED_CALL_FROM = "Rejected_Call_from: ";

    public static final String _EVENT_ID = "Event_ID";
    public static final String _EVENT_TITLE = "Event_Title";
    public static final String _EVENT_DESCRIPTION = "Event_Description";
    public static final String _EVENT_START = "Event_Start_Time";
    public static final String _EVENT_END = "Event_End_Time";
    public static final String _EVENT_DURATION = "Event_Duration";

    static Context mContext;
    private static UtilsFactory _utilsFactory;

    private Calendar_Utils() {
        try {
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    // return new instance
    public static Calendar_Utils getInstance(Context context) {
        if (calendarUtils == null)
            calendarUtils = new Calendar_Utils();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        return calendarUtils;
    }

    // return map of calendars of Phone
    public Map<Integer, String> getCalendars(Context context) {
        // if calendar list already loaded, return it
        if (!_CALENDARS.isEmpty())
            return _CALENDARS;
        // if calendar list first time loading, get list of calendar
        String[] projection = new String[]{CalendarContract.Calendars._ID, CalendarContract.Calendars.NAME};
        Uri calendars = CalendarContract.Calendars.CONTENT_URI;
        //if (Build.VERSION.SDK_INT < 8 )
        //    calendars = Uri.parse("content://calendar/calendars");
        Cursor managedCursor = context.getContentResolver().query(calendars, projection, null, null, null);

        // Retrieve info of calendar
            String calName;
            Integer calId;
        int nameColumn, idColumn;
            while (managedCursor.moveToNext()) {
                nameColumn = managedCursor.getColumnIndex(CalendarContract.Calendars.NAME);
                idColumn = managedCursor.getColumnIndex(CalendarContract.Calendars._ID);
                calName = managedCursor.getString(nameColumn);
                calId = managedCursor.getInt(idColumn);
                _CALENDARS.put(calId, calName);
            }
        // Get Calendars display list
        _CALENDARS_DISPLAY = new String[_CALENDARS.size()];
        int i = 0;
        List<Integer> sortedKeys = new ArrayList(_CALENDARS.keySet());
        Collections.sort(sortedKeys);
        for (Integer key : sortedKeys) {
            _CALENDARS_DISPLAY[i] = key + ". " + _CALENDARS.get(key);
            i++;
        }
        return _CALENDARS;
    }

    // return display calendar name list
    public String[] getCalendarDisplayName(Activity activity) {
        // if calendar list already loaded, return it
        if (_CALENDARS_DISPLAY != null)
            return _CALENDARS_DISPLAY;
        getCalendars(activity);
        return _CALENDARS_DISPLAY;
    }

    // return map of calendars of Phone
    public String getCalendarNameById(Context context, int calID) {
        String[] projection = new String[]{CalendarContract.Calendars.NAME};
        Uri calendars = CalendarContract.Calendars.CONTENT_URI;
        //if (Build.VERSION.SDK_INT < 8 )
        //    calendars = Uri.parse("content://calendar/calendars");
        String selection = CalendarContract.Calendars._ID + " = " + calID;
        Cursor managedCursor = context.getContentResolver().query(calendars, projection, selection, null, null);

        //Map<Integer, String> calendarMap = new HashMap<Integer, String>();
        String calName = "";
        // Retrieve info of calendar
        if (managedCursor!=null && managedCursor.moveToFirst()) {
            calName = managedCursor.getString(0);
        }
        return calName;
    }

    // check an existing event at supposed time, if exising, return the id of event and new name contact of SMS
    List<String> eid_name;
    String smsContact;
    String smsNumber;
    Long smsTime;
    String selection;
    String[] INSTANCE_PROJECTION_SMS;
    String eventContact, eventNumber;
    String title, description, id;

    /* Batch Create SMS Event in Calendar */
    public void batch_Create_SMS_Event(Context context, int calId, List<SMS> smsList){
        try{
            ContentValues[] smsEventList = new ContentValues[smsList.size()];
            SMS sms;
            Filter_Utils filterUtils = Filter_Utils.getInstance(context);
            for (int i = 0; i<smsList.size(); i++){
                sms = smsList.get(i);
                // setup event information
                ContentValues contentValues = new ContentValues();
                // Each event needs to be tied to a specific Calendar
                TrackingFilter trackingFilter = filterUtils.selectRecord(sms.getNumbers());
                if (trackingFilter==null)
                    contentValues.put(CalendarContract.Events.CALENDAR_ID, calId);
                else
                    contentValues.put(CalendarContract.Events.CALENDAR_ID, trackingFilter.getSmsCalendarId());
                // We then set some of the basic information about the event
                contentValues.put(CalendarContract.Events.TITLE, sms.getGeneratedTitle());
                contentValues.put(CalendarContract.Events.DESCRIPTION, sms.getGeneratedDescription());
                contentValues.put(CalendarContract.Events.DTSTART, sms.getStartTime());
                contentValues.put(CalendarContract.Events.DTEND, sms.getEndTime());
                contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
                // add event value to list
                smsEventList[i] = contentValues;
            }
            // then adding the event to calendar
            Uri eventsUri = CalendarContract.Events.CONTENT_URI;
            //Uri eventsUri = Uri.parse("content://com.android.calendar/events");
            if (Build.VERSION.SDK_INT < 8)
                eventsUri = Uri.parse("content://calendar/events");
            context.getContentResolver().bulkInsert(eventsUri, smsEventList);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /* Batch Create SMS Event in Calendar */
    public void batch_Update_SMS_Event(Context context, int calId, List<SMS> smsList){
        try{
            // then adding the event to calendar
            Uri eventsUri;
            ContentValues event;
            Filter_Utils filterUtils = Filter_Utils.getInstance(context);
            for (SMS sms : smsList){
                eventsUri = CalendarContract.Events.CONTENT_URI;
                //Uri eventsUri = Uri.parse("content://com.android.calendar/events");
                if (Build.VERSION.SDK_INT < 8)
                    eventsUri = Uri.parse("content://calendar/events");
                eventsUri = ContentUris.withAppendedId(eventsUri, Long.parseLong(sms.getCal_event_id()));

                // Each event needs to be tied to a specific Calendar
                event = new ContentValues();
                TrackingFilter trackingFilter = filterUtils.selectRecord(sms.getNumbers());
                if (trackingFilter==null)
                    event.put(CalendarContract.Events.CALENDAR_ID, calId);
                else
                    event.put(CalendarContract.Events.CALENDAR_ID, trackingFilter.getSmsCalendarId());
                event.put(CalendarContract.Events.TITLE, sms.getGeneratedTitle());
                event.put(CalendarContract.Events.DESCRIPTION, sms.getGeneratedDescription());
                context.getContentResolver().update(eventsUri, event, null, null);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    // check an existing event at supposed time, if exising, return the id of event and (new) name contact of Call log
    String callLogContact;
    String callLogNumber;
    Long fromTime;
    Long toTime;
    String[] INSTANCE_PROJECTION;

    /* Batch Create SMS Event in Calendar */
    public void batch_Create_CALL_LOG_Event(Context context, int calId, List<Call_Log> call_logList){
        //int rows;
        try{
            ContentValues[] call_logEventList = new ContentValues[call_logList.size()];
            Call_Log call_log;
            Filter_Utils filterUtils = Filter_Utils.getInstance(context);
            for (int i = 0; i<call_logList.size(); i++){
                call_log = call_logList.get(i);
                // setup event information
                ContentValues contentValues = new ContentValues();
                // Each event needs to be tied to a specific Calendar
                TrackingFilter trackingFilter = filterUtils.selectRecord(call_log.getNumbers());
                if (trackingFilter==null)
                    contentValues.put(CalendarContract.Events.CALENDAR_ID, calId);
                else
                    contentValues.put(CalendarContract.Events.CALENDAR_ID, trackingFilter.getCallCalendarId());
                // We then set some of the basic information about the event
                contentValues.put(CalendarContract.Events.TITLE, call_log.getGeneratedTitle());
                contentValues.put(CalendarContract.Events.DESCRIPTION, call_log.getGeneratedDescription());
                contentValues.put(CalendarContract.Events.DTSTART, call_log.getFromTime());
                contentValues.put(CalendarContract.Events.DTEND, call_log.getFromTime()+call_log.getDurationMiliSecond());
                contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
                // add event value to list
                call_logEventList[i] = contentValues;
            }
            // then adding the event to calendar
            Uri eventsUri = CalendarContract.Events.CONTENT_URI;
            //Uri eventsUri = Uri.parse("content://com.android.calendar/events");
            if (Build.VERSION.SDK_INT < 8)
                eventsUri = Uri.parse("content://calendar/events");
            context.getContentResolver().bulkInsert(eventsUri, call_logEventList);
            System.out.println();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /* Batch Create SMS Event in Calendar */
    public void batch_Update_CALL_LOG_Event(Context context, int calId, List<Call_Log> call_logList){
        //int rows;
        try{
            // then adding the event to calendar
            Uri eventsUri;
            ContentValues event;
            Filter_Utils filterUtils = Filter_Utils.getInstance(context);
            for (Call_Log call_log : call_logList){
                //Uri eventsUri = Uri.parse("content://com.android.calendar/events");
                if (Build.VERSION.SDK_INT < 14)
                    eventsUri = Uri.parse("content://calendar/events");
                else
                    eventsUri = CalendarContract.Events.CONTENT_URI;
                eventsUri = ContentUris.withAppendedId(eventsUri, Long.parseLong(call_log.getCal_EventId()));

                // Create a new Single-Occurrence Event
                event = new ContentValues();
                // Each event needs to be tied to a specific Calendar
                TrackingFilter trackingFilter = filterUtils.selectRecord(call_log.getNumbers());
                if (trackingFilter==null)
                    event.put(CalendarContract.Events.CALENDAR_ID, calId);
                else
                    event.put(CalendarContract.Events.CALENDAR_ID, trackingFilter.getCallCalendarId());
                event.put(CalendarContract.Events.TITLE, call_log.getGeneratedTitle());
                event.put(CalendarContract.Events.DESCRIPTION, call_log.getGeneratedDescription());
                context.getContentResolver().update(eventsUri, event, null, null);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /*
    * Get All Events from Calendar
    * */
    public List<ContentValues> getAllCalendarEvents(List<String> titlePrefixes, String phoneNumber, int calId, Context context){
        return getCalendarEvents(-1, -1, titlePrefixes, phoneNumber, calId, context, 0, 0);
    }
    /*
    * Get Events from Calendar from a Specific Time to Current Time
    * */
    public List<ContentValues> get_Now_to_Future_ScheduleSMS(List<String> titlePrefixes, String phoneNumber, int calId, Context context, int limit, int offset){
        long fromTime = Calendar.getInstance().getTimeInMillis();
        long toTime = -1;
        return getCalendarEvents(fromTime, toTime, titlePrefixes, phoneNumber, calId, context, limit, offset);
    }
    /*
    * Get Events from Calendar from a Specific Time to Current Time
    * */
    public List<ContentValues> get_From_to_Now_CalendarEvents(List<String> titlePrefixes, String phoneNumber, int calId, Context context, Calendar calendar){
        long fromTime = calendar.getTimeInMillis();
        long toTime = Calendar.getInstance().getTimeInMillis();
        return getCalendarEvents(fromTime, toTime, titlePrefixes, phoneNumber, calId, context, 0, 0);
    }
    /*
    * Get Events from Calendar from Period A to Period B
    * */
    public List<ContentValues> get_From_To_CalendarEvents(List<String> titlePrefixes, String phoneNumber, int calId, Context context, Calendar calendarFrom, Calendar calendarTo){
        long fromTime = calendarFrom.getTimeInMillis();
        long toTime = calendarTo.getTimeInMillis();
        return getCalendarEvents(fromTime, toTime, titlePrefixes, phoneNumber, calId, context, 0, 0);
    }
    /*
    * Get Calendar Events from Calendar from FromTime to ToTime when Title of Event contains TitlePrefix
    * */
    public List<ContentValues> getCalendarEvents(long fromTime, long toTime, List<String> titlePrefixes, String phoneNumber, int calId, Context context, int limit, int offset){
        List<ContentValues> values = new ArrayList<ContentValues>();
        String[] arguments = new String[titlePrefixes.size()];
        int i=0;
        for (String title : titlePrefixes) {
            arguments[i]= title + "%";
            i++;
        }
        String orderBy;
        try {
            INSTANCE_PROJECTION = new String[]{
                    CalendarContract.Instances.EVENT_ID,      // 0
                    CalendarContract.Instances.TITLE,      // 1
                    CalendarContract.Instances.DESCRIPTION,         // 2
                    CalendarContract.Instances.DTSTART,         // 3
                    CalendarContract.Instances.DTEND,         // 4
                    CalendarContract.Instances.DURATION         // 5
            };
            // condition to select from Instances table
            selection = "( ("+CalendarContract.Instances.CALENDAR_ID + " = " + calId+")";
            //selection += " AND (" + CalendarContract.Instances.TITLE + " LIKE ? COLLATE NOCASE)";
            selection += " AND (";
            for (i=0; i<titlePrefixes.size(); i++){
                selection += CalendarContract.Instances.TITLE + " LIKE ? COLLATE NOCASE OR ";
            }
            selection = selection.substring(0, selection.lastIndexOf("OR")-1) + ")";
            if (phoneNumber!=null && !phoneNumber.equals("")){
                selection += " AND (" + CalendarContract.Instances.DESCRIPTION + " LIKE ? COLLATE NOCASE)";
                arguments = new String[titlePrefixes.size() + 1];
                i=0;
                for (String title : titlePrefixes) {
                    arguments[i]= title + "%";
                    i++;
                }
                arguments[i] = "%"+phoneNumber+"%";
            }
            selection += ")";

            if (limit>0 && offset>0)
                orderBy = CalendarContract.Instances.DTSTART + " ASC LIMIT " + limit + " OFFSET " + offset;
            else
                orderBy = CalendarContract.Instances.DTSTART + " ASC";

            // retrieve the event by supposed fromTime
            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            //if (Build.VERSION.SDK_INT < 8 )
            //  builder = Uri.parse("content://calendar/instances/when").buildUpon();

            long now = new Date().getTime();
            if (fromTime < 0){
                ContentUris.appendId(builder, now - DateUtils.YEAR_IN_MILLIS*100);
                ContentUris.appendId(builder, now);
            }
            else if (fromTime == toTime){
                toTime = fromTime + DateUtils.DAY_IN_MILLIS;
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);

            }
            else if (fromTime < toTime){
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);
            }
            else if (fromTime > toTime && toTime==-1){
                ContentUris.appendId(builder, now);
                ContentUris.appendId(builder, now + DateUtils.YEAR_IN_MILLIS*100);
            }

            Cursor eventCursor = context.getContentResolver().query(
                    builder.build(),
                    INSTANCE_PROJECTION,
                    selection,
                    arguments,
                    orderBy);

                long startTime, endTime, duration;
                while (eventCursor.moveToNext()){
                    // get event's information
                    id = eventCursor.getString(0);
                    title = eventCursor.getString(1);
                    description = eventCursor.getString(2);
                    startTime = eventCursor.getLong(3);
                    endTime = eventCursor.getLong(4);
                    duration = eventCursor.getLong(5);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(_EVENT_ID, id);
                    contentValues.put(_EVENT_TITLE, title);
                    contentValues.put(_EVENT_DESCRIPTION, description);
                    contentValues.put(_EVENT_START, startTime);
                    contentValues.put(_EVENT_END, endTime);
                    contentValues.put(_EVENT_DURATION, duration);

                    values.add(contentValues);
                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return values;
    }
    /*
    * Get SMS Events from Calendar from FromTime to ToTime when Title of Event contains TitlePrefix
    * */
    public Map<String, SMS> getCalendarSMS(long fromTime, long toTime, List<Long> startTimeList, List<Long> endTimeList, int calId, Context context){
        Map<String, SMS> result = new HashMap<String, SMS>();
        ContentValues contentValues = new ContentValues();
        try {
            INSTANCE_PROJECTION = new String[]{
                    CalendarContract.Instances.EVENT_ID,      // 0
                    CalendarContract.Instances.TITLE,      // 1
                    CalendarContract.Instances.DESCRIPTION,         // 2
                    CalendarContract.Instances.DTSTART,         // 3
                    CalendarContract.Instances.DTEND,         // 4
                    CalendarContract.Instances.DURATION         // 5
            };
            // condition to select from Instances table
            selection = "("+CalendarContract.Instances.CALENDAR_ID + " = " + calId+")";
            selection += " AND (";
            selection += CalendarContract.Instances.DTSTART + "=" + CalendarContract.Instances.DTEND + ") AND (";
            selection += CalendarContract.Instances.DTSTART + " IN (";
            for (int i = 0; i<startTimeList.size(); i++){
                //selection += " (" + CalendarContract.Instances.DTSTART + "=" + startTimeList.get(i) + " AND " + CalendarContract.Instances.DTEND + "=" + endTimeList.get(i) + ") ";
                selection += startTimeList.get(i);
                if (i<startTimeList.size()-1)
                    selection += ",";
            }
            selection += "))";

            // retrieve the event by supposed fromTime
            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            //if (Build.VERSION.SDK_INT < 8 )
            //  builder = Uri.parse("content://calendar/instances/when").buildUpon();

            if (fromTime < 0){
                long now = new Date().getTime();
                ContentUris.appendId(builder, now - DateUtils.YEAR_IN_MILLIS*20);
                ContentUris.appendId(builder, now);
            }
            else if (fromTime == toTime){
                toTime = fromTime + DateUtils.DAY_IN_MILLIS;
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);

            }
            else if (fromTime < toTime){
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);
            }

            Cursor eventCursor = context.getContentResolver().query(
                    builder.build(),
                    INSTANCE_PROJECTION,
                    selection,
                    null,
                    null);

            long startTime, endTime, duration;
                while (eventCursor.moveToNext()){
                    // get event's information
                    id = eventCursor.getString(0);
                    title = eventCursor.getString(1);
                    description = eventCursor.getString(2);
                    startTime = eventCursor.getLong(3);
                    endTime = eventCursor.getLong(4);
                    duration = eventCursor.getLong(5);

                    contentValues.put(_EVENT_ID, id);
                    contentValues.put(_EVENT_TITLE, title);
                    contentValues.put(_EVENT_DESCRIPTION, description);
                    contentValues.put(_EVENT_START, startTime);
                    contentValues.put(_EVENT_END, endTime);
                    contentValues.put(_EVENT_DURATION, duration);

                    SMS sms = _utilsFactory.getSmsUtils().getSmsFromCalendarEvent(contentValues);
                    // put <hashvalue,call_log> item into Map result
                    result.put(sms.getFixedHashValue(), sms);
                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    /*
    * Get SMS Events from Calendar from FromTime to ToTime when Title of Event contains TitlePrefix
    * */
    public Map<String, Call_Log> getCalendarCALL(long fromTime, long toTime, List<Long> startTimeList, List<Long> endTimeList, int calId, Context context){
        Map<String, Call_Log> result = new HashMap<String, Call_Log>();
        ContentValues contentValues = new ContentValues();
        try {
            INSTANCE_PROJECTION = new String[]{
                    CalendarContract.Instances.EVENT_ID,      // 0
                    CalendarContract.Instances.TITLE,      // 1
                    CalendarContract.Instances.DESCRIPTION,         // 2
                    CalendarContract.Instances.DTSTART,         // 3
                    CalendarContract.Instances.DTEND,         // 4
                    CalendarContract.Instances.DURATION         // 5
            };
            // condition to select from Instances table
            selection = "("+CalendarContract.Instances.CALENDAR_ID + " = " + calId+")";
            selection += " AND ("+CalendarContract.Instances.DTSTART+" IN (";
            for (int i = 0; i<startTimeList.size(); i++){
                selection += startTimeList.get(i);
                if (i<startTimeList.size()-1)
                    selection += ",";
            }
            selection += "))";

            // retrieve the event by supposed fromTime
            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            //if (Build.VERSION.SDK_INT < 8 )
            //  builder = Uri.parse("content://calendar/instances/when").buildUpon();

            if (fromTime < 0){
                long now = new Date().getTime();
                ContentUris.appendId(builder, now - DateUtils.YEAR_IN_MILLIS*100);
                ContentUris.appendId(builder, now);
            }
            else if (fromTime == toTime){
                toTime = fromTime + DateUtils.DAY_IN_MILLIS;
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);

            }
            else if (fromTime < toTime){
                // calendar's event does not use millisecond, so we have to eliminate millisecond part (convert to 0 millisecond)
                ContentUris.appendId(builder, (fromTime / 1000) * 1000);
                ContentUris.appendId(builder, (toTime / 1000) * 1000);
            }

            Cursor eventCursor = context.getContentResolver().query(
                    builder.build(),
                    INSTANCE_PROJECTION,
                    selection,
                    null,
                    null);

            long startTime, endTime, duration;
            while (eventCursor.moveToNext()) {
                    // get event's information
                    id = eventCursor.getString(0);
                    title = eventCursor.getString(1);
                    description = eventCursor.getString(2);
                    startTime = eventCursor.getLong(3);
                    endTime = eventCursor.getLong(4);
                    duration = eventCursor.getLong(5);

                    contentValues.put(_EVENT_ID, id);
                    contentValues.put(_EVENT_TITLE, title);
                    contentValues.put(_EVENT_DESCRIPTION, description);
                    contentValues.put(_EVENT_START, startTime);
                    contentValues.put(_EVENT_END, endTime);
                    contentValues.put(_EVENT_DURATION, duration);
                    // get call log item from event value
                    Call_Log call_log = _utilsFactory.getCallLogUtils().getCallLogFromCalendarEvent(contentValues);
                if (call_log == null)
                    continue;
                    // put <hashvalue,call_log> item into Map result
                    result.put(call_log.getFixedHashValue(), call_log);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    /* Batch delete Events in Calendar */
    public int deleteEvents(Context context, List<String> eventIdList){
        int result = 0;
        try{
            // return false if calendar id < 0
            if (eventIdList==null || eventIdList.size()==0)
                return result;
            // process delete event if calendar id > 0
            // find and delete calendar event based on id
            Uri eventsUri;
            //Uri eventsUri = Uri.parse("content://com.android.calendar/events");
            if (Build.VERSION.SDK_INT < 8)
                eventsUri = Uri.parse("content://calendar/events");
            else
                eventsUri = CalendarContract.Events.CONTENT_URI;

            String condition = CalendarContract.Events._ID + " IN (";
            String[] argument = new String[eventIdList.size()];
            for (int i=0; i<eventIdList.size(); i++){
                condition += "?,";
                argument[i] = eventIdList.get(i);
            }
            condition = condition.substring(0, condition.length()-1);
            condition += ")";
            result = context.getContentResolver().delete(eventsUri, condition, argument);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }
 }
