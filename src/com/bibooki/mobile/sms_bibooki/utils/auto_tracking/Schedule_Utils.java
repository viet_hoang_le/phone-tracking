package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.content.Context;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

import java.util.concurrent.TimeUnit;

public class Schedule_Utils {

    static Context mContext;

    static UtilsFactory _utilsFactory;
    static Schedule_Utils _schedule_utils = null;
    private static PreferencesHelper _preferenceHelper;
    private static NotificationUtils _notificationUtils;

    private AlarmSMS_BroadcastReceiver alarmSMS = new AlarmSMS_BroadcastReceiver();
    private AlarmCall_BroadcastReceiver alarmCall = new AlarmCall_BroadcastReceiver();
    // return new instance
    public static Schedule_Utils getInstance(Context context){
        if (_schedule_utils == null)
            _schedule_utils = new Schedule_Utils();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        _notificationUtils = _utilsFactory.getNotificationUtils();
        return _schedule_utils;
    }

    public void startRepeatingTimer_Sms(Context context) {
        if(alarmSMS != null){
            long trackingTime = 0;
            int autoTrackingMinute = _preferenceHelper.read_Auto_Tracking_Time(context);
            trackingTime = TimeUnit.MINUTES.toMillis(autoTrackingMinute);
            alarmSMS.SetRepeatAlarm(context, trackingTime);
            _notificationUtils.showShortToast(
                    context.getResources().getString(R.string.notification_smsTracking_timer) + " " +
                    TimeUnit.MILLISECONDS.toHours(trackingTime) + " " +
                            context.getResources().getString(R.string.notification_timer_hours));

        }else{
            //Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }
    public void startRepeatingTimer_Call(Context context) {
        if(alarmCall != null){
            long trackingTime = 0;
            int autoTrackingMinute = _preferenceHelper.read_Auto_Tracking_Time(context);
            trackingTime = TimeUnit.MINUTES.toMillis(autoTrackingMinute);
            alarmCall.SetRepeatAlarm(context, trackingTime);
            _notificationUtils.showShortToast(
                    context.getResources().getString(R.string.notification_callTracking_timer) + " " +
                            TimeUnit.MILLISECONDS.toHours(trackingTime) + " " +
                            context.getResources().getString(R.string.notification_timer_hours));
        }else{
            //Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelSMS_RepeatingTimer(Context context){
        if(alarmSMS != null){
            alarmSMS.CancelAlarm(context);
            _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_stopAutoSMS_message));
        }
    }
    public void cancelCall_RepeatingTimer(Context context){
        if(alarmSMS != null){
            if(alarmCall != null){
                alarmCall.CancelAlarm(context);
                _notificationUtils.showShortToast(context.getResources().getString(R.string.notification_stopAutoCall_message));
            }
        }
    }

    public void onetimeTimer_Sms(Context context, long trackingTime){
        if(alarmSMS != null){
            alarmSMS.CancelAlarm(context);
            alarmSMS.setOnetimeTimer(context, trackingTime);
        }else{
            //Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }
    public void onetimeTimer_Call(Context context, long trackingTime){
        if(alarmCall != null){
            alarmCall.CancelAlarm(context);
            alarmCall.setOnetimeTimer(context, trackingTime);
        }else{
            //Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }
}
