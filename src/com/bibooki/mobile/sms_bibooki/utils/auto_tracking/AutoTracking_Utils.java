package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.content.Context;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

public class AutoTracking_Utils {
    static AutoTracking_Utils _autoTracking_utils = null;

    static Context mContext;
    private static UtilsFactory _utilsFactory;
    static PreferencesHelper _preferenceHelper;


    // return new instance
    public static AutoTracking_Utils getInstance(Context context){
        if (_autoTracking_utils == null)
            _autoTracking_utils = new AutoTracking_Utils();
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        return _autoTracking_utils;
    }
/*
    // create the auto Tracking dialog for sms and call tracking
    
    public Dialog setAutoTrackingDialog(Context _context, Activity activity, final CheckedTextView cView, TextView autoTrackingStt){
        Dialog dialog;
        String title = _context.getResources().getString(R.string.auto_tracking_dialog_title);
        String ck_text = _context.getResources().getString(R.string.auto_tracking_ck_text_view);
        String[] items = {autoTrackingOption1, autoTrackingOption2, autoTrackingOption3, autoTrackingOption4, autoTrackingOption5,
                            autoTrackingOption6, autoTrackingOption7, autoTrackingOption8, autoTrackingOption9};
        dialog = _utilsFactory.getDialog_Utils().createDialog(_context, activity, Dialog_Utils.DIALOG_TYPE.TRACKING_TIME_DIALOG, items, title, cView, ck_text, null);
        return  dialog;
    }
           */
}
