package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CalendarContract;
import android.provider.CallLog;
import android.support.v4.app.NotificationCompat;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.SplashScreen;
import com.bibooki.mobile.sms_bibooki.utils.*;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Sms_Service extends Service {
    static Context mContext;
    private static Uri CONTENT_CALENDAR;
    private static final Uri CONTENT_SMS = Uri.parse("content://sms");
    private static final Uri CONTENT_CALL_LOG = CallLog.Calls.CONTENT_URI;
    private static final String TAG = "Sms_CallLog_Service";
    private static NotificationUtils _notificationUtils;

    private WindowManager windowManager;
    private ImageView chatHead;
    private LinearLayout deleteBar;
    private ImageView deleteIcon;
    Point size = new Point();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        _notificationUtils.showShortToast("Unbinded Sms_Service!");
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        _notificationUtils = UtilsFactory.newInstance(mContext).getNotificationUtils();

        ContentResolver contentResolver = mContext.getContentResolver();
        //Log.v(TAG, "starting........");
        // SMS observer
        Alarm_SmsObserver sms_observer = new Alarm_SmsObserver(mContext, new Handler());
        contentResolver.registerContentObserver(CONTENT_SMS,true, sms_observer);
        // CALL LOG observer
        Alarm_CallLogObserver call_observer = new Alarm_CallLogObserver(mContext, new Handler());
        contentResolver.registerContentObserver(CONTENT_CALL_LOG,true, call_observer);
        // CALENDAR observer
        Alarm_CalendarObserver calendar_observer = new Alarm_CalendarObserver(mContext, new Handler());

        if (Build.VERSION.SDK_INT >= 14)
            CONTENT_CALENDAR = CalendarContract.Events.CONTENT_URI;
        if (Build.VERSION.SDK_INT >= 8 && Build.VERSION.SDK_INT < 14)
            CONTENT_CALENDAR = Uri.parse("content://com.android.calendar/events");
        if (Build.VERSION.SDK_INT < 8)
            CONTENT_CALENDAR = Uri.parse("content://calendar/events");
        contentResolver.registerContentObserver(CONTENT_CALENDAR, true, calendar_observer);

        //NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(i.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext).setSmallIcon(R.drawable.ic_notification);
        mBuilder.setContentTitle("Sms Bibooki")
                .setContentText("SMS & Call_Log in Calendars");
        mBuilder.setWhen(System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, SplashScreen.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        mBuilder.setContentIntent(pendingIntent);
        // Hide the notification after its selected
        /*
        _notificationUtils.showShortToast("Phone Tracking service is started!");*/
        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_FOREGROUND_SERVICE;
        startForeground(1988, notification);
    }

    @Override
    public void onDestroy() {
        //Log.d(TAG, "stopping........");
        // notification
        Format formatter = new SimpleDateFormat("hh:mm:ss a");
        _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_stopService_message), formatter.format(new Date()));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.v(TAG, "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }
}
