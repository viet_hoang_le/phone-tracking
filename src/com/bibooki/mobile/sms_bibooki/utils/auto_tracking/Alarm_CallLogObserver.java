package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;

import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

import java.util.concurrent.TimeUnit;


class Alarm_CallLogObserver extends ContentObserver {
    private Context mContext;
    private static UtilsFactory _utilsFactory;
    private static Schedule_Utils _scheduleUtils;
    private static PreferencesHelper _preferenceHelper;

    public Alarm_CallLogObserver(Context context, Handler handler) {
        super(handler);
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _scheduleUtils = _utilsFactory.getSchedule_Utils();
       _preferenceHelper = _utilsFactory.getPreferencesHelper();
    }
    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        //Log.v(TAG, "****************************** SMS change detected *************************************");
        //Log.v(TAG, "Notification on SMS observer");
        // custom process here
        int autoTrackingMinute = _preferenceHelper.read_Auto_Tracking_Time(mContext);
        // if the configuration had set
        if (autoTrackingMinute!=60 && autoTrackingMinute!=360 && autoTrackingMinute!=720 && autoTrackingMinute!=1440){
            long trackingTime = TimeUnit.MINUTES.toMillis(autoTrackingMinute);
            _scheduleUtils.onetimeTimer_Call(mContext, trackingTime);
        }
        // this will make it point to the first record, which is the last SMS sent
        //Log.v(TAG, "content: " + content);
    }
    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }
}
