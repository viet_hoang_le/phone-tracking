package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;

public class AutoStart extends BroadcastReceiver
{
    private Context mContext;
    private static UtilsFactory _utilsFactory;
    private static NotificationUtils _notificationUtils;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        mContext = context;
        //Toast.makeText(mContext, "Device boot completed", Toast.LENGTH_SHORT).show();
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _notificationUtils = _utilsFactory.getNotificationUtils();
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
        {
            _notificationUtils.showShortToast(mContext.getResources().getString(R.string.notification_startService_message));
            // Start service for auto tracking SMS & Call Log
            Intent service = new Intent(mContext, Sms_Service.class);
            mContext.startService(service);
            // process schedule here
            _utilsFactory.getSchedule_Utils().startRepeatingTimer_Sms(mContext);
            _utilsFactory.getSchedule_Utils().startRepeatingTimer_Call(mContext);
        }
    }
}
