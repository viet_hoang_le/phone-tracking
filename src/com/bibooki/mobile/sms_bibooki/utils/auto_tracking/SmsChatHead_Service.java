package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bibooki.mobile.sms_bibooki.R;
import com.bibooki.mobile.sms_bibooki.SmsDialogActivity;
import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class SmsChatHead_Service extends Service {

    //String from, msg, smsUri;
    //long time;
    static Context mContext;
    private static final String TAG = "SmsChatHead_Service";
    private static NotificationUtils _notificationUtils;
    ContactsHelper _contactsHelper;

    private WindowManager windowManager;
    private RelativeLayout chatHeadLayout;
    private ImageView chatHead_contact;
    private LinearLayout deleteBar;
    private ImageView deleteIcon;
    Point size = new Point();


    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind SmsChatHead_Service");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        //_notificationUtils.showShortToast("Unbinded SmsChatHead_Service!");
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        _notificationUtils = UtilsFactory.newInstance(mContext).getNotificationUtils();
        _contactsHelper = ContactsHelper.getInstance(mContext);
    }

    @Override
    public void onDestroy() {
        //Log.d(TAG, "stopping........");
        // notification
        Format formatter = new SimpleDateFormat("hh:mm:ss a");
        _notificationUtils.notifyMessage(mContext.getResources().getString(R.string.notification_stopService_message), formatter.format(new Date()));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.v(TAG, "Received start id " + startId + ": " + intent);

        String smsUri = intent.getStringExtra("smsUri");
        String from = intent.getStringExtra("From");
        String msg = intent.getStringExtra("Msg");
        long time = intent.getLongExtra("Time", 0);
        showNewSms(smsUri, from, msg, time);
        stopSelf();
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }
    /** method for clients */
    public void showNewSms(final String smsUri, final String smsNumber, final String smsBody, final long smsTime) {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getSize(size);
        final int width = size.x;
        final int height = size.y;
        final int imageWidth = 200;
        final int imageHeight = 200;
        final int radius = 20;

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = width;
        params.y = 150;
        final WindowManager.LayoutParams paramsDeleteBar = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        paramsDeleteBar.gravity = Gravity.TOP | Gravity.LEFT;

        deleteBar = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.delete_bar, null);
        deleteBar.setVisibility(View.GONE);
        deleteIcon = (ImageView) deleteBar.findViewById(R.id.imageView_deleteView);

        chatHeadLayout = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.headchat_sms, null);
        chatHead_contact = (ImageView) chatHeadLayout.findViewById(R.id.sms_icon);
        Log.i(TAG, "from = " + smsNumber);
        Bitmap bm = _contactsHelper.getContactBitmapImageByNumber(smsNumber);
        if (bm!=null)
            chatHead_contact.setImageBitmap(bm);
        chatHead_contact.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ViewGroup.LayoutParams paramsDeleteIcon = deleteIcon.getLayoutParams();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if (event.getRawY() <= height / 5 && event.getRawX() >= (width / 2 - 300) && event.getRawX() <= (width / 2 + 300)) {
                            windowManager.removeViewImmediate(chatHeadLayout);
                        } else if (event.getRawX() >= (initialTouchX - radius) && event.getRawX() <= (initialTouchX + radius) &&
                                event.getRawY() >= (initialTouchY - radius) && event.getRawY() <= (initialTouchY + radius)) {
                            //Log.i(TAG, "on click");
                            Intent smsDialog = new Intent(mContext, SmsDialogActivity.class);
                            smsDialog.putExtra("smsUri", smsUri);
                            smsDialog.putExtra("Msg", smsBody);
                            smsDialog.putExtra("From", smsNumber);
                            smsDialog.putExtra("Time", smsTime);
                            smsDialog.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(smsDialog);
                            params.x = width - 130;
                            params.y = 130;
                            windowManager.updateViewLayout(chatHeadLayout, params);
                        }
                        deleteBar.setVisibility(View.GONE);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHeadLayout, params);
                        deleteBar.setVisibility(View.VISIBLE);
                        if (event.getRawY() <= height / 5 && event.getRawX() >= (width / 2 - 300) && event.getRawX() <= (width / 2 + 300)) {
                            paramsDeleteIcon.width = imageWidth + 80;
                            paramsDeleteIcon.height = imageHeight + 80;
                            deleteIcon.setLayoutParams(paramsDeleteIcon);

                            params.x = (int) deleteIcon.getX() + (paramsDeleteIcon.width - chatHeadLayout.getWidth())/2;
                            params.y = (int) deleteIcon.getY() + (paramsDeleteIcon.height - chatHeadLayout.getHeight())/2;

                            windowManager.updateViewLayout(chatHeadLayout, params);
                        } else{
                            paramsDeleteIcon.height = imageHeight;
                            paramsDeleteIcon.width = imageWidth;
                            deleteIcon.setLayoutParams(paramsDeleteIcon);
                        }
                        return true;
                }
                return false;
            }
        });
        windowManager.addView(deleteBar, paramsDeleteBar);
        windowManager.addView(chatHeadLayout, params);
    }
}
