package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.Auto_SMS_Receiver;

class Alarm_CalendarObserver extends ContentObserver {
    private Context mContext;

    private static UtilsFactory _utilsFactory;
    private static PreferencesHelper _preferenceHelper;
    private static Schedule_Utils _scheduleUtils;

    public Alarm_CalendarObserver(Context context, Handler handler) {
        super(handler);
        mContext = context;
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        _scheduleUtils = _utilsFactory.getSchedule_Utils();
    }
    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        //Log.v(TAG, "****************************** CALENDAR change detected *************************************");
        //Log.v(TAG, "Notification on CALENDAR observer");
        // custom process here
        // reading Shared Preferences to choose the URI for CALENDAR or CALL Log
        Intent intent = new Intent(mContext, Auto_SMS_Receiver.class);
        mContext.sendBroadcast(intent);
        // this will make it point to the first record, which is the last SMS sent
        //Log.v(TAG, "content: " + content);
    }
    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }
}
