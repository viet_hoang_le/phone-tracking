package com.bibooki.mobile.sms_bibooki.utils.auto_tracking;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;

import com.bibooki.mobile.sms_bibooki.utils.Tracking_Utils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

import java.text.Format;
import java.text.SimpleDateFormat;

public class AlarmCall_BroadcastReceiver extends BroadcastReceiver {

    Format formatterMinute = new SimpleDateFormat("mm");

    private static final String TAG = "AlarmCall_BroadcastReceiver";
    final public static String ONE_TIME = "onetime";
    private static PreferencesHelper _preferenceHelper;
    private static Tracking_Utils _trackingUtils;

    @Override
    public void onReceive(Context context, Intent intent) {
        _preferenceHelper = UtilsFactory.newInstance(context).getPreferencesHelper();
        _trackingUtils = UtilsFactory.newInstance(context).getTracking_Utils();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        //Acquire the lock
        wl.acquire();

        //You can do the processing here update the widget/remote views.
        // notificate by Toast Message
        Bundle extras = intent.getExtras();
        StringBuilder msgStr = new StringBuilder();

        if (extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)) {
            msgStr.append("One time Timer");
        }
        // process Tracking
        //Log.v(TAG, "Ready to tracking Call Log");
        _trackingUtils.trackingCallLog(context, true);

        //msgStr.append("Alarm Call Tracking at: " + formatter.format(new Date()));
        //Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();

        //Release the lock
        wl.release();

    }
    public void SetRepeatAlarm(Context context, long trackingTime) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmCall_BroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After trackingTime
        if (trackingTime>0)
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), trackingTime, pi);
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmCall_BroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
    public void setOnetimeTimer(Context context, long trackingTime) {
        //Log.v(TAG, "One time timer for Call Log");
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmCall_BroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After trackingTime
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+trackingTime, pi);
        //Toast.makeText(context, "Call Log will be tracked in " + formatterMinute.format(trackingTime) + " minutes later.", Toast.LENGTH_SHORT).show();
    }
}
