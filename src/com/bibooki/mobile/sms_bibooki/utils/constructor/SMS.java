package com.bibooki.mobile.sms_bibooki.utils.constructor;

import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;

public class SMS {

    public static final String SMS_COLUMN_BODY = "body";
    public static final String SMS_COLUMN_READ = "read";
    public static final String SMS_COLUMN_ADDRESS = "address";
    public static final String SMS_COLUMN_SERVICE_CENTER = "service_center";
    public static final String SMS_COLUMN_DATE = "date";
    public static final String SMS_COLUMN_TYPE = "type";

    public final static int RECEIVE_TYPE = 1;
    public final static int SEND_TYPE = 2;

    String smsBody = "",
            numbers = "",
            contactNames = "",
            ownerName = "",
            ownerNumber = "",
            cal_event_id = "",
            calendarTitleHash = "",
            fixedHashValue = "";
    long smsTime = 0;
    // type 1 is sms that we received, and 2 is sms that we sent
    int type = 0;
    boolean isRead = false;

    String smsId = "";

    String eventTitle = "";
    String eventDescription = "";

    String generatedTitle = "";
    String generatedDescription = "";
    long startTime = 0, endTime = 0;
    // custom attributes
    public boolean isCalendarTracked = false;
    public boolean isCalendarUpdated = true;

    String fromNumber, toNumber;
    String service_center="";

    public String getSmsBody() {
        return smsBody;
    }

    public void setSmsBody(String smsBody) {
        this.smsBody = smsBody;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getContactNames() {
        return contactNames;
    }

    public void setContactNames(String contactNames) {
        this.contactNames = contactNames;
    }

    public long getSmsTime() {
        return smsTime;
    }

    public void setSmsTime(long smsTime) {
        this.smsTime = smsTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerNumber() {
        return ownerNumber;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.ownerNumber = ownerNumber;
    }

    public String getCal_event_id() {
        return cal_event_id;
    }

    public void setCal_event_id(String cal_event_id) {
        this.cal_event_id = cal_event_id;
    }

    public String getCalendarTitleHash() {
        return calendarTitleHash;
    }

    public void setCalendarTitleHash(String calendarTitleHash) {
        this.calendarTitleHash = calendarTitleHash;
    }

    public String getFixedHashValue() {
        return fixedHashValue;
    }

    public void setFixedHashValue(String fixedHashValue) {
        this.fixedHashValue = fixedHashValue;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getGeneratedTitle() {
        return generatedTitle;
    }

    public void setGeneratedTitle(String generatedTitle) {
        this.generatedTitle = generatedTitle;
    }

    public String getGeneratedDescription() {
        return generatedDescription;
    }

    public void setGeneratedDescription(String generatedDescription) {
        this.generatedDescription = generatedDescription;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getService_center() {
        return service_center;
    }

    public void setService_center(String service_center) {
        this.service_center = service_center;
    }

    public void regenerateDescription(){
        int smsType = getType();
        String description = "";
        switch (smsType) {
            case 1:
                description = Calendar_Utils._SMS_FROM + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + getSmsTime();
                break;
            case 2:
                description = Calendar_Utils._SMS_TO + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + getSmsTime();
        }
        setGeneratedDescription(description);
    }

    public String getSmsId() {
        return smsId;
    }

    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }
}
