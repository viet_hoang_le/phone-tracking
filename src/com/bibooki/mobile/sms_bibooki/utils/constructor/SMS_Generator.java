package com.bibooki.mobile.sms_bibooki.utils.constructor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class SMS_Generator {
    public static String _UNKNOWN_PHONE_NUMBER = "Unidentified Phone Number";

    private Context mContext;
    private UtilsFactory _utilsFactory;
    private ContactsHelper _contactsHelper;
    private AdvancedSettingPreference _advancedSettingPreference;

    MessageDigest messageDigest = null;

    SMS sms;
    // reading sms
    String smsBody, number, contactName, title = null, description = null;
    String serviceCenter;
    String encryptedString;
    long smsTime;
    int smsType;
    int smsId;
    boolean isRead;
    // reading owner name and number
    String ownerName, ownerNumber;

    public SMS_Generator(Context context){
        try {
            mContext = context;
            _utilsFactory = UtilsFactory.newInstance(context);
            _contactsHelper = ContactsHelper.getInstance(context);
            _advancedSettingPreference = _utilsFactory.getAdvancedSettingPreference();
            messageDigest = MessageDigest.getInstance("SHA-256");

            // get my own name and phone number
            ownerNumber = _contactsHelper.getMyPhoneNumber();
            if (ownerNumber == null || ownerNumber.equals("") || ownerNumber.equals("?"))
                ownerNumber = _UNKNOWN_PHONE_NUMBER;
            ownerName = _contactsHelper.getContactDisplayNameByNumber(ownerNumber);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public SMS getSMSfromCursor(Cursor curSms){
        try {

            SMS sms = new SMS();

            smsId = curSms.getInt(curSms.getColumnIndexOrThrow(BaseColumns._ID));
            smsBody = curSms.getString(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_BODY));
            number = curSms.getString(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_ADDRESS));
            serviceCenter = curSms.getString(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_SERVICE_CENTER));
            contactName = _contactsHelper.getContactDisplayNameByNumber( number);
            if (contactName.equals(ContactsHelper._UNKNOWN_CONTACT)) {
                contactName = number;
            }
            smsTime = curSms.getLong(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_DATE));
            smsType = curSms.getInt(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_TYPE));
            isRead = curSms.getInt(curSms.getColumnIndexOrThrow(SMS.SMS_COLUMN_READ))>0;

            sms.setContactNames(contactName);
            sms.setSmsId(String.valueOf(smsId));
            sms.setSmsBody(smsBody);
            sms.setNumbers(number);
            sms.setSmsTime(smsTime);
            sms.setType(smsType);
            sms.setOwnerName(ownerName);
            sms.setOwnerNumber(ownerNumber);
            sms.setService_center(serviceCenter);
            sms.setRead(isRead);
            sms = setFromTo(sms);

            // set up sms information for Calendar Event
            sms.setGeneratedTitle(generateCalendarEventTitle(sms));
            sms.setGeneratedDescription(generateCalendarEventDescription(sms));

            sms.setStartTime(smsTime);
            sms.setEndTime(smsTime);
            // get hash value of sms description
            encryptedString = getHashValue(ownerName, contactName, sms.getGeneratedTitle());
            sms.setCalendarTitleHash(encryptedString);
            encryptedString = getFixedHashValue(number, ownerNumber, smsBody, smsType, smsTime);
            sms.setFixedHashValue(encryptedString);

            return sms;
        } catch (Exception e) {
            e.printStackTrace();
            return  null;
        }
    }

    public SMS convertEventToSMS(ContentValues contentValues) {
        //String event_id, title;
        String tempString;
        String event_id, eventContactName = null, eventOwnerName = null, eventTitle = null, eventDescription = null;

        sms = new SMS();
        // get values from event
        event_id = contentValues.getAsString(Calendar_Utils._EVENT_ID);
        eventTitle = contentValues.getAsString(Calendar_Utils._EVENT_TITLE);
        eventDescription = contentValues.getAsString(Calendar_Utils._EVENT_DESCRIPTION);
        try {
            if (eventDescription.contains(Calendar_Utils._SMS_FROM)) {
                smsType = 1;
                // get Owner Number of the Phone
                ownerNumber = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MY_NUMBER)).replace(Calendar_Utils._MY_NUMBER, "");
                ownerNumber = ownerNumber.substring(0, ownerNumber.indexOf("\n"));
                // get Owner Name of the Phone
                eventOwnerName = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._TO_ME)).replace(Calendar_Utils._TO_ME, "");
                eventOwnerName = eventOwnerName.substring(0, eventOwnerName.indexOf("\n"));
                // get Contact Number of the SMS
                eventContactName = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._SMS_FROM)).replace(Calendar_Utils._SMS_FROM, "");
                eventContactName = eventContactName.substring(0, eventContactName.indexOf("\n"));
                // get Contact Name of the SMS
                number = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MOBILE_NUMBER)).replace(Calendar_Utils._MOBILE_NUMBER, "");
                number = number.substring(0, number.indexOf("\n"));
                // get content of the SMS
                smsBody = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._LINE)).replaceFirst(Calendar_Utils._LINE, "");
                smsBody = smsBody.substring(0, smsBody.indexOf("\n" + Calendar_Utils._LINE));
                // get Time of the SMS
                tempString = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._AT_TIME)).replace(Calendar_Utils._AT_TIME, "").trim();
                smsTime = Long.parseLong(tempString);
                // get Service Center used to send/receive
                // adding if condition because old version does not have this field value
                if (eventDescription.contains(Calendar_Utils._SERVICE_CENTER)){
                    serviceCenter = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._SERVICE_CENTER)).replace(Calendar_Utils._SERVICE_CENTER, "");
                    serviceCenter = serviceCenter.substring(0, serviceCenter.indexOf("\n"));
                }
            } else if (eventDescription.contains(Calendar_Utils._SMS_TO)) {
                smsType = 2;
                // get Owner Number of the Phone
                ownerNumber = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MY_NUMBER)).replace(Calendar_Utils._MY_NUMBER, "");
                ownerNumber = ownerNumber.substring(0, ownerNumber.indexOf("\n"));
                // get Owner Name of the Phone
                eventOwnerName = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._FROM_ME)).replace(Calendar_Utils._FROM_ME, "");
                eventOwnerName = eventOwnerName.substring(0, eventOwnerName.indexOf("\n"));
                // get Contact Number of the SMS
                eventContactName = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._SMS_TO)).replace(Calendar_Utils._SMS_TO, "");
                eventContactName = eventContactName.substring(0, eventContactName.indexOf("\n"));
                // get Contact Name of the SMS
                number = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MOBILE_NUMBER)).replace(Calendar_Utils._MOBILE_NUMBER, "");
                number = number.substring(0, number.indexOf("\n"));
                // get content of the SMS
                smsBody = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._LINE)).replaceFirst(Calendar_Utils._LINE, "");
                smsBody = smsBody.substring(0, smsBody.indexOf("\n" + Calendar_Utils._LINE));
                // get Time of the SMS
                tempString = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._AT_TIME)).replace(Calendar_Utils._AT_TIME, "").trim();
                smsTime = Long.parseLong(tempString);
                // get Service Center used to send/receive
                // adding if condition because old version does not have this field value
                if (eventDescription.contains(Calendar_Utils._SERVICE_CENTER)) {
                    serviceCenter = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._SERVICE_CENTER)).replace(Calendar_Utils._SERVICE_CENTER, "");
                    serviceCenter = serviceCenter.substring(0, serviceCenter.indexOf("\n"));
                }
            }
            if (ownerNumber == null || ownerNumber.equals("") || ownerNumber.equals("?"))
                ownerNumber = _UNKNOWN_PHONE_NUMBER;
            ownerName = _contactsHelper.getContactDisplayNameByNumber(ownerNumber);
            contactName = _contactsHelper.getContactDisplayNameByNumber(number);
            if (contactName.equals(ContactsHelper._UNKNOWN_CONTACT)) {
                contactName = number;
            }

            // set sms values
            sms.setCal_event_id(event_id);
            sms.setContactNames(contactName);
            sms.setSmsBody(smsBody);
            sms.setOwnerNumber(ownerNumber);
            sms.setOwnerName(ownerName);
            sms.setNumbers(number);
            sms.setSmsTime(smsTime);
            sms.setType(smsType);
            sms.setService_center(serviceCenter);
            sms = setFromTo(sms);

            sms.setEventTitle(eventTitle);
            sms.setEventDescription(eventDescription);

            sms.setGeneratedTitle(generateCalendarEventTitle(sms));
            sms.setGeneratedDescription(generateCalendarEventDescription(sms));

            sms.setStartTime(smsTime);
            sms.setEndTime(smsTime);
            // get hash value of sms description
            encryptedString = getHashValue(eventOwnerName, eventContactName, eventTitle);
            sms.setCalendarTitleHash(encryptedString);
            encryptedString = getFixedHashValue(number, ownerNumber, smsBody, smsType, smsTime);
            sms.setFixedHashValue(encryptedString);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sms;
    }

    // get generated description for sms
    private String generateCalendarEventTitle(SMS sms) {
        int smsType = sms.getType();
        String title = "";
        switch (smsType) {
            case 1:
                title = Calendar_Utils._SMS_FROM + sms.getContactNames() + " (" + _utilsFactory.getSmsUtils().getSmsTitleContent(mContext, sms) + ")";
                break;
            case 2:
                title = Calendar_Utils._SMS_TO + sms.getContactNames() + " (" + _utilsFactory.getSmsUtils().getSmsTitleContent(mContext, sms) + ")";
        }
        return title;
    }

    // get generated description for sms
    private String generateCalendarEventDescription(SMS sms) {
        int smsType = sms.getType();
        String description = "";
        switch (smsType) {
            case 1:
                description = Calendar_Utils._SMS_FROM + sms.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + sms.getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + sms.getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + sms.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + sms.getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + sms.getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + sms.getSmsTime();
                break;
            case 2:
                description = Calendar_Utils._SMS_TO + sms.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + sms.getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + sms.getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + sms.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + sms.getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + sms.getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + sms.getSmsTime();
        }
        return description;
    }

    // get generated description for sms
    private String generateMailContent(SMS sms) {
        int smsType = sms.getType();
        String description = "";
        switch (smsType) {
            case 1:
                description =
                        sms.getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._SMS_FROM + sms.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + sms.getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + sms.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + sms.getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + sms.getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + sms.getSmsTime();
                break;
            case 2:
                description =
                        sms.getSmsBody() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._SMS_TO + sms.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + sms.getNumbers() + "\n"
                        + Calendar_Utils._SMS_CONTENT
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + sms.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + sms.getOwnerNumber() + "\n"
                        + Calendar_Utils._SERVICE_CENTER + sms.getService_center() + "\n"
                        + Calendar_Utils._AT_TIME + sms.getSmsTime();
        }
        return description;
    }

    // get generated from/to address for sms in mail backup
    private SMS setFromTo(SMS sms) {
        int smsType = sms.getType();
        String from = null,to = null;
        switch (smsType) {
            case 1:
                from = sms.getNumbers();
                to = sms.getOwnerNumber();
                break;
            case 2:
                from = sms.getOwnerNumber();
                to = sms.getNumbers();
        }
        sms.setFromNumber(from);
        sms.setToNumber(to);
        return sms;
    }

    private String getHashValue(String ownerName, String contactName, String title) {
        messageDigest.reset();
        messageDigest.update(ownerName.getBytes());
        messageDigest.update(contactName.getBytes());
        messageDigest.update(title.getBytes());
        return new String(messageDigest.digest());
    }

    private String getFixedHashValue(String number, String ownerNumber, String smsBody,
                                     int smsType, Long smsTime) {
        messageDigest.reset();
        // check if users wanna ignore the phone number
        if (_advancedSettingPreference.read_ignoreOwner_setting() == false)
            messageDigest.update(ownerNumber.getBytes());
        messageDigest.update(number.getBytes());
        messageDigest.update(smsBody.getBytes());
        messageDigest.update(String.valueOf(smsType).getBytes());
        messageDigest.update(String.valueOf(smsTime).getBytes());
        return new String(messageDigest.digest());
    }
}
