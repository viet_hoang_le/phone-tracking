package com.bibooki.mobile.sms_bibooki.utils.constructor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;
import com.bibooki.mobile.sms_bibooki.utils.preferences.AdvancedSettingPreference;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class Call_Log_Generator {
    public static String _UNKNOWN_PHONE_NUMBER = "Unidentified Phone Number";

    private static String _MAIL_ADDRESS_POSTFIX = "@phonetracking.bibooki.com";
    private static String _MAIL_SUBJECT_PREFIX = "Call with ";

    private Context mContext;
    private UtilsFactory _utilsFactory;
    private ContactsHelper _contactsHelper;
    private AdvancedSettingPreference _advancedSettingPreference;

    MessageDigest messageDigest = null;

    Call_Log call_log;
    // reading call log
    // reading owner name and number
    String ownerName, ownerNumber;
    // reading call log
    String number, contactName;
    long fromTime, duration;
    int callType, eventTitleType;
    String encryptedString, title = null, description = null;

    public Call_Log_Generator(Context context){
        try {
            mContext = context;
            _utilsFactory = UtilsFactory.newInstance(context);
            _contactsHelper = ContactsHelper.getInstance(context);
            _advancedSettingPreference = _utilsFactory.getAdvancedSettingPreference();

            messageDigest = MessageDigest.getInstance("SHA-256");

            // get my own name and phone number
            ownerNumber = _contactsHelper.getMyPhoneNumber();
            if (ownerNumber == null || ownerNumber.equals("") || ownerNumber.equals("?"))
                ownerNumber = _UNKNOWN_PHONE_NUMBER;
            ownerName = _contactsHelper.getContactDisplayNameByNumber(ownerNumber);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public Call_Log getCallLogfromCursor(Cursor curCallLog){
        try {
            // create new Call Log for storing to List of Call
            call_log = new Call_Log();

            callType = curCallLog.getInt(curCallLog.getColumnIndexOrThrow(CallLog.Calls.TYPE));
            // check if should ignore the custom call type
            if (_advancedSettingPreference.read_ignorePhoneCallType_setting() && callType>3)
                return null;

            contactName = curCallLog.getString(curCallLog.getColumnIndex(CallLog.Calls.CACHED_NAME));
            contactName = contactName == null ? ContactsHelper._UNKNOWN_CONTACT : contactName;
            number = curCallLog.getString(curCallLog.getColumnIndexOrThrow(CallLog.Calls.NUMBER));
            duration = curCallLog.getLong(curCallLog.getColumnIndexOrThrow(CallLog.Calls.DURATION));  // convert from second to millisecond
            fromTime = curCallLog.getLong(curCallLog.getColumnIndexOrThrow(CallLog.Calls.DATE));
            call_log.setCallId(curCallLog.getString(curCallLog.getColumnIndexOrThrow(CallLog.Calls._ID)));
            call_log.setContactNames(contactName);
            call_log.setNumbers(number);
            call_log.setFromTime(fromTime);
            call_log.setStartTime(fromTime);
            call_log.setDurationSecond(duration);
            call_log.setEndTime(fromTime + TimeUnit.SECONDS.toMillis(duration));
            call_log.setDurationMiliSecond(TimeUnit.SECONDS.toMillis(duration));
            call_log.setType(callType);
            call_log.setOwnerName(ownerName);
            call_log.setOwnerNumber(ownerNumber);
            call_log = setFromTo(call_log);
            // We then set some of the basic information about_dialog the event
            if (contactName.equals(ContactsHelper._UNKNOWN_CONTACT)) {
                contactName = call_log.getNumbers();
            }
            title = generateCalendarEventTitle(call_log, contactName);
            description = generateCalendarEventDescription(call_log);
            call_log.setGeneratedTitle(title);
            call_log.setGeneratedDescription(description);
            // get hash value of call log description
            encryptedString = getHashValue(ownerName, contactName, title);
            call_log.setCalendarTitleHash(encryptedString);
            encryptedString = getFixedHashValue(number, ownerNumber, callType, duration, fromTime);
            call_log.setFixedHashValue(encryptedString);

            return call_log;
        } catch (Exception e) {
            e.printStackTrace();
            return  null;
        }
    }

    public Call_Log convertEventToCallLog(ContentValues contentValues) {
        //String title;
        String id;
        //long startTime, endTime, eventDuration;
        String contactName = "";
        String number = "";
        String ownerName = "";
        String ownerNumber = "";
        long atTime = 0;
        long duration = 0;
        int callType = 0;
        int toMeIndex = 0, fromMeIndex = 0, callFromIndex = 0, callToIndex = 0, missedCallIndex = 0, rejectedCallIndex = 0;
        // type 1 is the call that we received, and 2 is the call that we made, 3 is missed call, and 4 is rejected call
        //int type = 0;
        String tempString = null, encryptedString, generatedTitle = null, generatedDescription = null, eventContactName = null, eventOwnerName = null, eventTitle = null, eventDescription = null;
        int eventTitleType;

        Call_Log call_log = new Call_Log();
        try {
            // get values from event
            id = contentValues.getAsString(Calendar_Utils._EVENT_ID);
            eventTitle = contentValues.getAsString(Calendar_Utils._EVENT_TITLE);
            eventDescription = contentValues.getAsString(Calendar_Utils._EVENT_DESCRIPTION);
            //startTime = contentValues.getAsLong(Calendar_Utils._EVENT_START);
            //endTime = contentValues.getAsLong(Calendar_Utils._EVENT_END);
            //eventDuration = contentValues.getAsLong(Calendar_Utils._EVENT_DURATION);
            eventTitleType = _utilsFactory.getPreferencesHelper().read_CALL_EventTitle(mContext);
            // get Type of the Call
            tempString = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._CALL_TYPE)).replace(Calendar_Utils._CALL_TYPE, "").trim();
            callType = Integer.parseInt(tempString);
            call_log.setType(callType);
            // check if should ignore the custom call type
            if (_advancedSettingPreference.read_ignorePhoneCallType_setting() && callType>3)
                return null;

            // get index of String
            fromMeIndex = eventDescription.indexOf(Calendar_Utils._FROM_ME);
            fromMeIndex = fromMeIndex < 0 ? 0 : fromMeIndex;
            toMeIndex = eventDescription.indexOf(Calendar_Utils._TO_ME);
            toMeIndex = toMeIndex < 0 ? 0 : toMeIndex;
            callFromIndex = eventDescription.indexOf(Calendar_Utils._CALL_FROM);
            callFromIndex = callFromIndex < 0 ? 0 : callFromIndex;
            callToIndex = eventDescription.indexOf(Calendar_Utils._CALL_TO);
            callToIndex = callToIndex < 0 ? 0 : callToIndex;
            missedCallIndex = eventDescription.indexOf(Calendar_Utils._MISSED_CALL_FROM);
            missedCallIndex = missedCallIndex < 0 ? 0 : missedCallIndex;
            rejectedCallIndex = eventDescription.indexOf(Calendar_Utils._REJECTED_CALL_FROM);
            rejectedCallIndex = rejectedCallIndex < 0 ? 0 : rejectedCallIndex;
            // get Owner Number of the Phone
            ownerNumber = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MY_NUMBER)).replace(Calendar_Utils._MY_NUMBER, "");
            ownerNumber = ownerNumber.substring(0, ownerNumber.indexOf("\n"));
            // get Owner Name of the Phone
            eventOwnerName = eventDescription.substring(toMeIndex).replace(Calendar_Utils._TO_ME, "")
                    .substring(fromMeIndex).replace(Calendar_Utils._FROM_ME, "");
            eventOwnerName = eventOwnerName.substring(0, eventOwnerName.indexOf("\n"));
            // get Contact Number of the Call
            eventContactName = eventDescription.substring(callFromIndex).replace(Calendar_Utils._CALL_FROM, "")
                    .substring(callToIndex).replace(Calendar_Utils._CALL_TO, "")
                    .substring(missedCallIndex).replace(Calendar_Utils._MISSED_CALL_FROM, "")
                    .substring(rejectedCallIndex).replace(Calendar_Utils._REJECTED_CALL_FROM, "");
            eventContactName = eventContactName.substring(0, eventContactName.indexOf("\n"));
            // get Contact Name of the Call
            number = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._MOBILE_NUMBER)).replace(Calendar_Utils._MOBILE_NUMBER, "");
            number = number.substring(0, number.indexOf("\n"));
            // get duration of the Call
            tempString = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._LINE)).replaceFirst(Calendar_Utils._LINE, "");
            tempString = tempString.replace(Calendar_Utils._CALL_DURATION, "").trim();
            tempString = tempString.substring(0, tempString.indexOf(" "));
            duration = Long.parseLong(tempString);
            // get Time of the Call
            tempString = eventDescription.substring(eventDescription.indexOf(Calendar_Utils._AT_TIME)).replace(Calendar_Utils._AT_TIME, "").trim();
            tempString = tempString.substring(0, tempString.indexOf("\n")).trim();
            atTime = Long.parseLong(tempString);
            // set call log values
            call_log.setCal_EventId(id);
            call_log.setOwnerNumber(ownerNumber);
            if (ownerNumber == null || ownerNumber.equals("") || ownerNumber.equals("?"))
                ownerNumber = SMS_Utils._UNKNOWN_PHONE_NUMBER;
            ownerName = _contactsHelper.getContactDisplayNameByNumber(ownerNumber);
            call_log.setOwnerName(ownerName);
            call_log.setNumbers(number);
            contactName = _contactsHelper.getContactDisplayNameByNumber(number);
            call_log.setContactNames(contactName);
            call_log.setFromTime(atTime);
            call_log.setDurationSecond(duration);
            call_log.setDurationMiliSecond(TimeUnit.SECONDS.toMillis(duration));
            if (contactName.equals(ContactsHelper._UNKNOWN_CONTACT)) {
                contactName = number;
            }
            generatedTitle = generateCalendarEventTitle(call_log, contactName);
            generatedDescription = generateCalendarEventDescription(call_log);

            call_log.setEventTitle(eventTitle);
            call_log.setEventDescription(eventDescription);
            call_log.setGeneratedTitle(generatedTitle);
            call_log.setGeneratedDescription(generatedDescription);
            call_log.setStartTime(atTime);
            call_log.setEndTime(atTime + TimeUnit.SECONDS.toMillis(duration));
            call_log = setFromTo(call_log);

            // get hash value of call log description
            if (eventContactName.equals(ContactsHelper._UNKNOWN_CONTACT)) {
                eventContactName = number;
            }
            encryptedString = getHashValue(eventOwnerName, eventContactName, eventTitle);
            call_log.setCalendarTitleHash(encryptedString);
            encryptedString = getFixedHashValue(number, ownerNumber, call_log.getType(), duration, atTime);
            call_log.setFixedHashValue(encryptedString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return call_log;
    }

    // get generated title for call log
    private String generateCalendarEventTitle(Call_Log call_log, String contactName) {
        int eventTitleType = _utilsFactory.getPreferencesHelper().read_CALL_EventTitle(mContext);
        int callType = call_log.getType();
        String title = "";
        switch (eventTitleType) {
            case -1:
                switch (callType) {
                    case CallLog.Calls.INCOMING_TYPE:
                        title = Calendar_Utils._CALL_FROM + contactName;
                        break;
                    case CallLog.Calls.OUTGOING_TYPE:
                        title = Calendar_Utils._CALL_TO + contactName;
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        title = Calendar_Utils._MISSED_CALL_FROM + contactName;
                        break;
                    default:
                        title = Calendar_Utils._REJECTED_CALL_FROM + contactName;
                        break;
                }
                break;
            case 1:
                switch (callType) {
                    case CallLog.Calls.INCOMING_TYPE:
                        title = Calendar_Utils._CALL_FROM + contactName;
                        break;
                    case CallLog.Calls.OUTGOING_TYPE:
                        title = Calendar_Utils._CALL_TO + contactName;
                        if (call_log.getDurationSecond() == 0)
                            title += " " + Calendar_Utils._HE_NOT_ANSWER;
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        title = Calendar_Utils._MISSED_CALL_FROM + contactName;
                        title += " " + Calendar_Utils._MISSED_CALL;
                        break;
                    default:
                        title = Calendar_Utils._REJECTED_CALL_FROM + contactName;
                        title += " " + Calendar_Utils._YOU_NOT_ANSWER;
                        break;
                }
                break;
            default:
        }
        title += " - " + call_log.getDurationSecond() + "s";
        return title;
    }

    // get generated description for call log
    private String generateCalendarEventDescription(Call_Log call_log) {
        int callType = call_log.getType();
        String description = "";
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                description = Calendar_Utils._CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + call_log.getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                description = Calendar_Utils._CALL_TO + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + call_log.getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.MISSED_TYPE:
                description = Calendar_Utils._MISSED_CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            default:
                description = Calendar_Utils._REJECTED_CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
        }
        return description;
    }
    // get generated description for call log
    private String generateMailContent(Call_Log call_log) {
        int callType = call_log.getType();
        String description = "";
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                description =
                        Calendar_Utils._CALL_DURATION + call_log.getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                description = Calendar_Utils._CALL_DURATION + call_log.getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_TO + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.MISSED_TYPE:
                description = Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._MISSED_CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            default:
                description = Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._REJECTED_CALL_FROM + call_log.getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + call_log.getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + call_log.getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + call_log.getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + call_log.getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
        }
        return description;
    }

    // get generated from/to address for sms in mail backup
    private Call_Log setFromTo(Call_Log call_log) {
        int callType = call_log.getType();
        String from = null,to = null;
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                from = call_log.getNumbers();
                to = call_log.getOwnerNumber();
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                from = call_log.getOwnerNumber();
                to = call_log.getNumbers();
                break;
            case CallLog.Calls.MISSED_TYPE:
                from = call_log.getNumbers();
                to = call_log.getOwnerNumber();
                break;
            default:
                from = call_log.getNumbers();
                to = call_log.getOwnerNumber();
                break;
        }
        call_log.setFromNumber(from);
        call_log.setToNumber(to);
        return call_log;
    }

    // get generated from/to address for call log in mail backup
    private Call_Log setMailAddress(Call_Log call_log) {
        int callType = call_log.getType();
        String from = null,to = null;
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                from = call_log.getContactNames() + " <" + call_log.getNumbers() + _MAIL_ADDRESS_POSTFIX + ">";
                to = call_log.getOwnerName() + " <" + call_log.getOwnerNumber() + _MAIL_ADDRESS_POSTFIX + ">";
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                from = call_log.getOwnerName() + " <" + call_log.getOwnerNumber() + _MAIL_ADDRESS_POSTFIX + ">";
                to = call_log.getContactNames() + " <" + call_log.getNumbers() + _MAIL_ADDRESS_POSTFIX + ">";
                break;
            case CallLog.Calls.MISSED_TYPE:
                from = call_log.getContactNames() + " <" + call_log.getNumbers() + _MAIL_ADDRESS_POSTFIX + ">";
                to = call_log.getOwnerName() + " <" + call_log.getOwnerNumber() + _MAIL_ADDRESS_POSTFIX + ">";
                break;
            default:
                from = call_log.getContactNames() + " <" + call_log.getNumbers() + _MAIL_ADDRESS_POSTFIX + ">";
                to = call_log.getOwnerName() + " <" + call_log.getOwnerNumber() + _MAIL_ADDRESS_POSTFIX + ">";
                break;
        }
        return call_log;
    }

    private String getHashValue(String ownerName, String contactName, String title) {
        messageDigest.reset();
        messageDigest.update(ownerName.getBytes());
        messageDigest.update(contactName.getBytes());
        messageDigest.update(title.getBytes());
        return new String(messageDigest.digest());
    }

    private String getFixedHashValue(String number, String ownerNumber,
                                     int callType, Long duration, Long atTime) {
        if (atTime==Long.parseLong("1405816881967"))
            System.out.println("test");
        messageDigest.reset();
        // check if users wanna ignore the phone number
        if (_advancedSettingPreference.read_ignoreOwner_setting() == false)
            messageDigest.update(ownerNumber.getBytes());
        messageDigest.update(number.getBytes());
        messageDigest.update(String.valueOf(callType).getBytes());
        messageDigest.update(String.valueOf(duration).getBytes());
        messageDigest.update(String.valueOf(atTime).getBytes());
        return new String(messageDigest.digest());
    }
}
