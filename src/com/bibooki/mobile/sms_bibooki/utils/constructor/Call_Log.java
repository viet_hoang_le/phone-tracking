package com.bibooki.mobile.sms_bibooki.utils.constructor;

import android.provider.CallLog;
import com.bibooki.mobile.sms_bibooki.utils.calendar.Calendar_Utils;

public class Call_Log {

    String contactNames = "";
    String numbers = "";
    String ownerName = "";
    String ownerNumber = "";
    String cal_EventId = "";
    String calendarTitleHash = "";
    String fixedHashValue = "";
    long fromTime = 0;
    long durationSecond = 0;
    long durationMiliSecond = 0;

    // type 1 is the call that we received,
    // 2 is the call that we made,
    // 3 is missed call,
    int type = 0;
    String callId = "";
    String fromNumber, toNumber;

    String eventTitle = "";
    String eventDescription = "";
    String generatedTitle = "";
    String generatedDescription = "";
    long startTime = 0, endTime = 0;
    // custom attributes
    public boolean isCalendarTracked = false;
    public boolean isCalendarUpdated = true;

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getContactNames() {
        return contactNames;
    }

    public void setContactNames(String contactNames) {
        this.contactNames = contactNames;
    }

    public long getFromTime() {
        return fromTime;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerNumber() {
        return ownerNumber;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.ownerNumber = ownerNumber;
    }

    public void setFromTime(long fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getDurationSecond() {
        return durationSecond;
    }

    public void setDurationSecond(long durationSecond) {
        this.durationSecond = durationSecond;
    }

    public long getDurationMiliSecond() {
        return durationMiliSecond;
    }

    public void setDurationMiliSecond(long durationMiliSecond) {
        this.durationMiliSecond = durationMiliSecond;
    }

    public String getCal_EventId() {
        return cal_EventId;
    }

    public void setCal_EventId(String cal_EventId) {
        this.cal_EventId = cal_EventId;
    }

    public String getCalendarTitleHash() {
        return calendarTitleHash;
    }

    public void setCalendarTitleHash(String calendarTitleHash) {
        this.calendarTitleHash = calendarTitleHash;
    }

    public String getFixedHashValue() {
        return fixedHashValue;
    }

    public void setFixedHashValue(String fixedHashValue) {
        this.fixedHashValue = fixedHashValue;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getGeneratedTitle() {
        return generatedTitle;
    }

    public void setGeneratedTitle(String generatedTitle) {
        this.generatedTitle = generatedTitle;
    }

    public String getGeneratedDescription() {
        return generatedDescription;
    }

    public void setGeneratedDescription(String generatedDescription) {
        this.generatedDescription = generatedDescription;
    }

    public void regenerateDescription(){
        int callType = getType();
        String description = "";
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                description = Calendar_Utils._CALL_FROM + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                description = Calendar_Utils._CALL_TO + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + getDurationSecond() + " seconds\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._FROM_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            case CallLog.Calls.MISSED_TYPE:
                description = Calendar_Utils._MISSED_CALL_FROM + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
            default:
                description = Calendar_Utils._REJECTED_CALL_FROM + getContactNames() + "\n"
                        + Calendar_Utils._MOBILE_NUMBER + getNumbers() + "\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._CALL_DURATION + "0 second\n"
                        + Calendar_Utils._LINE
                        + Calendar_Utils._TO_ME + getOwnerName() + "\n"
                        + Calendar_Utils._MY_NUMBER + getOwnerNumber() + "\n"
                        + Calendar_Utils._AT_TIME + getFromTime() + "\n"
                        + Calendar_Utils._CALL_TYPE + callType;
                break;
        }
        setGeneratedDescription(description);
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }
}
