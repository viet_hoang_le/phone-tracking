package com.bibooki.mobile.sms_bibooki.utils.constructor;

public class TrackingFilter {
    String phonenumber = "",
            contactname = "";
    int smsCalendarId = -1,
        callCalendarId = -1;
    String smsCalendarName = "",
            callCalendarName = "";

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public int getSmsCalendarId() {
        return smsCalendarId;
    }

    public void setSmsCalendarId(int smsCalendarId) {
        this.smsCalendarId = smsCalendarId;
    }

    public int getCallCalendarId() {
        return callCalendarId;
    }

    public void setCallCalendarId(int callCalendarId) {
        this.callCalendarId = callCalendarId;
    }

    public String getSmsCalendarName() {
        return smsCalendarName;
    }

    public void setSmsCalendarName(String smsCalendarName) {
        this.smsCalendarName = smsCalendarName;
    }

    public String getCallCalendarName() {
        return callCalendarName;
    }

    public void setCallCalendarName(String callCalendarName) {
        this.callCalendarName = callCalendarName;
    }
}
