package com.bibooki.mobile.sms_bibooki.utils.filter;

import android.content.Context;

import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.constructor.TrackingFilter;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;

import java.util.ArrayList;
import java.util.List;

public class Filter_Utils{
    private static Filter_Utils filterUtils = null;
    private static DBTrackCalendarOpenHelper _dbTrackCalendarOpenHelper;
    private static Context _context;

    static UtilsFactory _utilsFactory;

    // return new instance
    public static Filter_Utils getInstance(Context context){
        filterUtils = new Filter_Utils();
        _dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(context);
        _context = context;
        _utilsFactory = UtilsFactory.newInstance(_context);
        return filterUtils;
    }

    public boolean updateCreateRecords(String phoneNumber, Integer smsCalendarId, Integer callCalendarId){
        return _dbTrackCalendarOpenHelper.updateCreateRecords(phoneNumber, smsCalendarId, callCalendarId);
    }

    public List<TrackingFilter> selectRecords() {
        List<TrackingFilter> trackingFilterList = new ArrayList<TrackingFilter>();
        TrackingFilter trackingFilter;
        for (TrackCalendarDbItem item : _dbTrackCalendarOpenHelper.selectRecordsNotDefault()){
            trackingFilter = new TrackingFilter();
            trackingFilter.setPhonenumber(item.getPhoneNumber());
            trackingFilter.setContactname(item.getContactName());
            trackingFilter.setSmsCalendarId(item.getSmsCalendarId());
            trackingFilter.setCallCalendarId(item.getCallCalendarId());
            trackingFilter.setSmsCalendarName(item.getSmsCalendarName());
            trackingFilter.setCallCalendarName(item.getCallCalendarName());
            trackingFilterList.add(trackingFilter);
        }
        return trackingFilterList; // iterate to get each value.
    }
    public TrackingFilter selectRecord(String phoneNumber) {
        TrackCalendarDbItem item = _dbTrackCalendarOpenHelper.selectRecord(phoneNumber);
        if (item==null)
            return null;
        TrackingFilter trackingFilter = new TrackingFilter();
        trackingFilter.setPhonenumber(item.getPhoneNumber());
        trackingFilter.setContactname(item.getContactName());
        trackingFilter.setSmsCalendarId(item.getSmsCalendarId());
        trackingFilter.setCallCalendarId(item.getCallCalendarId());
        trackingFilter.setSmsCalendarName(item.getSmsCalendarName());
        trackingFilter.setCallCalendarName(item.getCallCalendarName());
        return trackingFilter;
    }
    public void deleteFilter(String phoneNumber)
    {
        _dbTrackCalendarOpenHelper.deleteRecord(phoneNumber);
    }
}