package com.bibooki.mobile.sms_bibooki;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.Schedule_Utils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

public class SyncActivity extends BaseMenuActivity {

    private static Context mContext;
    private static Activity mActivity;

    RadioGroup radioGroup;
    RadioButton radio_unset, radio_1h, radio_6h, radio_12h, radio_24h, radio_custom_h;

    static UtilsFactory _utilsFactory;
    static PreferencesHelper _preferenceHelper;
    static Schedule_Utils _scheduleUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sync_setting);
        mActivity = this;
        mContext = this;

        _utilsFactory = UtilsFactory.newInstance(getBaseContext());
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        _scheduleUtils = _utilsFactory.getSchedule_Utils();

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup_sync);
        radio_unset = (RadioButton) findViewById(R.id.radio_unset);
        radio_1h = (RadioButton) findViewById(R.id.radio_1h);
        radio_6h = (RadioButton) findViewById(R.id.radio_6h);
        radio_12h = (RadioButton) findViewById(R.id.radio_12h);
        radio_24h = (RadioButton) findViewById(R.id.radio_24h);
        radio_custom_h = (RadioButton) findViewById(R.id.radio_custom_h);

        setupUI();

        radio_unset.setOnClickListener(radioUnset_OnClickListener);
        radio_1h.setOnClickListener(radio1h_OnClickListener);
        radio_6h.setOnClickListener(radio6h_OnClickListener);
        radio_12h.setOnClickListener(radio12h_OnClickListener);
        radio_24h.setOnClickListener(radio24h_OnClickListener);
        radio_custom_h.setOnClickListener(radioCustom_OnClickListener);
    }
    // setup the UI of activity
    private void setupUI(){
        // reset backgroun color of all radio
        radio_unset.setBackgroundColor(0x00000000);
        radio_1h.setBackgroundColor(0x00000000);
        radio_6h.setBackgroundColor(0x00000000);
        radio_12h.setBackgroundColor(0x00000000);
        radio_24h.setBackgroundColor(0x00000000);
        radio_custom_h.setBackgroundColor(0x00000000);
        radio_custom_h.setText(R.string.sync_every_custom_time);
        int autoTrackingMinutes = _preferenceHelper.read_Auto_Tracking_Time(mContext);
        if (autoTrackingMinutes==0) {
            radioGroup.check(radio_unset.getId());
            radio_unset.setBackgroundColor(Color.YELLOW);
        }
        else if (autoTrackingMinutes==60) {
            radioGroup.check(radio_1h.getId());
            radio_1h.setBackgroundColor(Color.YELLOW);
        }
        else if (autoTrackingMinutes==360) {
            radioGroup.check(radio_6h.getId());
            radio_6h.setBackgroundColor(Color.YELLOW);
        }
        else if (autoTrackingMinutes==720) {
            radioGroup.check(radio_12h.getId());
            radio_12h.setBackgroundColor(Color.YELLOW);
        }
        else if (autoTrackingMinutes==1440) {
            radioGroup.check(radio_24h.getId());
            radio_24h.setBackgroundColor(Color.YELLOW);
        }
        else{
            radioGroup.check(radio_custom_h.getId());
            radio_custom_h.setBackgroundColor(Color.YELLOW);
            String timeStt="";
            if (autoTrackingMinutes == 1)
                timeStt = "1 minute after send/receive sms or call_log";
            else if (autoTrackingMinutes > 1) {
                int hour = autoTrackingMinutes/60;
                autoTrackingMinutes = autoTrackingMinutes - hour*60;
                if (hour==0)
                    timeStt = autoTrackingMinutes + " minutes after send/receive sms or call_log";
                else if (hour==1)
                    timeStt = "1 hour";
                else if (hour > 1)
                    timeStt = hour + " hours";
                if (autoTrackingMinutes==1)
                    timeStt += " 1 minute";
                else if (autoTrackingMinutes > 1)
                    timeStt += " "+autoTrackingMinutes+" minutes";
                timeStt += " after send/receive sms or call_log";
                timeStt = timeStt.trim();
            }
            radio_custom_h.setText(timeStt);
        }
    }
    // radioButton onClick listener
    private View.OnClickListener radioUnset_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _preferenceHelper.store_Auto_Tracking_Time(mContext, 0);
            setupUI();
            // cancel alarm
            _scheduleUtils.cancelSMS_RepeatingTimer(mContext);
            _scheduleUtils.cancelCall_RepeatingTimer(mContext);
        }
    };
    private View.OnClickListener radio1h_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _preferenceHelper.store_Auto_Tracking_Time(mContext, 60);
            setupUI();
            // process schedule here
            _scheduleUtils.startRepeatingTimer_Sms(mContext);
            _scheduleUtils.startRepeatingTimer_Call(mContext);
        }
    };
    private View.OnClickListener radio6h_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _preferenceHelper.store_Auto_Tracking_Time(mContext, 360);
            setupUI();
            // process schedule here
            _scheduleUtils.startRepeatingTimer_Sms(mContext);
            _scheduleUtils.startRepeatingTimer_Call(mContext);
        }
    };
    private View.OnClickListener radio12h_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _preferenceHelper.store_Auto_Tracking_Time(mContext, 720);
            setupUI();
            // process schedule here
            _scheduleUtils.startRepeatingTimer_Sms(mContext);
            _scheduleUtils.startRepeatingTimer_Call(mContext);
        }
    };
    private View.OnClickListener radio24h_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            _preferenceHelper.store_Auto_Tracking_Time(mContext, 1440);
            setupUI();
            // process schedule here
            _scheduleUtils.startRepeatingTimer_Sms(mContext);
            _scheduleUtils.startRepeatingTimer_Call(mContext);
        }
    };
    private View.OnClickListener radioCustom_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            storeCustomSyncTime(R.id.radio_custom_h);
        }
    };

    private void storeCustomSyncTime(final int radioId){
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View npView = inflater.inflate(R.layout.number_picker_dialog, null);
        final NumberPicker hNumberPicker = (NumberPicker) npView.findViewById(R.id.numberPicker1);
        hNumberPicker.setMaxValue(240);
        hNumberPicker.setMinValue(0);
        hNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        final NumberPicker mNumberPicker = (NumberPicker) npView.findViewById(R.id.numberPicker2);
        mNumberPicker.setMaxValue(59);
        mNumberPicker.setMinValue(1);
        mNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        TextView title = new TextView(mContext);
        // You Can Customise your Title here
        title.setText("Hour : Minute");
        title.setBackgroundColor(Color.DKGRAY);
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(20);
        builder.setCustomTitle(title);
        builder.setView(npView);
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        setupUI();
                    }
                });
        builder.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int pHour = hNumberPicker.getValue();
                        int pMinute = mNumberPicker.getValue();
                        _preferenceHelper.store_Auto_Tracking_Time(mContext, pHour*60 + pMinute);
                        setupUI();
                    }
                });
        Dialog dialog = builder.create();
        dialog.show();
    }
}

