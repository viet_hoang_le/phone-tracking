package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.database.DBTrackCalendarOpenHelper;
import com.bibooki.mobile.sms_bibooki.utils.database.TrackCalendarDbItem;
import com.bibooki.mobile.sms_bibooki.utils.preferences.PreferencesHelper;

public class CalendarActivity extends BaseMenuActivity {
    private static Context mContext;
    private static Activity mActivity;

    private UtilsFactory _utilsFactory;
    private PreferencesHelper _preferenceHelper;
    private NotificationUtils _notificationUtils;

    private static DBTrackCalendarOpenHelper dbTrackCalendarOpenHelper = null;

    // Sms Elements
    ImageView imageView_calendar_sms;
    CheckedTextView checkedTextView_sms_calendar;
    RadioButton radioButton_sms_1, radioButton_sms_2, radioButton_sms_3;
    // Call Log Elements
    ImageView imageView_calendar_call;
    CheckedTextView checkedTextView_call_calendar;
    RadioButton radioButton_call_1, radioButton_call_2;
    // Schedule Elements
    ImageView imageView_calendar_schedule;
    CheckedTextView checkedTextView_schedule_calendar;
    RadioButton radioButton_scheduele_1, radioButton_scheduele_2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_setting);

        mActivity = this;
        mContext = this;

        dbTrackCalendarOpenHelper = new DBTrackCalendarOpenHelper(mActivity);

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _preferenceHelper = _utilsFactory.getPreferencesHelper();
        _notificationUtils = _utilsFactory.getNotificationUtils();

        // setup elements
        imageView_calendar_sms = (ImageView) findViewById(R.id.calendar_sms_icon);
        checkedTextView_sms_calendar = (CheckedTextView) findViewById(R.id.checkedTextView_sms_Calendar);
        radioButton_sms_1 = (RadioButton) findViewById(R.id.radioButton_sms_1);
        radioButton_sms_2 = (RadioButton) findViewById(R.id.radioButton_sms_2);
        radioButton_sms_3 = (RadioButton) findViewById(R.id.radioButton_sms_3);
        imageView_calendar_call = (ImageView) findViewById(R.id.calendar_call_icon);
        checkedTextView_call_calendar = (CheckedTextView) findViewById(R.id.checkedTextView_call_Calendar);
        radioButton_call_1 = (RadioButton) findViewById(R.id.radioButton_call_1);
        radioButton_call_2 = (RadioButton) findViewById(R.id.radioButton_call_2);
        imageView_calendar_schedule = (ImageView) findViewById(R.id.calendar_schedule_icon);
        checkedTextView_schedule_calendar = (CheckedTextView) findViewById(R.id.checkedTextView_schedule_calendar);
        radioButton_scheduele_1 = (RadioButton) findViewById(R.id.radioButton_schedule_1);
        radioButton_scheduele_2 = (RadioButton) findViewById(R.id.radioButton_schedule_2);

        setupUI();
        // calendar icon click listener
        imageView_calendar_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _notificationUtils.showShortToast(getString(R.string.notification_calendar_sms_info));
            }
        });
        imageView_calendar_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _notificationUtils.showShortToast(getString(R.string.notification_calendar_call_info));
            }
        });
        imageView_calendar_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _notificationUtils.showShortToast(getString(R.string.notification_calendar_schedule_info));
            }
        });
        // checked text view click listener
        checkedTextView_sms_calendar.setOnClickListener(smsCalendarSelector);
        checkedTextView_call_calendar.setOnClickListener(callCalendarSelector);
        checkedTextView_schedule_calendar.setOnClickListener(smsScheduleCalendarSelector);

        radioButton_sms_1.setOnClickListener(sms_entire_content_Listener);
        radioButton_sms_2.setOnClickListener(sms_partial_content_Listener);
        radioButton_sms_3.setOnClickListener(sms_unset_content_Listener);

        radioButton_call_1.setOnClickListener(call_set_status_Listener);
        radioButton_call_2.setOnClickListener(call_unset_status_Listener);

        radioButton_scheduele_1.setOnClickListener(schedule_delete_on_Listener);
        radioButton_scheduele_2.setOnClickListener(schedule_delete_off_Listener);
    }
    private void setupUI(){
        TrackCalendarDbItem defaultTrackingCalendar = dbTrackCalendarOpenHelper.selectDefaultRecord();
        TrackCalendarDbItem defaultSchedulegCalendar = dbTrackCalendarOpenHelper.selectScheduleRecord();
        // sms setting
        String calendarName;
        if (defaultTrackingCalendar != null){
            if (defaultTrackingCalendar.getSmsCalendarId()>0){
                // build a new calendar name in format "id. calendarName" for displaying
                calendarName = defaultTrackingCalendar.getSmsCalendarId() + ". " + defaultTrackingCalendar.getSmsCalendarName();
                checkedTextView_sms_calendar.setSelected(true);
                checkedTextView_sms_calendar.setText(calendarName);
                checkedTextView_sms_calendar.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
            }
            if (defaultTrackingCalendar.getCallCalendarId()>0){
                // build a new calendar name in format "id. calendarName" for displaying
                calendarName = defaultTrackingCalendar.getCallCalendarId() + ". " + defaultTrackingCalendar.getCallCalendarName();
                checkedTextView_call_calendar.setSelected(true);
                checkedTextView_call_calendar.setText(calendarName);
                checkedTextView_call_calendar.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
            }
        }
        switch (_preferenceHelper.read_SMS_EventTitle(mContext)){
            case -1:
                radioButton_sms_3.setChecked(true);
                break;
            case 0:
                radioButton_sms_2.setChecked(true);
                break;
            case 1:
                radioButton_sms_1.setChecked(true);
                break;
            default:
                break;
        }
        switch (_preferenceHelper.read_CALL_EventTitle(mContext)){
            case -1:
                radioButton_call_2.setChecked(true);
                break;
            case 1:
                radioButton_call_1.setChecked(true);
                break;
            default:
                break;
        }
        // schedule setting
        if (defaultSchedulegCalendar != null){
            if (defaultSchedulegCalendar.getSmsCalendarId() > 0){
                // build a new calendar name in format "id. calendarName" for displaying
                calendarName = defaultSchedulegCalendar.getSmsCalendarId() + ". " + defaultSchedulegCalendar.getSmsCalendarName();
                checkedTextView_schedule_calendar.setSelected(true);
                checkedTextView_schedule_calendar.setText(calendarName);
                checkedTextView_schedule_calendar.setCheckMarkDrawable(R.drawable.btn_check_on_selected);
            }
        }
        switch (_preferenceHelper.read_delete_schedule_status(mContext)){
            case -1:
                radioButton_scheduele_2.setChecked(true);
                break;
            case 1:
                radioButton_scheduele_1.setChecked(true);
                break;
            default:
                break;
        }
    }
    // checked text view onclick listener
    private View.OnClickListener smsCalendarSelector = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView.setText(getString(R.string.calendar_selection_dialog_title));
                // reset calendar id in database
                dbTrackCalendarOpenHelper.updateCreateSmsDefault(-1);
                //Fragment_SMS.refresh();
            } else {
                // set dialog message
                Dialog calendarDialog = _utilsFactory.getSmsUtils().pickingCalendarDialog(mContext, mActivity, checkedTextView);
                calendarDialog.show();
            }
        }
    };
    private View.OnClickListener callCalendarSelector = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView.setText(getString(R.string.calendar_selection_dialog_title));
                // reset calendar id in database
                dbTrackCalendarOpenHelper.updateCreateCallDefault(-1);
            } else {
                // set dialog message
                Dialog calendarDialog = _utilsFactory.getCallLogUtils().pickingCalendarDialog(mContext, mActivity, checkedTextView);
                calendarDialog.show();
            }
        }
    };
    private View.OnClickListener smsScheduleCalendarSelector = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView.setText(getString(R.string.calendar_selection_dialog_title));
                // reset calendar id in database
                dbTrackCalendarOpenHelper.updateCreateSchedule(-1);
            } else {
                // set dialog message
                Dialog calendarDialog = _utilsFactory.getSmsUtils().pickingScheduledCalendarDialog(mContext, mActivity, checkedTextView);
                calendarDialog.show();
            }
        }
    };
    View.OnClickListener sms_entire_content_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_SMS_EventTitle(mContext,1);
        }
    };
    View.OnClickListener sms_partial_content_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_SMS_EventTitle(mContext,0);
        }
    };
    View.OnClickListener sms_unset_content_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_SMS_EventTitle(mContext,-1);
        }
    };
    View.OnClickListener call_set_status_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_CALL_EventTitle(mContext,1);
        }
    };
    View.OnClickListener call_unset_status_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_CALL_EventTitle(mContext,-1);
        }
    };
    View.OnClickListener schedule_delete_on_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_delete_schedule_status(mContext,1);
        }
    };
    View.OnClickListener schedule_delete_off_Listener = new View.OnClickListener(){
        public void onClick(View v) {
            _preferenceHelper.store_delete_schedule_status(mContext,-1);
        }
    };
}
