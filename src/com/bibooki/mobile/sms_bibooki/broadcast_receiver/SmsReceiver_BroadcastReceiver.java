package com.bibooki.mobile.sms_bibooki.broadcast_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.SmsMessage;

import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.auto_tracking.SmsChatHead_Service;
import com.bibooki.mobile.sms_bibooki.utils.constructor.SMS;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

public class SmsReceiver_BroadcastReceiver extends MessagingReceiver {

    private static final String TAG = "SmsReceiver_BroadcastReceiver";
    private static UtilsFactory _utilsFactory;
    private static SMS_Utils _smsUtils;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.abortBroadcast();

        _utilsFactory = UtilsFactory.newInstance(context);
        _smsUtils = _utilsFactory.getSmsUtils();

        SMS sms = new SMS();

        Uri uriResult;
        SmsMessage[] msgs = null;
        String sender = "";
        long firstReceiveTime = -1;
        StringBuilder smsContent = new StringBuilder();
        Bundle bundle = intent.getExtras();        			// SMS message passed

        if (bundle != null) {

            Object[] pdus = (Object[]) bundle.get("pdus");	// SMS message received
            msgs = new SmsMessage[pdus.length];

            for (int i=0; i<msgs.length; i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                if (sender.equals(""))
                    sender = msgs[i].getOriginatingAddress();
                if (firstReceiveTime==-1)
                    firstReceiveTime = msgs[i].getTimestampMillis();
                smsContent.append(msgs[i].getDisplayMessageBody());
            }
            // Toast.makeText(context, str, Toast.LENGTH_SHORT).show();    //display SMS
            // write the sms into database
            sms.setNumbers(sender);
            sms.setType(SMS.RECEIVE_TYPE);
            sms.setSmsBody(smsContent.toString());
            sms.setSmsTime(firstReceiveTime);
            uriResult = _smsUtils.writingSMS_Database(context, sms);

            // play alert notification sound
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
            Vibrator vibrator;
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
            vibrator.vibrate(500);

            // Start service for showing sms chathead
            Intent chatHead = new Intent(context, SmsChatHead_Service.class);
            chatHead.putExtra("smsUri", uriResult.toString());
            chatHead.putExtra("Msg", sms.getSmsBody());
            chatHead.putExtra("From", sms.getNumbers());
            chatHead.putExtra("Time", sms.getSmsTime());
            context.startService(chatHead);
            //MainActivity.resetFragment();
        }
    }
}
