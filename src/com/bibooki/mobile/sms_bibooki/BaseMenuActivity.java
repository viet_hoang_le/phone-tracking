package com.bibooki.mobile.sms_bibooki;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;

import com.codinguser.android.contactpicker.ContactsPickerActivity;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class BaseMenuActivity extends ActionBarActivity {
    static UtilsFactory _utilsFactory;
    static NotificationUtils _notificationUtils;
    static final int PICK_CONTACT_REQUEST = 0;
    ContactsHelper _contactsHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _utilsFactory = UtilsFactory.newInstance(this);
        _notificationUtils = _utilsFactory.getNotificationUtils();
        _contactsHelper = ContactsHelper.getInstance(this);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    // get Contact by picker (library)
    AutoCompleteTextView autoCompleteTextView;
    ImageView icon;
    public void pickContact(AutoCompleteTextView autoCompleteTextView, ImageView icon){
        this.autoCompleteTextView = autoCompleteTextView;
        this.icon = icon;
        startActivityForResult(new Intent(this, ContactsPickerActivity.class), BaseMenuActivity.PICK_CONTACT_REQUEST);
    }
    // Listen for results.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            // See which child activity is calling us back.
            switch (requestCode) {
                case PICK_CONTACT_REQUEST:
                    // This is the standard resultCode that is sent back if the
                    // activity crashed or didn't doesn't supply an explicit result.
                    if (resultCode == RESULT_CANCELED) {
                        _notificationUtils.showShortToast(getResources().getString(R.string.notification_noPhoneNumber_found));
                    } else {
                        String phoneNumber = (String) data.getExtras().get(ContactsPickerActivity.KEY_PHONE_NUMBER);
                        phoneNumber = phoneNumber.trim().replaceAll(" ", "");
                        //String contactName = (String) data.getExtras().get(ContactsPickerActivity.KEY_CONTACT_NAME);
                        //Toast.makeText(this, "Phone number found: " + phoneNumber , Toast.LENGTH_SHORT).show();
                        this.autoCompleteTextView.setText(phoneNumber);
                        Uri photoUri = _contactsHelper.getContactDisplayImageByNumber(phoneNumber);
                        icon.setImageURI(photoUri);
                    }
                default:
                    break;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
