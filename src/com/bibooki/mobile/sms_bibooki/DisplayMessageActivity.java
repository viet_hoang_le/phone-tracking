package com.bibooki.mobile.sms_bibooki;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class DisplayMessageActivity extends Activity {
    // Google AdMob
    protected AdView adView;
    // Finish Google AdMob

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Create the adView.
        adView = new AdView(this);
        adView.setAdUnitId(getApplicationContext().getResources().getString(R.string.admod_banner));
        adView.setAdSize(AdSize.BANNER);
        // Lookup your LinearLayout assuming it's been given
        // the attribute android:id="@+id/mainLayout".
        LinearLayout layout = (LinearLayout)findViewById(R.id.display_message_layout);
        // Add the adView to it.
        layout.addView(adView);
        // Initiate a generic request.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Load the adView with the ad request.
        adView.loadAd(adRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
