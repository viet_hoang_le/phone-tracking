package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bibooki.mobile.sms_bibooki.utils.Restore_Utils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.sms_utils.SMS_Utils;

import java.util.Iterator;
import java.util.Map;

import contactshelper.android.bibooki.com.contactshelper.ContactsHelper;

public class RestoreActivity extends BaseMenuActivity {
    private static Context mContext;
    private static Activity mActivity;

    private static UtilsFactory _utilsFactory;
    private static Restore_Utils _restoreUtils;
    private static ContactsHelper _contactsHelper;
    private static SMS_Utils _smsUtils;

    CheckedTextView checkedTextView_sms_all, checkedTextView_call_all,
                    checkedTextView_sms, checkedTextView_call;
    ImageView imageView_restore_all, imageView_clear_all,
                imageView_restore, imageView_clear;
    ImageView imageView_contactPicker;
    AutoCompleteTextView autoCompleteTextView_contact;
    Button defaultSmsBtn;
    LinearLayout layoutDefaultSms, layoutRestore;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restore_setting);

        mActivity = this;
        mContext = this;

        // setup utils
        _utilsFactory = UtilsFactory.newInstance(mContext);
        _restoreUtils = _utilsFactory.getRestore_Utils();
        _contactsHelper = ContactsHelper.getInstance(mContext);
        _smsUtils = _utilsFactory.getSmsUtils();

        layoutDefaultSms = (LinearLayout) findViewById(R.id.layout_default);
        layoutRestore = (LinearLayout) findViewById(R.id.layout_restore);
        defaultSmsBtn = (Button) findViewById(R.id.button_defaultSms);
        checkedTextView_sms_all = (CheckedTextView) findViewById(R.id.checkedTextView_SMSCalendar_all);
        checkedTextView_call_all = (CheckedTextView) findViewById(R.id.checkedTextView_CALLCalendar_all);
        checkedTextView_sms = (CheckedTextView) findViewById(R.id.checkedTextView_SMSCalendar);
        checkedTextView_call = (CheckedTextView) findViewById(R.id.checkedTextView_CALLCalendar);
        imageView_restore_all = (ImageView) findViewById(R.id.imageView_restore_all);
        imageView_clear_all = (ImageView) findViewById(R.id.imageView_clear_all);
        imageView_restore = (ImageView) findViewById(R.id.imageView_restore);
        imageView_clear = (ImageView) findViewById(R.id.imageView_clear);
        imageView_contactPicker = (ImageView) findViewById(R.id.filter_row_icon);
        autoCompleteTextView_contact = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView_Contact);

        checkedTextView_sms_all.setOnClickListener(smsCalendarSelector);
        checkedTextView_call_all.setOnClickListener(callCalendarSelector);
        checkedTextView_sms.setOnClickListener(smsCalendarSelector);
        checkedTextView_call.setOnClickListener(callCalendarSelector);

        imageView_restore_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int smsCal_id = -1, callCal_id = -1;
                String smsCalId = checkedTextView_sms_all.getText().toString();
                if (!smsCalId.equals(getResources().getString(R.string.sms_calendar))) {
                    smsCalId = smsCalId.substring(0, smsCalId.indexOf('.'));
                    smsCal_id = Integer.valueOf(smsCalId);
                }
                String callCalId = checkedTextView_call_all.getText().toString();
                if (!callCalId.equals(getResources().getString(R.string.call_calendar))) {
                    callCalId = callCalId.substring(0, callCalId.indexOf('.'));
                    callCal_id = Integer.valueOf(callCalId);
                }
                // restore dialog
                _restoreUtils.restoreDialog(mContext, mActivity, null, smsCal_id, callCal_id).show();
            }
        });
        imageView_clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SMS calendar
                checkedTextView_sms_all.setSelected(false);
                checkedTextView_sms_all.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView_sms_all.setText(R.string.sms_calendar);
                // Call Log calendar
                checkedTextView_call_all.setSelected(false);
                checkedTextView_call_all.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView_call_all.setText(R.string.call_calendar);
            }
        });
        imageView_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = autoCompleteTextView_contact.getText().toString();
                String purePhoneNumber = _contactsHelper.removePhoneNumberPrefix(phoneNumber);
                if (purePhoneNumber==null || purePhoneNumber.equals(""))
                    purePhoneNumber = phoneNumber;
                if (purePhoneNumber != null) {
                    int smsCal_id = -1, callCal_id = -1;
                    String smsCalId = checkedTextView_sms.getText().toString();
                    if (!smsCalId.equals(getResources().getString(R.string.sms_calendar))) {
                        smsCalId = smsCalId.substring(0, smsCalId.indexOf('.'));
                        smsCal_id = Integer.valueOf(smsCalId);
                    }
                    String callCalId = checkedTextView_call.getText().toString();
                    if (!callCalId.equals(getResources().getString(R.string.call_calendar))) {
                        callCalId = callCalId.substring(0, callCalId.indexOf('.'));
                        callCal_id = Integer.valueOf(callCalId);
                    }
                    // restore dialog
                    _restoreUtils.restoreDialog(mContext, mActivity, purePhoneNumber, smsCal_id, callCal_id).show();
                }
            }
        });
        imageView_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompleteTextView_contact.setText("");
                // SMS calendar
                checkedTextView_sms.setSelected(false);
                checkedTextView_sms.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView_sms.setText(R.string.sms_calendar);
                // Call Log calendar
                checkedTextView_call.setSelected(false);
                checkedTextView_call.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView_call.setText(R.string.call_calendar);
            }
        });

        imageView_contactPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity instanceof BaseMenuActivity) {
                    BaseMenuActivity baseMenuActivity = (BaseMenuActivity) mActivity;
                    baseMenuActivity.pickContact(autoCompleteTextView_contact, imageView_contactPicker);
                }
            }
        });

        setupUI();

        /*
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new Fragment_Restore())
                .commit();
        // Google AdMob
        adView = (AdView) findViewById(R.id.adView_frame);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);*/
    }
    private void setupUI(){
        if (SMS_Utils.isDefaultSmsApp(mContext) || !SMS_Utils.hasKitKat()) {
            layoutDefaultSms.setVisibility(View.GONE);
            layoutRestore.setVisibility(View.VISIBLE);
            autoCompleteTextView_contact.setThreshold(1);
            // get Contacts
            Map<String, String> numbers_names = _contactsHelper.getNumbers_Names();
            // build adapter for autocomplete textview
            final String[] CONTACTS = new String[numbers_names.size()];
            Iterator it = numbers_names.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                it.remove(); // avoids a ConcurrentModificationException
                CONTACTS[i] = pairs.getValue() + " (" + pairs.getKey() + ")";
                i++;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, CONTACTS);
            autoCompleteTextView_contact.setAdapter(adapter);
            // try this
            autoCompleteTextView_contact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    String str = (String) adapterView.getItemAtPosition(position);
                    str = str.substring(str.lastIndexOf('(') + 1, str.lastIndexOf(')')).trim().replaceAll(" ", "");
                    autoCompleteTextView_contact.setText(str);
                    Uri photoUri = _contactsHelper.getContactDisplayImageByNumber(str);
                    imageView_contactPicker.setImageURI(photoUri);
                }
            });
        }else{
            layoutRestore.setVisibility(View.GONE);
            layoutDefaultSms.setVisibility(View.VISIBLE);
            defaultSmsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _smsUtils.enableDefaultSmsApp();

                }
            });
        }
    }

    private View.OnClickListener smsCalendarSelector = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView.setText(R.string.sms_calendar);
                // reset calendar id in shared preferences
            } else {
                // set dialog message
                Dialog calendarDialog = _utilsFactory.getSmsUtils().calendarPicker(mContext, mActivity, checkedTextView);
                calendarDialog.show();
            }
        }
    };
    private View.OnClickListener callCalendarSelector = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckedTextView checkedTextView = (CheckedTextView) v;
            if (checkedTextView.isSelected()) {
                checkedTextView.setSelected(false);
                checkedTextView.setCheckMarkDrawable(R.drawable.btn_check_off_disable_focused);
                checkedTextView.setText(R.string.call_calendar);
                // reset calendar id in shared preferences
            } else {
                // set dialog message
                Dialog calendarDialog = _utilsFactory.getCallLogUtils().calendarPicker(mContext, mActivity, checkedTextView);
                calendarDialog.show();
            }
        }
    };
}
