package com.bibooki.mobile.sms_bibooki;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.bibooki.mobile.sms_bibooki.utils.NotificationUtils;
import com.bibooki.mobile.sms_bibooki.utils.UtilsFactory;
import com.bibooki.mobile.sms_bibooki.utils.call_log.Call_Log_Utils;
import com.bibooki.mobile.sms_bibooki.utils.constructor.Call_Log;

public class ConversationCall_Activity extends ActionBarActivity {

    Activity mActivity;

    static UtilsFactory _utilsFactory;
    static NotificationUtils _notificationUtils;
    private Call_Log_Utils _callLogUtils;

    private static ActionBar _actionBar;
    private static FragmentManager _fragmentManager;

    public static String itemId;
    public static String contact;
    Call_Log _callLog=null;

    // Google AdMob
    private RelativeLayout adFragment;
    // Finish Google AdMob

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;

        setContentView(R.layout.content_frame);

        adFragment = (RelativeLayout) findViewById(R.id.adFragment_layout);

        _utilsFactory = UtilsFactory.newInstance(mActivity);
        _notificationUtils = _utilsFactory.getNotificationUtils();
        _callLogUtils = _utilsFactory.getCallLogUtils();


        _actionBar = getSupportActionBar();
        _fragmentManager = getSupportFragmentManager();

        _actionBar.setHomeButtonEnabled(true);
        _actionBar.setDisplayHomeAsUpEnabled(true);

        itemId = getIntent().getExtras().getString("itemId");
        contact = getIntent().getExtras().getString("contact");
        Call_Log callLog = _callLogUtils.getCallLog_byId(mActivity, itemId);
        if (callLog!=null){
            _callLog = new Call_Log();
            _callLog.setNumbers(_callLog.getNumbers());
        }

        _actionBar.setTitle(contact);

        setSmsFragment();
    }
    private void setSmsFragment(){
        _fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new ConversationCall_Fragment())
                .commit();
    }
    /* Menu Setup */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
