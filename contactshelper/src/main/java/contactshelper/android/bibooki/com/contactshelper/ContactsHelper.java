package contactshelper.android.bibooki.com.contactshelper;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactsHelper {
    public static class MyContact{
        public long id = 0;
        public String displayName = null;
    };
    public static String _UNKNOWN_CONTACT = "Unknown Contact";
    public static String _UNKNOWN_NUMBER = "Unidentified numbers";
    private static ContactsHelper contactsHelper = null;
    private static Context mContext;
    // return new instance
    public static ContactsHelper getInstance(Context context){
        if (contactsHelper == null)
            contactsHelper = new ContactsHelper();
        mContext = context;
        return contactsHelper;
    }

    // Get Phone Number from SIM card of phone
    public String getMyPhoneNumber() {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return mTelephonyMgr.getLine1Number();
    }

    // get the Contact Display Name from Phone's Contacs by the number
    public MyContact getContactByNumber(String number) {
        MyContact myContact = null;
        if (number==null || number.equals(""))
            return myContact;
        Cursor contactLookup = null;
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

            ContentResolver contentResolver = mContext.getContentResolver();
            contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                myContact = new MyContact();
                myContact.id = contactLookup.getLong(contactLookup.getColumnIndex(BaseColumns._ID));
                myContact.displayName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));

            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }
        return myContact;
    }
    // get the Contact Display Name from Phone's Contacs by the number
    public String getContactDisplayNameByNumber(String number) {
        if (number==null || number.equals(""))
            return  _UNKNOWN_CONTACT;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String name = "?";

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                //String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        if (name==null || name.equals("") || name.equals("?"))
            name = _UNKNOWN_CONTACT;
        return name;
    }
    // get the Contact Display Name from Phone's Contacs by the number
    public long getContactIdByNumber(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        long contactId = -1;
        if (number==null || number.equals(""))
            return contactId;
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                contactId = contactLookup.getLong(contactLookup.getColumnIndex(ContactsContract.Data._ID));
                //String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }
        return contactId;
    }
    // get the Contact Display Image from Phone's Contacs by the number
    public Uri getContactDisplayImageByNumber(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Uri photoUri = null;
        String contactId = "";

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[]{ContactsContract.PhoneLookup._ID}, null, null, null);
        Cursor cursor = null;

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                contactId = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup._ID));
                cursor = contentResolver
                        .query(ContactsContract.Data.CONTENT_URI,
                                null,
                                ContactsContract.Data.CONTACT_ID
                                        + "="
                                        + contactId
                                        + " AND "

                                        + ContactsContract.Data.MIMETYPE
                                        + "='"
                                        + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                        + "'", null, null);

                if (cursor != null) {
                    if (!cursor.moveToFirst()) {
                        photoUri = null; // no photo
                    } else{
                        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
                        photoUri = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
                    }
                } else {
                    photoUri = null; // error in cursor process
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
            if (cursor != null) {
                cursor.close();
            }
        }
        return photoUri;
    }
    // get the Contact Display Image from Phone's Contacs by the number
    public Bitmap getContactBitmapImageByNumber(String number) {
        Bitmap thumbnail = null;
        if (number==null || number.equals(""))
            return thumbnail;
        try {
            Uri photoUri = getContactDisplayImageByNumber(number);
            if (photoUri!=null)
                thumbnail = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), photoUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return thumbnail;
    }
    // get the Contact Display Name from Phone's Contacs by the number
    public Map<String, String> getNumberByContactDisplayName(String contactName) {
        Map<String, String> mobileNumberList = new HashMap<String, String>();
        Map<String, String> homeNumberList = new HashMap<String, String>();
        Map<String, String> workNumberList = new HashMap<String, String>();
        String number, name;
        //
        //  Find contact based on name.
        //
        String[] arguments = contactName.split(" ");
        String selection = "";

        for (int i=0; i<arguments.length; i++){
            selection += ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? ";
            if (i+1 < arguments.length)
                selection += " AND ";
            arguments[i] = "%"+arguments[i]+"%";
        }
        ContentResolver cr = mContext.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                selection, arguments, null);
        if (cursor.moveToFirst()) {
            String contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            //
            //  Get all phone numbers.
            //
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
            while (phones.moveToNext()) {
                number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                switch (type) {
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        // do something with the Home number here...
                        homeNumberList.put(number, name);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        // do something with the Mobile number here...
                        mobileNumberList.put(number, name);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        // do something with the Work number here...
                        workNumberList.put(number, name);
                        break;
                }
            }
            phones.close();
        }
        cursor.close();
        return mobileNumberList;
    }

    // read all phone numbers and names
    public Map<String, String> getNumbers_Names(){
        Map<String, String> numbers_Names = new HashMap<String, String>();
        Cursor phones = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            numbers_Names.put(phoneNumber, name);
        }
        phones.close();
        return numbers_Names;
    }

    // get the Contact Display Name from Phone's Contacs by the number
    public Map<String, String> getNumberNames_by_inputString(String contactName) {
        Map<String, String> mobileNumberList = new HashMap<String, String>();
        Map<String, String> homeNumberList = new HashMap<String, String>();
        Map<String, String> workNumberList = new HashMap<String, String>();
        String number = null, name;
        //
        //  Find contact based on name.
        //
        String[] arguments = contactName.trim().split(" ");
        String selection = "";
        if (arguments.length == 1){
            if (isInteger(arguments[0])){
                number = arguments[0];
                name = getContactDisplayNameByNumber(number);
                mobileNumberList.put(number, name);
            }
            else {
                name = arguments[0];
                mobileNumberList = getNumberByContactDisplayName(name);
            }
        }
        else{
            boolean isLastNumber = false;
            for (int i=0; i<arguments.length; i++){
                if (i+1 == arguments.length && isInteger(arguments[i])){
                    isLastNumber = true;
                    number = arguments[i];
                    arguments = Arrays.copyOf(arguments, arguments.length-1);
                }
                else{
                    selection += ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? AND ";
                    arguments[i] = "%"+arguments[i]+"%";
                }
            }
            if (selection.endsWith(" AND "))
                selection = selection .substring(0, selection.lastIndexOf(" AND "));
            ContentResolver cr = mContext.getContentResolver();
            Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                    selection, arguments, null);
            while (cursor.moveToNext()) {
                String contactId =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                //
                //  Get all phone numbers.
                //
                selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId;
                arguments = null;
                if (isLastNumber){
                    selection += " AND "+ ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ?";
                    String numberArg = "";
                    for (int i=0;i<number.length(); i++)
                        numberArg += "%"+number.charAt(i);
                    arguments = new String[]{numberArg+"%"};
                }
                Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, selection
                        , arguments, null);
                while (phones.moveToNext()) {
                    number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    switch (type) {
                        case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                            // do something with the Home number here...
                            homeNumberList.put(number, name);
                            break;
                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                            // do something with the Mobile number here...
                            mobileNumberList.put(number, name);
                            break;
                        case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                            // do something with the Work number here...
                            workNumberList.put(number, name);
                            break;
                    }
                }
                phones.close();
            }
            cursor.close();
        }
        return (mobileNumberList.size()>0?mobileNumberList:(homeNumberList.size()>0?homeNumberList:workNumberList));
    }
    // return phone number without country code and 0 number at start position
    public String removePhoneNumberPrefix(String number){
        //int index = -1;
        try{
            if (number.startsWith("+")){
                for (String code:countryval){
                    if (number.startsWith("+"+code)){
                        number = number.substring(("+"+code).length());
                        countryval.indexOf(code);
                    }
                }
            }
            while (number.startsWith("0"))
                number = number.substring(1);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return number;
    }
    // return phone number without country code and 0 number at start position
    public String getCountryCode(String number){
        int index = -1;
        String countryCode = null;
        try{
            if (number.startsWith("+")){
                for (String code:countryval){
                    if (number.startsWith("+"+code)){
                        index = countryval.indexOf(code);
                        countryCode = countrycode.get(index);
                    }
                }
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return countryCode;
    }

    //is type of number
    public static boolean isInteger(String s)
    {
        boolean isInteger = true;
        for(int i=0;i<s.length() && isInteger; i++)
        {
            char c = s.charAt(i);
            isInteger = isInteger & ((c>='0' && c<='9'));
        }
        return isInteger;
    }

    // get email of owner of device
    public String getOwnerDeviceEmail(){
        return UserEmailFetcher.getEmail(mContext);
    }
    // get the Contact Display Image from Phone's Contacs by the email
    public Bitmap getContactDisplayImageByEmail(String incomingEmail) {
        Bitmap thumbnail = null;
        long contactID = -1;
        //Filter by email address first.
        final String[] emailProjection = new String[]{ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.CommonDataKinds.Email.DATA};
        final String emailSelection = ContactsContract.CommonDataKinds.Email.DATA + "=?";
        final String[] emailSelectionArgs = new String[]{incomingEmail};
        final String emailSortOrder = null;
        Cursor emailCursor = null;
        try {
            emailCursor = mContext.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    emailProjection,
                    emailSelection,
                    emailSelectionArgs,
                    emailSortOrder);
            if (emailCursor.moveToFirst()) {
                contactID = emailCursor.getLong(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                //Querry the specific contact if found.
                final String[] projection = new String[]{ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.PHOTO_ID,
                        ContactsContract.Contacts.LOOKUP_KEY};
                final String selection = ContactsContract.Contacts._ID + "=?";
                final String[] selectionArgs = new String[]{String.valueOf(contactID)};
                final String sortOrder = null;
                Cursor contactCursor = mContext.getContentResolver().query(
                        ContactsContract.Contacts.CONTENT_URI,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                if (contactCursor.moveToFirst()) {
                    String contactName = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String photoIDTmp = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
                    if (photoIDTmp != null) {
                        final String[] PHOTO_BITMAP_PROJECTION = new String[]{
                                ContactsContract.CommonDataKinds.Photo.PHOTO
                        };
                        long photoID = Long.parseLong(photoIDTmp);
                        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, photoID);
                        final Cursor cursor = mContext.getContentResolver().query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);
                        if (cursor.moveToFirst()) {
                            final byte[] thumbnailBytes = cursor.getBlob(0);
                            if (thumbnailBytes != null) {
                                thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                            }
                        }

                    }
                    String lookupKey = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                }
                contactCursor.close();
            } else {
                emailCursor.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try{
                emailCursor.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        //Return Contact ID, Contact Name, Contact Photo, Etc.
        return thumbnail;
    }
    // read all phone numbers and names
    public Map<String, String> getMail_Names(){
        Map<String, String> mail_Names = new HashMap<String, String>();
        long contactID = -1;
        String emailAddr = "";
        //Filter by email address first.
        final String[] emailProjection = new String[]{ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.CommonDataKinds.Email.DATA};
        final String emailSelection = ContactsContract.CommonDataKinds.Email.DATA + " NOT NULL";
        final String[] emailSelectionArgs = null;
        final String emailSortOrder = null;
        Cursor emailCursor = null;
        Cursor contactCursor = null;
        try {
            emailCursor = mContext.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    emailProjection,
                    emailSelection,
                    emailSelectionArgs,
                    emailSortOrder);
            while (emailCursor.moveToNext()) {
                contactID = emailCursor.getLong(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                emailAddr = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                //Querry the specific contact if found.
                final String[] projection = new String[]{ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME};
                final String selection = ContactsContract.Contacts._ID + "=?";
                final String[] selectionArgs = new String[]{String.valueOf(contactID)};
                final String sortOrder = null;
                try {
                    contactCursor = mContext.getContentResolver().query(
                            ContactsContract.Contacts.CONTENT_URI,
                            projection,
                            selection,
                            selectionArgs,
                            sortOrder);
                    if (contactCursor.moveToFirst()) {
                        String contactName = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        mail_Names.put(emailAddr, contactName);
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }finally {
                    try{
                        contactCursor.close();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try{
                emailCursor.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return mail_Names;
    }
    // get the Contact Display Image by contact ID
    public Bitmap getContactDisplayImageByID(long contactID) {
        Bitmap thumbnail = null;
        if (contactID < 1)
            return thumbnail;
        Cursor cursor = null;
        try {
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID);
            Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
            cursor = mContext.getContentResolver().query(photoUri,
                    new String[] {ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
            if (cursor == null) {
                return null;
            }
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    thumbnail = BitmapFactory.decodeByteArray(data, 0, data.length);
                }
            }
        } finally {
            cursor.close();
        }
        return thumbnail;
    }
    // get my Contact Display Image
    public Bitmap getMyAvatar() {
        Bitmap thumbnail = null;
        String myPhoneNumber = getMyPhoneNumber();
        if (myPhoneNumber==null || myPhoneNumber.equals("") || myPhoneNumber.contains(_UNKNOWN_NUMBER) || myPhoneNumber.matches(".*\\d.*")){
            String myEmail = getOwnerDeviceEmail();
            if (myEmail!=null && !myEmail.equals("")){
                thumbnail = getContactDisplayImageByEmail(myEmail);
            }
        }else{
            thumbnail = getContactBitmapImageByNumber(myPhoneNumber);
        }
        return thumbnail;
    }


    static final List<String> countrycode = Arrays.asList(new String[]{"AF", "AL", "DZ", "AD", "AO", "AQ", "AR",
            "AM", "AW", "AU", "AT", "AZ", "BH", "BD", "BY", "BE", "BZ", "BJ",
            "BT", "BO", "BA", "BW", "BR", "BN", "BG", "BF", "MM", "BI", "KH",
            "CM", "CA", "CV", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM",
            "CG", "CD", "CK", "CR", "HR", "CU", "CY", "CZ", "DK", "DJ", "TL",
            "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI",
            "FR", "PF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GT",
            "GN", "GW", "GY", "HT", "HN", "HK", "HU", "IN", "ID", "IR", "IQ",
            "IE", "IM", "IL", "IT", "CI", "JP", "JO", "KZ", "KE", "KI", "KW",
            "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO",
            "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MR", "MU", "YT",
            "MX", "FM", "MD", "MC", "MN", "ME", "MA", "MZ", "NA", "NR", "NP",
            "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "KP", "NO", "OM",
            "PK", "PW", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR",
            "QA", "RO", "RU", "RW", "BL", "WS", "SM", "ST", "SA", "SN", "RS",
            "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "KR", "ES", "LK",
            "SH", "PM", "SD", "SR", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ",
            "TH", "TG", "TK", "TO", "TN", "TR", "TM", "TV", "AE", "UG", "GB",
            "UA", "UY", "US", "UZ", "VU", "VA", "VE", "VN", "WF", "YE", "ZM",
            "ZW"});
    static final List<String> countryval = Arrays.asList(new String[]{ "93", "355", "213", "376", "244", "672",
            "54", "374", "297", "61", "43", "994", "973", "880", "375", "32",
            "501", "229", "975", "591", "387", "267", "55", "673", "359",
            "226", "95", "257", "855", "237", "1", "238", "236", "235", "56",
            "86", "61", "61", "57", "269", "242", "243", "682", "506", "385",
            "53", "357", "420", "45", "253", "670", "593", "20", "503", "240",
            "291", "372", "251", "500", "298", "679", "358", "33", "689",
            "241", "220", "995", "49", "233", "350", "30", "299", "502", "224",
            "245", "592", "509", "504", "852", "36", "91", "62", "98", "964",
            "353", "44", "972", "39", "225", "81", "962", "7", "254", "686",
            "965", "996", "856", "371", "961", "266", "231", "218", "423",
            "370", "352", "853", "389", "261", "265", "60", "960", "223",
            "356", "692", "222", "230", "262", "52", "691", "373", "377",
            "976", "382", "212", "258", "264", "674", "977", "31", "599",
            "687", "64", "505", "227", "234", "683", "850", "47", "968", "92",
            "680", "507", "675", "595", "51", "63", "870", "48", "351", "1",
            "974", "40", "7", "250", "590", "685", "378", "239", "966", "221",
            "381", "248", "232", "65", "421", "386", "677", "252", "27", "82",
            "34", "94", "290", "508", "249", "597", "268", "46", "41", "963",
            "886", "992", "255", "66", "228", "690", "676", "216", "90", "993",
            "688", "971", "256", "44", "380", "598", "1", "998", "678", "39",
            "58", "84", "681", "967", "260", "263", });
}
