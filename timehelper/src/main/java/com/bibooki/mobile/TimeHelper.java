package com.bibooki.mobile;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeHelper {
    private static TimeHelper timeHelper;

    protected static Format simpleTimeFormat = new SimpleDateFormat("HH:mm:ss");
    protected static Format simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");

    final String TODAY_STT = "Today";
    final String YESTERDAY_STT = "Yesterday";
    final String LASTWEEK_STT = "Last week";
    final String LASTMONTH_STT = "Last month";
    final String LASTYEAR_STT = "Last year";
    final String DAYSAGO_STT = " days ago";
    final String SEPARATOR_STT = " - ";
    final String TOMORROW_STT = "Tomorrow";
    final String NEXTWEEK_STT = "Next week";
    final String NEXTMONTH_STT = "Next month";
    final String NEXTYEAR_STT = "Next year";
    final String DAYSLATER_STT = " days later";

    Calendar currentCal, processingCal;

    public TimeHelper() {
    }

    // return new instance
    public static TimeHelper getInstance(){
        if (timeHelper == null)
            timeHelper = new TimeHelper();
        return timeHelper;
    }

    public String getFormatedTime(long timeInMilliseconds){
        return simpleTimeFormat.format(timeInMilliseconds);
    }
    public String getFormatedDate(long timeInMilliseconds){
        return simpleDateFormat.format(timeInMilliseconds);
    }
    // return a status string based on input time, eg., "Today - Monday, Apr 28, 2015"
    // input timeInMilliseconds supported for the past, not in the future
    public String getDateSttForThePast(long timeInMilliseconds){
        String dateTimeStt = "";
        processingCal = Calendar.getInstance();
        processingCal.setTimeInMillis(timeInMilliseconds);
        currentCal = Calendar.getInstance();
        try {
            if (processingCal.after(currentCal))
                return dateTimeStt;
            int days = Days.daysBetween(new LocalDate(timeInMilliseconds),
                    new LocalDate(currentCal.getTimeInMillis())).getDays();
            int weeks = Weeks.weeksBetween(new LocalDate(timeInMilliseconds),
                    new LocalDate(currentCal.getTimeInMillis())).getWeeks();
            int months = Months.monthsBetween(new LocalDate(timeInMilliseconds),
                    new LocalDate(currentCal.getTimeInMillis())).getMonths();
            int years = Years.yearsBetween(new LocalDate(timeInMilliseconds),
                    new LocalDate(currentCal.getTimeInMillis())).getYears();
            if (years == 0){
                if (days == 0){
                    dateTimeStt = TODAY_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }else if(days == 1){
                    dateTimeStt = YESTERDAY_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }else if (weeks == 1){
                    dateTimeStt = LASTWEEK_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }else if (months == 1){
                    dateTimeStt = LASTMONTH_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }else{
                    dateTimeStt = days + DAYSAGO_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }
            }else if (years == 1){
                dateTimeStt = LASTYEAR_STT + ", " + days + DAYSAGO_STT + SEPARATOR_STT
                        + getFormatedDate(timeInMilliseconds);
            }else if (years > 1){
                dateTimeStt = days + DAYSAGO_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return dateTimeStt;
    }
    // return a status string based on input time, eg., "Today - Monday, Apr 28, 2015"
    // input timeInMilliseconds supported for the past and the future
    public String getFullDateStt(long timeInMilliseconds){
        String dateTimeStt = "";
        processingCal = Calendar.getInstance();
        processingCal.setTimeInMillis(timeInMilliseconds);
        currentCal = Calendar.getInstance();
        try {
            int days, weeks, months, years;
            if (processingCal.after(currentCal)) {
                days = Days.daysBetween(new LocalDate(currentCal.getTimeInMillis()),
                        new LocalDate(timeInMilliseconds)).getDays();
                weeks = Weeks.weeksBetween(new LocalDate(currentCal.getTimeInMillis()),
                        new LocalDate(timeInMilliseconds)).getWeeks();
                months = Months.monthsBetween(new LocalDate(currentCal.getTimeInMillis()),
                        new LocalDate(timeInMilliseconds)).getMonths();
                years = Years.yearsBetween(new LocalDate(currentCal.getTimeInMillis()),
                        new LocalDate(timeInMilliseconds)).getYears();
                if (years == 0) {
                    if (days == 0) {
                        dateTimeStt = TODAY_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (days == 1) {
                        dateTimeStt = TOMORROW_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (weeks == 1) {
                        dateTimeStt = NEXTWEEK_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (months == 1) {
                        dateTimeStt = NEXTMONTH_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else {
                        dateTimeStt = days + DAYSLATER_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    }
                } else if (years == 1) {
                    dateTimeStt = NEXTYEAR_STT + ", " + days + DAYSLATER_STT + SEPARATOR_STT
                            + getFormatedDate(timeInMilliseconds);
                } else if (years > 1) {
                    dateTimeStt = days + DAYSLATER_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }
            }else{
                days = Days.daysBetween(new LocalDate(timeInMilliseconds),
                        new LocalDate(currentCal.getTimeInMillis())).getDays();
                weeks = Weeks.weeksBetween(new LocalDate(timeInMilliseconds),
                        new LocalDate(currentCal.getTimeInMillis())).getWeeks();
                months = Months.monthsBetween(new LocalDate(timeInMilliseconds),
                        new LocalDate(currentCal.getTimeInMillis())).getMonths();
                years = Years.yearsBetween(new LocalDate(timeInMilliseconds),
                        new LocalDate(currentCal.getTimeInMillis())).getYears();
                if (years == 0) {
                    if (days == 0) {
                        dateTimeStt = TODAY_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (days == 1) {
                        dateTimeStt = YESTERDAY_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (weeks == 1) {
                        dateTimeStt = LASTWEEK_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else if (months == 1) {
                        dateTimeStt = LASTMONTH_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    } else {
                        dateTimeStt = days + DAYSAGO_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                    }
                } else if (years == 1) {
                    dateTimeStt = LASTYEAR_STT + ", " + days + DAYSAGO_STT + SEPARATOR_STT
                            + getFormatedDate(timeInMilliseconds);
                } else if (years > 1) {
                    dateTimeStt = days + DAYSAGO_STT + SEPARATOR_STT + getFormatedDate(timeInMilliseconds);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return dateTimeStt;
    }
    // Return the datetime status string of first millisecond parameter
    // only if the two parameter are in different date
    public String processIfDifferentDays(long timeInMillisToProcess, long timeInMillisToCompare){
        int days = Days.daysBetween(new LocalDate(timeInMillisToProcess), new LocalDate(timeInMillisToCompare)).getDays();
        if (days==0)
            return "";
        else
            return getFullDateStt(timeInMillisToProcess);
    }
    public String getFullDurationMinuteSecond(long timeInMillis1, long timeInMillis2){
        int minute = Minutes.minutesBetween(new LocalDateTime(timeInMillis1), new LocalDateTime(timeInMillis2)).getMinutes();
        int second = Seconds.secondsBetween(new LocalDateTime(timeInMillis1), new LocalDateTime(timeInMillis2)).getSeconds();
        second = second - minute*60;
        String durationTimeString="";
        if (minute==0 && second==0)
            durationTimeString = "0 second";
        else {
            if (minute == 1) {
                durationTimeString = "1 minute";
            } else if (minute > 1)
                durationTimeString = minute + " minutes";
            if (second == 1)
                durationTimeString += " 1 second";
            else if (second > 1)
                durationTimeString += " " + second + " seconds";
        }
        durationTimeString = durationTimeString.trim();
        return durationTimeString;
    }
    public String getShortDurationMinuteSecond(long timeInMillis1, long timeInMillis2){
        int minute = Minutes.minutesBetween(new LocalDateTime(timeInMillis1), new LocalDateTime(timeInMillis2)).getMinutes();
        int second = Seconds.secondsBetween(new LocalDateTime(timeInMillis1), new LocalDateTime(timeInMillis2)).getSeconds();
        second = second - minute*60;
        String durationTimeString="";
        if (minute==0 && second==0)
            durationTimeString = "0s";
        else {
            if (minute > 0)
                durationTimeString = minute + "m";
            if (second > 0)
                durationTimeString += second + "s";
        }
        durationTimeString = durationTimeString.trim();
        return durationTimeString;
    }
}
